close all; clear; clc
%%
DELIMITER = ',';
HEADERLINES = 1;
Rm2=0.0;

start=1;
enp=270;


% import the experimental records
newData1 = importdata('data_indoor/data_sgpvo_auto_xyzyaw.csv', DELIMITER, HEADERLINES);


% re-assign each value
frame           = newData1.data(:,1)';
flightMode      = newData1.data(:,2)';
time            = newData1.data(:,3)'/1000;
commandVals     = newData1.data(:,4:5)';
targetPos       = newData1.data(:,6:17)';
state           = newData1.data(:,18:29)';
motor           = newData1.data(:,30:35)';
eststate        = newData1.data(:,36:44)';
estxy           = newData1.data(:,45:51)';
gpss           = newData1.data(:,52:55)';
calctime        = newData1.data(:,56)';


% post-processing of data
targetPos(6,:) =- targetPos(6,:);
nm=size(targetPos(6,:),2);

for i=1:nm
    eststate(1,:)=eststate(1,:)-eststate(1,1)+state(1,1);
    eststate(2,:)=eststate(2,:)-eststate(2,1)+state(2,1);
    
    eststate(4,:)=eststate(4,:)-eststate(4,330)+state(4,330);
    eststate(5,:)=eststate(5,:)-eststate(5,330)+state(5,330);
    eststate(6,:)=eststate(6,:)-eststate(6,330)-state(6,330);
    
    if eststate(6,i) <= -pi
        eststate(6,i) = eststate(6,i) + 2*pi;
    end
end
estxy(1,:)=-estxy(1,:);

targetPos(4:6,:) = targetPos(4:6,:)*180/pi;

state(4:6,:) = state(4:6,:) * 180/pi;
eststate(4:6,:) = eststate(4:6,:) * 180/pi;
eststate(6,:) = -eststate(6,:);
ns=size(targetPos(5,:),2);


%%


figure(1);
set(gcf,'Name','Position information');
subplot(3,2,1);
plot(time(start:end), state(1,start:end), '-b'); hold on;       % Vicon
plot(time(start:end), eststate(1,start:end), '-.m');            % VO
legend('Vicon','VO'); ylabel('X [m]');
title('X-Y-Z Position');

subplot(3,2,3);
plot(time(start:end), state(2,start:end), '-b'); hold on;       % Vicon
plot(time(start:end), eststate(2,start:end), '-.m');            % VO
ylabel('Y [m]');

subplot(3,2,5);
plot(time(start:end), state(3,start:end), '-b'); hold on;       % Vicon
plot(time(start:end), eststate(3,start:end), '-.m');            % VO
ylabel('Z [m]');

subplot(3,2,2);
plot(time(start:end), state(4,start:end), '-b'); hold on        % IMU
plot(time(start:end), eststate(4,start:end), '-.m');            % VO
legend('IMU','VO'); ylabel('roll [deg]');
title('Roll-Pitch-Yaw Angle');

subplot(3,2,4);
plot(time(start:end), state(5,start:end), '-b'); hold on        % IMU
plot(time(start:end), eststate(5,start:end), '-.m');            % VO
ylabel('pitch [deg]');

subplot(3,2,6);
plot(time(start:end), state(6,start:end), '-b'); hold on        % Vicon
plot(time(start:end), eststate(6,start:end), '-.m');            % VO
ylabel('yaw [deg]');
axis([0 time(end) -10 10]);



figure(2);
subplot(3,1,1);
plot(time(start:end), state(7,start:end),'-b'); hold on;   % Vicon
plot(time(start:end), eststate(7,start:end),'-.m');        % VO
legend('Vicon', 'VO'); ylabel('V_X [m/s]');
title('Linear Velocity');

subplot(3,1,2);
plot(time(start:end), state(8,start:end),'-b'); hold on;   % Vicon
plot(time(start:end), eststate(8,start:end),'-.m');        % VO
ylabel('V_Y [m/s]');

subplot(3,1,3);
plot(time(start:end), state(9,start:end), '-b'); hold on;  % Vicon
plot(time(start:end), eststate(9,start:end), '-.m');       % VO
ylabel('V_Z [m/s]');



figure(3);
plot(state(1,1),state(2,1),'bo',state(1,start:end),state(2,start:end),'-b'); hold on; grid on;
plot(eststate(1,1),eststate(2,1),'mo',eststate(1,start:end),eststate(2,start:end),'-.m'); axis equal;
title('top view of moving trajectory');
xlabel('X [m]'); ylabel('Y [m]');





