close all; clear; clc
%%
DELIMITER = ',';
HEADERLINES = 1;
Rm2=0.0;

start=1;
enp=270;


% import the experimental records
newData1 = importdata('data_outdoor/data_2.csv', DELIMITER, HEADERLINES);


% re-assign each value
frame           = newData1.data(:,1)';
flightMode      = newData1.data(:,2)';
time            = newData1.data(:,3)'/1000;
commandVals     = newData1.data(:,4:5)';
targetPos       = newData1.data(:,6:17)';
state           = newData1.data(:,18:29)';
motor           = newData1.data(:,30:35)';
eststate        = newData1.data(:,36:44)';
estxy           = newData1.data(:,45:51)';
gpss           = newData1.data(:,52:55)';
calctime        = newData1.data(:,56)';


% post-processing of data
targetPos(6,:) =- targetPos(6,:);
nm=size(targetPos(6,:),2);

ango=-pi/3;
for i=1:nm
    estxy(1:2,i)=[cos(ango) sin(ango);-sin(ango) cos(ango)]*estxy(1:2,i);
    if estxy(7,i)>=pi
        estxy(7,i)=estxy(7,i)-2*pi;
    end
end
estxy(1,:)=-estxy(1,:);

targetPos(4:6,:) = targetPos(4:6,:)*180/pi;

state(4:6,:) = state(4:6,:)*180/pi;
eststate(4:6,:) = eststate(4:6,:)*180/pi;
targetPos(5,:) = targetPos(5,:)+0;


eststate(4,start:end) = eststate(4,start:end)+110;

ns=size(targetPos(5,:),2);


%%


figure(1);
set(gcf,'Name','Position information');
subplot(3,2,1);
plot(time(start:end), targetPos(1,start:end), '--r'); hold on;  % target
plot(time(start:end), state(1,start:end), '-b');                % Vicon
plot(time(start:end), eststate(1,start:end), '-.m');            % VO
plot(time(start:end), estxy(1,start:end), '-g');                % Firefly (GPS)
legend('target','Vicon','VO','Firefly'); ylabel('X [m]');
title('X-Y-Z Position');

subplot(3,2,3);
plot(time(start:end), targetPos(2,start:end), '--r'); hold on;  % target
plot(time(start:end), state(2,start:end), '-b');                % Vicon
plot(time(start:end), eststate(2,start:end), '-.m');            % VO
plot(time(start:end), estxy(2,start:end), '-g');                % Firefly (GPS)
ylabel('Y [m]');

subplot(3,2,5);
plot(time(start:end), targetPos(3,start:end), '--r'); hold on;  % target
plot(time(start:end), state(3,start:end), '-b');                % Vicon
plot(time(start:end), eststate(3,start:end), '-.m');            % VO
plot(time(start:end), estxy(3,start:end), '-g');                % Firefly (GPS + barometer)
ylabel('Z [m]');

subplot(3,2,2);
plot(time(start:end), targetPos(4,start:end), '--r'); hold on;  % target
plot(time(start:end), state(4,start:end), '-b');                % IMU
plot(time(start:end), eststate(4,start:end), '-.m');            % VO
legend('target','IMU','VO'); ylabel('roll [deg]');
title('Roll-Pitch-Yaw Angle');

subplot(3,2,4);
plot(time(start:end), targetPos(5,start:end), '--r'); hold on;  % target
plot(time(start:end), state(5,start:end), '-b');                % IMU
plot(time(start:end), eststate(5,start:end), '-.m');            % VO
ylabel('pitch [deg]');

subplot(3,2,6);
plot(time(start:end), targetPos(6,start:end), '--r'); hold on;  % target
plot(time(start:end), state(6,start:end), '-b');                % IMU
plot(time(start:end), eststate(6,start:end), '-.m');            % VO
plot(time(start:end), estxy(7,start:end)*180/pi, '-b');         % Firefly (magnetometer)
ylabel('yaw [deg]');



figure(2);
subplot(3,1,1);
plot(time(start:end), estxy(4,start:end),'-b'); hold on;   % Firefly (GPS)
plot(time(start:end), eststate(7,start:end),'-.m');        % VO
legend('Firefly', 'VO'); ylabel('V_X [m/s]');
title('Linear Velocity');

subplot(3,1,2);
plot(time(start:end), estxy(5,start:end),'-b'); hold on;   % Firefly (GPS)
plot(time(start:end), eststate(8,start:end),'-.m');        % VO
ylabel('V_Y [m/s]');

subplot(3,1,3);
plot(time(start:end), estxy(6,start:end), '-b'); hold on;  % Firefly (GPS + barometer)
plot(time(start:end), eststate(9,start:end), '-.m');       % VO
ylabel('V_Z [m/s]');



figure(3);
set(gcf,'Name','GPS status')
subplot(2,2,1);
plot(time(start:end), gpss(1,start:end),'-b'); hold on;
title('GPS position accuracy');

subplot(2,2,2);
plot(time(start:end), gpss(2,start:end),'-b'); hold on;
title('GPS height accuracy');

subplot(2,2,3);
plot(time(start:end), gpss(3,start:end),'-b'); hold on;
title('GPS speed accuracy');

subplot(2,2,4);
plot(time(start:end), gpss(4,start:end),'-b'); hold on;
title('number of satellites');
axis([time(start) time(end) 0 10]);



figure(4);
plot(eststate(1,1),eststate(2,1),'mo',eststate(1,start:end),eststate(2,start:end),'-.m'); hold on; grid on;
plot(estxy(1,1),estxy(2,1),'go',estxy(1,start:end),estxy(2,start:end),'-g'); axis equal;
title('top view of moving trajectory');
xlabel('X [m]'); ylabel('Y [m]');





