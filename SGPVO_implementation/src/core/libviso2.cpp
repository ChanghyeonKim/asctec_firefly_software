#include "core/libviso2.h"




void visualOdometryStereoMex(const cv::Mat& imLeft, const cv::Mat& imRight, VisualOdometryStereo& libviso2, Eigen::Matrix4d& T_21)
{
    // initialize variables
    Matrix T_21_libviso2 = Matrix::eye(4);
    T_21.fill(0);
    FLOAT r11, r12, r13, r21, r22, r23, r31, r32, r33, tx, ty, tz;


    // read image dimensions
    int32_t width  = imLeft.cols;
    int32_t height = imRight.rows;


    // for debug
    //std::cout<< width << std::endl;
    //std::cout<< height << std::endl;
    //usleep(1000000);


    // convert from input images to uint8_t buffer for libviso2
    uint8_t* imgLeftData  = (uint8_t*)malloc(width*height*sizeof(uint8_t));
    uint8_t* imgRightData = (uint8_t*)malloc(width*height*sizeof(uint8_t));
    int32_t k = 0;

    for(int32_t v = 0; v < height; v++)
    {
        for(int32_t u = 0; u < width; u++)
        {
            imgLeftData[k]  = imLeft.at<unsigned char>(v,u);
            imgRightData[k] = imRight.at<unsigned char>(v,u);
            k++;


            // for debug
            //printf("%d \n",imLeft.at<unsigned char>(v,u));
            //std::cout << k << std::endl;
            //usleep(1000000);
        }
    }


    // compute the transformation matrix (T_21)
    int32_t dims[] = {width,height,width};
    if (libviso2.process(imgLeftData,imgRightData,dims))
    {
        // if it successes, update current pose
        T_21_libviso2 = T_21_libviso2 * libviso2.getMotion();
        //std::cout << " libviso2 is done! " << std::endl;


        // for debug
        //double num_matches = libviso2.getNumberOfMatches();
        //double num_inliers = libviso2.getNumberOfInliers();
        //std::cout << ", Matches: " << num_matches;
        //std::cout << ", Inliers: " << 100.0*num_inliers/num_matches << " %" << ", Current pose: " << std::endl;
        //std::cout << T_21_libviso2 << std::endl << std::endl;
    }
    else
    {
        std::cout << " libviso2 failed! " << std::endl;
    }


    // convert from libviso2 matrix to Eigen matrix form
    T_21_libviso2.getData(&r11,0,0,0,0);
    T_21_libviso2.getData(&r12,0,1,0,1);
    T_21_libviso2.getData(&r13,0,2,0,2);

    T_21_libviso2.getData(&r21,1,0,1,0);
    T_21_libviso2.getData(&r22,1,1,1,1);
    T_21_libviso2.getData(&r23,1,2,1,2);

    T_21_libviso2.getData(&r31,2,0,2,0);
    T_21_libviso2.getData(&r32,2,1,2,1);
    T_21_libviso2.getData(&r33,2,2,2,2);

    T_21_libviso2.getData(&tx,0,3,0,3);
    T_21_libviso2.getData(&ty,1,3,1,3);
    T_21_libviso2.getData(&tz,2,3,2,3);


    T_21(0,0) = r11;
    T_21(0,1) = r12;
    T_21(0,2) = r13;

    T_21(1,0) = r21;
    T_21(1,1) = r22;
    T_21(1,2) = r23;

    T_21(2,0) = r31;
    T_21(2,1) = r32;
    T_21(2,2) = r33;

    T_21(0,3) = tx;
    T_21(1,3) = ty;
    T_21(2,3) = tz;
    T_21(3,3) = 1.0f;


    // for debug
    //std::cout << T_21_libviso2 << std::endl << std::endl;
    //std::cout << T_21 << std::endl;
    //usleep(1000000);


    // release uint8_t buffers
    free(imgLeftData);
    free(imgRightData);
}
