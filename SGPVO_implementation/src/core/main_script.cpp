#include "core/viso_SGPVO.h"
#include "core/velocityEstimatorRealsense.h"
#include "core/communication.h"


int main(void)
{
  // set communicateHostComputer parameters
  communicateHostComputer::parameters param_communication;
  param_communication.WiFi_setup.IP_address = "192.168.43.121";
  param_communication.WiFi_setup.num_port = 13120;

  // initialize communicator with host computer
  communicateHostComputer communicator(param_communication);


  // set velocityEstimatorRealsense parameters
  velocityEstimatorRealsense::parameters param_velocity;
  param_velocity.calib.fx = 620.608832234754;
  param_velocity.calib.fy = 619.113993685335;
  param_velocity.calib.cx = 323.902900972212;
  param_velocity.calib.cy = 212.418428046497;
  param_velocity.homography_RANSAC.threshold = 1.5;
  param_velocity.debug_flags.print_current_velocity = false;
  param_velocity.debug_flags.print_current_image = false;

  // initialize velocity estimator
  velocityEstimatorRealsense velocity_estimator(param_velocity);


  // set VisualOdometrySGP parameters
  VisualOdometrySGP::parameters param_SGPVO;
  param_SGPVO.LPF_setup.tau = 0.03;
  param_SGPVO.debug_flags.print_current_position = false;
  param_SGPVO.debug_flags.print_current_image = false;

  // initialize stereo visual odometry
  VisualOdometrySGP SGPVO(param_SGPVO);


  // run multi-threads
  std::thread t1 = communicator.runThread();
  std::thread t2 = velocity_estimator.runThread(&communicator);
  std::thread t3 = SGPVO.runThread(&communicator);

  t1.join();
  t2.join();
  t3.join();


  // exit the program
  return 0;
}
