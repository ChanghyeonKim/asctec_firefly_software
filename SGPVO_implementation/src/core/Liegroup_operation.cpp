#include "core/Liegroup_operation.h"



void hatOperator(const Eigen::Vector3d& columnVector, Eigen::Matrix3d& skewSymmetricMatrix)
{
    skewSymmetricMatrix(0,0) = 0;
    skewSymmetricMatrix(0,1) = -columnVector(2);
    skewSymmetricMatrix(0,2) = columnVector(1);

    skewSymmetricMatrix(1,0) = columnVector(2);
    skewSymmetricMatrix(1,1) = 0;
    skewSymmetricMatrix(1,2) = -columnVector(0);

    skewSymmetricMatrix(2,0) = -columnVector(1);
    skewSymmetricMatrix(2,1) = columnVector(0);
    skewSymmetricMatrix(2,2) = 0;
}



void LieAlgebra2LieGroup(const Eigen::MatrixXd& deltaZ, Eigen::Matrix4d& outputDeltaT)
{
    // initialize variables
    Eigen::Vector3d v, w;
    double length_w = 0;
    Eigen::Matrix3d Wx, R, V;
    Eigen::Vector3d t;

    v(0) = deltaZ(0);
    v(1) = deltaZ(1);
    v(2) = deltaZ(2);
    w(0) = deltaZ(3);
    w(1) = deltaZ(4);
    w(2) = deltaZ(5);

    length_w = std::sqrt(w.transpose() * w);
    hatOperator(w, Wx);
    if (length_w < 1e-7)
    {
        R = Eigen::Matrix3d::Identity(3,3) + Wx + 0.5 * Wx * Wx;
        V = Eigen::Matrix3d::Identity(3,3) + 0.5 * Wx + Wx * Wx / 3;
    }
    else
    {
        R = Eigen::Matrix3d::Identity(3,3) + (sin(length_w)/length_w) * Wx + ((1-cos(length_w))/(length_w*length_w)) * (Wx*Wx);
        V = Eigen::Matrix3d::Identity(3,3) + ((1-cos(length_w))/(length_w*length_w)) * Wx + ((length_w-sin(length_w))/(length_w*length_w*length_w)) * (Wx*Wx);
    }
    t = V * v;

    // assign rigid body transformation matrix (in SE(3))
    outputDeltaT = Eigen::MatrixXd::Identity(4,4);
    outputDeltaT(0,0) = R(0,0);
    outputDeltaT(0,1) = R(0,1);
    outputDeltaT(0,2) = R(0,2);

    outputDeltaT(1,0) = R(1,0);
    outputDeltaT(1,1) = R(1,1);
    outputDeltaT(1,2) = R(1,2);

    outputDeltaT(2,0) = R(2,0);
    outputDeltaT(2,1) = R(2,1);
    outputDeltaT(2,2) = R(2,2);

    outputDeltaT(0,3) = t(0);
    outputDeltaT(1,3) = t(1);
    outputDeltaT(2,3) = t(2);

    // for debug
    //cout << R << endl;
    //cout << t << endl;
    //usleep(10000000);
}



void angle2rotmtx(const Eigen::Vector3d& eulerAngle, Eigen::Matrix3d& rotationMatrix)
{
    /**
    % Project:  Patch-based Illumination invariant Visual Odometry (PIVO)
    % Function: angle2rotmtx
    %
    % Description:
    %   This function return the rotation matrix rotationMatrix
    %   [Body frame] = rotationMatrix * [Inertial frame]
    %   from [phi;theta;psi] angle defined as ZYX sequence to rotation matrix
    %
    % Example:
    %   OUTPUT:
    %   rotationMatrix = rotation Matrix [3x3] defined as [Body frame] = rotationMatrix * [Inertial frame]
    %
    %   INPUT:
    %   eulerAngle: angle vector composed of [phi;theta;psi]
    %               phi = Rotation angle along x direction in radians
    %               theta = Rotation angle along y direction in radians
    %               psi = Rotation angle along z direction in radians
    %
    % NOTE:
    %
    % Author: Pyojin Kim
    % Email: pjinkim1215@gmail.com
    % Website:
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % log:
    % 2014-08-20:
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    **/


    // assign roll, pitch, yaw values
    double phi = eulerAngle(0);
    double theta = eulerAngle(1);
    double psi = eulerAngle(2);

    Eigen::Matrix3d rotMtx_Rx = Eigen::Matrix3d::Identity(3,3);
    Eigen::Matrix3d rotMtx_Ry = Eigen::Matrix3d::Identity(3,3);
    Eigen::Matrix3d rotMtx_Rz = Eigen::Matrix3d::Identity(3,3);

    rotMtx_Rx << 1,        0,         0,
              0, cos(phi), -sin(phi),
              0, sin(phi),  cos(phi);

    rotMtx_Ry << cos(theta),  0, sin(theta),
              0,  1,          0,
              -sin(theta),  0, cos(theta);

    rotMtx_Rz << cos(psi), -sin(psi),   0,
              sin(psi),  cos(psi),   0,
              0,         0,   1;

    Eigen::Matrix3d rotMtxBody2Inertial = rotMtx_Rz * rotMtx_Ry * rotMtx_Rx;  // [Inertial frame] = rotMtxBody2Inertial * [Body frame]
    Eigen::Matrix3d rotMtxInertial2Body = rotMtxBody2Inertial.transpose();    //     [Body frame] = rotMtxInertial2Body * [Inertial frame]

    rotationMatrix = rotMtxInertial2Body;
}



void statevector2transformatrix(const Eigen::VectorXd& stateVector, Eigen::Matrix4d& transformationMatrix)
{
    /**
    % Project:  Patch-based Illumination invariant Visual Odometry (PIVO)
    % Function: statevector2transformatrix
    %
    % Description:
    %   change the 'form' from state vector [6x1] to transformation matrix [4x4]
    %
    % Example:
    %   OUTPUT:
    %   transformationMatrix: Transformation Matrix included in SE(3)
    %                         which means camera motion matrix [4x4]
    %                         [P_body] = T * [P_inertial]
    %
    %
    %   INPUT:
    %   stateVector: state vector which represents the location and pose of object
    %                [x; y; z; roll; pitch; yaw] w.r.t. the inertial frame
    %
    % NOTE:
    %
    % Author: Pyojin Kim
    % Email: pjinkim1215@gmail.com
    % Website:
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % log:
    % 2015-02-06: Complete
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    **/


    // calculate rotation matrix
    Eigen::Vector3d eulerAngle;
    eulerAngle(0) = stateVector(3);
    eulerAngle(1) = stateVector(4);
    eulerAngle(2) = stateVector(5);

    Eigen::Matrix3d rotMtx = Eigen::Matrix3d::Identity(3,3);
    angle2rotmtx(eulerAngle,rotMtx);


    // calculate translation vector w.r.t. body frame
    Eigen::Vector3d translationExpressedInertialframe;
    translationExpressedInertialframe(0) = stateVector(0);
    translationExpressedInertialframe(1) = stateVector(1);
    translationExpressedInertialframe(2) = stateVector(2);

    Eigen::Vector3d translationExpressedBodyframe = rotMtx * (-translationExpressedInertialframe);


    // assign transformationMatrix ( [P_body] = T * [P_inertial] )
    transformationMatrix = Eigen::Matrix4d::Identity(4,4);

    transformationMatrix(0,0) = rotMtx(0,0);
    transformationMatrix(0,1) = rotMtx(0,1);
    transformationMatrix(0,2) = rotMtx(0,2);

    transformationMatrix(1,0) = rotMtx(1,0);
    transformationMatrix(1,1) = rotMtx(1,1);
    transformationMatrix(1,2) = rotMtx(1,2);

    transformationMatrix(2,0) = rotMtx(2,0);
    transformationMatrix(2,1) = rotMtx(2,1);
    transformationMatrix(2,2) = rotMtx(2,2);

    transformationMatrix(0,3) = translationExpressedBodyframe(0);
    transformationMatrix(1,3) = translationExpressedBodyframe(1);
    transformationMatrix(2,3) = translationExpressedBodyframe(2);

    transformationMatrix(3,0) = 0.0f;
    transformationMatrix(3,1) = 0.0f;
    transformationMatrix(3,2) = 0.0f;
    transformationMatrix(3,3) = 1.0f;;
}



void rotmtx2angle(const Eigen::Matrix3d& rotationMatrix, Eigen::Vector3d& eulerAngle)
{
    /**
    % Project:  Patch-based Illumination invariant Visual Odometry (PIVO)
    % Function: rotmtx2angle
    %
    % Description:
    %   This function return the euler angle along x,y and z direction
    %   from rotationMatrix to [phi;theta;psi] angle defined as ZYX sequence
    %
    % Example:
    %   OUTPUT:
    %   eulerAngle: angle vector composed of [phi;theta;psi]
    %               phi = Rotation angle along x direction in radians
    %               theta = Rotation angle along y direction in radians
    %               psi = Rotation angle along z direction in radians
    %
    %   INPUT:
    %   rotationMatrix = Rotation Matrix [3x3] defined as [Body frame] = rotationMatrix * [Inertial frame]
    %
    % NOTE:
    %
    % Author: Pyojin Kim
    % Email: pjinkim1215@gmail.com
    % Website:
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % log:
    % 2015-02-06: Complete
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    **/


    // change rotMtxBody / [Inertial frame] = rotMtxBody * [Body frame]
    Eigen::Matrix3d rotMtxBody = rotationMatrix.transpose();

    double phi, theta, psi;
    phi = atan2(rotMtxBody(2,1),rotMtxBody(2,2));
    theta = asin(-rotMtxBody(2,0));
    psi = atan2(rotMtxBody(1,0),rotMtxBody(0,0));

    // assign roll, pitch, yaw values
    eulerAngle(0) = phi;
    eulerAngle(1) = theta;
    eulerAngle(2) = psi;
}



void transformatrix2statevector(const Eigen::Matrix4d& transformationMatrix, Eigen::VectorXd& stateVector)
{
    /**
    % Project:  Patch-based Illumination invariant Visual Odometry (PIVO)
    % Function: transformatrix2statevector
    %
    % Description:
    %   change the 'form' from transformation matrix [4x4] to state vector [6x1]
    %
    % Example:
    %   OUTPUT:
    %   stateVector: state vector which represents the location and pose of object
    %                [x; y; z; roll; pitch; yaw] w.r.t. the inertial frame
    %
    %
    %   INPUT:
    %   transformationMatrix: Transformation Matrix included in SE(3)
    %                         which means camera motion matrix [4x4]
    %                         [P_body] = T * [P_inertial]
    %
    % NOTE:
    %
    % Author: Pyojin Kim
    % Email: pjinkim1215@gmail.com
    % Website:
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % log:
    % 2015-02-06: Complete
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    **/


    // extract the motion information from transformationMatrix
    Eigen::Matrix3d rotationMtx;
    rotationMtx(0,0) = transformationMatrix(0,0);
    rotationMtx(0,1) = transformationMatrix(0,1);
    rotationMtx(0,2) = transformationMatrix(0,2);

    rotationMtx(1,0) = transformationMatrix(1,0);
    rotationMtx(1,1) = transformationMatrix(1,1);
    rotationMtx(1,2) = transformationMatrix(1,2);

    rotationMtx(2,0) = transformationMatrix(2,0);
    rotationMtx(2,1) = transformationMatrix(2,1);
    rotationMtx(2,2) = transformationMatrix(2,2);

    Eigen::Vector3d translationExpressedBodyframe;
    translationExpressedBodyframe(0) = transformationMatrix(0,3);
    translationExpressedBodyframe(1) = transformationMatrix(1,3);
    translationExpressedBodyframe(2) = transformationMatrix(2,3);


    // convert translation vector from [body frame] to [inertial frame]
    Eigen::Matrix3d rotationMtxBody = rotationMtx.transpose();
    Eigen::Vector3d translationExpressedInertialframe = rotationMtxBody * (-translationExpressedBodyframe);


    // convert euler angle [phi;theta;psi] from rotation matrix
    Eigen::Vector3d eulerAngle;
    rotmtx2angle(rotationMtx,eulerAngle);


    // save the values to state vector
    stateVector(0) = translationExpressedInertialframe(0);
    stateVector(1) = translationExpressedInertialframe(1);
    stateVector(2) = translationExpressedInertialframe(2);
    stateVector(3) = eulerAngle(0);
    stateVector(4) = eulerAngle(1);
    stateVector(5) = eulerAngle(2);
}



void updateStateEstiData(const Eigen::Matrix4d& T_vo_esti, const int& keyIdx, const int& imgIdx, Eigen::Matrix4d& outputT_vo_esti, Eigen::MatrixXd& stateEsti)
{
    /**
    % Project:  Patch-based Illumination invariant Visual Odometry (PIVO)
    % Function: updateStateEstiData
    %
    % Description:
    %   Update the estimated 6-dof state values from [T_vo_esti] matrix
    %
    % Example:
    %   OUTPUT:
    %   outputT_vo_esti: current rigid body transformation matrix, which means that it is "T_1i"
    %   stateEsti: updated estimated 6-dof state w.r.t. ntimeRgb stamps
    %
    %   INPUT:
    %   stateEsti: current estimated 6-dof state w.r.t. ntimeRgb stamps
    %   T_vo_esti: estimated transformation mtx btwn [keyIdx] & [imgIdx]
    %   keyIdx: current keyframe index
    %   imgIdx: current image index
    %
    % NOTE:
    %
    % Author: Pyojin Kim
    % Email: pjinkim1215@gmail.com
    % Website:
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % log:
    % 2015-01-26: Complete
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    **/


    // transformation matrix at [keyIdx]
    Eigen::Matrix4d T_keyIdx;
    statevector2transformatrix(stateEsti.col(keyIdx-1),T_keyIdx);


    // transformation matrix at [imgIdx]
    Eigen::Matrix4d T_imgIdx;
    T_imgIdx = T_vo_esti * T_keyIdx;


    // assign the transformation matrix into state vector
    outputT_vo_esti = T_imgIdx.inverse(); // [Inertial frame] = outputT_vo_esti * [Body frame], which means that (outputT_vo_esti) is (T_1i)


    Eigen::VectorXd outputStateEsti(6);
    transformatrix2statevector(T_imgIdx,outputStateEsti);
    stateEsti(0,(imgIdx-1)) = outputStateEsti(0);
    stateEsti(1,(imgIdx-1)) = outputStateEsti(1);
    stateEsti(2,(imgIdx-1)) = outputStateEsti(2);
    stateEsti(3,(imgIdx-1)) = outputStateEsti(3);
    stateEsti(4,(imgIdx-1)) = outputStateEsti(4);
    stateEsti(5,(imgIdx-1)) = outputStateEsti(5);
}



double EuclideanDist(const Eigen::MatrixXd& stateEsti, const int& keyIdx, const int& imgIdx)
{
    double del_tx, del_ty, del_tz;
    double distance = 0;

    del_tx = stateEsti(0,(keyIdx-1)) - stateEsti(0,(imgIdx-1));
    del_ty = stateEsti(1,(keyIdx-1)) - stateEsti(1,(imgIdx-1));
    del_tz = stateEsti(2,(keyIdx-1)) - stateEsti(2,(imgIdx-1));

    distance = sqrt( (del_tx*del_tx) + (del_ty*del_ty) + (del_tz*del_tz) );

    // for debug
    //std::cout << distance << std::endl;
    //usleep(10000000);

    return distance;
}



bool EulerAngleDist(const Eigen::MatrixXd& T_imgIdx_keyIdx, const double& maximum_euler_angle)
{
  // obtain euler angles
  Eigen::VectorXd outputStateEsti(6);
  outputStateEsti.fill(0);
  transformatrix2statevector(T_imgIdx_keyIdx, outputStateEsti);

  if (outputStateEsti(3) >= maximum_euler_angle || outputStateEsti(4) >= maximum_euler_angle || outputStateEsti(5) >= maximum_euler_angle)
  {
    return true;
  }
  else
  {
    return false;
  }
}
