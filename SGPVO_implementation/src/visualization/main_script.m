clc;
close all;
clear variables; %clear classes;
rand('state',0); % rand('state',sum(100*clock));
dbstop if error;


%% basic setup for TVIO


stateEsti_SGPVO = load('../../build/stateEsti.txt');


figure(10);
k = 1800;
plot3(stateEsti_SGPVO(1,1:k),stateEsti_SGPVO(2,1:k),stateEsti_SGPVO(3,1:k),'r','LineWidth',2);
grid on; axis equal;


figure(10);
for k=1:1550
    figure(10);
    cla;
    plot3(stateEsti_SGPVO(1,1:k),stateEsti_SGPVO(2,1:k),stateEsti_SGPVO(3,1:k),'r','LineWidth',2);
    grid on; axis equal;
    refresh; pause(0.01);
end

