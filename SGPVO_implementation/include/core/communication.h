#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h> // memset()
#include <unistd.h>
#include <arpa/inet.h> // struct sockaddr_in
#include <sys/types.h> // socket(), bind()
#include <sys/socket.h> // socket(), bind()
#include <iostream>

#include <fcntl.h>  // File control definitions
#include <termios.h> // POSIX terminal control definitionss
#include <time.h>

#include <sstream>
#include <cstring>
#include <vector>
#include "core/Common.h"
#include "TNT/tnt.h"
#include "TNT_Utils/TNT_Utils.h"
#include <thread>
#include <eigen3/Eigen/Dense>

using namespace std;
using namespace toadlet::egg;
using namespace toadlet;
using namespace TNT;



class communicateHostComputer {

public:

  // WiFi communication parameters
  struct WiFiCommunication {
    std::string IP_address;
    int num_port;
    WiFiCommunication () {
      IP_address = "192.168.43.121";
      num_port = 13120;
    }
  };

  // general parameters
  struct parameters {
    communicateHostComputer::WiFiCommunication WiFi_setup;            // WiFi communication parameters
  };


  // constructor, takes as input a parameter structure:
  communicateHostComputer(parameters param) : param(param) {

    camera_velocity_.fill(0);
    camera_pose_.fill(0);

    // initialize WiFi communication with server (host computer)
    onBtnConnect_clicked(this->param.WiFi_setup.IP_address, this->param.WiFi_setup.num_port);
    std::cout << "#########################################################" << std::endl;
    std::cout << "Connecting to Host IP : " << this->param.WiFi_setup.IP_address << " / Port number : " << this->param.WiFi_setup.num_port << std::endl;
    usleep(2000000);
  }


  // deconstructor
  ~communicateHostComputer() {
    std::cout << "close communicateHostComputer." << std::endl;
  }




  // run velocity estimation thread
  std::thread runThread() {
    return std::thread([=] { process(); });
  }


  // process communication with host computer (server).
  void process() {

    while(true) {

      if(mSocketUDP != NULL) {

        Packet pCamVelocity, pCamPose;


        pCamVelocity.dataFloat.resize(6);
        mMutexCamVelocity_.lock();
        pCamVelocity.dataFloat[0] = camera_velocity_(0);
        pCamVelocity.dataFloat[1] = camera_velocity_(1);
        pCamVelocity.dataFloat[2] = camera_velocity_(2);
        pCamVelocity.dataFloat[3] = camera_velocity_(3);
        pCamVelocity.dataFloat[4] = camera_velocity_(4);
        pCamVelocity.dataFloat[5] = camera_velocity_(5);
        mMutexCamVelocity_.unlock();
        pCamVelocity.type = COMM_VEL;
        pCamVelocity.time = 10;


        pCamPose.dataFloat.resize(6);
        mMutexCamPose_.lock();
        pCamPose.dataFloat[0] = camera_pose_(0);
        pCamPose.dataFloat[1] = camera_pose_(1);
        pCamPose.dataFloat[2] = camera_pose_(2);
        pCamPose.dataFloat[3] = camera_pose_(3);
        pCamPose.dataFloat[4] = camera_pose_(4);
        pCamPose.dataFloat[5] = camera_pose_(5);
        mMutexCamPose_.unlock();
        pCamPose.type = COMM_POS;
        pCamPose.time = 10;


        Collection<tbyte> buff_velocity;
        pCamVelocity.serialize(buff_velocity);
        sendUDP(buff_velocity.begin(), buff_velocity.size());
        std::cout << "Send 6 DoF camera velocity." << std::endl;


        Collection<tbyte> buff_pose;
        pCamPose.serialize(buff_pose);
        sendUDP(buff_pose.begin(), buff_pose.size());
        std::cout << "Send 6 DoF camera pose." << std::endl;


        // delay 15 ms
        usleep(15000);
      }
    }
  }




  void onBtnConnect_clicked(const string& IP_address, const int& num_port) {
    mIP = IP_address;
    mPort = num_port;
    std::cout << "Connecting to Host at " << mIP << ":" << mPort << " ... ";
    try {
      if(mSocketUDP != NULL) {
        mSocketUDP->close();
      }
      mSocketUDP = Socket::ptr(Socket::createUDPSocket());
      mSocketUDP->bind(mPort);
      mSocketUDP->setBlocking(false);
      std::cout << "done." << std::endl;
    }
    catch (...) {
      if(mSocketUDP != NULL) {
        mSocketUDP->close();
      }
      mSocketUDP = NULL;
      std::cout << "Failed to connect" << std::endl;
    }
  }


  bool sendUDP(tbyte* data, int size) {
    if(mSocketUDP == NULL) {
      return false;
    }

    try {
      mMutex_socketUDP.lock();
      mSocketUDP->sendTo(data,size,toadlet::egg::Socket::stringToIP(String(mIP.c_str())),mPort);
      mMutex_socketUDP.unlock();
    }
    catch (...) {
      cout << "Send failure" << endl;
      mMutex_socketUDP.lock();
      mSocketUDP->close();
      mSocketUDP = NULL;
      mMutex_socketUDP.unlock();
      mIsConnected = false;
      cout << "send udp catch" << endl;
      return false;
    }

    return true;
  }


  bool updateCamVelocity(const Eigen::Matrix<double, 6, 1>& Xhat_temp_) {
    mMutexCamVelocity_.lock();
    camera_velocity_(0) = Xhat_temp_(0);
    camera_velocity_(1) = Xhat_temp_(1);
    camera_velocity_(2) = Xhat_temp_(2);
    camera_velocity_(3) = Xhat_temp_(3);
    camera_velocity_(4) = Xhat_temp_(4);
    camera_velocity_(5) = Xhat_temp_(5);
    mMutexCamVelocity_.unlock();
    return true;
  }


  bool updateCamPose(const Eigen::Vector3d& center_position, const Eigen::Vector3d& euler_angle) {
    mMutexCamPose_.lock();
    camera_pose_(0) = center_position(0);
    camera_pose_(1) = center_position(1);
    camera_pose_(2) = center_position(2);
    camera_pose_(3) = euler_angle(0);
    camera_pose_(4) = euler_angle(1);
    camera_pose_(5) = euler_angle(2);
    mMutexCamPose_.unlock();
    return true;
  }




private:

  // common parameters
  parameters param;


  // connection parameters
  bool mIsConnected;
  string mIP;
  int mPort;
  toadlet::Socket::ptr mSocketUDP;
  toadlet::uint64 mTimeMS;
  toadlet::egg::Mutex mMutex_socketUDP;
  toadlet::egg::Mutex mMutexCamVelocity_;
  toadlet::egg::Mutex mMutexCamPose_;


  // data storage parameters
  Eigen::Matrix<double, 6, 1> camera_velocity_;
  Eigen::Matrix<double, 6, 1> camera_pose_;
};

#endif /* _COMMUNICATION_H_ */
