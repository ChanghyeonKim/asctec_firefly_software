/*
Copyright 2016. All rights reserved.
Intelligent Control Systems Laboratory
Seoul National University, South Korea

This file is part of velocity estimator with RGB-D camera.
Authors: Pyojin Kim

velocity estimator is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or any later version.

velocity estimator is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
velocity estimator; if not, write to the Free Software Foundation, Inc., 51 Franklin
Street, Fifth Floor, Boston, MA 02110-1301, USA
*/

#ifndef _VELOCITY_ESTIMATOR_REALSENSE_H_
#define _VELOCITY_ESTIMATOR_REALSENSE_H_

#include <thread>
#include <string>
#include "core/time.h"
#include "core/communication.h"
#include <librealsense/rs.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>


#define PI 3.141592


class velocityEstimatorRealsense {

public:

  // RGB camera parameters (all are mandatory / need to be supplied)
  struct calibration {
    double fx;  //  focal length (in pixels)
    double fy;  //  focal length (in pixels)
    double cx;  //  principal point (u-coordinate)
    double cy;  //  principal point (v-coordinate)
    calibration () {
      fx = 620.608832234754;
      fy = 619.113993685335;
      cx = 323.902900972212;
      cy = 212.418428046497;
    }
  };

  // featureDetection parameters (for goodFeaturesToTrack)
  struct featureDetection {
    int max_features;            // maximal number of features in image
    double quality_level;
    double min_distance;
    int block_size;
    bool use_Harris_detector;
    double k;
    featureDetection () {
      max_features  = 1000;
      quality_level = 0.01;
      min_distance = 10;
      block_size = 4;
      use_Harris_detector = false;
      k = 0.04;
    }
  };

  // featureTracking parameters (for calcOpticalFlowPyrLK)
  struct featureTracking {
    int win_size;
    int max_level;
    int flags_R;
    featureTracking () {
      win_size = 11;
      max_level = 5;
      flags_R = 0;
    }
  };

  // RANSAC parameters (for findHomography)
  struct RANSAC {
    double threshold;        // Maximum allowed reprojection error to treat a point pair as an inlier
    RANSAC () {
      threshold = 1.5;
    }
  };

  // Kalman filtering parameters
  struct KalmanFilter {
    double u_prnoise;          // Linear Velocity(u) noise (m/s) - standard deviation level
    double v_prnoise;          // Linear Velocity(v) noise (m/s) - standard deviation level
    double w_prnoise;          // Linear Velocity(w) noise (m/s) - standard deviation level
    double p_prnoise;          // Angular Velocity(p) noise (rad/s) - standard deviation level
    double q_prnoise;          // Angular Velocity(q) noise (rad/s) - standard deviation level
    double r_prnoise;          // Angular Velocity(r) noise (rad/s) - standard deviation level

    double u_noise;            // Linear Velocity(u) noise (m/s) - standard deviation level
    double v_noise;            // Linear Velocity(v) noise (m/s) - standard deviation level
    double w_noise;            // Linear Velocity(w) noise (m/s) - standard deviation level
    double p_noise;            // Angular Velocity(p) noise (rad/s) - standard deviation level
    double q_noise;            // Angular Velocity(q) noise (rad/s) - standard deviation level
    double r_noise;            // Angular Velocity(r) noise (rad/s) - standard deviation level
    KalmanFilter () {
      u_prnoise = 0.001;
      v_prnoise = 0.001;
      w_prnoise = 0.001;
      p_prnoise = 0.001;
      q_prnoise = 0.001;
      r_prnoise = 0.001;

      u_noise = 0.004;
      v_noise = 0.004;
      w_noise = 0.004;
      p_noise = 0.003;
      q_noise = 0.003;
      r_noise = 0.003;
    }
  };

  // debug parameters
  struct debugFlags {
    bool print_current_velocity;
    bool print_current_image;
    debugFlags () {
      print_current_velocity = false;
      print_current_image = false;
    }
  };

  // general parameters
  struct parameters {
    velocityEstimatorRealsense::calibration calib;                       // camera calibration parameters
    velocityEstimatorRealsense::featureDetection feature_detect;         // feature detection parameters
    velocityEstimatorRealsense::featureTracking feature_track;           // feature tracking parameters
    velocityEstimatorRealsense::RANSAC homography_RANSAC;                // RANSAC parameters
    velocityEstimatorRealsense::KalmanFilter Kalman_filter_noise;        // Kalman filter parameters
    velocityEstimatorRealsense::debugFlags debug_flags;                  // debug parameters
  };


  // constructor, takes as input a parameter structure:
  velocityEstimatorRealsense(parameters param) : param(param) {

    first_initialization_              = true;
    image_counter_                     = 0;
    dt_                                = 0;

    // initialize Kalman filter
    Xhat_temp_                         = Eigen::MatrixXd::Zero(6,1);

    A_                                 = Eigen::MatrixXd::Identity(6,6);
    B_                                 = Eigen::MatrixXd::Zero(3,3);
    C_                                 = Eigen::MatrixXd::Identity(6,6);
    eye_6_                             = Eigen::MatrixXd::Identity(6,6);

    P_                                 = Eigen::MatrixXd::Identity(6,6);
    Q_                                 = Eigen::MatrixXd::Identity(6,6);
    Q_(0,0)                            = (param.Kalman_filter_noise.u_prnoise * param.Kalman_filter_noise.u_prnoise);
    Q_(1,1)                            = (param.Kalman_filter_noise.v_prnoise * param.Kalman_filter_noise.v_prnoise);
    Q_(2,2)                            = (param.Kalman_filter_noise.w_prnoise * param.Kalman_filter_noise.w_prnoise);
    Q_(3,3)                            = (param.Kalman_filter_noise.p_prnoise * param.Kalman_filter_noise.p_prnoise);
    Q_(4,4)                            = (param.Kalman_filter_noise.q_prnoise * param.Kalman_filter_noise.q_prnoise);
    Q_(5,5)                            = (param.Kalman_filter_noise.r_prnoise * param.Kalman_filter_noise.r_prnoise);

    R_                                 = Eigen::MatrixXd::Identity(6,6);
    R_(0,0)                            = (param.Kalman_filter_noise.u_noise * param.Kalman_filter_noise.u_noise);
    R_(1,1)                            = (param.Kalman_filter_noise.v_noise * param.Kalman_filter_noise.v_noise);
    R_(2,2)                            = (param.Kalman_filter_noise.w_noise * param.Kalman_filter_noise.w_noise);
    R_(3,3)                            = (param.Kalman_filter_noise.p_noise * param.Kalman_filter_noise.p_noise);
    R_(4,4)                            = (param.Kalman_filter_noise.q_noise * param.Kalman_filter_noise.q_noise);
    R_(5,5)                            = (param.Kalman_filter_noise.r_noise * param.Kalman_filter_noise.r_noise);


    // initialize Intel realsense sensor (R200)
    std::cout << "#########################################################" << std::endl;
    std::cout << "There are " << ctx_.get_device_count() << " connected RealSense device.\n" << std::endl;
    dev_ = ctx_.get_device(0);
    std::cout << "\nUsing device 0, an " << dev_->get_name() << std::endl;
    std::cout << "Serial number: " << dev_->get_serial() << std::endl;
    std::cout << "Firmware version: " << dev_->get_firmware_version() << std::endl;
    depth_scale_ = dev_->get_depth_scale();
    std::cout << "Depth scale : " << depth_scale_ << std::endl;

    dev_->enable_stream(rs::stream::color, 640, 480, rs::format::bgr8, 60);  //  RGB 60 frames per second
    dev_->enable_stream(rs::stream::depth, 640, 480, rs::format::z16, 60);   //  depth 60 frames per second
    dev_->start();
    usleep(2000000);
  }


  // deconstructor
  ~velocityEstimatorRealsense() {
    std::cout << "close velocityEstimatorRealsense." << std::endl;
  }




  // run velocity estimation thread
  std::thread runThread(communicateHostComputer *communicator) {
    return std::thread([=] { process(communicator); });
  }


  // process velocity estimation with Intel realsense camera.
  void process(communicateHostComputer *communicator) {

    // initialize time variables
    auto init_time_us = std::chrono::high_resolution_clock::now();
    auto prev_time_us = std::chrono::high_resolution_clock::now();
    auto curr_time_us = std::chrono::high_resolution_clock::now();

    while(true) {

      // read current color and depth frame
      this->captureCurrentFrame(curr_time_us, prev_time_us);


      // intialize new image frame if all of the features are lost
      if (first_initialization_) {
        // extract features in prev_gray_image_
        prev_gray_image_.release();
        prev_gray_image_ = curr_gray_image_;
        prev_features_.clear();
        cv::goodFeaturesToTrack(prev_gray_image_, prev_features_, param.feature_detect.max_features, param.feature_detect.quality_level, param.feature_detect.min_distance, cv::Mat(), param.feature_detect.block_size, param.feature_detect.use_Harris_detector, param.feature_detect.k);
        cv::cornerSubPix(prev_gray_image_, prev_features_, cv::Size(7,7), cv::Size(-1,-1), cvTermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03));

        // skip the next iteration
        first_initialization_ = false;
        continue;
      }


      // track features with KLT tracker (optical flow)
      debugging_image_ = curr_gray_image_.clone();
      this->doOpticalFlow();


      // compute Homography based RANSAC
      this->doRANSAC();


      // estimate raw 6 DoF velocity w.r.t. camera frame
      Eigen::VectorXd velocity_raw_measurement;
      velocity_raw_measurement.fill(0);
      velocity_raw_measurement = this->estimateVelocity();


      // post-processing to remove image noise etc.
      this->doKalmanFiltering(velocity_raw_measurement);


      // WiFi commuincation with server
      communicator->updateCamVelocity(Xhat_temp_);
      image_counter_ += 1;
      printf("update camera velocity : in %7.5lf seconds / in %05d image counter \n", dt_, image_counter_);
      printf("number of inliers : %04d / number of valid depth : %04d \n", num_inliers_, num_depth_valid_);


      if (param.debug_flags.print_current_velocity) {
        printf("x_dot : %7.5lf \n", Xhat_temp_(0));
        printf("y_dot : %7.5lf \n", Xhat_temp_(1));
        printf("z_dot : %7.5lf \n", Xhat_temp_(2));
        printf("phi_dot : %7.5lf \n", Xhat_temp_(3));
        printf("theta_dot : %7.5lf \n", Xhat_temp_(4));
        printf("psi_dot : %7.5lf \n", Xhat_temp_(5));
        printf("Processing time [sec]: %7.5lf / Image counter: %05d \n", dt_, image_counter_);
      }

      if (param.debug_flags.print_current_image) {
        // display the current debugging_image_
        char str_for_resultview[100];
        double font_scale = 0.45;
        cv::Scalar font_color = CV_RGB(0,0,0);
        int font_thickness = 1.8;

        // display the number of valid points
        int height = debugging_image_.rows;
        int width = debugging_image_.cols;

        sprintf(str_for_resultview,"The number of valid feature pts : %d", num_features_valid_);
        putText(debugging_image_, str_for_resultview, cv::Point2f(5,height-50), cv::FONT_HERSHEY_TRIPLEX, font_scale, font_color, font_thickness );

        sprintf(str_for_resultview,"The number of valid feature pts (inlier) : %d", num_inliers_);
        putText(debugging_image_, str_for_resultview, cv::Point2f(5,height-30), cv::FONT_HERSHEY_TRIPLEX, font_scale, font_color, font_thickness );

        sprintf(str_for_resultview,"The number of valid depth feature pts : %d", num_depth_valid_);
        putText(debugging_image_, str_for_resultview, cv::Point2f(5,height-10), cv::FONT_HERSHEY_TRIPLEX, font_scale, font_color, font_thickness );

        // draw the optical flow vectors
        quiver(debugging_image_, prev_inliers_, curr_inliers_);
      }


      // check current number of features
      this->checkCurrentFeatureStatus();

      if (param.debug_flags.print_current_image) {
        cv::imshow("Result View Image", debugging_image_);
        cv::waitKey(1);
      }
    }
  }


  // capture current gray and depth image frame with Intel realsense
  void captureCurrentFrame(auto& curr_time_us, auto& prev_time_us) {

    dev_->wait_for_frames();
    // read current color and depth frame and change the format to cv::Mat
    const void * color_frame = dev_->get_frame_data(rs::stream::rectified_color);
    const void * depth_frame = dev_->get_frame_data(rs::stream::depth_aligned_to_rectified_color);
    cv::Mat color_native_image(480, 640, CV_8UC3, (void*)color_frame);
    cv::Mat depth_native_image(480, 640, CV_16UC1, (void*)depth_frame);

    // compute time difference between previous and current image frame
    this->computeTimeDifference(curr_time_us, prev_time_us);

    // convert to gray (CV_8UC1) image
    cv::Mat gray_image;
    cvtColor(color_native_image, gray_image, CV_BGR2GRAY);
    gray_image.convertTo(curr_gray_image_, CV_8UC1);

    // convert to depth (CV_64FC1) image in meter
    cv::Mat depth_image;
    depth_native_image.convertTo(depth_image, CV_64FC1);
    curr_depth_image_ = depth_image * depth_scale_;  //  unit : [m]
  }


  // do optical flow with cv::calcOpticalFlowPyrLK
  void doOpticalFlow() {

    // initialize variables for calcOpticalFlowPyrLK
    std::vector<uchar> status_pts;
    std::vector<float> error;

    curr_features_.clear();
    cv::calcOpticalFlowPyrLK(prev_gray_image_, curr_gray_image_, prev_features_, curr_features_, status_pts, error, cv::Size(param.feature_track.win_size, param.feature_track.win_size), param.feature_track.max_level, cvTermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03), param.feature_track.flags_R);
    //flags_R |= CV_LKFLOW_PYR_A_READY;

    // extract only valid features from prev_features_ and curr_features_
    prev_features_valid_.clear();
    curr_features_valid_.clear();
    num_features_valid_ = 0;

    for(int i = 0; i < (int)curr_features_.size(); i++) {
      if ( status_pts[i] != 0 ) {    // just skip if it is not valid feature
        prev_features_valid_.push_back(prev_features_[i]);
        curr_features_valid_.push_back(curr_features_[i]);
        num_features_valid_ += 1;
      }
    }
  }


  // do RANSAC with cv::findHomography (plane model assumption)
  void doRANSAC() {

    // initialize variables for findHomography
    std::vector<uchar> RANSAC_mask;
    cv::findHomography(prev_features_valid_, curr_features_valid_, CV_RANSAC, param.homography_RANSAC.threshold, RANSAC_mask);

    // extract only inliers
    prev_inliers_.clear();
    curr_inliers_.clear();
    num_inliers_ = 0;

    for (int i = 0; i < (int)curr_features_valid_.size(); i++) {
      if ((int)RANSAC_mask[i] == 1) {    // select only the inliers (mask entry set to 1)
        prev_inliers_.push_back(prev_features_valid_[i]);
        curr_inliers_.push_back(curr_features_valid_[i]);
        num_inliers_ += 1;
      }
    }
  }


  // estimate current raw 6 DoF velocity expressed in current camera frame
  Eigen::VectorXd estimateVelocity() {

    // calculate the number of valid depth features
    num_depth_valid_ = 0;
    double depth_test = 0;

    for(int i = 0; i < (int)curr_inliers_.size(); i++) {
      // round that pixel point
      curr_inliers_[i].x = (int)this->rounding(curr_inliers_[i].x, 0);
      curr_inliers_[i].y = (int)this->rounding(curr_inliers_[i].y, 0);

      if (this->inImage(curr_inliers_[i], curr_depth_image_)) {    // extract the depth value at that point
        depth_test = 0;
        depth_test = curr_depth_image_.at<double>(curr_inliers_[i]);

        if ( depth_test >= 0.02 ) {
          num_depth_valid_ += 1;
        }
      }
    }


    // initialize variables for velocity estimation
    Eigen::VectorXd V_fp(2*num_depth_valid_);
    Eigen::MatrixXd image_Jacobian(2*num_depth_valid_, 6);
    Eigen::VectorXd V_c(6);
    V_c << 0, 0, 0, 0, 0, 0;

    double depth_in_meter = 0;
    int count = 0;


    // compute V_fp and image_Jacobian with valid features
    for(int i = 0; i < (int)curr_inliers_.size(); i++) {
      if (this->inImage(curr_inliers_[i], curr_depth_image_)) {
        // extract the depth value at that point
        depth_in_meter = 0;
        depth_in_meter = curr_depth_image_.at<double>(curr_inliers_[i]);

        if ( depth_in_meter >= 0.02 ) {

          // compute V_fp (optical flow) vector ( 2n x 1 )
          double xdot = 0;
          double ydot = 0;
          double pre_norm_feature_pts_x = 0, pre_norm_feature_pts_y = 0;
          double norm_feature_pts_x = 0, norm_feature_pts_y = 0;

          pre_norm_feature_pts_x = (prev_inliers_[i].x - param.calib.cx) / param.calib.fx;
          pre_norm_feature_pts_y = (prev_inliers_[i].y - param.calib.cy) / param.calib.fy;

          norm_feature_pts_x = (curr_inliers_[i].x - param.calib.cx) / param.calib.fx;
          norm_feature_pts_y = (curr_inliers_[i].y - param.calib.cy) / param.calib.fy;

          xdot = (norm_feature_pts_x - pre_norm_feature_pts_x) / dt_;
          ydot = (norm_feature_pts_y - pre_norm_feature_pts_y) / dt_;

          V_fp(2*count) = xdot;
          V_fp(2*count+1) = ydot;


          // compute image Jacobian matrix ( 2n x 6 )
          image_Jacobian(2*count, 0) = -1/depth_in_meter;
          image_Jacobian(2*count, 1) = 0;
          image_Jacobian(2*count, 2) = norm_feature_pts_x/depth_in_meter;
          image_Jacobian(2*count, 3) = norm_feature_pts_x*norm_feature_pts_y;
          image_Jacobian(2*count, 4) = -( 1 + norm_feature_pts_x*norm_feature_pts_x );
          image_Jacobian(2*count, 5) = norm_feature_pts_y;

          image_Jacobian(2*count+1, 0) = 0;
          image_Jacobian(2*count+1, 1) = -1/depth_in_meter;
          image_Jacobian(2*count+1, 2) = norm_feature_pts_y/depth_in_meter;
          image_Jacobian(2*count+1, 3) = ( 1 + norm_feature_pts_y*norm_feature_pts_y );
          image_Jacobian(2*count+1, 4) = - norm_feature_pts_x*norm_feature_pts_y;
          image_Jacobian(2*count+1, 5) = -norm_feature_pts_x;


          // count the number of valid features ( n )
          count += 1;
        }
      }
    }


    // check the number of count and valid depth points are same
    if (num_depth_valid_ != count) {
      printf("\n\n The number of valid depth points != The count number !! \n\n");
      printf("The number of valid depth : %d \n", num_depth_valid_);
      printf("The number of iteration : %d \n", count);
      printf("\n\n Finish This Program. Thank you! \n\n");
      usleep(1000000000);
      return V_c;
    }


    // estimate current 6 DoF RGB-D camera velocity with respect to the current camera frame
    Eigen::MatrixXd Img_Jacobian_square(6, 6);
    Img_Jacobian_square = image_Jacobian.transpose() * image_Jacobian;
    V_c = (Img_Jacobian_square.inverse() * image_Jacobian.transpose() * V_fp);
    return V_c;
  }


  // post Kalman filter processing to smoothen estimated velocity
  void doKalmanFiltering(const Eigen::VectorXd& z) {

    // prediction step
    Xhat_temp_ = A_ * Xhat_temp_;
    P_ = A_ * P_ * A_.transpose() + Q_;

    // update step
    Eigen::MatrixXd K_gain;
    K_gain.fill(0);
    K_gain = P_ * C_.transpose() * (C_ * P_ * C_.transpose() + R_).inverse();
    P_ = (eye_6_ - K_gain * C_) * P_ * (eye_6_ - K_gain * C_).transpose() + K_gain * R_ * K_gain.transpose();
    Xhat_temp_ = Xhat_temp_ + K_gain * (z - C_ * Xhat_temp_);

    // artificial velocity saturation
    for (int i = 0; i < 3; i++) {
      if (Xhat_temp_(i) > 1.0) {
        Xhat_temp_(i) = 1.0;
      }
      else if (Xhat_temp_(i) < -1.0) {
        Xhat_temp_(i) = -1.0;
      }
    }
  }


  // check current status of features and perform feature detection if they are in bad condition
  void checkCurrentFeatureStatus() {

    // save this [k] step feature points, images as [k-1] step feature points, images
    if (num_inliers_ <= 130 || num_depth_valid_ <= 100) {

      // extract again features in curr_gray_image_
      prev_features_.clear();

      cv::goodFeaturesToTrack(curr_gray_image_, prev_features_, param.feature_detect.max_features, param.feature_detect.quality_level, param.feature_detect.min_distance, cv::Mat(), param.feature_detect.block_size, param.feature_detect.use_Harris_detector, param.feature_detect.k);
      cv::cornerSubPix(curr_gray_image_, prev_features_, cv::Size(7,7), cv::Size(-1,-1), cvTermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03));

      if (param.debug_flags.print_current_image) {
        // draw new detected features
        for (int i = 0; i < prev_features_.size(); i++) {
          cv::circle(debugging_image_, prev_features_[i], 5, CV_RGB(0,255,0), -1, 8, 0);
        }
      }
    }
    else {

      // save valid features only
      prev_features_.clear();

      for(int i = 0; i < (int)curr_features_valid_.size(); i++) {
        prev_features_.push_back(curr_features_valid_[i]);
      }
    }
    prev_gray_image_ = curr_gray_image_.clone();
  }


  // compute time difference (dt_ : seconds)
  void computeTimeDifference(auto& curr_time_us, auto& prev_time_us) {
    curr_time_us = std::chrono::high_resolution_clock::now();
  	auto ticks = std::chrono::duration_cast<std::chrono::microseconds>(curr_time_us - prev_time_us);
    unsigned long diff_ticks = (unsigned long)ticks.count();
    dt_ = (double)diff_ticks * 1e-6;

    // save for next iteration
    prev_time_us = curr_time_us;
  }


  // get time difference (dt_ : seconds)
  double getTimeDifference() const {
    return this->dt_;
  }






  // round double variables
  double rounding(const double& x, const int& digit) {
    double round_result = ( floor( (x) * pow( float(10), digit ) + 0.5f ) / ( pow( float(10), digit ) ) );
    return round_result;
  }

  // check current feature in the image or not
  bool inImage(const cv::Point2f& test_point, const cv::Mat& Reference_Image) {
    int row_size = Reference_Image.rows;
    int col_size = Reference_Image.cols;

    double u = test_point.x;
    double v = test_point.y;

    if ( u >= 0 && u <= (col_size-1) && v >=0 && v <= (row_size-1) ) {
      return true;
    }
    else {
      return false;
    }
  }

  // draw quivers in debugging_image_
  void quiver(cv::Mat& result_view, const std::vector<cv::Point2f>& pre_feature_points, const std::vector<cv::Point2f>& feature_points) {
    // draw the result of optical flow ( depict arrow ) at Mat& result_view
    int width = result_view.cols;
    int height = result_view.rows;

    for(int i = 0; i < (int)feature_points.size(); i++) {
      int line_thickness = 1;
      cv::Scalar line_color = CV_RGB(255,0,0);

      // calculate two points of line
      cv::Point2f p, q;
      p = pre_feature_points[i];
      q = feature_points[i];

      double angle;
      double arrow_length;

      angle = atan2( (double) p.y - q.y, (double) p.x - q.x );
      arrow_length = sqrt( pow((float)(p.y - q.y),2) + pow((float)(p.x - q.x),2) );

      int mag_arrow = 3;

      q.x = (int) (p.x - mag_arrow * arrow_length * cos(angle));  // draw arrow mag_arrow times of original arrow magnitude.
      q.y = (int) (p.y - mag_arrow * arrow_length * sin(angle));

      if ((arrow_length<0)|(arrow_length>30)) {   // condition of magnitude of arrow.
        continue; // skip the arrow is too short or long.
      }

      // draw arrow
      cv::line(result_view, p, q, line_color, line_thickness, CV_AA);
      p.x = (int) (q.x + 10 * cos(angle + PI / 4));
      if(p.x>=width)
      p.x=width-1;
      else if(p.x<0)
      p.x=0;
      p.y = (int) (q.y + 10 * sin(angle + PI / 4));
      if(p.y>=height)
      p.y=height-1;
      else if(p.y<0)
      p.y=0;
      cv::line(result_view, p, q, line_color, line_thickness, CV_AA);
      p.x = (int) (q.x + 10 * cos(angle - PI / 4));
      if(p.x>=width)
      p.x=width-1;
      else if(p.x<0)
      p.x=0;
      p.y = (int) (q.y + 10 * sin(angle - PI / 4));
      if(p.y>height)
      p.y=height-1;
      else if(p.y<0)
      p.y=0;
      cv::line(result_view, p, q, line_color, line_thickness, CV_AA);
    }
  }




private:

  // common parameters
  parameters param;
  bool first_initialization_;
  int image_counter_;
  double dt_;


  // Intel realsense variables
  rs::context ctx_;
  rs::device * dev_;
  double depth_scale_;


  // image processing variables
  cv::Mat prev_gray_image_;                       //  previous gray image at k-1
  cv::Mat curr_gray_image_;                       //  current gray image at k
  cv::Mat curr_depth_image_;                      //  current depth image at k

  cv::Mat debugging_image_;                       // to show the result view

  std::vector<cv::Point2f> prev_features_;
  std::vector<cv::Point2f> curr_features_;

  std::vector<cv::Point2f> prev_features_valid_;
  std::vector<cv::Point2f> curr_features_valid_;
  int num_features_valid_;

  std::vector<cv::Point2f> prev_inliers_;
  std::vector<cv::Point2f> curr_inliers_;
  int num_inliers_;

  int num_depth_valid_;


  // Kalman filter variables
  Eigen::Matrix<double, 6, 1> Xhat_temp_;

  Eigen::Matrix<double, 6, 6> A_;
  Eigen::Matrix<double, 3, 3> B_;
  Eigen::Matrix<double, 6, 6> C_;
  Eigen::Matrix<double, 6, 6> eye_6_;

  Eigen::Matrix<double, 6, 6> P_;
  Eigen::Matrix<double, 6, 6> Q_;
  Eigen::Matrix<double, 6, 6> R_;
};

#endif // _VELOCITY_ESTIMATOR_REALSENSE_H_
