#ifndef __VISO_PIVO_H__
#define __VISO_PIVO_H__


#include "core/datatypes.h"
#include "core/dense_tracking.h"
#include <string>
#include "core/libelas.h"
#include "core/rgbd_image.h"


class VisualOdometryPIVO
{

public:

    // set camera calibration parameters MANUALLY
    camCalibrationParam cam;
    optsPIVO opts;

    // constructor, takes as input a parameter structure:
    //VisualOdometryPIVO();

    // deconstructor
    //~VisualOdometryPIVO();






    // functions for PIVO
    bool initialize()
    {
      T_21_final = Eigen::Matrix4d::Identity(4,4);
      T_21_ini = Eigen::Matrix4d::Identity(4,4);
      for(int patchIdx = 0; patchIdx < this->opts.numWinTotal; ++patchIdx)
      {
          illParamPIVO.patch[patchIdx].contrast = 1.0f;
          illParamPIVO.patch[patchIdx].bias = 0.0f;
      }
      return true;
    }


    bool initializeKeyframe()
    {
      // declare Mat variables
      cv::Mat grey_64F, depth_64F;
      cv::Mat img0rect, img1rect;
      img0rect = this->I2Ref_cam0_rect_;
      img1rect = this->I2Ref_cam1_rect_;


      // fit the pyramid size (32 times)
      cv::Mat targetImage;
      cv::Rect roi(0, 0, 32*23, 32*15);  // roi(uStart,vStart,uLength,vLength)
      targetImage = img0rect(roi);


      // transform from integer to double
      targetImage.convertTo(grey_64F, CV_64FC1); // grey_64F.type() is CV_64FC1 (double)


      // calculate stereo disparity map
      cv::Mat disLeftTranspose(img0rect.cols, img0rect.rows, CV_64FC1);
      imwrite("tempLeftImg.pgm", img0rect);
      imwrite("tempRightImg.pgm", img1rect);
      process("tempLeftImg.pgm", "tempRightImg.pgm", disLeftTranspose);

      cv::Mat disLeft;
      cv::transpose(disLeftTranspose, disLeft);


      // extract depth map (Z) from disparity map
      cv::Mat imgZ = disLeft;
      for(int32_t v = 0; v < disLeft.rows; v++)
      {
          for(int32_t u = 0; u < disLeft.cols; u++)
          {
              if (disLeft.at<double>(v,u) > 0)
              {
                  imgZ.at<double>(v,u) = (1/disLeft.at<double>(v,u));
              }
              else
              {
                  imgZ.at<double>(v,u) = 0;
              }
          }
      }
      imgZ = this->cam.K(0,0) * this->cam.baseline * imgZ;


      // remove depth value over 50 m (unreliable value)
      for(int32_t v = 0; v < imgZ.rows; v++)
      {
          for(int32_t u = 0; u < imgZ.cols; u++)
          {
              if (imgZ.at<double>(v,u) > 50)
              {
                  imgZ.at<double>(v,u) = 0;
              }
          }
      }

      // for debug
      //std::cout << imgZ.cols << std::endl;
      //std::cout << imgZ.rows << std::endl;
      //std::cout << imgZ.at<double>(86,516) << std::endl;
      //std::cout << (imgZ.type() == CV_64FC1) << std::endl;
      //usleep(10000000);


      // fit the pyramid size (32 times)
      cv::Mat targetDepthImage;
      targetDepthImage = imgZ(roi);


      // transform from double to double
      targetDepthImage.convertTo(depth_64F, CV_64FC1); // depth_64F.type() is CV_64FC1 (double)


      // save I1Ref_ & D1Ref_
      this->I1Ref_.release();
      this->D1Ref_.release();
      this->I1Ref_ = grey_64F;
      this->D1Ref_ = depth_64F;


      // generate I1RefPyr_ & D1RefPyr_ image pyramid
      this->I1RefPyr_.clear();
      this->D1RefPyr_.clear();
      genPyramidImage(this->I1RefPyr_, this->D1RefPyr_, this->I1Ref_, this->D1Ref_, this->opts);


      // extract keyPts_ & keyPtsPyr_ in grey image
      this->keyPts_.resize(0,0);
      this->keyPts_.resize(2, this->opts.numWinTotal);
      detectKeypoints(this->I1Ref_, this->keyPts_, this->opts); // 1-based pixel coordinates (u,v)
      this->keyPtsPyr_.clear();
      genPyramidKeyPts(this->keyPtsPyr_, this->keyPts_, this->opts);

      // for debug
      //std::cout << this->keyPts_ << std::endl;
      //std::cout << this->keyPtsPyr_[0] << std::endl;
      //usleep(10000000);
      return true;
    }


    bool initializeImageframe(const cv::Mat& vi_cam0_image_rect, const cv::Mat& vi_cam1_image_rect)
    {
      // save for keyframe initializiation
      this->I2Ref_cam0_rect_.release();
      this->I2Ref_cam1_rect_.release();
      this->I2Ref_cam0_rect_ = vi_cam0_image_rect;
      this->I2Ref_cam1_rect_ = vi_cam1_image_rect;


      // I2Ref_ image frame with proper region of interest
      cv::Mat targetImage, grey_64F;
      cv::Rect roi(0, 0, 32*23, 32*15);  // roi(uStart,vStart,uLength,vLength)
      targetImage = vi_cam0_image_rect(roi);


      // transform from integer to double
      targetImage.convertTo(grey_64F, CV_64FC1); // grey_64F.type() is CV_64FC1 (double)


      // save I2Ref_
      this->I2Ref_.release();
      this->I2Ref_ = grey_64F;

      // for debug
      //cv::imshow("targetImage view",targetImage);
      //cv::waitKey(0);
      //std::cout << this->I2Ref_.cols << std::endl;
      //std::cout << this->I2Ref_.rows << std::endl;
      //std::cout << (this->I2Ref_.type() == CV_64FC1) << std::endl;
      //usleep(10000000);


      // construct I2Ref_ image pyramid
      this->I2RefPyr_.clear();
      genPyramidImageOnly(this->I2RefPyr_, this->I2Ref_, this->opts);

      // for debug
      //cv::Mat forDebugImg;
      //this->I2RefPyr_[2].convertTo(forDebugImg,CV_8UC1);
      //imshow("camera view", forDebugImg);
      //cv::waitKey(0);
      //usleep(100000000);
      return true;
    }


    bool updateMotion(const Eigen::Matrix4d& T_ini, const illParam_perImg& illParam_ini, Eigen::Matrix4d& T_output, illParam_perImg& illParam_output)
    {
      // assign initial model parameters
      Eigen::Matrix4d inputT_temp = T_ini;
      Eigen::Matrix4d outputT_temp = T_ini;
      illParam_perImg inputIllParam_temp = illParam_ini;
      illParam_perImg outputIllParam_temp = illParam_ini;

      // initialize PIVO status
      double err_temp = 0;
      Eigen::MatrixXd residuals_temp;
      Eigen::MatrixXd weights_temp;

      for(int pyrLevel = this->opts.numPyr; pyrLevel >= this->opts.pyrEndLevel; --pyrLevel) // (1-based pyramid level index (pyrLevel))
      {
          // initialize parameters and variables
          inputT_temp = outputT_temp;
          inputIllParam_temp = outputIllParam_temp;

          Eigen::Matrix3d tempInParasMtx = this->cam.K_pyramid[pyrLevel-1];
          double winSize = floor((double)this->opts.winSize/pow(2,pyrLevel)) * 2 + 1;

          // main loop
          EstimateCameraMotionPIVO(this->I1RefPyr_[pyrLevel-1], this->D1RefPyr_[pyrLevel-1], this->keyPtsPyr_[pyrLevel-1], this->I2RefPyr_[pyrLevel-1],
                                   inputT_temp, inputIllParam_temp, tempInParasMtx, winSize, this->opts,
                                   outputT_temp, outputIllParam_temp, err_temp, residuals_temp, weights_temp);
      }

      // save PIVO status
      this->modified_photometric_errors = err_temp;
      this->residuals = residuals_temp;
      this->weights = weights_temp;


      // save PIVO results
      if (err_temp <= 100)
      {
        T_output = outputT_temp;
        illParam_output = outputIllParam_temp;
      }
      else
      {
        // PIVO failure case
        T_output = T_ini;
        illParam_output = illParam_ini;
      }
      return true;
    }



    bool initializeVelocity()
    {
      this->T_gc_previous = Eigen::Matrix4d::Identity(4,4);
      return true;
    }


    bool updateVelocity(const Eigen::Matrix4d& T_gc_current, const double& dt, Eigen::Vector3d& v_current)
    {
      double tx_p, ty_p, tz_p;
      double tx_c, ty_c, tz_c;

      // previous camera position
      tx_p = this->T_gc_previous(0,3);
      ty_p = this->T_gc_previous(1,3);
      tz_p = this->T_gc_previous(2,3);

      // current camera position
      tx_c = T_gc_current(0,3);
      ty_c = T_gc_current(1,3);
      tz_c = T_gc_current(2,3);

      // calculate current velocity
      v_current.fill(0);
      v_current(0) = (tx_c - tx_p) / dt;
      v_current(1) = (ty_c - ty_p) / dt;
      v_current(2) = (tz_c - tz_p) / dt;

      // save for next step
      this->T_gc_previous = T_gc_current;
      return true;
    }



    double getModifiedPhotometricError() const
    {
      return this->modified_photometric_errors;
    }


    cv::Mat getPreviousLeftImage() const
    {
      return this->I2Ref_cam0_rect_;
    }


    cv::Mat getPreviousRightImage() const
    {
      return this->I2Ref_cam1_rect_;
    }




private:

    // grey stereo images at previous frame for keyframe initializiation
    cv::Mat I2Ref_cam0_rect_, I2Ref_cam1_rect_;

    // grey & depth image at keyframe
    cv::Mat I1Ref_, D1Ref_;
    std::vector<cv::Mat> I1RefPyr_, D1RefPyr_;
    Eigen::MatrixXd keyPts_;
    std::vector<Eigen::MatrixXd> keyPtsPyr_;

    // grey & depth image at current frame
    cv::Mat I2Ref_;
    std::vector<cv::Mat> I2RefPyr_;


    // PIVO motion estimation results
    Eigen::Matrix4d T_21_final;
    Eigen::Matrix4d T_21_ini;
    illParam_perImg illParamPIVO;

    // PIVO velocity estimation
    Eigen::Matrix4d T_gc_previous;

    // PIVO motion estimation status
    double modified_photometric_errors;
    Eigen::MatrixXd residuals;
    Eigen::MatrixXd weights;
};





















#endif /* __VISO_PIVO_H__ */
