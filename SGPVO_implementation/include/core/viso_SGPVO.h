/*
Copyright 2016. All rights reserved.
Intelligent Control Systems Laboratory
Seoul National University, South Korea

This file is part of SGPVO with stereo vi sensor.
Authors: Pyojin Kim

SGPVO is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or any later version.

SGPVO is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
SGPVO; if not, write to the Free Software Foundation, Inc., 51 Franklin
Street, Fifth Floor, Boston, MA 02110-1301, USA
*/

#ifndef __VISO_SGPVO_H__
#define __VISO_SGPVO_H__

#include "core/libviso2.h"
#include "core/communication.h"
#include "core/vi_sensor_interface.hpp"
#include "core/intrinsic_matrix.h"
#include "core/weight_calculation.h"
#include "core/least_squares.h"
#include "core/fileIO.h"
#include "core/Liegroup_operation.h"
#include "core/viso_PIVO.h"
#include "core/time.h"
#include <thread>



class VisualOdometrySGP {

public:

  // low pass filter parameters
  struct lowPassFilter {
    double tau;
    lowPassFilter () {
      tau = 0.05;
    }
  };

  // debug parameters
  struct debugFlags {
    bool print_current_position;
    bool print_current_image;
    debugFlags () {
      print_current_position = false;
      print_current_image = false;
    }
  };

  // general parameters
  struct parameters {
    VisualOdometrySGP::lowPassFilter LPF_setup;                 // low pass filter parameters
    VisualOdometrySGP::debugFlags debug_flags;                  // debug parameters
  };


  // constructor, takes as input a parameter structure:
  VisualOdometrySGP(parameters param) : param(param) {

    first_initialization_              = true;
    image_counter_                     = 0;
    dt_                                = 0;

    // initialize PIVO
    initParameters("ESM", &visoPIVO_.cam, &visoPIVO_.opts);
    genPyramidKmatrix(visoPIVO_.cam.K_pyramid, visoPIVO_.cam.K, visoPIVO_.opts);


    // initialize rigid body motion SE(3) variables
    T_vo_esti_ = Eigen::Matrix4d::Identity(4,4);
    T_vo_ini_ = Eigen::Matrix4d::Identity(4,4);
    T_libviso2_ = Eigen::Matrix4d::Identity(4,4);
    Tr_total_ = Eigen::Matrix4d::Identity(4,4);
    stateEsti_.resize(6, 10000);
    stateEsti_.fill(0);


    // initialize illumination change model variables
    for(int patchIdx = 0; patchIdx < visoPIVO_.opts.numWinTotal; ++patchIdx) {
      illParamPIVO_ini_.patch[patchIdx].contrast = 1.0f;
      illParamPIVO_ini_.patch[patchIdx].bias = 0.0f;
      illParamPIVO_final_.patch[patchIdx].contrast = 1.0f;
      illParamPIVO_final_.patch[patchIdx].bias = 0.0f;
    }
  }


  // deconstructor
  ~VisualOdometrySGP() {
    std::cout << "close VisualOdometrySGP." << std::endl;
  }




  // run visual odometry with SGPVO thread
  std::thread runThread(communicateHostComputer *communicator) {
    return std::thread([=] { process(communicator); });
  }


  // process visual odometry with SGPVO algorithm with VI sensor.
  void process(communicateHostComputer *communicator) {

    // initialize libviso2
    VisualOdometryStereo::parameters param_libviso2;
    param_libviso2.calib.f  = visoPIVO_.cam.K(0,0);      // focal length in pixels
    param_libviso2.calib.cu = visoPIVO_.cam.K(0,2);      // principal point (u-coordinate) in pixels
    param_libviso2.calib.cv = visoPIVO_.cam.K(1,2);      // principal point (v-coordinate) in pixels
    param_libviso2.base     = visoPIVO_.cam.baseline;    // baseline in meters
    VisualOdometryStereo viso(param_libviso2);


    // initialize variables
    Eigen::Vector3d center_position = Eigen::MatrixXd::Zero(3,1);
    Eigen::Vector3d euler_angle = Eigen::MatrixXd::Zero(3,1);
    int imgIdx = 2, keyIdx = 1;


    // initialize time variables
    auto init_time_us = std::chrono::high_resolution_clock::now();
    auto prev_time_us = std::chrono::high_resolution_clock::now();
    auto curr_time_us = std::chrono::high_resolution_clock::now();


    // initialize vi sensor
    ViSensorInterface visensor(25, 200);
    visensor.run();
    std::cout << "#########################################################" << std::endl;
    std::cout << "VI sensor start." << std::endl;
    usleep(2000000);


    // do stereo visual odometry
    while(visensor.vi_sensor_connected_) {
      usleep(10);

      if(visensor.image_rect_flag_) {
        /**********************************************
        *           keyframe initialization
        ***********************************************/
        if ( (EuclideanDist(stateEsti_, keyIdx, (imgIdx-1))) >= 0.3 || EulerAngleDist(T_vo_esti_, ((4) * 3.14/180)) || first_initialization_ ) {
          // select keyframe index
          keyIdx = imgIdx - 1;
          std::cout << ":::::::::::::::::::::::::::::::::::::::::::::::::" << std::endl;
          std::cout << ":::::::::::::Keyframe Initialization:::::::::::::" << std::endl;
          std::cout << ":::::::::::::::::::::::::::::::::::::::::::::::::" << std::endl;


          // first initialization
          if (first_initialization_) {
            // read left and right rectified images from vi sensor
            vi_cam0_image_rect_ = visensor.left_image_rect_;
            vi_cam1_image_rect_ = visensor.right_image_rect_;
            visensor.image_rect_flag_ = 0;

            // libviso2 initialization
            visualOdometryStereoMex(vi_cam0_image_rect_, vi_cam1_image_rect_, viso, T_libviso2_);

            // PIVO initialization
            visoPIVO_.initialize();
            visoPIVO_.initializeImageframe(vi_cam0_image_rect_, vi_cam1_image_rect_);
            visoPIVO_.initializeKeyframe();
            visoPIVO_.initializeVelocity();


            // initialize the model parameters (for Motion and Multi-Patch illumination Changes)
            T_vo_ini_ = Eigen::Matrix4d::Identity(4,4);
            for(int patchIdx = 0; patchIdx < visoPIVO_.opts.numWinTotal; ++patchIdx) {
              illParamPIVO_ini_.patch[patchIdx].contrast = 1.0f;
              illParamPIVO_ini_.patch[patchIdx].bias = 0.0f;
              illParamPIVO_final_.patch[patchIdx].contrast = 1.0f;
              illParamPIVO_final_.patch[patchIdx].bias = 0.0f;
            }


            // skip the next part
            first_initialization_ = false;
            continue;
          }
          else {
            // read left and right rectified images from previous image frame
            vi_cam0_image_rect_ = visoPIVO_.getPreviousLeftImage();
            vi_cam1_image_rect_ = visoPIVO_.getPreviousRightImage();


            // libviso2 initialization
            visualOdometryStereoMex(vi_cam0_image_rect_, vi_cam1_image_rect_, viso, T_libviso2_);

            // PIVO initialization
            visoPIVO_.initialize();
            visoPIVO_.initializeKeyframe();


            // initialize the model parameters (for Motion and Multi-Patch illumination Changes)
            T_vo_ini_ = Eigen::Matrix4d::Identity(4,4);
            for(int patchIdx = 0; patchIdx < visoPIVO_.opts.numWinTotal; ++patchIdx) {
              illParamPIVO_ini_.patch[patchIdx].contrast = 1.0f;
              illParamPIVO_ini_.patch[patchIdx].bias = 0.0f;
              illParamPIVO_final_.patch[patchIdx].contrast = 1.0f;
              illParamPIVO_final_.patch[patchIdx].bias = 0.0f;
            }
          }
        }
        /**********************************************
        *    motion estimation (libviso2 -> PIVO)
        ***********************************************/

        // read left and right rectified images from vi sensor
        vi_cam0_image_rect_ = visensor.left_image_rect_;
        vi_cam1_image_rect_ = visensor.right_image_rect_;
        visensor.image_rect_flag_ = 0;


        // libviso2 estimation
        visualOdometryStereoMex(vi_cam0_image_rect_, vi_cam1_image_rect_, viso, T_libviso2_);
        T_vo_ini_ = T_libviso2_ * T_vo_ini_;


        // PIVO estimation
        visoPIVO_.initializeImageframe(vi_cam0_image_rect_, vi_cam1_image_rect_);
        visoPIVO_.updateMotion(T_vo_ini_, illParamPIVO_ini_, T_vo_esti_, illParamPIVO_final_);
        illParamPIVO_ini_ = illParamPIVO_final_;

        /**********************************************
        *       update current pose T_gc_current
        ***********************************************/

        // update stateEsti_ variable and calculate T_gc_current
        updateStateEstiData(T_vo_esti_, keyIdx, imgIdx, Tr_total_, stateEsti_);


        // inertial and body frame compensation (GPS or VICON)
        std::string current_inertial_frame = "VICON"; // or VICON
        Eigen::Matrix4d Tr_total_final = Eigen::Matrix4d::Identity(4,4);
        Tr_total_final = this->transformCoordinate(current_inertial_frame);


        // calculate current 6 DoF camera pose
        this->computeTimeDifference(curr_time_us, prev_time_us);
        center_position = Tr_total_final.block(0, 3, 3, 1);
        rotmtx2angle(Tr_total_final.block(0, 0, 3, 3).inverse(), euler_angle);


        // WiFi commuincation with server
        communicator->updateCamPose(center_position, euler_angle);
        printf("update camera 6 DoF pose : in %7.5lf seconds / in %05d image counter \n", dt_, image_counter_);


        if (this->param.debug_flags.print_current_position) {
          // display current libviso2 status
          double num_matches = viso.getNumberOfMatches();
          double num_inliers = viso.getNumberOfInliers();
          printf("Frame: %04d, Matches: %04d, Inliers: %4.1lf %% \n", imgIdx, (int)num_matches, (100.0*num_inliers/num_matches));

          // display current position and orientation
          printf("X : %7.3lf \n", center_position(0));
          printf("Y : %7.3lf \n", center_position(1));
          printf("Z : %7.3lf \n", center_position(2));
          printf("roll : %7.3lf \n", euler_angle(0)*(180/3.14));
          printf("pitch : %7.3lf \n", euler_angle(1)*(180/3.14));
          printf("yaw : %7.3lf \n", euler_angle(2)*(180/3.14));
          printf("Processing time [sec]: %7.5lf / Image counter: %05d \n", dt_, image_counter_);
        }


        if (this->param.debug_flags.print_current_image) {
          // declare trivial variables to display current image
          Eigen::Vector3d x_axis_end_c, x_axis_end_g;
          Eigen::Vector3d y_axis_end_c, y_axis_end_g;
          Eigen::Vector3d z_axis_end_c, z_axis_end_g;
          x_axis_end_c << 0.5, 0, 0;
          y_axis_end_c << 0, 0.5, 0;
          z_axis_end_c << 0, 0, 0.5;
          x_axis_end_g.fill(0);
          y_axis_end_g.fill(0);
          z_axis_end_g.fill(0);

          // display the current trajectory (x-z plane) and (y-z plane)
          cv::Mat traj_xz = cv::Mat::zeros(800, 800, CV_8UC3);
          cv::Mat traj_yz = cv::Mat::zeros(800, 800, CV_8UC3);
          x_axis_end_g = Tr_total_.block(0, 0, 3, 3) * x_axis_end_c + center_position;
          y_axis_end_g = Tr_total_.block(0, 0, 3, 3) * y_axis_end_c + center_position;
          z_axis_end_g = Tr_total_.block(0, 0, 3, 3) * z_axis_end_c + center_position;

          line(traj_xz, cv::Point((center_position(0)*100) + 400, -(center_position(2)*100) + 400),
          cv::Point((x_axis_end_g(0)*100) + 400, -(x_axis_end_g(2)*100) + 400), CV_RGB(255,0,0), 3);
          line(traj_xz, cv::Point((center_position(0)*100) + 400, -(center_position(2)*100) + 400),
          cv::Point((z_axis_end_g(0)*100) + 400, -(z_axis_end_g(2)*100) + 400), CV_RGB(0,0,255), 3);
          cv::circle(traj_xz, cv::Point((center_position(0)*100) + 400, -(center_position(2)*100) + 400), 2, CV_RGB(255,255,255), 2);
          cv::imshow("Estimated Trajectory (x-z plane)", traj_xz);

          line(traj_yz, cv::Point((center_position(2)*100) + 400, (center_position(1)*100) + 400),
          cv::Point((y_axis_end_g(2)*100) + 400, (y_axis_end_g(1)*100) + 400), CV_RGB(0,255,0), 3);
          line(traj_yz, cv::Point((center_position(2)*100) + 400, (center_position(1)*100) + 400),
          cv::Point((z_axis_end_g(2)*100) + 400, (z_axis_end_g(1)*100) + 400), CV_RGB(0,0,255), 3);
          cv::circle(traj_yz, cv::Point((center_position(2)*100) + 400, (center_position(1)*100) + 400), 2, CV_RGB(255,255,255), 2);
          cv::imshow("Estimated Trajectory (y-z plane)", traj_yz);


          // display the current image frames
          cv::imshow("vi_cam0_image_rect_", vi_cam0_image_rect_);
          cv::imshow("vi_cam1_image_rect_", vi_cam1_image_rect_);
          cv::waitKey(1);
        }


        // for next step
        imgIdx += 1;
        image_counter_ += 1;
        if (0) {
          break;
        }
      }
    }
  }


  // transform coordinate to proper test environments
  Eigen::Matrix4d transformCoordinate(const std::string& current_inertial_frame) {

    // GPS coordinate transformation
    if ( !strcmp("GPS", current_inertial_frame.c_str()) ) {
      Eigen::Vector3d euler_angle_GPS;
      euler_angle_GPS.fill(0);
      euler_angle_GPS(0) = -110 * (3.14/180);   // rotation along X axis from inertial to body
      euler_angle_GPS(1) = 0 * (3.14/180);      // rotation along Y axis from inertial to body
      euler_angle_GPS(2) = 0 * (3.14/180);      // rotation along Z axis from inertial to body

      Eigen::Matrix3d temp, rotation_matrix_GPS;
      temp.fill(0);
      rotation_matrix_GPS.fill(0);
      angle2rotmtx(euler_angle_GPS, temp);
      rotation_matrix_GPS = temp.inverse();

      Eigen::Matrix4d transformation_GPS = Eigen::Matrix4d::Identity(4,4);
      transformation_GPS(0,0) = rotation_matrix_GPS(0,0);
      transformation_GPS(0,1) = rotation_matrix_GPS(0,1);
      transformation_GPS(0,2) = rotation_matrix_GPS(0,2);

      transformation_GPS(1,0) = rotation_matrix_GPS(1,0);
      transformation_GPS(1,1) = rotation_matrix_GPS(1,1);
      transformation_GPS(1,2) = rotation_matrix_GPS(1,2);

      transformation_GPS(2,0) = rotation_matrix_GPS(2,0);
      transformation_GPS(2,1) = rotation_matrix_GPS(2,1);
      transformation_GPS(2,2) = rotation_matrix_GPS(2,2);

      Eigen::Vector3d euler_angle_BODY;
      euler_angle_BODY.fill(0);
      euler_angle_BODY(0) = -110 * (3.14/180);   //  rotation along X axis from inertial to body
      euler_angle_BODY(1) = 0 * (3.14/180);      //  rotation along Y axis from inertial to body
      euler_angle_BODY(2) = 0 * (3.14/180);    //  rotation along Z axis from inertial to body

      Eigen::Matrix3d rotation_matrix_BODY;
      rotation_matrix_BODY.fill(0);
      angle2rotmtx(euler_angle_BODY, rotation_matrix_BODY);

      Eigen::Matrix4d transformation_BODY = Eigen::Matrix4d::Identity(4,4);
      transformation_BODY(0,0) = rotation_matrix_BODY(0,0);
      transformation_BODY(0,1) = rotation_matrix_BODY(0,1);
      transformation_BODY(0,2) = rotation_matrix_BODY(0,2);

      transformation_BODY(1,0) = rotation_matrix_BODY(1,0);
      transformation_BODY(1,1) = rotation_matrix_BODY(1,1);
      transformation_BODY(1,2) = rotation_matrix_BODY(1,2);

      transformation_BODY(2,0) = rotation_matrix_BODY(2,0);
      transformation_BODY(2,1) = rotation_matrix_BODY(2,1);
      transformation_BODY(2,2) = rotation_matrix_BODY(2,2);

      return transformation_GPS * Tr_total_ * transformation_BODY;
    }
    else if ( !strcmp("VICON", current_inertial_frame.c_str()) ) {   // VICON coordinate transformation
      Eigen::Vector3d euler_angle_VICON;
      euler_angle_VICON.fill(0);
      euler_angle_VICON(0) = -110 * (3.14/180);   //  rotation along X axis from inertial to body
      euler_angle_VICON(1) = 0 * (3.14/180);      //  rotation along Y axis from inertial to body
      euler_angle_VICON(2) = -90 * (3.14/180);    //  rotation along Z axis from inertial to body

      Eigen::Matrix3d temp, rotation_matrix_VICON;
      temp.fill(0);
      rotation_matrix_VICON.fill(0);
      angle2rotmtx(euler_angle_VICON, temp);
      rotation_matrix_VICON = temp.inverse();

      Eigen::Matrix4d transformation_VICON = Eigen::Matrix4d::Identity(4,4);
      transformation_VICON(0,0) = rotation_matrix_VICON(0,0);
      transformation_VICON(0,1) = rotation_matrix_VICON(0,1);
      transformation_VICON(0,2) = rotation_matrix_VICON(0,2);

      transformation_VICON(1,0) = rotation_matrix_VICON(1,0);
      transformation_VICON(1,1) = rotation_matrix_VICON(1,1);
      transformation_VICON(1,2) = rotation_matrix_VICON(1,2);

      transformation_VICON(2,0) = rotation_matrix_VICON(2,0);
      transformation_VICON(2,1) = rotation_matrix_VICON(2,1);
      transformation_VICON(2,2) = rotation_matrix_VICON(2,2);

      Eigen::Vector3d euler_angle_BODY;
      euler_angle_BODY.fill(0);
      euler_angle_BODY(0) = -110 * (3.14/180);   //  rotation along X axis from inertial to body
      euler_angle_BODY(1) = 0 * (3.14/180);      //  rotation along Y axis from inertial to body
      euler_angle_BODY(2) = -90 * (3.14/180);    //  rotation along Z axis from inertial to body

      Eigen::Matrix3d rotation_matrix_BODY;
      rotation_matrix_BODY.fill(0);
      angle2rotmtx(euler_angle_BODY, rotation_matrix_BODY);

      Eigen::Matrix4d transformation_BODY = Eigen::Matrix4d::Identity(4,4);
      transformation_BODY(0,0) = rotation_matrix_BODY(0,0);
      transformation_BODY(0,1) = rotation_matrix_BODY(0,1);
      transformation_BODY(0,2) = rotation_matrix_BODY(0,2);

      transformation_BODY(1,0) = rotation_matrix_BODY(1,0);
      transformation_BODY(1,1) = rotation_matrix_BODY(1,1);
      transformation_BODY(1,2) = rotation_matrix_BODY(1,2);

      transformation_BODY(2,0) = rotation_matrix_BODY(2,0);
      transformation_BODY(2,1) = rotation_matrix_BODY(2,1);
      transformation_BODY(2,2) = rotation_matrix_BODY(2,2);

      return transformation_VICON * Tr_total_ * transformation_BODY;
    }
  }


  // compute time difference (dt_ : seconds)
  void computeTimeDifference(auto& curr_time_us, auto& prev_time_us) {
    curr_time_us = std::chrono::high_resolution_clock::now();
  	auto ticks = std::chrono::duration_cast<std::chrono::microseconds>(curr_time_us - prev_time_us);
    unsigned long diff_ticks = (unsigned long)ticks.count();
    dt_ = (double)diff_ticks * 1e-6;

    // save for next iteration
    prev_time_us = curr_time_us;
  }


  // get time difference (dt_ : seconds)
  double getTimeDifference() const {
    return this->dt_;
  }




private:

  // common parameters
  parameters param;
  bool first_initialization_;
  int image_counter_;
  double dt_;


  // PIVO variables
  VisualOdometryPIVO visoPIVO_;


  // image processing variables
  cv::Mat vi_cam0_image_rect_;
  cv::Mat vi_cam1_image_rect_;


  // rigid body motion SE(3) variables
  Eigen::Matrix4d T_vo_esti_;
  Eigen::Matrix4d T_vo_ini_;
  Eigen::Matrix4d T_libviso2_;
  Eigen::Matrix4d Tr_total_;
  Eigen::MatrixXd stateEsti_;


  // illumination change model variables
  illParam_perImg illParamPIVO_ini_;
  illParam_perImg illParamPIVO_final_;
};

#endif // __VISO_SGPVO_H__
