#ifndef _COMMON_H_
#define _COMMON_H_

#include <toadlet/egg.h>

enum LogFlags
{
	LOG_FLAG_OTHER			= 0,
	LOG_FLAG_OBSV_UPDATE	= 1 << 0,

};

enum CommID
{
	COMM_PING = 0,
	COMM_POS,         // 6 : x y z phi theta psi
	COMM_VEL,         // 6 : u v w p q r
	COMM_CLEAR_BUFFER,
	COMM_CLIENT_EXIT,
	COMM_HOST_EXIT,
	COMM_ACTIVATED, //
	COMM_DEACTIVATED,

  COMM_TO_ORIGIN,
	COMM_RELEASING_WALL,
	COMM_PUSHING_WALL
};

class Packet
{
	public:
	int32_t size, type;
	uint64_t time;
	toadlet::egg::Collection<int32_t> dataInt32;
	toadlet::egg::Collection<float> dataFloat;
	toadlet::egg::Collection<bool> dataBool;

	/*!
	 * \param serial Object to store the serialized packet information in
	 */
	void serialize(toadlet::egg::Collection<toadlet::tbyte> &serial)
	{
		size = sizeof(int32_t); // size
		size += sizeof(int32_t); // type
		size += sizeof(uint64_t); //time
		size += sizeof(int32_t); // number of int32
		size += dataInt32.size()*sizeof(int32_t); // ints
		size += sizeof(int32_t); // number of floats
		size += dataFloat.size()*sizeof(float); // floats
		size += sizeof(int32_t); // number of bools
		size += dataBool.size()*sizeof(bool); // bools
		serial.resize(size);
		toadlet::tbyte *p = &serial.front();

		memcpy(p, &size, sizeof(int32_t)); p += sizeof(int32_t);
		memcpy(p, &type, sizeof(int32_t)); p += sizeof(int32_t);
		memcpy(p, &time, sizeof(uint64_t)); p += sizeof(uint64_t);

		int32_t nInt32 = dataInt32.size();
		memcpy(p, &nInt32, sizeof(int32_t)); p += sizeof(int32_t);
		memcpy(p, &dataInt32.front(), dataInt32.size()*sizeof(int32_t)); p += dataInt32.size()*sizeof(int32_t);

		int32_t nFloat = dataFloat.size();
		memcpy(p, &nFloat, sizeof(int32_t)); p += sizeof(int32_t);
		memcpy(p, &dataFloat.front(), dataFloat.size()*sizeof(float)); p += dataFloat.size()*sizeof(float);

		int32_t nBool= dataBool.size();
		memcpy(p, &nBool, sizeof(int32_t)); p += sizeof(int32_t);
		memcpy(p, &dataBool.front(), dataBool.size()*sizeof(bool)); p += dataBool.size()*sizeof(bool);
	}

	void deserialize(const toadlet::egg::Collection<toadlet::tbyte> &serial)
	{
		const toadlet::tbyte *p = &serial.front();
		memcpy(&size, p, sizeof(int32_t)); p += sizeof(int32_t);
		memcpy(&type, p, sizeof(int32_t)); p += sizeof(int32_t);
		memcpy(&time, p, sizeof(uint64_t)); p += sizeof(uint64_t);

		int32_t nInt32;
		memcpy(&nInt32, p, sizeof(int32_t)); p += sizeof(int32_t);
		dataInt32.resize(nInt32);
		memcpy(&dataInt32.front(), p, dataInt32.size()*sizeof(int32_t)); p += dataInt32.size()*sizeof(int32_t);

		int32_t nFloat;
		memcpy(&nFloat, p, sizeof(int32_t)); p += sizeof(int32_t);
		dataFloat.resize(nFloat);
		memcpy(&dataFloat.front(), p, dataFloat.size()*sizeof(float)); p += dataFloat.size()*sizeof(float);

		int32_t nBool;
		memcpy(&nBool, p, sizeof(int32_t)); p += sizeof(int32_t);
		dataBool.resize(nBool);
		memcpy(&dataBool.front(), p, dataBool.size()*sizeof(bool)); p += dataBool.size()*sizeof(bool);
	}
};

#endif /* _COMMON_H_ */
