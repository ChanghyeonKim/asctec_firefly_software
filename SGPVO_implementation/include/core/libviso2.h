#ifndef __LIBVISO2_H__
#define __LIBVISO2_H__


#include "core/filter.h"
#include "core/matcher.h"
#include "core/matrix.h"
#include "core/reconstruction.h"
#include "core/timer.h"
#include "core/triangle.h"
#include "core/viso.h"
#include "core/viso_mono.h"
#include "core/viso_stereo.h"

#include <eigen3/Eigen/Dense>
#include <opencv2/opencv.hpp>


void visualOdometryStereoMex(const cv::Mat& imLeft, const cv::Mat& imRight, VisualOdometryStereo& libviso2, Eigen::Matrix4d& T_21);


































#endif /* __LIBVISO2_H__ */
