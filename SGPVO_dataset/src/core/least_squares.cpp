#include "core/least_squares.h"
#include "core/rgbd_image.h"
#include "core/weight_calculation.h"



void computeJ_I(const cv::Mat& I1dx, const cv::Mat& I1dy, const cv::Mat& I2dx, const cv::Mat& I2dy, const Eigen::MatrixXd& pixelPtsRef, const Eigen::MatrixXd& pixelPtsNext, const size_t& pixelIdx,
                Eigen::RowVector2d& J_I)
{
    double J_I_x = 0, J_I_y=0;

    // 1-based pixel coordinates (u,v)
    int uRef = (int) pixelPtsRef((1-1),(pixelIdx-1));
    int vRef = (int) pixelPtsRef((2-1),(pixelIdx-1));
    int uNext = (int) pixelPtsNext((1-1),(pixelIdx-1));
    int vNext = (int) pixelPtsNext((2-1),(pixelIdx-1));


    J_I_x = (( I1dx.at<double>((vRef-1),(uRef-1)) + I2dx.at<double>((vNext-1),(uNext-1)) ) * 0.5f);
    J_I_y = (( I1dy.at<double>((vRef-1),(uRef-1)) + I2dy.at<double>((vNext-1),(uNext-1)) ) * 0.5f);

    J_I(0) = J_I_x;
    J_I(1) = J_I_y;

    // for debug
    //cout << I1dx.at<double>((vRef-1),(uRef-1)) << endl;
    //cout << I2dx.at<double>((vNext-1),(uNext-1)) << endl;
    //cout << I1dy.at<double>((vRef-1),(uRef-1)) << endl;
    //cout << I2dy.at<double>((vNext-1),(uNext-1)) << endl;
    //cout << J_I_x << " " << J_I_y << endl;
}



void computeJ_w(const Eigen::MatrixXd& ptsCloudRef, const Eigen::MatrixXd& ptsCloudNext, const Eigen::Matrix4d& inputT, const Eigen::Matrix3d& inputKmatrix, const optsPIVO& opts, const size_t& pixelIdx,
                Eigen::MatrixXd& J_w)
{
    double fx = inputKmatrix((1-1),(1-1));
    double fy = inputKmatrix((2-1),(2-1));

    double X = ptsCloudRef((1-1),(pixelIdx-1));
    double Y = ptsCloudRef((2-1),(pixelIdx-1));
    double Z = ptsCloudRef((3-1),(pixelIdx-1));

    double Xprm = ptsCloudNext((1-1),(pixelIdx-1));
    double Yprm = ptsCloudNext((2-1),(pixelIdx-1));
    double Zprm = ptsCloudNext((3-1),(pixelIdx-1));

    double r11 = inputT((1-1),(1-1));
    double r21 = inputT((2-1),(1-1));
    double r31 = inputT((3-1),(1-1));
    double r12 = inputT((1-1),(2-1));
    double r22 = inputT((2-1),(2-1));
    double r32 = inputT((3-1),(2-1));
    double r13 = inputT((1-1),(3-1));
    double r23 = inputT((2-1),(3-1));
    double r33 = inputT((3-1),(3-1));

    double tx = inputT((1-1),(4-1));
    double ty = inputT((2-1),(4-1));
    double tz = inputT((3-1),(4-1));

    double inputVector[20] = {fx,fy,X,Y,Z,Xprm,Yprm,Zprm,r11,r21,r31,r12,r22,r32,r13,r23,r33,tx,ty,tz};
    double J_w_Vector[12] = {0};

    J_wForFA(inputVector, J_w_Vector);

    // assign J_w matrix used in Forward Additive, Efficient Second-order Minimization (ESM) method
    J_w((1-1),(1-1)) = J_w_Vector[0];
    J_w((2-1),(1-1)) = J_w_Vector[1];
    J_w((1-1),(2-1)) = J_w_Vector[2];
    J_w((2-1),(2-1)) = J_w_Vector[3];
    J_w((1-1),(3-1)) = J_w_Vector[4];
    J_w((2-1),(3-1)) = J_w_Vector[5];
    J_w((1-1),(4-1)) = J_w_Vector[6];
    J_w((2-1),(4-1)) = J_w_Vector[7];
    J_w((1-1),(5-1)) = J_w_Vector[8];
    J_w((2-1),(5-1)) = J_w_Vector[9];
    J_w((1-1),(6-1)) = J_w_Vector[10];
    J_w((2-1),(6-1)) = J_w_Vector[11];


    // for debug
    //cout << "J_w_Vector" << endl;
    //cout << J_w_Vector[0] << endl;
    //cout << J_w_Vector[1] << endl;
    //cout << J_w_Vector[2] << endl;
    //cout << J_w_Vector[3] << endl;
    //cout << J_w_Vector[4] << endl;
    //cout << J_w_Vector[5] << endl;
    //cout << J_w_Vector[6] << endl;
    //cout << J_w_Vector[7] << endl;
    //cout << J_w_Vector[8] << endl;
    //cout << J_w_Vector[9] << endl;
    //cout << J_w_Vector[10] << endl;
    //cout << J_w_Vector[11] << endl;
    //usleep(10000000);
}



void J_wForFA(const double* in1, double* J_w)
{
    double t2;
    double t3;
    double x[12];

    /*     This function was generated by the Symbolic Math Toolbox version 6.0. */
    /*     30-Jan-2015 15:25:34 */

    t2 = 1.0 / (in1[7] * in1[7]);
    t3 = 1.0 / in1[7];
    x[0] = in1[0] * t3;
    x[1] = 0.0;
    x[2] = 0.0;
    x[3] = in1[1] * t3;
    x[4] = -in1[5] * in1[0] * t2;
    x[5] = -in1[6] * in1[1] * t2;
    x[6] = ((-in1[5] * in1[0] * t2 * in1[18] - in1[2] * in1[5] * in1[0] * in1[9] *
             t2) - in1[5] * in1[3] * in1[0] * in1[12] * t2) - in1[5] * in1[4] *
           in1[0] * in1[15] * t2;
    x[7] = ((((((-in1[1] * t3 * in1[19] - in1[2] * in1[1] * in1[10] * t3) - in1[3]
                * in1[1] * in1[13] * t3) - in1[4] * in1[1] * in1[16] * t3) - in1[6]
              * in1[1] * t2 * in1[18]) - in1[2] * in1[6] * in1[1] * in1[9] * t2) -
            in1[3] * in1[6] * in1[1] * in1[12] * t2) - in1[6] * in1[4] * in1[1] *
           in1[15] * t2;
    x[8] = ((((((in1[0] * t3 * in1[19] + in1[2] * in1[0] * in1[10] * t3) + in1[3] *
                in1[0] * in1[13] * t3) + in1[4] * in1[0] * in1[16] * t3) + in1[5] *
              in1[0] * t2 * in1[17]) + in1[2] * in1[5] * in1[0] * in1[8] * t2) +
            in1[5] * in1[3] * in1[0] * in1[11] * t2) + in1[5] * in1[4] * in1[0] *
           in1[14] * t2;
    x[9] = ((in1[6] * in1[1] * t2 * in1[17] + in1[2] * in1[6] * in1[1] * in1[8] *
             t2) + in1[3] * in1[6] * in1[1] * in1[11] * t2) + in1[6] * in1[4] *
           in1[1] * in1[14] * t2;
    x[10] = ((-in1[0] * t3 * in1[18] - in1[2] * in1[0] * in1[9] * t3) - in1[3] *
             in1[0] * in1[12] * t3) - in1[4] * in1[0] * in1[15] * t3;
    x[11] = ((in1[1] * t3 * in1[17] + in1[2] * in1[1] * in1[8] * t3) + in1[3] *
             in1[1] * in1[11] * t3) + in1[4] * in1[1] * in1[14] * t3;
    memcpy(&J_w[0], &x[0], 12U * sizeof(double));
}



void computeJacobianResidual(const cv::Mat& I1RefImgPyr, const Eigen::MatrixXd& inputKeyPtsPyr, const cv::Mat& I2RefImgPyr,
                             const Eigen::MatrixXd& pixelPtsRef, const Eigen::MatrixXd& ptsCloudRef, const Eigen::RowVectorXi& patchIdxRef, const Eigen::MatrixXd& pixelPtsNext, const Eigen::MatrixXd& ptsCloudNext,
                             const Eigen::Matrix4d& inputT, const illParam_perImg& inputIllParam, const Eigen::Matrix3d& inputKmatrixPyr, const double& winSize, const optsPIVO& opts,
                             Eigen::MatrixXd& J_tot, Eigen::MatrixXd& r_tot)
{
    // initialize variables and parameters
    size_t pixelIdx = 0;
    int uNext = 0;
    int vNext = 0;
    int uRef = 0;
    int vRef = 0;

    double I2intensity = 0;
    double I2compensated = 0;
    double I1intensity = 0;
    int patchIdx = 0;

    Eigen::RowVector2d J_I;
    Eigen::MatrixXd J_w(2,6);
    Eigen::MatrixXd J_I_w(1,6);

    J_I.fill(0);
    J_w.fill(0);
    J_I_w.fill(0);

    cv::Mat I1dx, I1dy, I2dx, I2dy;

    // compute gradient image I1dx and I1dy
    calcDerivX(I1RefImgPyr, I1dx);
    calcDerivY(I1RefImgPyr, I1dy);

    // compute gradient image I2dx and I2dy
    calcDerivX(I2RefImgPyr, I2dx);
    calcDerivY(I2RefImgPyr, I2dy);

    // for debug
    //saveMatImage("I1dx.txt", I1dx);
    //usleep(10000000);


    // generate Jacobian matrix, residual vector of patch images with ESM method
    for(pixelIdx = 1; pixelIdx <= pixelPtsNext.cols(); ++pixelIdx) // ( 1-based pixel index )
    {
        if (pixelPtsNext((3-1),(pixelIdx-1)) != 0) // I2 valid pixel
        {
            // calculate J_I jacobian matrix for checking ( 1-based pixel coordinates (u,v) )
            computeJ_I(I1dx, I1dy, I2dx, I2dy, pixelPtsRef, pixelPtsNext, pixelIdx, J_I);

            if ((std::abs(J_I(1-1)) <= opts.eps) && (std::abs(J_I(2-1)) <= opts.eps)) // check for J_I is valid or not
            {
                continue;
            }
            else
            {
                // calculate I1 & I2 intensity value for checking ( 1-based pixel coordinates (u,v) )
                uRef = (int) pixelPtsRef((1-1),(pixelIdx-1));
                vRef = (int) pixelPtsRef((2-1),(pixelIdx-1));

                uNext = (int) pixelPtsNext((1-1),(pixelIdx-1));
                vNext = (int) pixelPtsNext((2-1),(pixelIdx-1));

                if ( I1RefImgPyr.at<double>((vRef-1),(uRef-1)) <= 5.0f || I1RefImgPyr.at<double>((vRef-1),(uRef-1)) >= 250.0f             // check for I1 intensity value is valid or not
                        || I2RefImgPyr.at<double>((vNext-1),(uNext-1)) <= 5.0f || I2RefImgPyr.at<double>((vNext-1),(uNext-1)) >= 250.0f ) // check for I2 intensity value is valid or not
                {
                    continue;
                }
                else
                {
                    // calculate and assign residual vector elements ( 1-based pixel coordinates (u,v) )
                    I2intensity = I2RefImgPyr.at<double>((vNext-1),(uNext-1));

                    patchIdx = (int) patchIdxRef(pixelIdx-1);
                    I2compensated = inputIllParam.patch[patchIdx-1].contrast * I2intensity + inputIllParam.patch[patchIdx-1].bias;
                    I1intensity = I1RefImgPyr.at<double>((vRef-1),(uRef-1));

                    r_tot(pixelIdx-1) = I2compensated - I1intensity;


                    // calculate and assign Jacobian matrix elements
                    computeJ_w(ptsCloudRef, ptsCloudNext, inputT, inputKmatrixPyr, opts, pixelIdx, J_w);

                    for (size_t J_wIdx = 0; J_wIdx < J_tot.cols() ; ++J_wIdx)
                    {
                        J_tot((pixelIdx-1),J_wIdx) = 0;
                    }

                    J_I_w = inputIllParam.patch[patchIdx-1].contrast * (J_I * J_w);

                    J_tot((pixelIdx-1),(1-1)) = J_I_w(0,1-1);
                    J_tot((pixelIdx-1),(2-1)) = J_I_w(0,2-1);
                    J_tot((pixelIdx-1),(3-1)) = J_I_w(0,3-1);
                    J_tot((pixelIdx-1),(4-1)) = J_I_w(0,4-1);
                    J_tot((pixelIdx-1),(5-1)) = J_I_w(0,5-1);
                    J_tot((pixelIdx-1),(6-1)) = J_I_w(0,6-1);

                    J_tot((pixelIdx-1),(2*patchIdx+5-1)) = I2intensity;
                    J_tot((pixelIdx-1),(2*patchIdx+6-1)) = 1;


                    // for debug
                    //cout << J_w << endl;
                    //cout << J_tot.row((pixelIdx-1)) << endl;
                    //cout << J_tot.row((pixelIdx)) << endl;
                    //usleep(10000000);
                }
            }

            // for debug
            //cout << J_I << endl;
            //cout << J_I.rows() << " " << J_I.cols() << endl;
            //usleep(10000000);
        }
    }
}



void computeJRW(const cv::Mat& I1RefImgPyr, const cv::Mat& D1RefImgPyr, const Eigen::MatrixXd& inputKeyPtsPyr, const cv::Mat& I2RefImgPyr,
                const Eigen::Matrix4d& inputT, const illParam_perImg& inputIllParam, const Eigen::Matrix3d& inputKmatrixPyr, const double& winSize, const optsPIVO& opts,
                Eigen::MatrixXd& J_tot, Eigen::MatrixXd& r_tot, Eigen::MatrixXd& W_tot)
{
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////  warpPoints ///////////////////////////
    /////////////////////////////////////////////////////////////////////


    // reference frame values
    Eigen::MatrixXd pixelPtsRef(3,(opts.numWinTotal * (size_t)winSize * (size_t)winSize));
    Eigen::MatrixXd ptsCloudRef(4,(opts.numWinTotal * (size_t)winSize * (size_t)winSize));
    Eigen::RowVectorXi patchIdxRef(1,(opts.numWinTotal * (size_t)winSize * (size_t)winSize));

    // next frame values
    Eigen::MatrixXd pixelPtsNext(3,(opts.numWinTotal * (size_t)winSize * (size_t)winSize));
    Eigen::MatrixXd ptsCloudNext(4,(opts.numWinTotal * (size_t)winSize * (size_t)winSize));

    // initialize
    pixelPtsRef.fill(0);
    ptsCloudRef.fill(0);
    patchIdxRef.fill(0);
    pixelPtsNext.fill(0);
    ptsCloudNext.fill(0);

    warpPoints(D1RefImgPyr, inputKeyPtsPyr, inputT, inputKmatrixPyr, winSize, opts, pixelPtsRef, ptsCloudRef, patchIdxRef, pixelPtsNext, ptsCloudNext);


    // for debug
    //saveEigenMatrix("pixelPtsRef.txt", pixelPtsRef);
    //saveEigenMatrix("ptsCloudRef.txt", ptsCloudRef);
    //saveEigenMatrix("pixelPtsNext.txt", pixelPtsNext);
    //saveEigenMatrix("ptsCloudNext.txt", ptsCloudNext);
    //usleep(10000000);


    /////////////////////////////////////////////////////////////////////
    ///////////////////////  computeJacobianResidual ////////////////////
    /////////////////////////////////////////////////////////////////////


    // initialize Jacobian matrix and residual vector
    J_tot.fill(-1000);
    r_tot.fill(-1000);

    computeJacobianResidual(I1RefImgPyr, inputKeyPtsPyr, I2RefImgPyr, pixelPtsRef, ptsCloudRef, patchIdxRef, pixelPtsNext, ptsCloudNext,
                            inputT, inputIllParam, inputKmatrixPyr, winSize, opts, J_tot, r_tot);


    // for debug
    //saveEigenMatrix("J_tot.txt",J_tot);
    //saveEigenMatrix("r_tot.txt",r_tot);
    //usleep(10000000);


    /////////////////////////////////////////////////////////////////////
    ///////////////////////////  computeWeight //////////////////////////
    /////////////////////////////////////////////////////////////////////


    // initialize weighting vector and scale
    W_tot.fill(0);
    double estimatedScale = 0;

    computeWeight(r_tot, opts, estimatedScale, W_tot);

    // for debug
    //cout << estimatedScale << endl;
    //saveEigenMatrix("W_tot.txt",W_tot);
    //usleep(10000000);
}



void calcCostFtn(const Eigen::MatrixXd& W_tot, const Eigen::MatrixXd& r_tot, double& meanCostValue)
{
    // initialize variables
    size_t idx = 0;
    double num = 0.0f;
    double costSum = 0.0f;


    // calculate cost (energy) function
    for(idx = 0; idx < r_tot.rows(); ++idx)
    {
        if(r_tot(idx) != -1000)
        {
            num += 1.0f;
            costSum += (W_tot(idx) * r_tot(idx) * r_tot(idx));
        }
    }

    // mean cost value
    meanCostValue = (costSum / num);

    // for debug
    //cout << meanCostValue << endl;
    //cout << num << endl;
    //usleep(10000000);
}

////////////////////////////////////ops

void calcDeltaZ(const Eigen::MatrixXd& J_tot, const Eigen::MatrixXd& r_tot, const Eigen::MatrixXd& W_tot, const optsPIVO& opts, const std::vector<int>& invalidPatchIdx,
                Eigen::MatrixXd& deltaZ)
{
    if (invalidPatchIdx.size() == 0)
    {
        // initialize variables
        deltaZ.fill(0);
        Eigen::MatrixXd HessianMatrix((6 + 2 * opts.numWinTotal),(6 + 2 * opts.numWinTotal));
        HessianMatrix.fill(0);
        Eigen::MatrixXd b;
        Eigen::MatrixXd temp(J_tot.cols(), J_tot.rows());
        temp.fill(0);

        for (int i = 0; i < J_tot.rows(); i++)
        {
          if (J_tot(i,0) != -1000)
          {
            temp.col(i) = (J_tot.row(i) * W_tot(i,0)).transpose();
          }
        }

        HessianMatrix = temp * J_tot;

        // approximate Hessian matrix ( 2nd derivative )
        //HessianMatrix = J_tot.transpose() * (W_tot * oneRow).cwiseProduct(J_tot);

        /* much slower than above method.
        for(size_t idx = 0; idx < W_tot.rows(); ++idx)
        {
            HessianMatrix += ( J_tot.row(idx).transpose() * W_tot(idx) * J_tot.row(idx) );
        }
        */

        // calculate b in Ax = b eqn.
        b = - J_tot.transpose() * W_tot.cwiseProduct(r_tot);

        // calculate delta z
        deltaZ = HessianMatrix.lu().solve(b);

        // for debug
        //saveEigenMatrix("HessianMatrix.txt",HessianMatrix);
        //saveEigenMatrix("b.txt",b);
        //cout <<  deltaZ << endl;
        //usleep(10000000);
    }
    else
    {
        // if there exists invalid patch....
        int numValidPatch = opts.numWinTotal - invalidPatchIdx.size();
        int patchIdx = 0;
        Eigen::MatrixXd J_tot_valid = J_tot;

        // remove the invalid patch information (along column index)
        for(size_t i = invalidPatchIdx.size(); i > 0; --i)
        {
            // assign invalid patch
            patchIdx = invalidPatchIdx[i-1]; // (1-based patch index (patchIdx) in invalidPatchIdx vector)

            // remove columns in J_tot_valid
            removeColumn(J_tot_valid,2*patchIdx+6 - 1);
            removeColumn(J_tot_valid,2*patchIdx+5 - 1);
        }

        // for debug
        //saveEigenMatrix("J_tot.txt",J_tot);
        //saveEigenMatrix("J_tot_valid.txt",J_tot_valid);
        //usleep(10000000);


        // initialize variables
        deltaZ.fill(0);
        Eigen::MatrixXd HessianMatrix((6 + 2*numValidPatch),(6 + 2*numValidPatch));
        HessianMatrix.fill(0);
        Eigen::MatrixXd b;
        Eigen::MatrixXd temp(J_tot_valid.cols(), J_tot_valid.rows());
        temp.fill(0);

        for (int i = 0; i < J_tot_valid.rows(); i++)
        {
          if (J_tot_valid(i,0) != -1000)
          {
            temp.col(i) = (J_tot_valid.row(i) * W_tot(i,0)).transpose();
          }
        }

        HessianMatrix = temp * J_tot_valid;

        // calculate b in Ax = b eqn.
        b = - J_tot_valid.transpose() * W_tot.cwiseProduct(r_tot);

        // calculate temp_deltaZ (reduced deltaZ)
        Eigen::MatrixXd temp_deltaZ = HessianMatrix.lu().solve(b);


        // convert temp_deltaZ (only with valid patches) to deltaZ (with all patches)
        size_t invalidCnt = 0;
        size_t j = 0;
        for(size_t i = 1; i <= deltaZ.rows(); ++i)
        {
            // check the invalid patch index
            invalidCnt = 0;
            for(size_t k = 1; k <= invalidPatchIdx.size(); ++k)
            {
                // assign invalid patch
                patchIdx = invalidPatchIdx[k-1]; // (1-based patch index (patchIdx) in invalidPatchIdx vector)
                if ( i == (2*patchIdx+5) || i == (2*patchIdx+6) )
                {
                    invalidCnt += 1;
                }
            }

            // skip the invalid patch index
            if (invalidCnt > 0)
            {
                continue;
            }

            // assign valid patch values
            j += 1;
            deltaZ(i-1) = temp_deltaZ(j-1);
        }

        // for debug
        //std::cout << deltaZ << std::endl;
        //usleep(10000000);
    }
}



void getInvalidPatchIdx(const Eigen::MatrixXd& J_tot, std::vector<int>& invalidPatchIdx, const optsPIVO& opts)
{
    // initialize variables
    invalidPatchIdx.clear();
    Eigen::RowVectorXd numPixelInEachPatch(1,opts.numWinTotal);
    numPixelInEachPatch.fill(0);

    // count the number of pixels in each patch
    size_t squareWinSize = (double)J_tot.rows() / opts.numWinTotal;
    size_t sumNumValidPixel = 0;
    double minNumPixelInEachPatch = ceil(0.2 * (double)squareWinSize);

    for(size_t patchIdx = 1; patchIdx <= opts.numWinTotal; ++patchIdx) // (1-based patch index (patchIdx))
    {
        // initialize as zero
        sumNumValidPixel = 0;

        // count the valid number of pixels in each patch
        for(size_t pixelIdx = 0; pixelIdx < squareWinSize; ++pixelIdx)
        {
            if ( J_tot((squareWinSize*(patchIdx-1)+1 + pixelIdx - 1),(2*patchIdx+6 - 1)) != -1000 )
            {
                sumNumValidPixel += 1;
            }
        }

        // save the valid number of pixels in each patch
        numPixelInEachPatch(patchIdx-1) = sumNumValidPixel;
    }

    // extract the invalid patch index
    for(int patchIdx = 1; patchIdx <= opts.numWinTotal; ++patchIdx) // (1-based patch index (patchIdx))
    {
        if( numPixelInEachPatch(patchIdx-1) <= minNumPixelInEachPatch )
        {
            invalidPatchIdx.push_back(patchIdx);
        }
    }

    // for debug
    //std::cout << squareWinSize << std::endl;
    //std::cout << numPixelInEachPatch << std::endl;
    //usleep(10000000);
}



void removeColumn(Eigen::MatrixXd& matrix, const int& colToRemove)
{
    // initialize variables
    int numRows = matrix.rows();
    int numCols = matrix.cols()-1;

    // Note : colToRemove is 0-based column index
    if( colToRemove < numCols )
    {
        matrix.block(0,colToRemove,numRows,numCols-colToRemove) = matrix.block(0,colToRemove+1,numRows,numCols-colToRemove);
    }

    matrix.conservativeResize(numRows,numCols);
}
