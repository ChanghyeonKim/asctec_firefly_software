#include <cstdio>
#include <fstream>
#include <opencv2/highgui/highgui_c.h>
#include "core/fileIO.h"


void getImgInKITTIdataset(const std::string& leftImgFileName, const std::string& rightImgFileName, const camCalibrationParam& cam, cv::Mat& outputGreyImg, cv::Mat& outputDepthImg)
{
    // declare Mat variables
    cv::Mat grey_64F, depth_64F;


    // read left gray image
    cv::Mat imLeft, imRight;
    imLeft = cv::imread(leftImgFileName, 0);      // read a grayscale image
    imRight = cv::imread(rightImgFileName, 0);    // read a grayscale image

    // for debug
    //cv::imshow("imLeft view",imLeft);
    //cv::waitKey(0);
    //std::cout << imLeft.cols << std::endl;
    //std::cout << imLeft.rows << std::endl;
    //std::cout << (imLeft.type() == CV_8UC1) << std::endl;
    //usleep(1000000);


    // fit the pyramid size (32 times)
    cv::Mat targetImage;
    cv::Rect roi(0, 0, 32*38, 32*11);  // roi(uStart,vStart,uLength,vLength)
    targetImage = imLeft(roi);

    // for debug
    //cv::imshow("targetImage view",targetImage);
    //cv::waitKey(0);
    //std::cout << targetImage.cols << std::endl;
    //std::cout << targetImage.rows << std::endl;
    //std::cout << (targetImage.type() == CV_8UC1) << std::endl;
    //usleep(10000000);


    // transform from integer to double
    targetImage.convertTo(grey_64F, CV_64FC1); // grey_64F.type() is CV_64FC1 (double)

    // for debug
    //std::cout << grey_64F.cols << std::endl;
    //std::cout << grey_64F.rows << std::endl;
    //std::cout << grey_64F.at<double>(17,33)<< std::endl;
    //std::cout << (grey_64F.type() == CV_64FC1) << std::endl;
    //usleep(10000000);




    // perform stereo matching
    cv::Mat disLeftTranspose(imLeft.cols, imLeft.rows, CV_64FC1);
    imwrite("tempLeftImg.pgm", imLeft);
    imwrite("tempRightImg.pgm", imRight);
    process("tempLeftImg.pgm", "tempRightImg.pgm", disLeftTranspose);

    cv::Mat disLeft;
    cv::transpose(disLeftTranspose, disLeft);

    // for debug
    //std::cout << disLeft.cols << std::endl;
    //std::cout << disLeft.rows << std::endl;
    //std::cout << disLeft.at<double>(114,575) << std::endl;
    //std::cout << (disLeft.type() == CV_64FC1) << std::endl;
    //usleep(10000000);


    // extract depth map (Z) from disparity map
    cv::Mat imgZ = disLeft;
    for(int32_t v = 0; v < disLeft.rows; v++)
    {
        for(int32_t u = 0; u < disLeft.cols; u++)
        {
            if (disLeft.at<double>(v,u) > 0)
            {
                imgZ.at<double>(v,u) = (1/disLeft.at<double>(v,u));
            }
            else
            {
                imgZ.at<double>(v,u) = 0;
            }
        }
    }
    imgZ = cam.K(0,0) * cam.baseline * imgZ;


    // remove depth value over 50 m (unreliable value)
    for(int32_t v = 0; v < imgZ.rows; v++)
    {
        for(int32_t u = 0; u < imgZ.cols; u++)
        {
            if (imgZ.at<double>(v,u) > 50)
            {
                imgZ.at<double>(v,u) = 0;
            }
        }
    }

    // for debug
    //std::cout << imgZ.cols << std::endl;
    //std::cout << imgZ.rows << std::endl;
    //std::cout << imgZ.at<double>(86,516) << std::endl;
    //std::cout << (imgZ.type() == CV_64FC1) << std::endl;
    //usleep(10000000);


    // fit the pyramid size (32 times)
    cv::Mat targetDepthImage;
    targetDepthImage = imgZ(roi);


    // transform from double to double
    targetDepthImage.convertTo(depth_64F, CV_64FC1); // depth_64F.type() is CV_64FC1 (double)

    // for debug
    //std::cout << depth_64F.cols << std::endl;
    //std::cout << depth_64F.rows << std::endl;
    //std::cout << depth_64F.at<double>(38,517) << std::endl;
    //std::cout << (depth_64F.type() == CV_64FC1) << std::endl;
    //usleep(10000000);


    // return the results
    outputGreyImg = grey_64F;
    outputDepthImg = depth_64F;
}



void getImgInICSLdataset(const std::string& leftImgFileName, const std::string& rightImgFileName, const camCalibrationParam& cam, cv::Mat& outputGreyImg, cv::Mat& outputDepthImg)
{
    // declare Mat variables
    cv::Mat grey_64F, depth_64F;


    // read left gray image
    cv::Mat imLeft, imRight;
    imLeft = cv::imread(leftImgFileName, 0);      // read a grayscale image
    imRight = cv::imread(rightImgFileName, 0);    // read a grayscale image

    // for debug
    //cv::imshow("imLeft view",imLeft);
    //cv::waitKey(0);
    //std::cout << imLeft.cols << std::endl;
    //std::cout << imLeft.rows << std::endl;
    //std::cout << (imLeft.type() == CV_8UC1) << std::endl;
    //usleep(1000000);


    // rectify left and right image
    int image_width = imLeft.cols;
    int image_height = imLeft.rows;
    double c0[9];
    double d0[5];
    double r0[9];
    double p0[12];
    double rot0[9];
    double t0[3];
    double c1[9];
    double d1[5];
    double r1[9];
    double p1[12];
    double rot1[9];
    double t1[3];
    double r[9];
    double t[3];

    d0[0] = -0.293253358441277;
    d0[1] = 0.0880652209769358;
    d0[2] = 0.0;
    d0[3] = 0.0;
    d0[4] = 0.0;
    c0[0] = 474.287260117510;
    c0[1] = 0.0;
    c0[2] = 393.321665748649;
    c0[3] = 0.0;
    c0[4] = 475.020204688627;
    c0[5] = 255.547400714026;
    c0[6] = 0.0;
    c0[7] = 0.0;
    c0[8] = 1.0;
    d1[0] = -0.288619119686433;
    d1[1] = 0.0827149217964409;
    d1[2] = 0.0;
    d1[3] = 0.0;
    d1[4] = 0.0;
    c1[0] = 473.726344788349;
    c1[1] = 0.0;
    c1[2] = 356.080702251162;
    c1[3] = 0.0;
    c1[4] = 474.789206409847;
    c1[5] = 260.755592898289;
    c1[6] = 0.0;
    c1[7] = 0.0;
    c1[8] = 1.0;

    double r_temp[9];


    //cv::Mat wrapped(rows, cols, CV_32FC1, external_mem, CV_AUTOSTEP);
    cv::Mat C0(3, 3, CV_64FC1, c0, 3 * sizeof(double));
    cv::Mat D0(5, 1, CV_64FC1, d0, 1 * sizeof(double));
    cv::Mat R0(3, 3, CV_64FC1, r0, 3 * sizeof(double));
    cv::Mat P0(3, 4, CV_64FC1, p0, 4 * sizeof(double));

    cv::Mat C1(3, 3, CV_64FC1, c1, 3 * sizeof(double));
    cv::Mat D1(5, 1, CV_64FC1, d1, 1 * sizeof(double));
    cv::Mat R1(3, 3, CV_64FC1, r1, 3 * sizeof(double));
    cv::Mat P1(3, 4, CV_64FC1, p1, 4 * sizeof(double));

    cv::Mat R(3, 3, CV_64FC1, r_temp, 3 * sizeof(double));
    R.at<double>(0,0) = 0.999846198179162;
    R.at<double>(0,1) = 0.00584058576763007;
    R.at<double>(0,2) = 0.0165368541315247;
    R.at<double>(1,0) = -0.00584002447244078;
    R.at<double>(1,1) = 0.999982943530453;
    R.at<double>(1,2) = -8.22334044186012e-05;
    R.at<double>(2,0) = -0.0165370523624273;
    R.at<double>(2,1) = -1.43548760540450e-05;
    R.at<double>(2,2) = 0.999863253496747;

    cv::Mat T(3, 1, CV_64FC1, t, 1 * sizeof(double));
    T.at<double>(0,0) = 0.109239986268189;
    T.at<double>(1,0) = 0.000101962420640836;
    T.at<double>(2,0) = 0.00140221850095609;

    cv::Size img_size(image_width, image_height);

    cv::Rect roi1, roi2;
    cv::Mat Q;

    // for debug
    //std::cout << "C0 : " << C0 << std::endl;
    //std::cout << "D0 : " << D0 << std::endl;
    //std::cout << "C1 : " << C1 << std::endl;
    //std::cout << "D1 : " << D1 << std::endl;
    //std::cout << "R : " << R << std::endl;
    //std::cout << "T : " << T << std::endl;
    //std::cout << "img_size.width : " << img_size.width << std::endl;
    //std::cout << "img_size.height : " << img_size.height << std::endl;
    //usleep(1000000);

    cv::stereoRectify(C0, D0, C1, D1, img_size, R, T, R0, R1, P0, P1, Q, cv::CALIB_ZERO_DISPARITY, 0,
                      img_size, &roi1, &roi2);

    cv::Mat map00_, map01_, map10_, map11_;
    cv::initUndistortRectifyMap(C0, D0, R0, P0, img_size, CV_16SC2, map00_, map01_);
    cv::initUndistortRectifyMap(C1, D1, R1, P1, img_size, CV_16SC2, map10_, map11_);

    //std::cout << "img_size.width : " << img_size.width << std::endl;
    //std::cout << "img_size.height : " << img_size.height << std::endl;

    cv::Mat img0rect, img1rect;
    cv::remap(imLeft, img0rect, map00_, map01_, cv::INTER_LINEAR);
    cv::remap(imRight, img1rect, map10_, map11_, cv::INTER_LINEAR);

    // for debug
    //for( int j = 0; j < img0rect.rows; j += 16 )
    //            line(img0rect, cv::Point(0, j), cv::Point(img0rect.cols, j), cv::Scalar(0, 255, 0), 1, 8);
    //cv::imshow("img0rect view", img0rect);
    //for( int j = 0; j < img1rect.rows; j += 16 )
    //            line(img1rect, cv::Point(0, j), cv::Point(img1rect.cols, j), cv::Scalar(0, 255, 0), 1, 8);
    //cv::imshow("img1rect view",img1rect);
    //cv::waitKey(0);
    //std::cout << img0rect.cols << std::endl;
    //std::cout << img0rect.rows << std::endl;
    //std::cout << (img0rect.type() == CV_8UC1) << std::endl;
    //usleep(1000000);


    // fit the pyramid size (32 times)
    cv::Mat targetImage;
    cv::Rect roi(0, 0, 32*23, 32*15);  // roi(uStart,vStart,uLength,vLength)
    targetImage = img0rect(roi);

    // for debug
    //cv::imshow("targetImage view",targetImage);
    //cv::waitKey(0);
    //std::cout << targetImage.cols << std::endl;
    //std::cout << targetImage.rows << std::endl;
    //std::cout << (targetImage.type() == CV_8UC1) << std::endl;
    //usleep(10000000);


    // transform from integer to double
    targetImage.convertTo(grey_64F, CV_64FC1); // grey_64F.type() is CV_64FC1 (double)

    // for debug
    //std::cout << grey_64F.cols << std::endl;
    //std::cout << grey_64F.rows << std::endl;
    //std::cout << grey_64F.at<double>(17,33)<< std::endl;
    //std::cout << (grey_64F.type() == CV_64FC1) << std::endl;
    //usleep(10000000);


    // perform stereo matching
    cv::Mat disLeftTranspose(img0rect.cols, img0rect.rows, CV_64FC1);
    imwrite("tempLeftImg.pgm", img0rect);
    imwrite("tempRightImg.pgm", img1rect);
    process("tempLeftImg.pgm", "tempRightImg.pgm", disLeftTranspose);

    cv::Mat disLeft;
    cv::transpose(disLeftTranspose, disLeft);

    // for debug
    //std::cout << disLeft.cols << std::endl;
    //std::cout << disLeft.rows << std::endl;
    //std::cout << disLeft.at<double>(114,575) << std::endl;
    //std::cout << (disLeft.type() == CV_64FC1) << std::endl;
    //usleep(10000000);


    // extract depth map (Z) from disparity map
    cv::Mat imgZ = disLeft;
    for(int32_t v = 0; v < disLeft.rows; v++)
    {
        for(int32_t u = 0; u < disLeft.cols; u++)
        {
            if (disLeft.at<double>(v,u) > 0)
            {
                imgZ.at<double>(v,u) = (1/disLeft.at<double>(v,u));
            }
            else
            {
                imgZ.at<double>(v,u) = 0;
            }
        }
    }
    imgZ = cam.K(0,0) * cam.baseline * imgZ;


    // remove depth value over 50 m (unreliable value)
    for(int32_t v = 0; v < imgZ.rows; v++)
    {
        for(int32_t u = 0; u < imgZ.cols; u++)
        {
            if (imgZ.at<double>(v,u) > 50)
            {
                imgZ.at<double>(v,u) = 0;
            }
        }
    }

    // for debug
    //std::cout << imgZ.cols << std::endl;
    //std::cout << imgZ.rows << std::endl;
    //std::cout << imgZ.at<double>(86,516) << std::endl;
    //std::cout << (imgZ.type() == CV_64FC1) << std::endl;
    //usleep(10000000);


    // fit the pyramid size (32 times)
    cv::Mat targetDepthImage;
    targetDepthImage = imgZ(roi);


    // transform from double to double
    targetDepthImage.convertTo(depth_64F, CV_64FC1); // depth_64F.type() is CV_64FC1 (double)

    // for debug
    //std::cout << depth_64F.cols << std::endl;
    //std::cout << depth_64F.rows << std::endl;
    //std::cout << depth_64F.at<double>(38,517) << std::endl;
    //std::cout << (depth_64F.type() == CV_64FC1) << std::endl;
    //usleep(10000000);


    // return the results
    outputGreyImg = grey_64F;
    outputDepthImg = depth_64F;
}



void getImgRectInICSLdataset(const std::string& leftImgFileName, const std::string& rightImgFileName, const camCalibrationParam& cam, cv::Mat& outputGreyImg, cv::Mat& outputLeftImgRect, cv::Mat& outputRightImgRect)
{
    // declare Mat variables
    cv::Mat grey_64F;


    // read left gray image
    cv::Mat imLeft, imRight;
    imLeft = cv::imread(leftImgFileName, 0);      // read a grayscale image
    imRight = cv::imread(rightImgFileName, 0);    // read a grayscale image

    // for debug
    //cv::imshow("imLeft view",imLeft);
    //cv::waitKey(0);
    //std::cout << imLeft.cols << std::endl;
    //std::cout << imLeft.rows << std::endl;
    //std::cout << (imLeft.type() == CV_8UC1) << std::endl;
    //usleep(1000000);


    // rectify left and right image
    int image_width = imLeft.cols;
    int image_height = imLeft.rows;
    double c0[9];
    double d0[5];
    double r0[9];
    double p0[12];
    double rot0[9];
    double t0[3];
    double c1[9];
    double d1[5];
    double r1[9];
    double p1[12];
    double rot1[9];
    double t1[3];
    double r[9];
    double t[3];

    d0[0] = -0.293253358441277;
    d0[1] = 0.0880652209769358;
    d0[2] = 0.0;
    d0[3] = 0.0;
    d0[4] = 0.0;
    c0[0] = 474.287260117510;
    c0[1] = 0.0;
    c0[2] = 393.321665748649;
    c0[3] = 0.0;
    c0[4] = 475.020204688627;
    c0[5] = 255.547400714026;
    c0[6] = 0.0;
    c0[7] = 0.0;
    c0[8] = 1.0;
    d1[0] = -0.288619119686433;
    d1[1] = 0.0827149217964409;
    d1[2] = 0.0;
    d1[3] = 0.0;
    d1[4] = 0.0;
    c1[0] = 473.726344788349;
    c1[1] = 0.0;
    c1[2] = 356.080702251162;
    c1[3] = 0.0;
    c1[4] = 474.789206409847;
    c1[5] = 260.755592898289;
    c1[6] = 0.0;
    c1[7] = 0.0;
    c1[8] = 1.0;

    double r_temp[9];


    //cv::Mat wrapped(rows, cols, CV_32FC1, external_mem, CV_AUTOSTEP);
    cv::Mat C0(3, 3, CV_64FC1, c0, 3 * sizeof(double));
    cv::Mat D0(5, 1, CV_64FC1, d0, 1 * sizeof(double));
    cv::Mat R0(3, 3, CV_64FC1, r0, 3 * sizeof(double));
    cv::Mat P0(3, 4, CV_64FC1, p0, 4 * sizeof(double));

    cv::Mat C1(3, 3, CV_64FC1, c1, 3 * sizeof(double));
    cv::Mat D1(5, 1, CV_64FC1, d1, 1 * sizeof(double));
    cv::Mat R1(3, 3, CV_64FC1, r1, 3 * sizeof(double));
    cv::Mat P1(3, 4, CV_64FC1, p1, 4 * sizeof(double));

    cv::Mat R(3, 3, CV_64FC1, r_temp, 3 * sizeof(double));
    R.at<double>(0,0) = 0.999846198179162;
    R.at<double>(0,1) = 0.00584058576763007;
    R.at<double>(0,2) = 0.0165368541315247;
    R.at<double>(1,0) = -0.00584002447244078;
    R.at<double>(1,1) = 0.999982943530453;
    R.at<double>(1,2) = -8.22334044186012e-05;
    R.at<double>(2,0) = -0.0165370523624273;
    R.at<double>(2,1) = -1.43548760540450e-05;
    R.at<double>(2,2) = 0.999863253496747;

    cv::Mat T(3, 1, CV_64FC1, t, 1 * sizeof(double));
    T.at<double>(0,0) = 0.109239986268189;
    T.at<double>(1,0) = 0.000101962420640836;
    T.at<double>(2,0) = 0.00140221850095609;

    cv::Size img_size(image_width, image_height);

    cv::Rect roi1, roi2;
    cv::Mat Q;

    // for debug
    //std::cout << "C0 : " << C0 << std::endl;
    //std::cout << "D0 : " << D0 << std::endl;
    //std::cout << "C1 : " << C1 << std::endl;
    //std::cout << "D1 : " << D1 << std::endl;
    //std::cout << "R : " << R << std::endl;
    //std::cout << "T : " << T << std::endl;
    //std::cout << "img_size.width : " << img_size.width << std::endl;
    //std::cout << "img_size.height : " << img_size.height << std::endl;
    //usleep(1000000);

    cv::stereoRectify(C0, D0, C1, D1, img_size, R, T, R0, R1, P0, P1, Q, cv::CALIB_ZERO_DISPARITY, 0,
                      img_size, &roi1, &roi2);

    cv::Mat map00_, map01_, map10_, map11_;
    cv::initUndistortRectifyMap(C0, D0, R0, P0, img_size, CV_16SC2, map00_, map01_);
    cv::initUndistortRectifyMap(C1, D1, R1, P1, img_size, CV_16SC2, map10_, map11_);

    //std::cout << "img_size.width : " << img_size.width << std::endl;
    //std::cout << "img_size.height : " << img_size.height << std::endl;

    cv::Mat img0rect, img1rect;
    cv::remap(imLeft, img0rect, map00_, map01_, cv::INTER_LINEAR);
    cv::remap(imRight, img1rect, map10_, map11_, cv::INTER_LINEAR);

    // for debug
    //for( int j = 0; j < img0rect.rows; j += 16 )
    //            line(img0rect, cv::Point(0, j), cv::Point(img0rect.cols, j), cv::Scalar(0, 255, 0), 1, 8);
    //cv::imshow("img0rect view", img0rect);
    //for( int j = 0; j < img1rect.rows; j += 16 )
    //            line(img1rect, cv::Point(0, j), cv::Point(img1rect.cols, j), cv::Scalar(0, 255, 0), 1, 8);
    //cv::imshow("img1rect view",img1rect);
    //cv::waitKey(0);
    //std::cout << img0rect.cols << std::endl;
    //std::cout << img0rect.rows << std::endl;
    //std::cout << (img0rect.type() == CV_8UC1) << std::endl;
    //usleep(1000000);


    // fit the pyramid size (32 times)
    cv::Mat targetImage;
    cv::Rect roi(0, 0, 32*23, 32*15);  // roi(uStart,vStart,uLength,vLength)
    targetImage = img0rect(roi);

    // for debug
    //cv::imshow("targetImage view",targetImage);
    //cv::waitKey(0);
    //std::cout << targetImage.cols << std::endl;
    //std::cout << targetImage.rows << std::endl;
    //std::cout << (targetImage.type() == CV_8UC1) << std::endl;
    //usleep(10000000);


    // transform from integer to double
    targetImage.convertTo(grey_64F, CV_64FC1); // grey_64F.type() is CV_64FC1 (double)

    // for debug
    //std::cout << grey_64F.cols << std::endl;
    //std::cout << grey_64F.rows << std::endl;
    //std::cout << grey_64F.at<double>(17,33)<< std::endl;
    //std::cout << (grey_64F.type() == CV_64FC1) << std::endl;
    //usleep(10000000);


    // return the results
    outputGreyImg = grey_64F;
    outputLeftImgRect = img0rect;
    outputRightImgRect = img1rect;
}



void saveEigenMatrix(const char *_Filename, const Eigen::MatrixXd& inputMatrix)
{
    // initialize text stream
    std::ofstream matrixTextFile_opener(_Filename);


    // save the inputMatrix
    if (matrixTextFile_opener.is_open())
    {
        matrixTextFile_opener << inputMatrix << '\n';
        matrixTextFile_opener.close();
        std::cout << "File save success in saveEigenMatrix!" << std::endl;
    }
    else
    {
        std::cout << "File open error in saveEigenMatrix!" << std::endl;
    }
}



void saveMatImage(const char *_Filename, const cv::Mat& inputImage)
{
    // initialize text stream
    std::ofstream matTextFile_opener(_Filename);


    // save the inputImage
    if (matTextFile_opener.is_open())
    {
        for(size_t i = 0; i < inputImage.rows; i++)
        {
            for(size_t j = 0; j < inputImage.cols; j++)
            {
                matTextFile_opener << inputImage.at<double>(i,j) << "\t";
            }
            matTextFile_opener << std::endl;
        }
        matTextFile_opener.close();
        std::cout << "File save success in saveMatImage!" << std::endl;
    }
    else
    {
        std::cout << "File open error in saveMatImage!" << std::endl;
    }
}



void saveSGPVOresults(const std::vector<Eigen::Matrix4d>& T_1i_Save, const Eigen::MatrixXd& stateEsti, const std::vector<illParam_perImg>& illParamPIVO_Save,
                      const std::vector<double>& errPIVO_Save, const std::vector<int>& keyIdx_Save, const optsPIVO& opts)
{
    // save the motion estimation results (T_1i_Save)
    Eigen::MatrixXd Tvector(16,T_1i_Save.size());
    Eigen::Matrix4d Tcurrent = Eigen::Matrix4d::Identity(4,4);

    for(size_t T_idx = 0; T_idx < T_1i_Save.size(); ++T_idx)
    {
        // assign current estimated transformation matrix : Tcurrent
        Tcurrent = T_1i_Save[T_idx];

        // vectorization from (Tcurrent) to (Tvector)
        Tvector(0,T_idx) = Tcurrent(0,0);
        Tvector(1,T_idx) = Tcurrent(1,0);
        Tvector(2,T_idx) = Tcurrent(2,0);
        Tvector(3,T_idx) = Tcurrent(3,0);

        Tvector(4,T_idx) = Tcurrent(0,1);
        Tvector(5,T_idx) = Tcurrent(1,1);
        Tvector(6,T_idx) = Tcurrent(2,1);
        Tvector(7,T_idx) = Tcurrent(3,1);

        Tvector(8,T_idx) = Tcurrent(0,2);
        Tvector(9,T_idx) = Tcurrent(1,2);
        Tvector(10,T_idx) = Tcurrent(2,2);
        Tvector(11,T_idx) = Tcurrent(3,2);

        Tvector(12,T_idx) = Tcurrent(0,3);
        Tvector(13,T_idx) = Tcurrent(1,3);
        Tvector(14,T_idx) = Tcurrent(2,3);
        Tvector(15,T_idx) = Tcurrent(3,3);
    }

    saveEigenMatrix("Tvector.txt",Tvector);



    // save the motion estimation results (stateEsti)
    saveEigenMatrix("stateEsti.txt",stateEsti);



    // save the light variation model parameters (illParamPIVO_Save)
    Eigen::MatrixXd illParameterPIVO((2*opts.numWinTotal),illParamPIVO_Save.size());

    for(size_t Ill_idx = 0; Ill_idx < illParamPIVO_Save.size(); ++Ill_idx)
    {

        for(size_t patchIdx = 0; patchIdx < opts.numWinTotal; ++patchIdx )
        {
            illParameterPIVO(( 2*patchIdx ),Ill_idx) = illParamPIVO_Save[Ill_idx].patch[patchIdx].contrast;
            illParameterPIVO((2*patchIdx+1),Ill_idx) = illParamPIVO_Save[Ill_idx].patch[patchIdx].bias;
        }

        //illParameterPIVO(0,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[0].contrast;
        //illParameterPIVO(1,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[0].bias;

        //illParameterPIVO(2,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[1].contrast;
        //illParameterPIVO(3,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[1].bias;

        //illParameterPIVO(4,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[2].contrast;
        //illParameterPIVO(5,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[2].bias;

        //illParameterPIVO(6,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[3].contrast;
        //illParameterPIVO(7,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[3].bias;

        //illParameterPIVO(8,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[4].contrast;
        //illParameterPIVO(9,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[4].bias;
    }

    saveEigenMatrix("illParameterPIVO.txt",illParameterPIVO);



    // save the error values from the proposed cost function (errPIVO_Save)
    Eigen::RowVectorXd errVector(errPIVO_Save.size());

    for(size_t err_idx = 0; err_idx < errPIVO_Save.size(); ++err_idx)
    {
        errVector(err_idx) = errPIVO_Save[err_idx];
    }

    saveEigenMatrix("errVector.txt",errVector);



    // save the keyframe index for debug
    Eigen::RowVectorXd keyIdxVector(keyIdx_Save.size());

    for(size_t key_idx = 0; key_idx < keyIdx_Save.size(); ++key_idx)
    {
        keyIdxVector(key_idx) = keyIdx_Save[key_idx];
    }

    saveEigenMatrix("keyIdxVector.txt",keyIdxVector);
}
