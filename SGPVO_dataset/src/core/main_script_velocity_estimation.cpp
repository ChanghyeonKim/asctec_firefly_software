#include "core/libelas.h"
#include "core/libviso2.h"
#include "core/datatypes.h"
#include "core/rgbd_image.h"
#include "core/intrinsic_matrix.h"
#include "core/weight_calculation.h"
#include "core/least_squares.h"
#include "core/fileIO.h"
#include "core/Liegroup_operation.h"
#include "core/dense_tracking.h"


#define PI 3.1416



double Rounding(double x, int digit);
void quiver(cv::Mat& result_view, std::vector<cv::Point2f>& pre_feature_points, std::vector<cv::Point2f>& feature_points);
bool InImage(cv::Point2f& test_point, cv::Mat& Reference_Image);



int main(void)
{
    /**********************************************
    *
    * PART 0:
    * load ICSL dataset's information.
    * define options for the SPGVO
    *
    ***********************************************/

    // set the absolute directory of the KITTI dataset and the number of stereo images in BaseDir.
    std::string BaseDir = "/home/pjinkim/Desktop/KITTIdataset/Residential/2011_09_30/2011_09_30_drive_0033_sync";
    int imInit = 0;
    int numImg = 594;


    // set camera calibration parameters MANUALLY
    camCalibrationParam cam;
    optsPIVO opts;
    initParameters("ESM", &cam, &opts);
    genPyramidKmatrix(cam.K_pyramid, cam.K, opts);


    // set camera calibration parameters for libviso2 (look at: viso_stereo.h)
    VisualOdometryStereo::parameters param;
    param.calib.f  = cam.K(0,0);    // focal length in pixels
    param.calib.cu = cam.K(0,2);    // principal point (u-coordinate) in pixels
    param.calib.cv = cam.K(1,2);    // principal point (v-coordinate) in pixels
    param.base     = cam.baseline;  // baseline in meters

    // for debug
    //std::cout << param.calib.f << std::endl;
    //std::cout << param.calib.cu << std::endl;
    //std::cout << param.calib.cv << std::endl;
    //std::cout << param.base << std::endl;
    //usleep(10000000);


    // init visual odometry (libviso2)
    VisualOdometryStereo viso(param);


    /**********************************************
    *
    * PART 1:
    * Main Part -> Calculate Transformation matrix
    *
    ***********************************************/

    // declare trivial variables for drawing
    cv::Mat traj = cv::Mat::zeros(800, 800, CV_8UC3);
    FLOAT tx, ty, tz;
    char text[100];
    int fontFace = cv::FONT_HERSHEY_PLAIN;
    double fontScale = 1;
    int thickness = 1;
    cv::Point textOrg(10, 50);


    // initialize variables
    std::vector<int> keyIdx_Save;
    std::vector<Eigen::Vector2f> keyPts_Save;

    std::vector<Eigen::Matrix4d> T_1i_Save;
    std::vector<illParam_perImg> illParamSGPVO_Save;
    std::vector<double> errSGPVO_Save;

    Eigen::MatrixXd stateEsti(6,numImg);
    stateEsti.fill(0);

    std::string leftImgFileName, rightImgFileName;
    cv::Mat vi_cam0_image_rect, vi_cam1_image_rect;
    cv::Mat tempLeftImg, tempRightImg;

    cv::Mat I1Ref, D1Ref;  // grey & depth image at keyframe
    std::vector<cv::Mat> I1RefPyr, D1RefPyr;
    Eigen::MatrixXd keyPts(2, opts.numWinTotal);
    std::vector<Eigen::MatrixXd> keyPtsPyr;

    cv::Mat I2Ref, D2Ref;  // grey & depth image at current frame
    std::vector<cv::Mat> I2RefPyr, D2RefPyr;
    int imgIdx = 2, keyIdx = 1;


    // initialize variables for velocity estimation
    cv::Mat pre_src_gray_Img;     // image at k-1
    cv::Mat src_gray_Img;         // image at k
    cv::Mat src_depth_Img;        // image at k
    cv::Mat result_view_Img;      // to show the result view

    std::vector<cv::Point2f> pre_feature_pts;

    double Img_timestep = 0.1;
    Eigen::MatrixXd velocityEsti(6, numImg);

		double fx, fy, cx, cy;
		fx = cam.K(0,0);
		fy = cam.K(0,0);
		cx = cam.K(0,2);
		cy = cam.K(1,2);
    bool first_initialization = true;


    // Parameters for goodFeaturesToTrack
    int maxCorners = 1000;
    double qualityLevel = 0.01;
    double minDistance = 10;
    int blockSize = 4;
    bool useHarrisDetector = false;
    double k = 0.04;


    // initialize the model parameters and temporary rigid body transformation matrices
    Eigen::Matrix4d T_vo_esti = Eigen::Matrix4d::Identity(4,4);
    Eigen::Matrix4d T_vo_ini = Eigen::Matrix4d::Identity(4,4);
    illParam_perImg illParamPIVO;
    for(int patchIdx = 0; patchIdx < opts.numWinTotal; ++patchIdx)
    {
        illParamPIVO.patch[patchIdx].contrast = 1.0f;
        illParamPIVO.patch[patchIdx].bias = 0.0f;
    }

    Eigen::Matrix4d T_temp = Eigen::Matrix4d::Identity(4,4);
    Eigen::Matrix4d T_output = Eigen::Matrix4d::Identity(4,4);


    for(imgIdx=2; imgIdx < (numImg+1); ++imgIdx)
    {
        if ( ((imgIdx - keyIdx) >= 5) || (EuclideanDist(stateEsti, keyIdx, (imgIdx-1))) >= 1 || (imgIdx <= 2) )
        {
            /**********************************************
            *
            * PART 1-1:
            * Keyframe initialization
            *
            ***********************************************/

            // select keyframe index
            keyIdx = imgIdx - 1;
            keyIdx_Save.push_back(keyIdx);


            // extract the gray and depth image at keyframe index [keyIdx-1]
            char base_name[256];
            sprintf(base_name,"/data/%010d.png",(imInit+keyIdx-1));
            leftImgFileName  = BaseDir + "/image_00" + base_name;
            rightImgFileName = BaseDir + "/image_01" + base_name;
            getImgInKITTIdataset(leftImgFileName, rightImgFileName, cam, I1Ref, D1Ref);

            // for debug
            //std::cout << leftImgFileName << std::endl;
            //std::cout << rightImgFileName << std::endl;
            //std::cout << I1Ref.rows << "/" << I1Ref.cols << std::endl;
            //std::cout << D1Ref.rows << "/" << D1Ref.cols << std::endl;
            //std::cout << I1Ref.at<double>(74,1183) << std::endl;
            //std::cout << D1Ref.at<double>(211,900) << std::endl;
            //usleep(10000000);


            // generate grey and depth image pyramid
            I1RefPyr.clear();
            D1RefPyr.clear();
            genPyramidImage(I1RefPyr, D1RefPyr, I1Ref, D1Ref, opts);


            // extract keypoints in grey image
            detectKeypoints(I1Ref, keyPts, opts); // 1-based pixel coordinates (u,v)
            keyPtsPyr.clear();
            genPyramidKeyPts(keyPtsPyr, keyPts, opts);

            // for debug
            //std::cout << keyPts << std::endl;
            //std::cout << keyPtsPyr[3] << std::endl;
            //usleep(10000000);

            // for debug
            //std::cout << I1RefPyr[1].size().width << std::endl;
            //std::cout << I1RefPyr[1].size().height << std::endl;
            //printf("%lf \n", I1RefPyr[1].at<double>(31,57));
            //std::cout << D1RefPyr[3].size().width << std::endl;
            //std::cout << D1RefPyr[3].size().height << std::endl;
            //printf("%2.11lf \n", D1RefPyr[3].at<double>(38,38));
            //usleep(100000000);

            //cv::Mat forDebugImg;
            //I1RefPyr[3].convertTo(forDebugImg,CV_8UC1);
            //imshow("camera view",forDebugImg);
            //cv::waitKey(0);
            //usleep(100000000);


            if (first_initialization)
            {
            // extract feature points in pre_src_gray_Img
            I1Ref.convertTo(pre_src_gray_Img, CV_8UC1);
            cv::goodFeaturesToTrack(pre_src_gray_Img, pre_feature_pts, maxCorners, qualityLevel, minDistance, cv::Mat(), blockSize, useHarrisDetector, k);
            cv::cornerSubPix(pre_src_gray_Img, pre_feature_pts, cv::Size(7,7), cv::Size(-1,-1), cvTermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03));
            first_initialization = false;

            // for debug
            //std::cout << pre_feature_pts.size() << std::endl;
            //usleep(10000000);
            }

            // initialize the model parameters ( for Motion and Multi-Patch illumination Changes )
            T_vo_ini = Eigen::Matrix4d::Identity(4,4);
            for(int patchIdx = 0; patchIdx < opts.numWinTotal; ++patchIdx)
            {
                illParamPIVO.patch[patchIdx].contrast = 1.0f;
                illParamPIVO.patch[patchIdx].bias = 0.0f;
            }

            // for debug
            //std::cout << T_vo_ini << std::endl;
            //for(int patchIdx = 0; patchIdx < opts.numWinTotal; ++patchIdx)
            //{
            //    std::cout <<"<<"<<patchIdx<<">>"<< std::endl;
            //    std::cout << illParamPIVO.patch[patchIdx].contrast << std::endl;
            //    std::cout << illParamPIVO.patch[patchIdx].bias << std::endl;
            //}
            //usleep(100000000);


            // read left and right images at [keyIdx-1]
            tempLeftImg = cv::imread(leftImgFileName, 0);    // read a grayscale image
            tempRightImg = cv::imread(rightImgFileName, 0);  // read a grayscale image
            visualOdometryStereoMex(tempLeftImg, tempRightImg, viso, T_temp);

            // for debug
            //std::cout << T_temp << std::endl;
            //usleep(100000000);
        }

        /**********************************************
        *
        * PART 2-1:
        * Get next image frame
        *
        ***********************************************/

        // extract the gray and depth image at imgIdx index [imgIdx-1]
        char base_name[256];
        sprintf(base_name,"/data/%010d.png",(imInit+imgIdx-1));
        leftImgFileName  = BaseDir + "/image_00" + base_name;
        rightImgFileName = BaseDir + "/image_01" + base_name;
        getImgInKITTIdataset(leftImgFileName, rightImgFileName, cam, I2Ref, D2Ref);

        // for debug
        //std::cout << leftImgFileName << std::endl;
        //std::cout << rightImgFileName << std::endl;
        //usleep(100000000);


        // generate grey and depth image pyramid
        I2RefPyr.clear();
        D2RefPyr.clear();
        genPyramidImage(I2RefPyr, D2RefPyr, I2Ref, D2Ref, opts);

        // for debug
        //std::cout << I2RefPyr[4].size() << std::endl;
        //usleep(100000000);


        /**********************************************
        *
        * PART 2-1:
        * calculate current velocity
        *
        ***********************************************/

        // parameters for calcOpticalFlowPyrLK
        std::vector<cv::Point2f> feature_pts;
        std::vector<uchar> status_pts;
        std::vector<float> error;
        int winsize = 11;
        int maxlevel = 5;
        int flags_R = 0;

    		// do optical flow with Pyramidal LK method
        I2Ref.convertTo(src_gray_Img, CV_8UC1);
        result_view_Img = src_gray_Img.clone();
        cv::calcOpticalFlowPyrLK(pre_src_gray_Img, src_gray_Img, pre_feature_pts, feature_pts, status_pts, error, cv::Size(winsize,winsize), maxlevel, cvTermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03), flags_R);
        flags_R |= CV_LKFLOW_PYR_A_READY;

        // extract the only valid feature points from pre_feature_pts and feature_pts
        std::vector<cv::Point2f> pre_valid_feature_pts;
        std::vector<cv::Point2f> valid_feature_pts;
        int Numvalidpts = 0;

        for(int i = 0; i < (int)feature_pts.size(); i++)
        {
        	if ( status_pts[i] != 0 )    // just skip if it is not valid point
        	{
        		pre_valid_feature_pts.push_back( pre_feature_pts[i] );
        		valid_feature_pts.push_back( feature_pts[i] );
        		Numvalidpts = Numvalidpts + 1;
        	}
        }


        // compute Homography based RANSAC
        std::vector<uchar> RANSAC_mask;
        cv::findHomography(pre_valid_feature_pts, valid_feature_pts, CV_RANSAC, 4, RANSAC_mask);

        // only save the inlier points
        std::vector<cv::Point2f> pre_valid_feature_pts_inlier;
        std::vector<cv::Point2f> valid_feature_pts_inlier;
        int Numvalidpts_inlier = 0;

        for (int i = 0; i < (int)valid_feature_pts.size(); i++)
        {
        	// Select only the inliers (mask entry set to 1)
        	if ((int)RANSAC_mask[i] == 1)
        	{
        		pre_valid_feature_pts_inlier.push_back( pre_valid_feature_pts[i] );
        		valid_feature_pts_inlier.push_back( valid_feature_pts[i] );
        		Numvalidpts_inlier += 1;
        	}
        }


        // calculate the number of valid depth pts ( select the point depth != 0  )
        src_depth_Img = D2Ref;
        int Numvalid_depth_pts = 0;
        double depth_test = 0;

        for(int i = 0; i < (int)valid_feature_pts_inlier.size(); i++)
        {
        	// round that pixel point
        	valid_feature_pts_inlier[i].x = (int)Rounding( valid_feature_pts_inlier[i].x , 0 );
        	valid_feature_pts_inlier[i].y = (int)Rounding( valid_feature_pts_inlier[i].y , 0 );

        	if (InImage(valid_feature_pts_inlier[i], src_depth_Img))
        	{
        		// extract the depth value at that point
        		depth_test = 0;
        		depth_test = src_depth_Img.at<double>(valid_feature_pts_inlier[i]);

        		if ( depth_test >= 0.1 )
        		{
        			Numvalid_depth_pts += 1;
        		}
        	}
        }

        // declare the Matrix variable with Eigen3
        Eigen::VectorXd optical_flow(2*Numvalid_depth_pts);
        Eigen::MatrixXd Img_Jacobian_mtx(2*Numvalid_depth_pts, 6);
        Eigen::VectorXd Estimated_velocity(6);
        Estimated_velocity << 0, 0, 0, 0, 0, 0;

        double depth_value = 0;
        double depth_value_meter = 0;
        int count = 0;

        // calculate the valid feature points only.
        for(int i = 0; i < (int)valid_feature_pts_inlier.size(); i++)
        {
        	if (InImage(valid_feature_pts_inlier[i], src_depth_Img))
        	{
        		// extract the depth value at that point
        		depth_value = 0;
        		depth_value = src_depth_Img.at<double>(valid_feature_pts_inlier[i]);

        		depth_value_meter = 0;
        		depth_value_meter = (double)depth_value;

        		if ( depth_value >= 0.1 )
        		{
        			// compute the optical flow vector ( 2n x 1 )
        			double xdot = 0;
        			double ydot = 0;
        			double pre_norm_feature_pts_x = 0, pre_norm_feature_pts_y = 0;
        			double norm_feature_pts_x = 0, norm_feature_pts_y = 0;

        			pre_norm_feature_pts_x = (pre_valid_feature_pts_inlier[i].x - cx)/fx;
        			pre_norm_feature_pts_y = (pre_valid_feature_pts_inlier[i].y - cy)/fy;

        			norm_feature_pts_x = (valid_feature_pts_inlier[i].x - cx)/fx;
        			norm_feature_pts_y = (valid_feature_pts_inlier[i].y - cy)/fy;

    					xdot = (norm_feature_pts_x - pre_norm_feature_pts_x) / Img_timestep;
    					ydot = (norm_feature_pts_y - pre_norm_feature_pts_y) / Img_timestep;

    					optical_flow(2*count) = xdot;
    					optical_flow(2*count+1) = ydot;

    					// compute Image Jacobian Matrix ( 2n x 6 )
    					Img_Jacobian_mtx(2*count,0) = -1/depth_value_meter;
    					Img_Jacobian_mtx(2*count,1) = 0;
    					Img_Jacobian_mtx(2*count,2) = norm_feature_pts_x/depth_value_meter;
    					Img_Jacobian_mtx(2*count,3) = norm_feature_pts_x*norm_feature_pts_y;
    					Img_Jacobian_mtx(2*count,4) = -( 1 + norm_feature_pts_x*norm_feature_pts_x );
    					Img_Jacobian_mtx(2*count,5) = norm_feature_pts_y;

    					Img_Jacobian_mtx(2*count+1,0) = 0;
    					Img_Jacobian_mtx(2*count+1,1) = -1/depth_value_meter;
    					Img_Jacobian_mtx(2*count+1,2) = norm_feature_pts_y/depth_value_meter;
    					Img_Jacobian_mtx(2*count+1,3) = ( 1 + norm_feature_pts_y*norm_feature_pts_y );
    					Img_Jacobian_mtx(2*count+1,4) = - norm_feature_pts_x*norm_feature_pts_y;
    					Img_Jacobian_mtx(2*count+1,5) = -norm_feature_pts_x;

    					// count the number of valid pts ( n )
    					count += 1;
    				}
    			}
        }

        // check the number of count and valid depth points is same
        if (Numvalid_depth_pts != count)
        {
          printf("\n\n The number of valid depth points != The count number !! \n\n");
          printf("The number of valid depth : %d \n", Numvalid_depth_pts);
          printf("The number of iteration : %d \n", count);
          printf("\n\n Finish This Program. Thank you! \n\n");
          return -1;
        }

        // estimate the current RGB-D camera velocity with respect to camera-frame
        Eigen::MatrixXd Img_Jacobian_square(6,6);

        Img_Jacobian_square = Img_Jacobian_mtx.transpose() * Img_Jacobian_mtx;
        Estimated_velocity = ( Img_Jacobian_square.inverse() * Img_Jacobian_mtx.transpose() * optical_flow );


        // write the estimated RGB-D Velocity w.r.t. camera-frame in result.txt
        velocityEsti(0,(imgIdx-1)) = Estimated_velocity(0);
        velocityEsti(1,(imgIdx-1)) = Estimated_velocity(1);
        velocityEsti(2,(imgIdx-1)) = Estimated_velocity(2);
        velocityEsti(3,(imgIdx-1)) = Estimated_velocity(3);
        velocityEsti(4,(imgIdx-1)) = Estimated_velocity(4);
        velocityEsti(5,(imgIdx-1)) = Estimated_velocity(5);

        //////////////////////////////////// Display Part / Start

        // write the calculation result on the result_view_Img
        char str_for_resultview[100];
        double font_scale = 0.45;
        cv::Scalar font_color = CV_RGB(0,0,0);        // black
        int font_thickness = 1.8;

        // display the number of valid points
        int height = result_view_Img.rows;
        int width = result_view_Img.cols;

        sprintf(str_for_resultview,"The number of valid feature pts : %d", Numvalidpts );
        putText(result_view_Img, str_for_resultview, cv::Point2f(5,height-50), cv::FONT_HERSHEY_TRIPLEX, font_scale, font_color, font_thickness );

        sprintf(str_for_resultview,"The number of valid feature pts (inlier) : %d", Numvalidpts_inlier );
        putText(result_view_Img, str_for_resultview, cv::Point2f(5,height-30), cv::FONT_HERSHEY_TRIPLEX, font_scale, font_color, font_thickness );

        sprintf(str_for_resultview,"The number of valid depth feature pts : %d", Numvalid_depth_pts );
        putText(result_view_Img, str_for_resultview, cv::Point2f(5,height-10), cv::FONT_HERSHEY_TRIPLEX, font_scale, font_color, font_thickness );

        // draw the optical flow vectors
        quiver( result_view_Img, pre_valid_feature_pts_inlier, valid_feature_pts_inlier); // draw only inlier optical flow vectors

        //////////////////////////////////// Display Part / End


        //////////////////////////////////// save the data for next step of optical flow / Start

        // save this [k] step feature points, images as [k-1] step feature points, images
        if( Numvalidpts_inlier <= 100 || Numvalid_depth_pts <= 80 )
        {
          // remove the pre_feature_pts
          pre_feature_pts.clear();

          // extract again the feature points in src_gray_Img
          cv::goodFeaturesToTrack(src_gray_Img, pre_feature_pts, maxCorners, qualityLevel, minDistance, cv::Mat(), blockSize, useHarrisDetector, k);
          cv::cornerSubPix(src_gray_Img, pre_feature_pts, cv::Size(7,7), cv::Size(-1,-1), cvTermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03));

          // draw the new detected feature points
          for ( int i = 0; i < pre_feature_pts.size(); i++ )
          {
            cv::circle(result_view_Img, pre_feature_pts[i], 5, CV_RGB(0,255,0), -1, 8, 0);
          }
        }
        else
        {
          // remove the pre_feature_pts
          pre_feature_pts.clear();

          // save the valid feature points only.
          for(int i=0; i<(int)valid_feature_pts.size(); i++)
          {
            pre_feature_pts.push_back(valid_feature_pts[i]);
          }
        }
        pre_src_gray_Img = src_gray_Img.clone();

        //////////////////////////////////// save the data for next step of optical flow / End

        imshow("Result View Image", result_view_Img);
        cv::waitKey(30);




        /**********************************************
        *
        * PART 2-2:
        * minimize reprojection error with libviso2 algorithm
        *
        ***********************************************/

        // read left and right images at [imgIdx-1]
        tempLeftImg = cv::imread(leftImgFileName, 0);    // read a grayscale image
        tempRightImg = cv::imread(rightImgFileName, 0);  // read a grayscale image
        visualOdometryStereoMex(tempLeftImg, tempRightImg, viso, T_temp);


        // estimate T_vo_ini with libviso2
        T_vo_ini = T_temp * T_vo_ini;

        // for debug
        //std::cout << T_vo_ini << std::endl;
        //usleep(10000000);


        // display the current status
        double num_matches = viso.getNumberOfMatches();
        double num_inliers = viso.getNumberOfInliers();
        printf("Frame: %d, Matches: %d, Inliers: %lf %% \n", imgIdx, (int)num_matches, (100.0*num_inliers/num_matches));


        /**********************************************
        *
        * PART 2-3:
        * minimize modified photometric error with PIVO algorithm
        *
        ***********************************************/

        // assign initial model parameters
        Eigen::Matrix4d inputT = T_vo_ini;
        Eigen::Matrix4d outputT = T_vo_ini;
        illParam_perImg inputIllParam = illParamPIVO;
        illParam_perImg outputIllParam = illParamPIVO;

        double err = 0;
        Eigen::MatrixXd residuals;
        Eigen::MatrixXd weights;

        for(int pyrLevel = opts.numPyr; pyrLevel >= opts.pyrEndLevel; --pyrLevel) // (1-based pyramid level index (pyrLevel))
        {
            // initialize parameters and variables
            inputT = outputT;
            inputIllParam = outputIllParam;

            Eigen::Matrix3d tempInParasMtx = cam.K_pyramid[pyrLevel-1];
            double winSize = floor((double)opts.winSize/pow(2,pyrLevel)) * 2 + 1;

            // main loop
            EstimateCameraMotionPIVO(I1RefPyr[pyrLevel-1], D1RefPyr[pyrLevel-1], keyPtsPyr[pyrLevel-1], I2RefPyr[pyrLevel-1], inputT, inputIllParam, tempInParasMtx, winSize, opts,
                                     outputT, outputIllParam, err, residuals, weights);
        }


        // for next step
        if (err > 100)
        {
            // PIVO failure case
            T_vo_esti = T_vo_ini;
            illParamPIVO = illParamPIVO;
        }
        else
        {
            T_vo_esti = outputT;
            illParamPIVO = outputIllParam;
        }

        // for debug
        //std::cout << err << std::endl;
        //std::cout << T_vo_esti << std::endl;
        //for(int patchIdx = 0; patchIdx < opts.numWinTotal; ++patchIdx)
        //{
        //    std::cout <<"<<"<<patchIdx<<">>"<< std::endl;
        //    std::cout << illParamPIVO.patch[patchIdx].contrast << std::endl;
        //    std::cout << illParamPIVO.patch[patchIdx].bias << std::endl;
        //}
        //usleep(100000000);


        /**********************************************
        *
        * PART 2-4:
        * update the camera state from T_vo_esti
        *
        ***********************************************/

        updateStateEstiData(T_vo_esti, keyIdx, imgIdx, T_output, stateEsti);

        // for debug
        //std::cout << stateEsti.col(imgIdx-1) << std::endl;
        //std::cout << T_output << std::endl;
        //usleep(10000000);


        /**********************************************
        *
        * PART 2-5:
        * save model parameters and variables for debug
        *
        ***********************************************/


        // save the values for debug
        T_1i_Save.push_back(T_output);
        illParamSGPVO_Save.push_back(illParamPIVO);
        errSGPVO_Save.push_back(err);

        // display the current status
        printf("current modified mean residual error : %06.4f at [%04d/%04d] \n", err, imgIdx, numImg);
        printf("current location : x: %06.3f, y: %06.3f, z: %06.3f \n", stateEsti(0,imgIdx-1), stateEsti(1,imgIdx-1), stateEsti(2,imgIdx-1));

        tx = T_output(0,3);
        ty = T_output(1,3);
        tz = T_output(2,3);

        int x = int(tx) + 350;
        int z = int(tz) + 100;
        cv::circle(traj, cv::Point(x, z) ,1, CV_RGB(255,0,0), 2);
        cv::rectangle( traj, cv::Point(10, 30), cv::Point(550, 50), CV_RGB(0,0,0), CV_FILLED);
        sprintf(text, "current location: x = %03.3fm , y = %03.3fm , z = %03.3fm", tx, ty, tz);
        cv::putText(traj, text, textOrg, fontFace, fontScale, cv::Scalar::all(255), thickness, 8);

        cv::imshow( "Trajectory", traj );
        cv::waitKey(30);
    }


    // save the motion and light variation estimation results from PIVO
    saveSGPVOresults(T_1i_Save, stateEsti, illParamSGPVO_Save, errSGPVO_Save, keyIdx_Save, opts);
    saveEigenMatrix("velocityEsti.txt", velocityEsti);
    std::cout << "SGPVO demo complete! Exiting ..." << std::endl;


    // exit the program
    return 0;
}



double Rounding(double x, int digit)
{
	double round_result = ( floor( (x) * pow( float(10), digit ) + 0.5f ) / ( pow( float(10), digit ) ) );
	return round_result;
}



void quiver(cv::Mat& result_view, std::vector<cv::Point2f>& pre_feature_points, std::vector<cv::Point2f>& feature_points)
{
	// draw the result of optical flow ( depict arrow ) at Mat& result_view
	int width = result_view.cols;
	int height = result_view.rows;

	for(int i = 0; i<(int)feature_points.size(); i++)
	{
		int line_thickness = 1;
		cv::Scalar line_color = CV_RGB(255,0,0);

		// calculate two points of line
		cv::Point2f p, q;
		p = pre_feature_points[i];
		q = feature_points[i];

		double angle;
		double arrow_length;

		angle = atan2( (double) p.y - q.y, (double) p.x - q.x );
		arrow_length = sqrt( pow((float)(p.y - q.y),2) + pow((float)(p.x - q.x),2) );

		int mag_arrow = 3;

		q.x = (int) (p.x - mag_arrow * arrow_length * cos(angle));  // draw arrow mag_arrow times of original arrow magnitude.
		q.y = (int) (p.y - mag_arrow * arrow_length * sin(angle));

		if((arrow_length<0)|(arrow_length>30))   // condition of magnitude of arrow.
		{
			continue; // skip the arrow is too short or long.
		}

		// draw arrow
		cv::line( result_view, p, q, line_color, line_thickness, CV_AA );
		p.x = (int) (q.x + 10 * cos(angle + PI / 4));
		if(p.x>=width)
			p.x=width-1;
		else if(p.x<0)
			p.x=0;
		p.y = (int) (q.y + 10 * sin(angle + PI / 4));
		if(p.y>=height)
			p.y=height-1;
		else if(p.y<0)
			p.y=0;
		cv::line( result_view, p, q, line_color, line_thickness, CV_AA );
		p.x = (int) (q.x + 10 * cos(angle - PI / 4));
		if(p.x>=width)
			p.x=width-1;
		else if(p.x<0)
			p.x=0;
		p.y = (int) (q.y + 10 * sin(angle - PI / 4));
		if(p.y>height)
			p.y=height-1;
		else if(p.y<0)
			p.y=0;
		cv::line( result_view, p, q, line_color, line_thickness, CV_AA );
	}
}



bool InImage(cv::Point2f& test_point, cv::Mat& Reference_Image)
{
	int row_size, col_size;
	row_size = Reference_Image.rows;
	col_size = Reference_Image.cols;

	double u, v;
	u = test_point.x;
	v = test_point.y;

	if ( u >= 0 && u <= (col_size-1) && v >=0 && v <= (row_size-1) )
	{
		return true;
	}
	else
	{
		return false;
	}
}
