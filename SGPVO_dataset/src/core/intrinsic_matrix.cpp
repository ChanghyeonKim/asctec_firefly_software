#include "core/intrinsic_matrix.h"



void downsampleKmatrix(const Eigen::Matrix3d& inputKmatrix, Eigen::Matrix3d& outputKmatrix)
{
    outputKmatrix(0,0) = inputKmatrix(0,0) / 2;
    outputKmatrix(0,1) = 0;
    outputKmatrix(0,2) = ((inputKmatrix(0,2)+0.5) / 2) - 0.5;

    outputKmatrix(1,0) = 0;
    outputKmatrix(1,1) = inputKmatrix(1,1) / 2;
    outputKmatrix(1,2) = ((inputKmatrix(1,2)+0.5) / 2) - 0.5;

    outputKmatrix(2,0) = 0;
    outputKmatrix(2,1) = 0;
    outputKmatrix(2,2) = 1;
}



void genPyramidKmatrix(std::vector<Eigen::Matrix3d>& KmatrixPyramid, const Eigen::Matrix3d& intrinsicMatrix, const optsPIVO& opts)
{
    // initialize
    KmatrixPyramid.push_back(intrinsicMatrix);


    // perform pyramid reduction of camera intrinsic matrix
    Eigen::Matrix3d tempKmatrix;

    for(int pyrIdx=0; pyrIdx < (opts.numPyr-1); ++pyrIdx)
    {
        // intrinsic K matrix
        tempKmatrix.fill(0);
        downsampleKmatrix(KmatrixPyramid[pyrIdx],tempKmatrix);
        KmatrixPyramid.push_back(tempKmatrix);
    }
}
