#include "core/libelas.h"
#include "core/libviso2.h"
#include "core/datatypes.h"
#include "core/rgbd_image.h"
#include "core/intrinsic_matrix.h"
#include "core/weight_calculation.h"
#include "core/least_squares.h"
#include "core/fileIO.h"
#include "core/Liegroup_operation.h"
#include "core/dense_tracking.h"



int main(void)
{
    /**********************************************
    *
    * PART 0:
    * load ICSL dataset's information.
    * define options for the SPGVO
    *
    ***********************************************/

    // set the absolute directory of the KITTI dataset and the number of stereo images in BaseDir.
    std::string BaseDir = "/home/pjinkim/Desktop/KITTIdataset/Residential/2011_09_30/2011_09_30_drive_0033_sync";
    int imInit = 0;
    int numImg = 1594;


    // set camera calibration parameters MANUALLY
    camCalibrationParam cam;
    optsPIVO opts;
    initParameters("ESM", &cam, &opts);
    genPyramidKmatrix(cam.K_pyramid, cam.K, opts);


    // set camera calibration parameters for libviso2 (look at: viso_stereo.h)
    VisualOdometryStereo::parameters param;
    param.calib.f  = cam.K(0,0);    // focal length in pixels
    param.calib.cu = cam.K(0,2);    // principal point (u-coordinate) in pixels
    param.calib.cv = cam.K(1,2);    // principal point (v-coordinate) in pixels
    param.base     = cam.baseline;  // baseline in meters

    // for debug
    //std::cout << param.calib.f << std::endl;
    //std::cout << param.calib.cu << std::endl;
    //std::cout << param.calib.cv << std::endl;
    //std::cout << param.base << std::endl;
    //usleep(10000000);


    // init visual odometry (libviso2)
    VisualOdometryStereo viso(param);


    /**********************************************
    *
    * PART 1:
    * Main Part -> Calculate Transformation matrix
    *
    ***********************************************/

    // declare trivial variables for drawing
    cv::Mat traj = cv::Mat::zeros(800, 800, CV_8UC3);
    FLOAT tx, ty, tz;
    char text[100];
    int fontFace = cv::FONT_HERSHEY_PLAIN;
    double fontScale = 1;
    int thickness = 1;
    cv::Point textOrg(10, 50);


    // initialize variables
    std::vector<int> keyIdx_Save;
    std::vector<Eigen::Vector2f> keyPts_Save;

    std::vector<Eigen::Matrix4d> T_1i_Save;
    std::vector<illParam_perImg> illParamSGPVO_Save;
    std::vector<double> errSGPVO_Save;

    Eigen::MatrixXd stateEsti(6,numImg);
    stateEsti.fill(0);

    std::string leftImgFileName, rightImgFileName;
    cv::Mat vi_cam0_image_rect, vi_cam1_image_rect;
    cv::Mat tempLeftImg, tempRightImg;

    cv::Mat I1Ref, D1Ref;  // grey & depth image at keyframe
    std::vector<cv::Mat> I1RefPyr, D1RefPyr;
    Eigen::MatrixXd keyPts(2, opts.numWinTotal);
    std::vector<Eigen::MatrixXd> keyPtsPyr;

    cv::Mat I2Ref, D2Ref;  // grey & depth image at current frame
    std::vector<cv::Mat> I2RefPyr, D2RefPyr;
    int imgIdx = 2, keyIdx = 1;


    // initialize the model parameters and temporary rigid body transformation matrices
    Eigen::Matrix4d T_vo_esti = Eigen::Matrix4d::Identity(4,4);
    Eigen::Matrix4d T_vo_ini = Eigen::Matrix4d::Identity(4,4);
    illParam_perImg illParamPIVO;
    for(int patchIdx = 0; patchIdx < opts.numWinTotal; ++patchIdx)
    {
        illParamPIVO.patch[patchIdx].contrast = 1.0f;
        illParamPIVO.patch[patchIdx].bias = 0.0f;
    }

    Eigen::Matrix4d T_temp = Eigen::Matrix4d::Identity(4,4);
    Eigen::Matrix4d T_output = Eigen::Matrix4d::Identity(4,4);


    for(imgIdx=2; imgIdx < (numImg+1); ++imgIdx)
    {
        if ( ((imgIdx - keyIdx) >= 5) || (EuclideanDist(stateEsti, keyIdx, (imgIdx-1))) >= 1 || (imgIdx <= 2) )
        {
            /**********************************************
            *
            * PART 1-1:
            * Keyframe initialization
            *
            ***********************************************/

            // select keyframe index
            keyIdx = imgIdx - 1;
            keyIdx_Save.push_back(keyIdx);


            // extract the gray and depth image at keyframe index [keyIdx-1]
            char base_name[256];
            sprintf(base_name,"/data/%010d.png",(imInit+keyIdx-1));
            leftImgFileName  = BaseDir + "/image_00" + base_name;
            rightImgFileName = BaseDir + "/image_01" + base_name;
            getImgInKITTIdataset(leftImgFileName, rightImgFileName, cam, I1Ref, D1Ref);

            // for debug
            //std::cout << leftImgFileName << std::endl;
            //std::cout << rightImgFileName << std::endl;
            //std::cout << I1Ref.rows << "/" << I1Ref.cols << std::endl;
            //std::cout << D1Ref.rows << "/" << D1Ref.cols << std::endl;
            //std::cout << I1Ref.at<double>(74,1183) << std::endl;
            //std::cout << D1Ref.at<double>(211,900) << std::endl;
            //usleep(10000000);


            // generate grey and depth image pyramid
            I1RefPyr.clear();
            D1RefPyr.clear();
            genPyramidImage(I1RefPyr, D1RefPyr, I1Ref, D1Ref, opts);


            // extract keypoints in grey image
            detectKeypoints(I1Ref, keyPts, opts); // 1-based pixel coordinates (u,v)
            keyPtsPyr.clear();
            genPyramidKeyPts(keyPtsPyr, keyPts, opts);

            // for debug
            //std::cout << keyPts << std::endl;
            //std::cout << keyPtsPyr[3] << std::endl;
            //usleep(10000000);

            // for debug
            //std::cout << I1RefPyr[1].size().width << std::endl;
            //std::cout << I1RefPyr[1].size().height << std::endl;
            //printf("%lf \n", I1RefPyr[1].at<double>(31,57));
            //std::cout << D1RefPyr[3].size().width << std::endl;
            //std::cout << D1RefPyr[3].size().height << std::endl;
            //printf("%2.11lf \n", D1RefPyr[3].at<double>(38,38));
            //usleep(100000000);

            //cv::Mat forDebugImg;
            //I1RefPyr[3].convertTo(forDebugImg,CV_8UC1);
            //imshow("camera view",forDebugImg);
            //cv::waitKey(0);
            //usleep(100000000);


            // initialize the model parameters ( for Motion and Multi-Patch illumination Changes )
            T_vo_ini = Eigen::Matrix4d::Identity(4,4);
            for(int patchIdx = 0; patchIdx < opts.numWinTotal; ++patchIdx)
            {
                illParamPIVO.patch[patchIdx].contrast = 1.0f;
                illParamPIVO.patch[patchIdx].bias = 0.0f;
            }

            // for debug
            //std::cout << T_vo_ini << std::endl;
            //for(int patchIdx = 0; patchIdx < opts.numWinTotal; ++patchIdx)
            //{
            //    std::cout <<"<<"<<patchIdx<<">>"<< std::endl;
            //    std::cout << illParamPIVO.patch[patchIdx].contrast << std::endl;
            //    std::cout << illParamPIVO.patch[patchIdx].bias << std::endl;
            //}
            //usleep(100000000);


            // read left and right images at [keyIdx-1]
            tempLeftImg = cv::imread(leftImgFileName, 0);    // read a grayscale image
            tempRightImg = cv::imread(rightImgFileName, 0);  // read a grayscale image
            visualOdometryStereoMex(tempLeftImg, tempRightImg, viso, T_temp);

            // for debug
            //std::cout << T_temp << std::endl;
            //usleep(100000000);
        }

        /**********************************************
        *
        * PART 2-1:
        * Get next image frame
        *
        ***********************************************/

        // extract the gray and depth image at imgIdx index [imgIdx-1]
        char base_name[256];
        sprintf(base_name,"/data/%010d.png",(imInit+imgIdx-1));
        leftImgFileName  = BaseDir + "/image_00" + base_name;
        rightImgFileName = BaseDir + "/image_01" + base_name;
        getImgInKITTIdataset(leftImgFileName, rightImgFileName, cam, I2Ref, D2Ref);

        // for debug
        //std::cout << leftImgFileName << std::endl;
        //std::cout << rightImgFileName << std::endl;
        //usleep(100000000);


        // generate grey and depth image pyramid
        I2RefPyr.clear();
        D2RefPyr.clear();
        genPyramidImage(I2RefPyr, D2RefPyr, I2Ref, D2Ref, opts);

        // for debug
        //std::cout << I2RefPyr[4].size() << std::endl;
        //usleep(100000000);


        /**********************************************
        *
        * PART 2-2:
        * minimize reprojection error with libviso2 algorithm
        *
        ***********************************************/

        // read left and right images at [imgIdx-1]
        tempLeftImg = cv::imread(leftImgFileName, 0);    // read a grayscale image
        tempRightImg = cv::imread(rightImgFileName, 0);  // read a grayscale image
        visualOdometryStereoMex(tempLeftImg, tempRightImg, viso, T_temp);


        // estimate T_vo_ini with libviso2
        T_vo_ini = T_temp * T_vo_ini;

        // for debug
        //std::cout << T_vo_ini << std::endl;
        //usleep(10000000);


        // display the current status
        double num_matches = viso.getNumberOfMatches();
        double num_inliers = viso.getNumberOfInliers();
        printf("Frame: %d, Matches: %d, Inliers: %lf %% \n", imgIdx, (int)num_matches, (100.0*num_inliers/num_matches));


        /**********************************************
        *
        * PART 2-3:
        * minimize modified photometric error with PIVO algorithm
        *
        ***********************************************/

        // assign initial model parameters
        Eigen::Matrix4d inputT = T_vo_ini;
        Eigen::Matrix4d outputT = T_vo_ini;
        illParam_perImg inputIllParam = illParamPIVO;
        illParam_perImg outputIllParam = illParamPIVO;

        double err = 0;
        Eigen::MatrixXd residuals;
        Eigen::MatrixXd weights;

        for(int pyrLevel = opts.numPyr; pyrLevel >= opts.pyrEndLevel; --pyrLevel) // (1-based pyramid level index (pyrLevel))
        {
            // initialize parameters and variables
            inputT = outputT;
            inputIllParam = outputIllParam;

            Eigen::Matrix3d tempInParasMtx = cam.K_pyramid[pyrLevel-1];
            double winSize = floor((double)opts.winSize/pow(2,pyrLevel)) * 2 + 1;

            // main loop
            EstimateCameraMotionPIVO(I1RefPyr[pyrLevel-1], D1RefPyr[pyrLevel-1], keyPtsPyr[pyrLevel-1], I2RefPyr[pyrLevel-1], inputT, inputIllParam, tempInParasMtx, winSize, opts,
                                     outputT, outputIllParam, err, residuals, weights);
        }


        // for next step
        if (err > 100)
        {
            // PIVO failure case
            T_vo_esti = T_vo_ini;
            illParamPIVO = illParamPIVO;
        }
        else
        {
            T_vo_esti = outputT;
            illParamPIVO = outputIllParam;
        }

        // for debug
        //std::cout << err << std::endl;
        //std::cout << T_vo_esti << std::endl;
        //for(int patchIdx = 0; patchIdx < opts.numWinTotal; ++patchIdx)
        //{
        //    std::cout <<"<<"<<patchIdx<<">>"<< std::endl;
        //    std::cout << illParamPIVO.patch[patchIdx].contrast << std::endl;
        //    std::cout << illParamPIVO.patch[patchIdx].bias << std::endl;
        //}
        //usleep(100000000);


        /**********************************************
        *
        * PART 2-4:
        * update the camera state from T_vo_esti
        *
        ***********************************************/

        updateStateEstiData(T_vo_esti, keyIdx, imgIdx, T_output, stateEsti);

        // for debug
        //std::cout << stateEsti.col(imgIdx-1) << std::endl;
        //std::cout << T_output << std::endl;
        //usleep(10000000);


        /**********************************************
        *
        * PART 2-5:
        * save model parameters and variables for debug
        *
        ***********************************************/


        // save the values for debug
        T_1i_Save.push_back(T_output);
        illParamSGPVO_Save.push_back(illParamPIVO);
        errSGPVO_Save.push_back(err);

        // display the current status
        printf("current modified mean residual error : %06.4f at [%04d/%04d] \n", err, imgIdx, numImg);
        printf("current location : x: %06.3f, y: %06.3f, z: %06.3f \n", stateEsti(0,imgIdx-1), stateEsti(1,imgIdx-1), stateEsti(2,imgIdx-1));

        tx = T_output(0,3);
        ty = T_output(1,3);
        tz = T_output(2,3);

        int x = int(tx) + 350;
        int z = int(tz) + 100;
        cv::circle(traj, cv::Point(x, z) ,1, CV_RGB(255,0,0), 2);
        cv::rectangle( traj, cv::Point(10, 30), cv::Point(550, 50), CV_RGB(0,0,0), CV_FILLED);
        sprintf(text, "current location: x = %03.3fm , y = %03.3fm , z = %03.3fm", tx, ty, tz);
        cv::putText(traj, text, textOrg, fontFace, fontScale, cv::Scalar::all(255), thickness, 8);

        cv::imshow( "Trajectory", traj );
        cv::waitKey(30);
    }


    // save the motion and light variation estimation results from PIVO
    saveSGPVOresults(T_1i_Save, stateEsti, illParamSGPVO_Save, errSGPVO_Save, keyIdx_Save, opts);
    std::cout << "SGPVO demo complete! Exiting ..." << std::endl;


    // exit the program
    return 0;
}
