#ifndef _INTRINSIC_MATRIX_H_
#define _INTRINSIC_MATRIX_H_

#include <vector>
#include <eigen3/Eigen/Dense>
#include "core/datatypes.h"

void downsampleKmatrix(const Eigen::Matrix3d& inputKmatrix, Eigen::Matrix3d& outputKmatrix);

void genPyramidKmatrix(std::vector<Eigen::Matrix3d>& KmatrixPyramid, const Eigen::Matrix3d& intrinsicMatrix, const optsPIVO& opts);










#endif /* _INTRINSIC_MATRIX_H_ */
