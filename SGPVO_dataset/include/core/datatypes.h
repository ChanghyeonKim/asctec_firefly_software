#ifndef _DATATYPES_H_
#define _DATATYPES_H_

#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <eigen3/Eigen/Dense>


typedef struct camCalibrationParam
{
    Eigen::Matrix3d K;
    std::vector<Eigen::Matrix3d> K_pyramid;
    double baseline;
} camCalibrationParam;


typedef struct optsPIVO
{
    int numWinHeight;
    int numWinWidth;
    int numPtsInWin;
    int numWinTotal;

    int winSize;
    double minDepth;
    double thresPlane;
    int numPyr;
    int pyrEndLevel;

    std::string jacobianFlag;

    int iniSigma;
    int tDistriDof;
    double eps;
    int maxNumIter;
} optsPIVO;


typedef struct illParam_perPatch
{
    double contrast;
    double bias;
} illParam_perPatch;


typedef struct illParam_perImg
{
    illParam_perPatch patch[27];
} illParam_perImg;













#endif /* _DATATYPES_H_ */
