#ifndef _LEAST_SQUARES_H_
#define _LEAST_SQUARES_H_

#include <eigen3/Eigen/Dense>
#include <opencv2/opencv.hpp>
#include "core/datatypes.h"



void computeJ_I(const cv::Mat& I1dx, const cv::Mat& I1dy, const cv::Mat& I2dx, const cv::Mat& I2dy, const Eigen::MatrixXd& pixelPtsRef, const Eigen::MatrixXd& pixelPtsNext, const size_t& pixelIdx,
                Eigen::RowVector2d& J_I);

void computeJ_w(const Eigen::MatrixXd& ptsCloudRef, const Eigen::MatrixXd& ptsCloudNext, const Eigen::Matrix4d& inputT, const Eigen::Matrix3d& inputKmatrix, const optsPIVO& opts, const size_t& pixelIdx,
                Eigen::MatrixXd& J_w);

void J_wForFA(const double* in1, double* J_w);

void computeJacobianResidual(const cv::Mat& I1RefImgPyr, const Eigen::MatrixXd& inputKeyPtsPyr, const cv::Mat& I2RefImgPyr,
                             const Eigen::MatrixXd& pixelPtsRef, const Eigen::MatrixXd& ptsCloudRef, const Eigen::RowVectorXi& patchIdxRef, const Eigen::MatrixXd& pixelPtsNext, const Eigen::MatrixXd& ptsCloudNext,
                             const Eigen::Matrix4d& inputT, const illParam_perImg& inputIllParam, const Eigen::Matrix3d& inputKmatrixPyr, const double& winSize, const optsPIVO& opts,
                             Eigen::MatrixXd& J_tot, Eigen::MatrixXd& r_tot);

void computeJRW(const cv::Mat& I1RefImgPyr, const cv::Mat& D1RefImgPyr, const Eigen::MatrixXd& inputKeyPtsPyr, const cv::Mat& I2RefImgPyr,
                const Eigen::Matrix4d& inputT, const illParam_perImg& inputIllParam, const Eigen::Matrix3d& inputKmatrixPyr, const double& winSize, const optsPIVO& opts,
                Eigen::MatrixXd& J_tot, Eigen::MatrixXd& r_tot, Eigen::MatrixXd& W_tot);

void calcCostFtn(const Eigen::MatrixXd& W_tot, const Eigen::MatrixXd& r_tot, double& meanCostValue);

void calcDeltaZ(const Eigen::MatrixXd& J_tot, const Eigen::MatrixXd& r_tot, const Eigen::MatrixXd& W_tot, const optsPIVO& opts, const std::vector<int>& invalidPatchIdx,
                Eigen::MatrixXd& deltaZ);

void getInvalidPatchIdx(const Eigen::MatrixXd& J_tot, std::vector<int>& invalidPatchIdx, const optsPIVO& opts);

void removeColumn(Eigen::MatrixXd& matrix, const int& colToRemove);




















#endif /* _LEAST_SQUARES_H_ */
