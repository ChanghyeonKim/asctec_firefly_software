#include "svo/time.h"

auto ti = std::chrono::high_resolution_clock::now();
auto t1 = ti;

unsigned long get_ticks_us(){
	auto tf = std::chrono::high_resolution_clock::now();
	auto ticks = std::chrono::duration_cast<std::chrono::microseconds>(tf - ti);

	return (unsigned long)ticks.count();
}

unsigned long get_diff_ticks_us(){
	auto tf = std::chrono::high_resolution_clock::now();
	auto ticks = std::chrono::duration_cast<std::chrono::microseconds>(tf - t1);
	t1 = tf;

	return (unsigned long)ticks.count();
}
