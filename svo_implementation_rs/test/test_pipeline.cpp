// This file is part of SVO - Semi-direct Visual Odometry.
//
// Copyright (C) 2014 Christian Forster <forster at ifi dot uzh dot ch>
// (Robotics and Perception Group, University of Zurich, Switzerland).
//
// SVO is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// SVO is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <svo/config.h>
#include <svo/frame_handler_mono.h>
#include <svo/map.h>
#include <svo/frame.h>
#include <vector>
#include <string>
#include <vikit/math_utils.h>
#include <vikit/vision.h>
#include <vikit/abstract_camera.h>
#include <vikit/atan_camera.h>
#include <vikit/pinhole_camera.h>
#include <opencv2/opencv.hpp>
#include <sophus/se3.h>
#include <iostream>
#include "test_utils.h"
#include "svo/time.h"
#include "svo/communication.h"
#include <librealsense/rs.hpp>


void rotmtx2angle(const Eigen::Matrix3d& rotationMatrix, Eigen::Vector3d& eulerAngle);
void angle2rotmtx(const Eigen::Vector3d& eulerAngle, Eigen::Matrix3d& rotationMatrix);


int main(int argc, char** argv)
{
  // initialize visual odometry class (SVO)
  vk::AbstractCamera* cam_;
  svo::FrameHandlerMono* vo_;
  cam_ = new vk::PinholeCamera(640, 480, 619.573469561428, 623.660742404636, 324.563105543140, 225.224085611115);
  vo_ = new svo::FrameHandlerMono(cam_);
  vo_->start();


  // initialize WiFi communication with server (host computer)
  onBtnConnect_clicked();
  TNT::Array2D<double> Est_data(9,1,0.0);


  // initialize realsense sensor
  rs::context ctx;
  std::cout << "There are " << ctx.get_device_count() << " connected RealSense device.\n" << std::endl;
  rs::device * dev = ctx.get_device(0);
  std::cout << "\nUsing device 0, an " << dev->get_name() << std::endl;
  std::cout << "Serial number: " << dev->get_serial() << std::endl;
  std::cout << "Firmware version: " << dev->get_firmware_version() << std::endl;
  dev->enable_stream(rs::stream::color, 640, 480, rs::format::bgr8, 60);
  dev->start();
  usleep(3000000);


  // declare trivial variables for drawing
  Eigen::Matrix4d Tr_total;
  Eigen::Vector3d center_position, euler_angle;
  Tr_total.fill(0);
  center_position.fill(0);
  euler_angle.fill(0);
  int image_counter = 1;

  Eigen::Vector3d x_axis_end_c, x_axis_end_g;
  Eigen::Vector3d y_axis_end_c, y_axis_end_g;
  Eigen::Vector3d z_axis_end_c, z_axis_end_g;
  x_axis_end_c << 0.5, 0, 0;
  y_axis_end_c << 0, 0.5, 0;
  z_axis_end_c << 0, 0, 0.5;
  x_axis_end_g.fill(0);
  y_axis_end_g.fill(0);
  z_axis_end_g.fill(0);


  // do SVO with realsense
  while(true)
  {
    dev->wait_for_frames();
    // read color image from realsense
    const void * color_frame = dev->get_frame_data(rs::stream::color);
    cv::Mat color_image(480, 640, CV_8UC3, (void*)color_frame);
    cv::Mat gray_image;
    cvtColor(color_image, gray_image, CV_BGR2GRAY);
    assert(!gray_image.empty());


    // process frame
    vo_->addImage(gray_image, 0.01*image_counter);


    // display the current status
    if(vo_->lastFrame() != NULL)
    {
      // display current tracking quality
      std::cout << "Frame-Id: " << vo_->lastFrame()->id_ << " \t"
      << "#Features: " << vo_->lastNumObservations() << " \t"
      << "Proc. Time: " << vo_->lastProcessingTime()*1000 << "ms \n";


      // compute and accumulate egomotion with SVO
      Tr_total = vo_->lastFrame()->T_f_w_.inverse().matrix();



      // Inertial frame compensation (GPS or VICON)
      std::string current_inertial_frame = "VICON"; // or VICON
      Eigen::Matrix4d Tr_total_final = Eigen::Matrix4d::Identity(4,4);
      if ( !strcmp("GPS", current_inertial_frame.c_str()) )
      {
        Eigen::Vector3d euler_angle_GPS;
        euler_angle_GPS.fill(0);
        euler_angle_GPS(0) = -110 * (3.14/180);   // rotation along X axis from inertial to body
        euler_angle_GPS(1) = 0 * (3.14/180);      // rotation along Y axis from inertial to body
        euler_angle_GPS(2) = 0 * (3.14/180);      // rotation along Z axis from inertial to body

        Eigen::Matrix3d temp, rotation_matrix_GPS;
        temp.fill(0);
        rotation_matrix_GPS.fill(0);
        angle2rotmtx(euler_angle_GPS, temp);
        rotation_matrix_GPS = temp.inverse();

        Eigen::Matrix4d transformation_GPS = Eigen::Matrix4d::Identity(4,4);
        transformation_GPS(0,0) = rotation_matrix_GPS(0,0);
        transformation_GPS(0,1) = rotation_matrix_GPS(0,1);
        transformation_GPS(0,2) = rotation_matrix_GPS(0,2);

        transformation_GPS(1,0) = rotation_matrix_GPS(1,0);
        transformation_GPS(1,1) = rotation_matrix_GPS(1,1);
        transformation_GPS(1,2) = rotation_matrix_GPS(1,2);

        transformation_GPS(2,0) = rotation_matrix_GPS(2,0);
        transformation_GPS(2,1) = rotation_matrix_GPS(2,1);
        transformation_GPS(2,2) = rotation_matrix_GPS(2,2);

        Tr_total_final = transformation_GPS * Tr_total;
      }
      else if ( !strcmp("VICON", current_inertial_frame.c_str()) )
      {
        Eigen::Vector3d euler_angle_VICON;
        euler_angle_VICON.fill(0);
        euler_angle_VICON(0) = -180 * (3.14/180);   //  rotation along X axis from inertial to body
        euler_angle_VICON(1) = 0 * (3.14/180);      //  rotation along Y axis from inertial to body
        euler_angle_VICON(2) = -90 * (3.14/180);    //  rotation along Z axis from inertial to body

        Eigen::Matrix3d temp, rotation_matrix_VICON;
        temp.fill(0);
        rotation_matrix_VICON.fill(0);
        angle2rotmtx(euler_angle_VICON, temp);
        rotation_matrix_VICON = temp.inverse();

        Eigen::Matrix4d transformation_VICON = Eigen::Matrix4d::Identity(4,4);
        transformation_VICON(0,0) = rotation_matrix_VICON(0,0);
        transformation_VICON(0,1) = rotation_matrix_VICON(0,1);
        transformation_VICON(0,2) = rotation_matrix_VICON(0,2);

        transformation_VICON(1,0) = rotation_matrix_VICON(1,0);
        transformation_VICON(1,1) = rotation_matrix_VICON(1,1);
        transformation_VICON(1,2) = rotation_matrix_VICON(1,2);

        transformation_VICON(2,0) = rotation_matrix_VICON(2,0);
        transformation_VICON(2,1) = rotation_matrix_VICON(2,1);
        transformation_VICON(2,2) = rotation_matrix_VICON(2,2);

        Eigen::Vector3d euler_angle_BODY;
        euler_angle_BODY.fill(0);
        euler_angle_BODY(0) = -180 * (3.14/180);   //  rotation along X axis from inertial to body
        euler_angle_BODY(1) = 0 * (3.14/180);      //  rotation along Y axis from inertial to body
        euler_angle_BODY(2) = -90 * (3.14/180);    //  rotation along Z axis from inertial to body

        Eigen::Matrix3d rotation_matrix_BODY;
        rotation_matrix_BODY.fill(0);
        angle2rotmtx(euler_angle_BODY, rotation_matrix_BODY);

        Eigen::Matrix4d transformation_BODY = Eigen::Matrix4d::Identity(4,4);
        transformation_BODY(0,0) = rotation_matrix_BODY(0,0);
        transformation_BODY(0,1) = rotation_matrix_BODY(0,1);
        transformation_BODY(0,2) = rotation_matrix_BODY(0,2);

        transformation_BODY(1,0) = rotation_matrix_BODY(1,0);
        transformation_BODY(1,1) = rotation_matrix_BODY(1,1);
        transformation_BODY(1,2) = rotation_matrix_BODY(1,2);

        transformation_BODY(2,0) = rotation_matrix_BODY(2,0);
        transformation_BODY(2,1) = rotation_matrix_BODY(2,1);
        transformation_BODY(2,2) = rotation_matrix_BODY(2,2);

        Tr_total_final = transformation_VICON * Tr_total * transformation_BODY;
      }


      // calculate current estimated velocity
      unsigned long diff_ticks = get_diff_ticks_us();
      double dt = (double) diff_ticks * 1e-6;
      Eigen::Vector3d current_velocity;
      current_velocity(0) = (Tr_total_final(0,3) - center_position(0)) / dt;
      current_velocity(1) = (Tr_total_final(1,3) - center_position(1)) / dt;
      current_velocity(2) = (Tr_total_final(2,3) - center_position(2)) / dt;


      // calculate current 6 DoF pose and velocity
      center_position = Tr_total_final.block(0, 3, 3, 1);
      rotmtx2angle(Tr_total_final.block(0, 0, 3, 3).inverse(), euler_angle);
      std::cout << "X : " << center_position(0) << "/ Y : " << center_position(1) << "/ Z : " << center_position(2) << std::endl;
      std::cout << "roll : " << euler_angle(0)*(180/3.14) << "/ pitch : " << euler_angle(1)*(180/3.14) << "/ yaw : " << euler_angle(2)*(180/3.14) << std::endl;


      // WiFi commuincation with server
      Est_data[0][0] = center_position(0);
      Est_data[1][0] = center_position(1);
      Est_data[2][0] = center_position(2);
      Est_data[3][0] = euler_angle(0);
      Est_data[4][0] = euler_angle(1);
      Est_data[5][0] = euler_angle(2);
      Est_data[6][0] = current_velocity(0);
      Est_data[7][0] = current_velocity(1);
      Est_data[8][0] = current_velocity(2);
      onSenddata(Est_data);


      /* // display the current trajectory (x-z plane) and (y-z plane)
      cv::Mat traj_xz = cv::Mat::zeros(800, 800, CV_8UC3);
      cv::Mat traj_yz = cv::Mat::zeros(800, 800, CV_8UC3);
      x_axis_end_g = Tr_total_final.block(0, 0, 3, 3) * x_axis_end_c + center_position;
      y_axis_end_g = Tr_total_final.block(0, 0, 3, 3) * y_axis_end_/
      c + center_position;
      z_axis_end_g = Tr_total_final.block(0, 0, 3, 3) * z_axis_end_c + center_position;

      line(traj_xz, cv::Point((center_position(0)*100) + 400, -(center_position(2)*100) + 400),
                    cv::Point((x_axis_end_g(0)*100) + 400, -(x_axis_end_g(2)*100) + 400), CV_RGB(255,0,0), 3);
      line(traj_xz, cv::Point((center_position(0)*100) + 400, -(center_position(2)*100) + 400),
                    cv::Point((z_axis_end_g(0)*100) + 400, -(z_axis_end_g(2)*100) + 400), CV_RGB(0,0,255), 3);
      cv::circle(traj_xz, cv::Point((center_position(0)*100) + 400, -(center_position(2)*100) + 400), 2, CV_RGB(255,255,255), 2);
      cv::imshow("Estimated Trajectory (x-z plane)", traj_xz);

      line(traj_yz, cv::Point((center_position(2)*100) + 400, (center_position(1)*100) + 400),
                    cv::Point((y_axis_end_g(2)*100) + 400, (y_axis_end_g(1)*100) + 400), CV_RGB(0,255,0), 3);
      line(traj_yz, cv::Point((center_position(2)*100) + 400, (center_position(1)*100) + 400),
                    cv::Point((z_axis_end_g(2)*100) + 400, (z_axis_end_g(1)*100) + 400), CV_RGB(0,0,255), 3);
      cv::circle(traj_yz, cv::Point((center_position(2)*100) + 400, (center_position(1)*100) + 400), 2, CV_RGB(255,255,255), 2);
      cv::imshow("Estimated Trajectory (y-z plane)", traj_yz);

      cv::namedWindow("gray_image", CV_WINDOW_AUTOSIZE);
      cv::imshow("gray_image", gray_image);
      cv::waitKey(1); */
    }
  }

  printf("BenchmarkNode finished.\n");
  // exit the program
  return 0;
}






void rotmtx2angle(const Eigen::Matrix3d& rotationMatrix, Eigen::Vector3d& eulerAngle)
{
    /**
    % Project:  Patch-based Illumination invariant Visual Odometry (PIVO)
    % Function: rotmtx2angle
    %
    % Description:
    %   This function return the euler angle along x,y and z direction
    %   from rotationMatrix to [phi;theta;psi] angle defined as ZYX sequence
    %
    % Example:
    %   OUTPUT:
    %   eulerAngle: angle vector composed of [phi;theta;psi]
    %               phi = Rotation angle along x direction in radians
    %               theta = Rotation angle along y direction in radians
    %               psi = Rotation angle along z direction in radians
    %
    %   INPUT:
    %   rotationMatrix = Rotation Matrix [3x3] defined as [Body frame] = rotationMatrix * [Inertial frame]
    %
    % NOTE:
    %
    % Author: Pyojin Kim
    % Email: pjinkim1215@gmail.com
    % Website:
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % log:
    % 2015-02-06: Complete
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    **/


    // change rotMtxBody / [Inertial frame] = rotMtxBody * [Body frame]
    Eigen::Matrix3d rotMtxBody = rotationMatrix.transpose();

    double phi, theta, psi;
    phi = atan2(rotMtxBody(2,1),rotMtxBody(2,2));
    theta = asin(-rotMtxBody(2,0));
    psi = atan2(rotMtxBody(1,0),rotMtxBody(0,0));

    // assign roll, pitch, yaw values
    eulerAngle(0) = phi;
    eulerAngle(1) = theta;
    eulerAngle(2) = psi;
}


void angle2rotmtx(const Eigen::Vector3d& eulerAngle, Eigen::Matrix3d& rotationMatrix)
{
    /**
    % Project:  Patch-based Illumination invariant Visual Odometry (PIVO)
    % Function: angle2rotmtx
    %
    % Description:
    %   This function return the rotation matrix rotationMatrix
    %   [Body frame] = rotationMatrix * [Inertial frame]
    %   from [phi;theta;psi] angle defined as ZYX sequence to rotation matrix
    %
    % Example:
    %   OUTPUT:
    %   rotationMatrix = rotation Matrix [3x3] defined as [Body frame] = rotationMatrix * [Inertial frame]
    %
    %   INPUT:
    %   eulerAngle: angle vector composed of [phi;theta;psi]
    %               phi = Rotation angle along x direction in radians
    %               theta = Rotation angle along y direction in radians
    %               psi = Rotation angle along z direction in radians
    %
    % NOTE:
    %
    % Author: Pyojin Kim
    % Email: pjinkim1215@gmail.com
    % Website:
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % log:
    % 2014-08-20:
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    **/


    // assign roll, pitch, yaw values
    double phi = eulerAngle(0);
    double theta = eulerAngle(1);
    double psi = eulerAngle(2);

    Eigen::Matrix3d rotMtx_Rx = Eigen::Matrix3d::Identity(3,3);
    Eigen::Matrix3d rotMtx_Ry = Eigen::Matrix3d::Identity(3,3);
    Eigen::Matrix3d rotMtx_Rz = Eigen::Matrix3d::Identity(3,3);

    rotMtx_Rx << 1,        0,         0,
              0, cos(phi), -sin(phi),
              0, sin(phi),  cos(phi);

    rotMtx_Ry << cos(theta),  0, sin(theta),
              0,  1,          0,
              -sin(theta),  0, cos(theta);

    rotMtx_Rz << cos(psi), -sin(psi),   0,
              sin(psi),  cos(psi),   0,
              0,         0,   1;

    Eigen::Matrix3d rotMtxBody2Inertial = rotMtx_Rz * rotMtx_Ry * rotMtx_Rx;  // [Inertial frame] = rotMtxBody2Inertial * [Body frame]
    Eigen::Matrix3d rotMtxInertial2Body = rotMtxBody2Inertial.transpose();    //     [Body frame] = rotMtxInertial2Body * [Inertial frame]

    rotationMatrix = rotMtxInertial2Body;
}
