set (EXT_H ${EXT_H}
	${CMAKE_CURRENT_LIST_DIR}/tnt.h
	${CMAKE_CURRENT_LIST_DIR}/jama_lu.h
	${CMAKE_CURRENT_LIST_DIR}/jama_svd.h
	${CMAKE_CURRENT_LIST_DIR}/jama_eig.h
	${CMAKE_CURRENT_LIST_DIR}/jama_cholesky.h
	${CMAKE_CURRENT_LIST_DIR}/jama_qr.h
	)

set (includeDirsExt ${includeDirsExt}
	)

include_directories( includeDirsExt )

SOURCE_GROUP(ExternalSoftware\\TNT ${CMAKE_CURRENT_LIST_DIR}/tnt.*)
SOURCE_GROUP(ExternalSoftware\\JAMA ${CMAKE_CURRENT_LIST_DIR}/jama.*)
