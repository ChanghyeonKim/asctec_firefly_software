/*
 * Copyright (c) 2014, Skybotix AG, Switzerland (info@skybotix.com)
 *
 * All rights reserved.
 *
 * Redistribution and non-commercial use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the {organization} nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "svo/vi_sensor_interface.hpp"

ViSensorInterface::ViSensorInterface(uint32_t image_rate, uint32_t imu_rate)
    : computed_rectification_map_(false)
{
    StartIntegratedSensor(image_rate, imu_rate);
}

ViSensorInterface::ViSensorInterface()
    : computed_rectification_map_(false)
{
    StartIntegratedSensor(20, 200);
}

ViSensorInterface::~ViSensorInterface()
{

}

void ViSensorInterface::run(void)
{
    // set callback for completed frames
    drv_.setCameraCallback(boost::bind(&ViSensorInterface::ImageCallback, this, _1));

    // set callback for completed IMU messages
    drv_.setImuCallback(boost::bind(&ViSensorInterface::ImuCallback, this, _1));

    //Lets wait for the user to end program.
    //This is ok as program is callback-driven anyways
    boost::mutex m;
    boost::mutex::scoped_lock l(m);
    boost::condition_variable c;
//    c.wait(l);
}

void ViSensorInterface::StartIntegratedSensor(uint32_t image_rate, uint32_t imu_rate)
{
    if (image_rate > 30)
    {
        image_rate = 30;
        std::cout << "Desired image rate is too high, setting it to 30 Hz." << std::endl;
    }
    if (imu_rate > 800)
    {
        imu_rate = 800;
        std::cout << "Desired imu rate is too high, setting it to 800 Hz." << std::endl;
    }

    try
    {
        drv_.init();
    }
    catch (visensor::exceptions::ConnectionException const &ex)
    {
        std::cout << ex.what() << "\n";
        return;
    }

    // re-configure camera parameters
    drv_.setSensorConfigParam(visensor::SensorId::SensorId::CAM0, "row_flip", 0);
    drv_.setSensorConfigParam(visensor::SensorId::SensorId::CAM0, "column_flip", 0);

    drv_.setSensorConfigParam(visensor::SensorId::SensorId::CAM1, "row_flip", 1);
    drv_.setSensorConfigParam(visensor::SensorId::SensorId::CAM1, "column_flip", 1);

    drv_.setSensorConfigParam(visensor::SensorId::SensorId::CAM0, "min_coarse_shutter_width", 2);
    drv_.setSensorConfigParam(visensor::SensorId::SensorId::CAM0, "max_coarse_shutter_width", 550);

    drv_.setSensorConfigParam(visensor::SensorId::SensorId::CAM1, "min_coarse_shutter_width", 2);
    drv_.setSensorConfigParam(visensor::SensorId::SensorId::CAM1, "max_coarse_shutter_width", 550);

    drv_.startAllCameras(image_rate);
    drv_.startAllImus(imu_rate);
    vi_sensor_connected_ = true;
}

void ViSensorInterface::ImageCallback(visensor::ViFrame::Ptr frame_ptr)
{
    boost::mutex::scoped_lock lock(io_mutex_);  //lock thread as opencv does seem to have problems with multithreading
    uint32_t camera_id = frame_ptr->camera_id;
    frameQueue[camera_id].emplace_back(frame_ptr);

    if (frame_ptr->height > 0)
    {
      cv::Mat image;
      image.create(frame_ptr->height, frame_ptr->width, CV_8UC1);
      image.data = frame_ptr->getImageRawPtr();

      switch (camera_id)
      {
        case 0 :
        {
          this->image_left_ = image;
          this->image_left_flag_ = 1;
          break;
        }
        case 1 :
        {
          this->image_right_ = image;
          this->image_right_flag_ = 1;
          break;
        }
        default :
        {
          std::cout << "invalid camera id" << std::endl;
          break;
        }
      }
    }
}

void ViSensorInterface::ImuCallback(boost::shared_ptr<visensor::ViImuMsg> imu_ptr)
{
    Eigen::Vector3d gyro(imu_ptr->gyro[0], imu_ptr->gyro[1], imu_ptr->gyro[2]);
    Eigen::Vector3d acc(imu_ptr->acc[0], imu_ptr->acc[1], imu_ptr->acc[2]);
}
