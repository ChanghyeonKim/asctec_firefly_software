/* This file is autogenerated */
#ifndef TOADLET_PEEPER_H
#define TOADLET_PEEPER_H

#include <toadlet/peeper/AdaptorInfo.h>
#include <toadlet/peeper/BackableBuffer.h>
#include <toadlet/peeper/BackableVertexFormat.h>
#include <toadlet/peeper/BackablePixelBufferRenderTarget.h>
#include <toadlet/peeper/BackableTextureMipPixelBuffer.h>
#include <toadlet/peeper/BackableRenderState.h>
#include <toadlet/peeper/BackableShaderState.h>
#include <toadlet/peeper/BackableShader.h>
#include <toadlet/peeper/BackableTexture.h>
#include <toadlet/peeper/BlendState.h>
#include <toadlet/peeper/Buffer.h>
#include <toadlet/peeper/VariableBuffer.h>
#include <toadlet/peeper/VariableBufferFormat.h>
#include <toadlet/peeper/DepthState.h>
#include <toadlet/peeper/FogState.h>
#include <toadlet/peeper/IndexBuffer.h>
#include <toadlet/peeper/IndexBufferAccessor.h>
#include <toadlet/peeper/IndexData.h>
#include <toadlet/peeper/MaterialState.h>
#include <toadlet/peeper/LightState.h>
#include <toadlet/peeper/Query.h>
#include <toadlet/peeper/PixelBuffer.h>
#include <toadlet/peeper/PixelBufferRenderTarget.h>
#include <toadlet/peeper/GeometryState.h>
#include <toadlet/peeper/RasterizerState.h>
#include <toadlet/peeper/RenderDevice.h>
#include <toadlet/peeper/RenderCaps.h>
#include <toadlet/peeper/RenderState.h>
#include <toadlet/peeper/RenderTarget.h>
#include <toadlet/peeper/SamplerState.h>
#include <toadlet/peeper/Shader.h>
#include <toadlet/peeper/ShaderState.h>
#include <toadlet/peeper/Texture.h>
#include <toadlet/peeper/TextureFormat.h>
#include <toadlet/peeper/TextureFormatConversion.h>
#include <toadlet/peeper/TextureState.h>
#include <toadlet/peeper/Types.h>
#include <toadlet/peeper/VertexBuffer.h>
#include <toadlet/peeper/VertexBufferAccessor.h>
#include <toadlet/peeper/VertexData.h>
#include <toadlet/peeper/VertexFormat.h>
#include <toadlet/peeper/Viewport.h>
#include <toadlet/peeper/Version.h>
#include <toadlet/peeper/WindowRenderTargetFormat.h>

namespace toadlet{
using namespace toadlet::peeper;
}

#endif
