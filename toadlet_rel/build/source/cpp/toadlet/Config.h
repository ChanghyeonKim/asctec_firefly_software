/********** Copyright header - do not remove **********
 *
 * The Toadlet Engine
 *
 * Copyright 2009, Lightning Toads Productions, LLC
 *
 * Author(s): Alan Fischer, Andrew Fischer
 *
 * This file is part of The Toadlet Engine.
 *
 * The Toadlet Engine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * The Toadlet Engine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with The Toadlet Engine.  If not, see <http://www.gnu.org/licenses/>.
 *
 ********** Copyright header - do not remove **********/

#ifndef TOADLET_CONFIG_H
#define TOADLET_CONFIG_H

// CMake fills in this file with the proper values for our build

// Revision information
#define TOADLET_HG_REVNUMBER "-1:000000000000"
#define TOADLET_HG_DATE "Thu Jan 01 00:00:00 1970 +0000"

// Setup the platform
/* #undef TOADLET_PLATFORM_WIN32 */
/* #undef TOADLET_PLATFORM_WINCE */
#define TOADLET_PLATFORM_POSIX
/* #undef TOADLET_PLATFORM_OSX */
/* #undef TOADLET_PLATFORM_IOS */
/* #undef TOADLET_PLATFORM_ANDROID */
/* #undef ANDROID_NDK_API_LEVEL */
/* #undef TOADLET_PLATFORM_EMSCRIPTEN */

// Setup specific build options
/* #undef TOADLET_FIXED_POINT */
#define TOADLET_RTTI
#define TOADLET_EXCEPTIONS
#define TOADLET_HAS_SSE

// Optional packages
/* #undef TOADLET_HAS_GIF */
/* #undef TOADLET_HAS_JPEG */
/* #undef TOADLET_HAS_ZLIB */
/* #undef TOADLET_HAS_PNG */
/* #undef TOADLET_HAS_FFMPEG */
/* #undef TOADLET_HAS_FREETYPE */
/* #undef TOADLET_HAS_MXML */
/* #undef TOADLET_HAS_ZZIP */
/* #undef TOADLET_HAS_D3DM */
/* #undef TOADLET_HAS_D3D9 */
/* #undef TOADLET_HAS_D3D10 */
/* #undef TOADLET_HAS_D3D11 */
#define TOADLET_HAS_OPENGL
#define TOADLET_HAS_OPENAL
/* #undef TOADLET_HAS_OGGVORBIS */
/* #undef TOADLET_HAS_SIDPLAY */
/* #undef TOADLET_HAS_GME */
/* #undef TOADLET_HAS_MODPLUG */
/* #undef TOADLET_HAS_GDIPLUS */

#endif
