# Install script for directory: /home/icslkchlap/Documents/lmprojects/toadlet_rel/source/cpp/toadlet

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "required")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/toadlet" TYPE FILE FILES "/home/icslkchlap/Documents/lmprojects/toadlet_rel/source/cpp/toadlet/Types.h")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "required")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/toadlet" TYPE FILE FILES "/home/icslkchlap/Documents/lmprojects/toadlet_rel/build/source/cpp/toadlet/Config.h")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "egg")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/toadlet" TYPE DIRECTORY FILES "/home/icslkchlap/Documents/lmprojects/toadlet_rel/source/cpp/toadlet/egg" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "egg")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/toadlet" TYPE FILE FILES "/home/icslkchlap/Documents/lmprojects/toadlet_rel/build/source/cpp/toadlet/egg.h")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "egg")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/toadlet/egg" TYPE FILE FILES "/home/icslkchlap/Documents/lmprojects/toadlet_rel/build/source/cpp/toadlet/egg/Version.h")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "flick")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/toadlet" TYPE DIRECTORY FILES "/home/icslkchlap/Documents/lmprojects/toadlet_rel/source/cpp/toadlet/flick" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "flick")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/toadlet" TYPE FILE FILES "/home/icslkchlap/Documents/lmprojects/toadlet_rel/build/source/cpp/toadlet/flick.h")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "flick")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/toadlet/flick" TYPE FILE FILES "/home/icslkchlap/Documents/lmprojects/toadlet_rel/build/source/cpp/toadlet/flick/Version.h")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "hop")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/toadlet" TYPE DIRECTORY FILES "/home/icslkchlap/Documents/lmprojects/toadlet_rel/source/cpp/toadlet/hop" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "hop")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/toadlet" TYPE FILE FILES "/home/icslkchlap/Documents/lmprojects/toadlet_rel/build/source/cpp/toadlet/hop.h")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "hop")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/toadlet/hop" TYPE FILE FILES "/home/icslkchlap/Documents/lmprojects/toadlet_rel/build/source/cpp/toadlet/hop/Version.h")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "peeper")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/toadlet" TYPE DIRECTORY FILES "/home/icslkchlap/Documents/lmprojects/toadlet_rel/source/cpp/toadlet/peeper" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "peeper")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/toadlet" TYPE FILE FILES "/home/icslkchlap/Documents/lmprojects/toadlet_rel/build/source/cpp/toadlet/peeper.h")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "peeper")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/toadlet/peeper" TYPE FILE FILES "/home/icslkchlap/Documents/lmprojects/toadlet_rel/build/source/cpp/toadlet/peeper/Version.h")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "ribbit")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/toadlet" TYPE DIRECTORY FILES "/home/icslkchlap/Documents/lmprojects/toadlet_rel/source/cpp/toadlet/ribbit" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "ribbit")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/toadlet" TYPE FILE FILES "/home/icslkchlap/Documents/lmprojects/toadlet_rel/build/source/cpp/toadlet/ribbit.h")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "ribbit")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/toadlet/ribbit" TYPE FILE FILES "/home/icslkchlap/Documents/lmprojects/toadlet_rel/build/source/cpp/toadlet/ribbit/Version.h")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/toadlet" TYPE FILE FILES "/home/icslkchlap/Documents/lmprojects/toadlet_rel/build/source/cpp/toadlet/toadlet.h")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/icslkchlap/Documents/lmprojects/toadlet_rel/build/source/cpp/toadlet/egg/cmake_install.cmake")
  include("/home/icslkchlap/Documents/lmprojects/toadlet_rel/build/source/cpp/toadlet/flick/cmake_install.cmake")
  include("/home/icslkchlap/Documents/lmprojects/toadlet_rel/build/source/cpp/toadlet/hop/cmake_install.cmake")
  include("/home/icslkchlap/Documents/lmprojects/toadlet_rel/build/source/cpp/toadlet/peeper/cmake_install.cmake")
  include("/home/icslkchlap/Documents/lmprojects/toadlet_rel/build/source/cpp/toadlet/ribbit/cmake_install.cmake")

endif()

