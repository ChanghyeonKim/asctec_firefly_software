##How to install?
Provided with this repo is a CMakeLists.txt file, which you can use to directly compile the code as follows:

```bash
hg init
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
sudo make install
```