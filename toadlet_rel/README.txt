 *** The Toadlet Engine README ***

See the INSTALL.txt file for instructions
on building toadlet for your platform of
interest.

For more information about toadlet, check 
the website: 

  http://code.google.com/p/toadlet
