This is an OpenCV & Eigen3 based C++ implementation of libviso2


##Dependency
```bash
$ sudo apt-get install libopencv-dev
$ sudo apt-get install libeigen3-dev
$ sudo apt-get install libboost-all-dev
```


##Algorithm
Uses Geiger's libviso2 algorithm for geometric (reprojection) minimization.
More details are available [here as a libviso2](http://www.cvlibs.net/software/libviso/).


##Demo Video
[![Demo video](http://share.gifyoutube.com/Ke1ope.gif)](http://www.youtube.com/watch?v=homos4vd_Zs)


##Requirements
OpenCV 2.4.8 / Eigen3


##How to compile?
Provided with this repo is a CMakeLists.txt file, which you can use to directly compile the code as follows:
```bash
mkdir build
cd build
cmake ..
make -j2
```


##How to run? 
After compilation, in the build directly, type the following:
```bash
./libviso2
```


##Before you run
In order to run this algorithm, you need to have either your own data, 
or else the sequences from [KITTI's Visual Odometry Dataset](http://www.cvlibs.net/datasets/kitti/eval_odometry.php).
In order to run this algorithm on your own data, you must modify the intrinsic calibration parameters in the code.


##Performance
![Results on the KITTI VO Benchmark](http://avisingh599.github.io/images/visodo/2K.png)


##Contact
For any queries, contact: pjinkim1215@gmail.com


##License
MIT
WTF
