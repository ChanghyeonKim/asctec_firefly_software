#include "../tnt.h"

using namespace std;
using namespace TNT;

Array2D<double> function(Array2D<double> const &array)
{
	Array2D<double> array2(array.dim2(),array.dim1());

	int i;
	int j;
	for( i=0; i<array.dim1(); i++)
		for(j=0; j<array.dim2(); j++)
			array2[j][i] = array[i][j];
	
	return array2;
}
int main()
{
	Array2D<double> array(12,1);
	cout << array.dim1() << " x " << array.dim2() << endl;

	Array2D<double> array2 = function(array);
	cout << array2.dim1() << " x " << array2.dim2() << endl;
}
