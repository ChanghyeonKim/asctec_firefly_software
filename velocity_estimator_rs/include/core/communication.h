#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h> // memset()
#include <unistd.h>
#include <arpa/inet.h> // struct sockaddr_in
#include <sys/types.h> // socket(), bind()
#include <sys/socket.h> // socket(), bind()
#include <iostream>

#include <fcntl.h>  // File control definitions
#include <termios.h> // POSIX terminal control definitionss
#include <time.h>

#include <sstream>
#include <cstring>
#include <vector>
#include "core/Common.h"
#include "TNT/tnt.h"
#include "TNT_Utils/TNT_Utils.h"

using namespace std;
using namespace toadlet::egg;
using namespace toadlet;

void onBtnConnect_clicked(const string& IP_address, const int& num_port);

void onSenddata(const TNT::Array2D<double>& Est_data);

bool sendUDP(tbyte* data, int size);





#endif /* _COMMUNICATION_H_ */
