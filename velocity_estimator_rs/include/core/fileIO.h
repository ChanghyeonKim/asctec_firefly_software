#ifndef _FILEIO_H_
#define _FILEIO_H_

#include <string>
#include <eigen3/Eigen/Dense>
#include <opencv2/opencv.hpp>



void saveEigenMatrix(const char *_Filename, const Eigen::MatrixXd& inputMatrix);

void saveMatImage(const char *_Filename, const cv::Mat& inputImage);






















#endif /* _FILEIO_H_ */
