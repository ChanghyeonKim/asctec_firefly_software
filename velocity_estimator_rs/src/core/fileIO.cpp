#include <cstdio>
#include <fstream>
#include <opencv2/highgui/highgui_c.h>
#include "core/fileIO.h"



void saveEigenMatrix(const char *_Filename, const Eigen::MatrixXd& inputMatrix)
{
    // initialize text stream
    std::ofstream matrixTextFile_opener(_Filename);


    // save the inputMatrix
    if (matrixTextFile_opener.is_open())
    {
        matrixTextFile_opener << inputMatrix << '\n';
        matrixTextFile_opener.close();
        std::cout << "File save success in saveEigenMatrix!" << std::endl;
    }
    else
    {
        std::cout << "File open error in saveEigenMatrix!" << std::endl;
    }
}



void saveMatImage(const char *_Filename, const cv::Mat& inputImage)
{
    // initialize text stream
    std::ofstream matTextFile_opener(_Filename);


    // save the inputImage
    if (matTextFile_opener.is_open())
    {
        for(size_t i = 0; i < inputImage.rows; i++)
        {
            for(size_t j = 0; j < inputImage.cols; j++)
            {
                matTextFile_opener << inputImage.at<double>(i,j) << "\t";
            }
            matTextFile_opener << std::endl;
        }
        matTextFile_opener.close();
        std::cout << "File save success in saveMatImage!" << std::endl;
    }
    else
    {
        std::cout << "File open error in saveMatImage!" << std::endl;
    }
}
