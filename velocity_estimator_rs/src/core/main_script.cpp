#include "core/fileIO.h"
#include "core/Liegroup_operation.h"
#include "core/timer.h"
#include "core/velocityEstimatorRealsense.h"



int main(void)
{
  // set most important velocity estimator parameters (for a full parameter list, look at: velocityEstimatorRealsense.h)
  velocityEstimatorRealsense::parameters param;

  param.calib.fx = 620.608832234754;
  param.calib.fy = 619.113993685335;
  param.calib.cx = 323.902900972212;
  param.calib.cy = 212.418428046497;

  param.homography_RANSAC.threshold = 1.5;

  param.WiFi_setup.IP_address = "192.168.1.22";
  param.WiFi_setup.num_port = 13120;

  param.debug_flags.print_current_velocity = true;
  param.debug_flags.print_current_image = false;
  param.debug_flags.outdoor_flag = true;


  // initialize velocity estimator
  velocityEstimatorRealsense velocity_estimator(param);
  velocity_estimator.runThread();


  return 0;
}
