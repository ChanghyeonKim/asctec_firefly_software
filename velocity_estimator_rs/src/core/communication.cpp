#include "core/communication.h"


using namespace TNT;
using namespace std;


bool mIsConnected;
string mIP;
int mPort;
toadlet::Socket::ptr mSocketUDP;
toadlet::uint64 mTimeMS;
toadlet::egg::Mutex mMutex_socketUDP;


void onBtnConnect_clicked(const string& IP_address, const int& num_port)
{
	mIP = IP_address;
	mPort = num_port;
	std::cout << "Connecting to Host at " << mIP << ":" << mPort << " ... ";
	try
	{
		if(mSocketUDP != NULL)
		{
			mSocketUDP->close();
		}
		mSocketUDP = Socket::ptr(Socket::createUDPSocket());
		mSocketUDP->bind(mPort);
		mSocketUDP->setBlocking(false);
		std::cout << "done." << std::endl;
	}
	catch (...)
	{
		if(mSocketUDP != NULL)
		{
			mSocketUDP->close();
		}
		mSocketUDP = NULL;
		std::cout << "Failed to connect" << std::endl;
	}
}



void onSenddata(const TNT::Array2D<double>& Est_data)
{
	if(mSocketUDP != NULL)
	{
		Packet p;
		p.type = COMM_EST;
		p.time = 10;

		p.dataFloat.resize(Est_data.dim1());
		p.dataFloat[0] = Est_data[0][0];
		p.dataFloat[1] = Est_data[1][0];
		p.dataFloat[2] = Est_data[2][0];
		p.dataFloat[3] = Est_data[3][0];
		p.dataFloat[4] = Est_data[4][0];
		p.dataFloat[5] = Est_data[5][0];
		p.dataFloat[6] = Est_data[6][0];
		p.dataFloat[7] = Est_data[7][0];
		p.dataFloat[8] = Est_data[8][0];

		Collection<tbyte> buff;
		p.serialize(buff);
		sendUDP(buff.begin(), buff.size());
	}
}



bool sendUDP(tbyte* data, int size)
{
	if(mSocketUDP == NULL)
	{
		return false;
	}

	try
	{
		mMutex_socketUDP.lock();
		mSocketUDP->sendTo(data,size,toadlet::egg::Socket::stringToIP(String(mIP.c_str())),mPort);
		mMutex_socketUDP.unlock();
	}
	catch (...)
	{
		cout << "Send failure" << endl;
		mMutex_socketUDP.lock();
		mSocketUDP->close();
		mSocketUDP = NULL;
		mMutex_socketUDP.unlock();
		mIsConnected = false;
		cout << "send udp catch" << endl;
		return false;
	}

	return true;
}
