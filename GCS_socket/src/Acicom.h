#ifndef ACICOM
#define ACICOM

#include <iostream>
#include <mutex>
#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h>  // File control definitions
#include <termios.h> // POSIX terminal control definitionss
#include <vector>

#include "../Asctec_aci/asctecCommIntf.h"

using namespace std;

    class AciCom
    {
    public:
        AciCom();
        virtual ~AciCom();
        void initialize();

    public:
        static int fd;
        static int16_t cx ;
        static int16_t cy ;
        static int16_t cz ;
        static int16_t cvx ;
        static int16_t cvy ;
        static int16_t cvz ;
        static int16_t cyaw;

        static int16_t dx ;
        static int16_t dy ;
        static int16_t dz ;
        static int16_t dvx;
        static int16_t dvy ;
        static int16_t dvz ;
        static int16_t dyaw; // control command

        static uint8_t motor1;
        static uint8_t motor2;
        static uint8_t motor3;
        static uint8_t motor4;
        static uint8_t motor5;
        static uint8_t motor6;

        static int32_t angle_pitch;
        static int32_t angle_roll;

        static int16_t droll;
        static int16_t dpitch;

        static uint32_t x_accuracy;
        static uint32_t y_accuracy;
        static uint32_t z_accuracy;
        static uint32_t num_sat;


        static int32_t gps_x;
        static int32_t gps_y;
        static int32_t gps_z;
        static int32_t est_dx;
        static int32_t est_dy;
        static int32_t est_z;
        static int32_t est_dz;

        static unsigned char var_getted;
        static unsigned char ctrl_mode ;
        static unsigned char ctrl_enabled ;
        static unsigned char disable_motor_onoff_by_stick ;
        static unsigned char cmd_ready ;
    protected:
        std::mutex mMutex_send;

    };


void transmit(void* byte, unsigned short cnt);
void varListUpdateFinished();
void cmdListUpdateFinished();

#endif // ACICOM
