#include "SystemModelQuadrotor.h"
#include "TNT/jama_lu.h"
#include "constants.h"
#include "TNT_Utils/TNT_Utils.h"

namespace ICSL{
namespace Quadrotor{

    using namespace std;
    using namespace TNT;

	const string SystemModelQuadrotor::StateNames[] = {"x",	"y", "z", "Roll", "Pitch", "Yaw",
														"dx", "dy", "dz", "dr", "dp", "dy"};
	const string SystemModelQuadrotor::ActuatorNames[] = {"NorthMotor", "EastMotor", "SouthMotor", "WestMotor"};

    SystemModelQuadrotor::SystemModelQuadrotor()
    {
        mIsSimulated = false;
        mTimeOfLastUpdate = mTmr.getCurTimeMS();
        mSimState = Array2D<double>(12,1,0.0);
    //	mName = "ssk";
        mMass = 0;
        mCurState = Array2D<double>(12,1,0.0);
        mSimStartState= Array2D<double>(12,1,0.0);(12,1,0.0);

    }

    void SystemModelQuadrotor::onTelemetryUpdated(TelemetryViconDataRecord const &rec)
    {
        if(mIsSimulated && strcmp(rec.objectID.c_str(), "sims")==0)
        {
            mMutex_curPos.lock();
    //			double dt = (mTmr.getCurTimeMS()-mTimeOfLastUpdate)/1000.0;
            double dt = 0.01;
            mTimeOfLastUpdate= mTmr.getCurTimeMS();

            // need to rotate from Vicon coords to the baseline quadrotor coords
            // i.e. quad state at 0 roll, pitch, yaw
            Array2D<double> rotVicon2Quad = createRotMat(0,PI);
            Array2D<double> rotQuad2Vicon = transpose(rotVicon2Quad);
            Array2D<double> quadState = mSimState.copy();
            mMutex_DataAccess.unlock();

            mCurPos.inject(stackVertical(submat(mSimState,0,2,0,0),submat(mSimState,6,8,0,0)));
            mMutex_curPos.unlock();
        }
        else
        {
            if(strcmp(rec.objectID.c_str(), mName.c_str())==0)
            {
                if(!rec.invalidRead)
                {
                    mMutex_curPos.lock();
                    Array2D<double> state = convertTelemViconToState(rec);
                    mCurPos = stackVertical(submat(state,0,2,0,0),submat(state,6,8,0,0));
                    mMutex_curPos.unlock();
                }
            }
        }
    }

    void SystemModelQuadrotor::estimateFullState()
    {
        Array2D<double> state(12,1,0.0);
        mMutex_curPos.lock(); mMutex_DataAccess.lock(); mMutex_buffers.lock();
        assignRows(state,0,2,submat(mCurPos,0,2,0,0));
        assignRows(state,6,8,submat(mCurPos,3,5,0,0));

        mCurState = estimateState(state,mStateBuffer,mDeltaT);
        mStateBuffer.push_back(mCurState.copy());

        if(mIsSimulated == false)
        {
            mSimState.inject(mCurState);
            mTimeOfLastUpdate = mTmr.getCurTimeMS();
        }
        else // need this since the high speed time updates don't work well with a software timer // I don't know what this comment means TRR
        {
            assignRows(mCurState,0,2,submat(mSimState,0,2,0,0));
            assignRows(mCurState,6,8,submat(mSimState,6,8,0,0));
        }
        mMutex_buffers.unlock(); mMutex_DataAccess.unlock(); mMutex_curPos.unlock();
    }

    void SystemModelQuadrotor::resetState()
    {
        mSimState = mSimStartState.copy();
    }

    Array2D<double> SystemModelQuadrotor::convertTelemViconToState(TelemetryViconDataRecord const &curRec)
    {
        Array2D<double> state(12,1,0.0);
        if(!curRec.isEmpty)
        {
            state[0][0] = curRec.roll; state[1][0] = curRec.pitch; state[2][0] = curRec.yaw;
            state[6][0] = curRec.x; state[7][0] = curRec.y; state[8][0] = curRec.z;
        }

        return state;
    }

    Array2D<double> SystemModelQuadrotor::estimateState(TelemetryViconDataRecord const &curRec, list<Array2D<double> > const & stateBuffer, double deltaT)
    {
        Array2D<double> state = convertTelemViconToState(curRec);
        return estimateState(state, stateBuffer, deltaT);
    }

    /*!
    * Derivative filters taken from http://www.holoborodko.com/pavel/numerical-methods/numerical-derivative/smooth-low-noise-differentiators/
    *
    * \param state a 12 element state vector of the form
    *	[roll pitch yaw rollRate pitchRate yawRate x y z xRate y Rate zRate]
    * where all the rates are ignored
    * \param stateBuffer vector of the estimated full states(from oldest to newest)
    * \param deltaT time step size
    * \return the 12 element state vector with filtered velocity estimates
    * */
    Array2D<double> SystemModelQuadrotor::estimateState(Array2D<double> state, list<Array2D<double> > const & stateBufferOrig, double deltaT)
    {
        // filterCoeff[0][0] is coefficient for newest record
        // filterCoeff[end][0] is coefficient for oldest record
        Array2D<double> filterCoeff(min((int)(stateBufferOrig.size()+1),8),1);
        switch(filterCoeff.dim1())
        {
            case 1:
                filterCoeff[0][0] = 0;
                break;
            case 2:
                filterCoeff[0][0] = 1; filterCoeff[1][0] = -1;
                filterCoeff = 1.0/deltaT*filterCoeff;
                break;
            case 3:
                filterCoeff = Array2D<double>(2,1); // there is no entry for this so just use the 2 point average
                filterCoeff[0][0] = 1; filterCoeff[1][0] = -1;
                filterCoeff = 1.0/deltaT*filterCoeff;
                break;
            case 4:
                filterCoeff[0][0] = 2; filterCoeff[1][0] = -1; filterCoeff[2][0] = -2; filterCoeff[3][0] = 1;
                filterCoeff = 1.0/2.0/deltaT*filterCoeff;
                break;
            case 5:
                filterCoeff[0][0] = 7; filterCoeff[1][0] = 1; filterCoeff[2][0] = -10; filterCoeff[3][0] = -1;
                filterCoeff[4][0] = 3;
                filterCoeff = 1.0/10.0/deltaT*filterCoeff;
                break;
            case 6:
                filterCoeff[0][0] = 16; filterCoeff[1][0] = 1; filterCoeff[2][0] = -10; filterCoeff[3][0] = -10;
                filterCoeff[4][0] = -6; filterCoeff[5][0] = 9;
                filterCoeff = 1.0/28.0/deltaT*filterCoeff;
                break;
            case 7:
                filterCoeff[0][0] = 12; filterCoeff[1][0] = 5; filterCoeff[2][0] = -8; filterCoeff[3][0] = -6;
                filterCoeff[4][0] = -10; filterCoeff[5][0] = 1; filterCoeff[6][0] = 6;
                filterCoeff = 1.0/28.0/deltaT*filterCoeff;
                break;
            case 8:
                filterCoeff[0][0] = 22; filterCoeff[1][0] = 7; filterCoeff[2][0] = -6; filterCoeff[3][0] = -11;
                filterCoeff[4][0] = -14; filterCoeff[5][0] = -9; filterCoeff[6][0] = -2; filterCoeff[7][0] = 13;
                filterCoeff = 1.0/60.0/deltaT*filterCoeff;
                break;
            default:
                throw("Unknown filter size: " + filterCoeff.dim1());
        }

        // modify angle states so they are all in the same range in case
        // they have wrapped around
        list<Array2D<double> > stateBuffer;
        list<Array2D<double> >::const_iterator iter_state = stateBufferOrig.end();
        for(int i=0; i<filterCoeff.dim1()-1; i++)
        {
            iter_state--; // iter_state starts out 1 position beyond the last data so increment first
            stateBuffer.push_front((*iter_state).copy());
        }

        Array2D<double> stateTemp;
        for(int st=0; st<3; st++)
        {
            iter_state = stateBuffer.end();
            for(int j=1; j<filterCoeff.dim1(); j++)
            {
                iter_state--; // iter_state starts out 1 position beyond the last data so increment first
                stateTemp = *iter_state;
                while(state[st][0] - stateTemp[st][0] > PI) stateTemp[st][0] += 2*PI;
                while(stateTemp[st][0] - state[st][0] > PI) stateTemp[st][0] -= 2*PI;
            }
        }

        state[3][0] = filterCoeff[0][0]*state[0][0];
        state[4][0] = filterCoeff[0][0]*state[1][0];
        state[5][0] = filterCoeff[0][0]*state[2][0];
        state[9][0] = filterCoeff[0][0]*state[6][0];
        state[10][0] = filterCoeff[0][0]*state[7][0];
        state[11][0] = filterCoeff[0][0]*state[8][0];

        iter_state = stateBuffer.end();
        for(int i=1; i<filterCoeff.dim1(); i++)
        {
            stateTemp = *(--iter_state); // iter_state starts out 1 position beyond the last data
            for(int st=3; st<6; st++)
                state[st][0] += filterCoeff[i][0]*stateTemp[st-3][0];
            for(int st=9; st<12; st++)
                state[st][0] += filterCoeff[i][0]*stateTemp[st-3][0];
        }

        return state.copy();
    }

    void SystemModelQuadrotor::constrainVector(Array2D<double> &v, Array2D<double> const &vMin, Array2D<double> const &vMax)
    {
        for(int i=0; i<v.dim1(); i++)
            for(int j=0; j<v.dim2(); j++)
                v[i][j] = min(vMax[i][j],max(vMin[i][j],v[i][j]));
    }
}
}






