#pragma warning(disable : 4996)
#include <string>
#include <vector>
#include <stdlib.h>

#include "mxml.h"
#include "MeasurementSystem.h"
#include "TNT/tnt.h"

#include "vicon_sdk/Linux/Client.h"

#ifndef CLASS_MEASUREMENTSYSTEM_VICON
	#define CLASS_MEASUREMENTSYSTEM_VICON

/*
 Class definition for the Vicon camera position measurement system
*/
namespace ICSL
{
using namespace std;
using namespace TNT;
class MeasurementSystem_Vicon : public MeasurementSystem
{
	public:
		////////////////////// Functions inherited from MeasurementSystem
		MeasurementSystem_Vicon();
		virtual ~MeasurementSystem_Vicon();

		ConnectionStatus connectToSource();
		ConnectionStatus disconnectFromSource();
		void setSourceID( string sourceID );
		string getSourceID();

		void updateMeasurements();
		vector<TrackedObject> updateMeasurements2();
		TrackedObject* getTrackedObject(int i);

		void setSimulationMode(bool mode);

		int addTrackingObject(string name, int nMarkers, int originMarkerIndex);
		int addTrackingObject(string name, vector<string> markerNames, int originMarkerIndex);

		void setDataLog(mxml_node_t *xml){mDataLogger = xml;};
		void startReplay(mxml_node_t *xml){mReplayLog = xml; mLogIndexReplay = -1; mReplaying = true;};

		////////////////////// Functions specific to this implementation

	protected:
		string mSourceID;
		ViconDataStreamSDK::CPP::Client mClient;
		mxml_node_t *mDataLogger, *mLogIndexNode, *mLogObjectNode, *mReplayLog;
		int mLogIndexSave, mLogIndexReplay;
		bool mReplaying;

		void startNewLogEntry();
		void startObjectLog(string name);
		void logMarkerPosition(string name, TNT::Array2D<double> pos, bool isOccluded);
		void loadObjectReplay(mxml_node_t *xml, TrackedObject *object);
};
}
#endif
