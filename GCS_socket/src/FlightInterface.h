#ifndef QUADROTOR_FLIGHTINTERFACE
#define QUADROTOR_FLIGHTINTERFACE
#pragma warning(disable : 4996)

#include <iostream>
#include <exception>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
#include <cstring>

#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h>  // File control definitions
#include <termios.h> // POSIX terminal control definitionss
#include <time.h>

#include <QObject>
#include <QMainWindow>
#include <QtGui>
#include <QShortcut>
#include <QMetaType>
#include <QButtonGroup>

#include "TNT/tnt.h"
#include "constants.h"
#include "Timer.h"
#include "TNT_Utils/TNT_Utils.h"

#include "TelemetryVicon.h"

#include "QuadrotorInterface.h"
#include "ui_FlightInterface.h"
#include "quadrotorConstants.h"
#include "../Asctec_aci/asctecCommIntf.h"
#include "Acicom.h"
#include "Common.h"

#include <arpa/inet.h> // struct sockaddr_in
#include <sys/types.h> // socket(), bind()
#include <sys/socket.h> // socket(), bind()

using namespace std;
using namespace toadlet::egg;
using namespace toadlet;

namespace ICSL{
namespace Quadrotor{

    class FlightInterface : public QMainWindow, protected Ui::FlightInterface
    {
        Q_OBJECT

        public:
            explicit FlightInterface(QWidget *parent=0);
            virtual ~FlightInterface();
            void initialize();
            void run();
            void saveData(string dir, string filename);
            void setDeltaT(double dt);
            void onSendMass();


        protected slots:
            void endModechange();
            void updateDisplay();
            void pathplanner();
            void onBtnBeginTracking_clicked();
            void onBtnQuit_clicked();
            void onBtnClearBuffers_clicked();
            void onIncreaseHeight();
            void onDecreaseHeight();
            void onMoveLeft();
            void onMoveRight();
            void onMoveForward();
            void onMoveBackward();
            void pollData();
            void onBtnConnect_clicked();

        protected:
            bool ini_flag;
            TNT::Array2D<double> des_path,btnState,des_ini,VEstState,VEstVelState;;
            double mDeltaT;

            QTimer *mTmrComm,*mTmrDisp, *mTmrPath;

            void wifi_udp_data_transmit(const int sock,const struct sockaddr_in server_addr, const float t, const float r, const float p, const float y, const float _x, const float _y, const float _z);
            void wifi_udp_setting_server(int& sock, struct sockaddr_in &server_addr, const char* server_ip, const int server_port);
            void wifi_udp_data_receive(const int sock,struct sockaddr_in &server_addr, struct sockaddr_in &client_addr, std::vector<float> &result);

            bool mst,emode,grasp_flag, o1d_flag;
            double inix, iniy,r_1, ini_ox,ini_oy;
            double	mArm1xl, mArm1zl, mArm1xr, mArm1zr;
            double	mArm2xl, mArm2zl, mArm2xr, mArm2zr;
            double	mArmOffsetZ;
            unsigned long mStartTimeUniverseMS, mProfileStartTime, mProfileTrackingTime;

            toadlet::egg::Mutex mMutex_desiredAccess;

            TelemetryVicon mTelemVicon;
            ICSL::Timer mTmr;
            Timer mTime;
            FlightMode mFlightMode;
            QShortcut *mScBeginTracking, *mScQuit;
            QShortcut *mScIncreaseHeight, *mScDecreaseHeight, *mScMoveLeft, *mScMoveRight, *mScMoveForward, *mScMoveBackward, *mScClearBuff;
            QShortcut *mEmodechange;

            QuadrotorInterface* mQuads;

            bool mIsConnected;

            void pollUDP();

            bool sendUDP(tbyte* data, int size);
            int receiveUDP(Socket::ptr socket, tbyte* data, int size);
            bool receivePacket(Socket::ptr socket, Packet &pck, int size);

            void pingOnBoardCom();
            bool isConnected(){return mIsConnected;};

            string mIP;
            int mPort;
            Socket::ptr mSocketUDP;
            toadlet::uint64 mTimeMS;
            toadlet::egg::Mutex mMutex_socketUDP;
    };
}
}
#endif
