#ifndef CLASS_SYSTEMCONTROLLERFEEDBACKLIN
#define CLASS_SYSTEMCONTROLLERFEEDBACKLIN
#pragma warning(disable : 4996)
#include <fstream>
#include <exception>
#include <string>
#include <mxml.h>

#include "constants.h"
#include "SystemModelQuadrotor.h"

#include "TNT/tnt.h"
#include "TNT/jama_svd.h"
#include "Acicom.h"

namespace ICSL{
namespace Quadrotor{
    using namespace std;
    class SystemControllerFeedbackLinException : public exception
    {
        public:
            SystemControllerFeedbackLinException(){mMessage = "SystemControllerFeedbackLin exception occurred";};
            SystemControllerFeedbackLinException(string msg){mMessage = msg;};
            ~SystemControllerFeedbackLinException() throw(){};
            const char* what() const throw() {return mMessage.c_str();};

        protected:
            string mMessage;
    };

    class SystemControllerFeedbackLin
    {
        public:

            SystemControllerFeedbackLin();
            virtual ~SystemControllerFeedbackLin();
            void setDesiredState(TNT::Array2D<double> x);
            void setinitialState(TNT::Array2D<double> x);
            void setDeltaT(double dt){mDeltaT = dt;}
            void setCurState(TNT::Array2D<double> state1){ mMutex_cur.lock(); mCurState = state1.copy(); mMutex_cur.unlock(); }
            TNT::Array2D<double> getDesiredState();
            TNT::Array2D<double> calcControl();
            double hcx, hcz;

        protected:

            double mDeltaT;

            TNT::Array2D<double> mCurState,mDesiredState,mDend,miniState;


            toadlet::egg::Mutex mMutex_des,mMutex_cur,mMutex_ini;

            static double constrain(double val, double minVal, double maxVal)
            { return min(maxVal, max(minVal, val)); }
    };
}
}
#endif
