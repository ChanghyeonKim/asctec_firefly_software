#include "TNT/tnt.h"
#include "FlightInterface.h"

using namespace TNT;
using namespace std;

namespace ICSL{
    namespace Quadrotor{

        FlightInterface::FlightInterface(QWidget *parent){

            setupUi(this);

            mTmrDisp=new QTimer(this);
            mTmrPath=new QTimer(this);
            mTmrComm = new QTimer(this);

        }

        FlightInterface::~FlightInterface()
        {
            if(mTmrDisp != NULL)
                delete mTmrDisp;

            if(mTmrPath != NULL)
                delete mTmrPath;

            if(mTmrComm != NULL)
                delete mTmrComm;

            mTelemVicon.stopMonitor();
            mTelemVicon.disconnect();

            delete mQuads;
            delete mScClearBuff;
            delete mScIncreaseHeight;
            delete mScDecreaseHeight;
            delete mScMoveLeft;
            delete mScMoveRight;
            delete mScMoveForward;
            delete mScMoveBackward;
            delete mEmodechange;

        }

        void FlightInterface::initialize()
        {
            connect(btnBeginTracking, SIGNAL(clicked()), this, SLOT(onBtnBeginTracking_clicked()));
            connect(btnQuit, SIGNAL(clicked()), this, SLOT(onBtnQuit_clicked()));
            connect(btnConnect,SIGNAL(clicked()),this,SLOT(onBtnConnect_clicked()));

            mScIncreaseHeight = new QShortcut(Qt::Key_PageUp, this);
            mScDecreaseHeight = new QShortcut(Qt::Key_PageDown, this);
            mScMoveLeft = new QShortcut(Qt::Key_Left, this);
            mScMoveRight = new QShortcut(Qt::Key_Right, this);
            mScMoveForward = new QShortcut(Qt::Key_Up, this);
            mScMoveBackward = new QShortcut(Qt::Key_Down, this);
            mScClearBuff = new QShortcut(Qt::Key_C, this);

            mEmodechange=new  QShortcut(Qt::Key_S, this);


            connect(mScIncreaseHeight,SIGNAL(activated()), this, SLOT(onIncreaseHeight()));
            connect(mScDecreaseHeight,SIGNAL(activated()), this, SLOT(onDecreaseHeight()));
            connect(mScMoveLeft,SIGNAL(activated()), this, SLOT(onMoveLeft()));
            connect(mScMoveRight,SIGNAL(activated()), this, SLOT(onMoveRight()));
            connect(mScMoveForward,SIGNAL(activated()), this, SLOT(onMoveForward()));
            connect(mScMoveBackward,SIGNAL(activated()), this, SLOT(onMoveBackward()));
            connect(mEmodechange,SIGNAL(activated()), this, SLOT(endModechange()));

            connect(mScClearBuff,SIGNAL(activated()), this, SLOT(onBtnClearBuffers_clicked()));

            connect(mTmrComm,SIGNAL(timeout()),this,SLOT(pollData()));
            connect(mTmrDisp,SIGNAL(timeout()),this,SLOT(updateDisplay()));
            connect(mTmrPath,SIGNAL(timeout()),this,SLOT(pathplanner()));



            des_path = Array2D<double>(12,1,0.0);
            btnState = Array2D<double>(3,1,0.0);
            des_ini = Array2D<double>(4,1,0.0);
            VEstState = Array2D<double>(6,1,0.0);
            VEstVelState = Array2D<double>(6,1,0.0);

            emode=false; //tracking tracking mode when command mode
            ini_flag=false; //initial state of desired end-effector trajectory
            o1d_flag=true;

            mQuads=new QuadrotorInterface();
            mFlightMode = QUAD_IDLE;
            cout << "Initializing quad ... " << endl;

            mQuads->initialize();

            //string quadname = "quad"+QString::number(i).toStdString();

            if(mQuads->setName("lmFirefly"))
            //if(mQuads[i]->setName("quad"+QString::number(i).toStdString()))
                cout << " success." << endl;
            else
            {
                mQuads->setName("Unk" + QString::number(0).toStdString());
            }

            cout<<"Connecting to Vicon..."<<endl;
            try
            {
                mTelemVicon.setOriginPosition(Array2D<double>(3,1,0.0));
                mTelemVicon.initializeMonitor();

                if(mTelemVicon.connect("192.168.0.27")==false)
                //if(mTelemVicon.connect("localhost:801") == false)
                {
                    mQuads->setSimulated(true);
                    mQuads->setDeltaT(0.025); // need something here
                } //if
            } //try
            catch(const TelemetryViconException& ex)	{ cout << "Failure" << endl; throw(ex); }
            cout << "Success" << endl;

            mTelemVicon.addTrackedQuadrotor(mQuads->getName());
            cout<<mQuads->getName()<<endl;
            mTelemVicon.addListener(mQuads->getDynamicModel());

            layQuadA->addWidget(mQuads);

        }

        void FlightInterface::run()
        {
            mTelemVicon.startMonitor();
            mStartTimeUniverseMS = mTmr.getCurTimeMS();
            mQuads->syncStartTime(mStartTimeUniverseMS);

            mTmrDisp->start(100);
            mTmrPath->start(25);
            mTmrComm->start(15);
        }

        void FlightInterface::pollData()
        {
            if(mSocketUDP != NULL)
            {
                pollUDP();
            }
            pingOnBoardCom();

        }


        void FlightInterface::pathplanner()
        {


            des_path= Array2D<double>(12,1,0.0);
            double att_flag=1;

            double t = (mTmr.getCurTimeMS()-mProfileTrackingTime)*0.001;
            mMutex_desiredAccess.lock();

            double pp=0.1;

            if (emode){

                if (t<41 && t>0.05){

                    des_path = mQuads->getDesiredState().copy();

                    des_path[0][0]=0.0*sin(pp*PI*t)+mQuads->inix;
                    des_path[6][0]=0.0*pp*PI*cos(pp*PI*t); //x

                    des_path[1][0]=0.3*cos(pp*PI*t)+(-0.3+mQuads->iniy);;
                    des_path[7][0]=-0.3*pp*PI*sin(pp*PI*t); //y

                    // des_path[2][0]=0.45;
                    des_path[8][0]=0; //z

                    mQuads->setDesiredState(des_path);

                }
                else{
                    des_path = mQuads->getDesiredState().copy();
                    mQuads->setDesiredState(des_path);
                }

            }
            else{

                des_path = mQuads->getDesiredState().copy();

                mQuads->setDesiredState(des_path);

            }

            mMutex_desiredAccess.unlock();

            mQuads->startrun();
        }


        void FlightInterface::setDeltaT(double dt)
        {
            mDeltaT = dt;
            mQuads->setDeltaT(dt);
        }

        // save data
        void FlightInterface::saveData(string dir, string filename)
        {
            string subDir = dir+"/Firefly"+QString::number(0).toStdString();
            mQuads->saveData(subDir, filename);
        }

        // slots
        void FlightInterface::updateDisplay()
        {
            lblRunTime->setText(QString::number((int)((mTmr.getCurTimeMS()-mStartTimeUniverseMS)*0.001)));
            mQuads->updateDisplay();
        }

        void FlightInterface::onBtnConnect_clicked()
        {
            if(btnConnect->text() == "Connect")
            {
                mIP = "192.168.43.93";
                mPort = 13120;
                cout << "Connecting to Host at " << mIP << ":" << mPort << " ... ";
                try
                {
                    if(mSocketUDP != NULL)
                        mSocketUDP->close();
                    mSocketUDP = Socket::ptr(Socket::createUDPSocket());
                    mSocketUDP->bind(mPort);
                    mSocketUDP->setBlocking(false);
                    cout << "done." << endl;
                    btnConnect->setText("Disconnect");
                    //                if(mSocketTCP != NULL)
                    //                {
                    //                    mTmr.sleepMS(10);
                    //                    pollUDP();
                    //                }
                }
                catch (...)
                {

                    if(mSocketUDP != NULL)
                        mSocketUDP->close();
                    mSocketUDP = NULL;
                    cout << "Failed to connect" << endl;
                }
            }
            else
            {

                if(mSocketUDP != NULL)
                {
                    mMutex_socketUDP.lock();
                    mSocketUDP->close();
                    mSocketUDP = NULL;
                    mMutex_socketUDP.unlock();
                }
                btnConnect->setText("Connect");
            }

        }

        void FlightInterface::pollUDP()
        {
            mMutex_socketUDP.lock();
            int i=0;
            if(mSocketUDP == NULL)
            {
                cout << "mSocketUDP is null" << endl;
                mMutex_socketUDP.unlock();
                return;
            }

            mSocketUDP->setBlocking(true);
            while(mSocketUDP != NULL && mSocketUDP->pollRead(0))
            {
                int size=256;
                Packet pck;
                if(receivePacket(mSocketUDP, pck, size))
                {
                    //                if(pck.time >= mTimeMS)
                    //                    mTimeMS = pck.time;
                    //                else if(abs((int)(pck.time - mTimeMS)) > 1000)
                    //                {
                    //                    cout << "Old packet so assume this is a new starting point. dt = " << pck.time-mTimeMS << endl;
                    //                    mTimeMS = pck.time;
                    //                }
                    //                else
                    //                {
                    //                    cout << "Ignoring old packet" <<endl;
                    //                    continue;
                    //                }
                    //
                    //                if(pck.time < mTimeMS)
                    //                    cout << "Why am I here?" << endl;
                    try
                    {
                        switch(pck.type)
                        {
                            case COMM_POS:
                                {
                                    for(int ii=0;ii<6;ii++){
                                        VEstState[ii][0]= pck.dataFloat[ii];
                                    }

                                    mQuads->setVEstState(VEstState.copy());
                                }
                                break;
                            case COMM_VEL:
                                {
                                    for(int i=0;i<6;i++){
                                        VEstVelState[i][0]= pck.dataFloat[i];
                                    }

                                    double roll=-180*PI/180;
                                    double pitch=0;
                                    double yaw=-90*PI/180;

                                    Array2D<double> R = matmult(createRotMat(2, yaw), matmult(createRotMat(1, pitch), createRotMat(0, roll)));

                                    Array2D<double> vel(3,1,0.0);
                                    vel[0][0]=VEstVelState[0][0];
                                    vel[1][0]=VEstVelState[1][0];
                                    vel[2][0]=VEstVelState[2][0];
                                    Array2D<double> mVel = matmult(R,vel);
                                    VEstVelState[0][0]=mVel[0][0];
                                    VEstVelState[1][0]=mVel[1][0];
                                    VEstVelState[2][0]=mVel[2][0];

                                    Array2D<double> vel_att(3,1,0.0);
                                    vel_att[0][0]=VEstVelState[3][0];
                                    vel_att[1][0]=VEstVelState[4][0];
                                    vel_att[2][0]=VEstVelState[5][0];
                                    Array2D<double> mVel_att = matmult(R,vel_att);
                                    VEstVelState[3][0]=mVel_att[0][0];
                                    VEstVelState[4][0]=mVel_att[1][0];
                                    VEstVelState[5][0]=mVel_att[2][0];
                                    mQuads->setVEstVelState(VEstVelState.copy());
                                }
                                break;
                            default :
                                {
                                    //cout << "Unknown UDP code" <<  endl;
                                }
                        }
                    }
                    catch(...)
                    {
                        cout << "grr " << pck.type << endl;
                    }
                }
                mMutex_socketUDP.unlock();
            }
        }

        int FlightInterface::receiveUDP(Socket::ptr socket, tbyte* data, int size)
        {
            uint32 addr;
            int port;
            int received = socket->receiveFrom(data, size, addr, port);

            return received;
        }


        bool FlightInterface::sendUDP(tbyte* data, int size)
        {
            if(mSocketUDP == NULL)
                return false;

            try
            {
                mMutex_socketUDP.lock();
                mSocketUDP->sendTo(data,size,toadlet::egg::Socket::stringToIP(String(mIP.c_str())),mPort);
                mMutex_socketUDP.unlock();
            }
            catch (...)
            {
                cout << "Send failure" << endl;
                mMutex_socketUDP.lock();
                mSocketUDP->close();
                mSocketUDP = NULL;
                mMutex_socketUDP.unlock();
                mIsConnected = false;
                cout << "send udp catch" << endl;
                return false;
            }

            return true;
        }

        bool FlightInterface::receivePacket(Socket::ptr socket, Packet &pck, int size)
        {
            Collection<tbyte> buff(size);

            int result = receiveUDP(socket, buff, size);
            if(result > 0)
                pck.deserialize(buff);

            if(pck.size != result)
                cout << "Only received " << result << " vs " << pck.size << endl;;

            return pck.size == result;
        }

        void FlightInterface::pingOnBoardCom()
        {
            if(mSocketUDP != NULL)
            {
                Packet p;
                p.type  = COMM_PING;
                p.time = mTmr.getCurTimeMS() - mStartTimeUniverseMS;
                Collection<tbyte> buff;
                p.serialize(buff);
                sendUDP(buff.begin(),buff.size());
            }
        }


        void FlightInterface::onBtnBeginTracking_clicked()
        {

            if(mQuads->isEnabled())
            {
                mQuads->setFlightMode(QUAD_PROFILE_TRACKING);
                mQuads->resetAll();
            }
            mFlightMode = QUAD_PROFILE_TRACKING;
            mProfileStartTime = mTmr.getCurTimeMS();

        }

        void FlightInterface::onBtnQuit_clicked()
        {

            mFlightMode = QUAD_IDLE;
            mQuads->sendStopAndShutdown();
            mTmr.sleepMS(50);
            mTmrDisp->stop();
            mTmrPath->stop();
            qApp->quit();

        }

        void FlightInterface::onBtnClearBuffers_clicked()
        {

            mQuads->clearAllBuffers();
            cout<<"Buff is cleared"<<endl;
        }


        void FlightInterface::endModechange()
        {
            if (emode==true)
            {
                emode=false;
                cout<<"Command Mode"<<endl;
            }
            else
            {
                mProfileTrackingTime=mTmr.getCurTimeMS();
                emode=true;
                cout<<"Trajectory Tracking Mode"<<endl;
            }

        }

        void FlightInterface::onIncreaseHeight()
        {
            mMutex_desiredAccess.lock();
            Array2D<double> state = mQuads->getDesiredState();
            state[2][0] = min(state[2][0]+0.050,1.5);
            mQuads->setDesiredState(state);
            mMutex_desiredAccess.unlock();
        }

        void FlightInterface::onDecreaseHeight()
        {
            mMutex_desiredAccess.lock();
            Array2D<double> state = mQuads->getDesiredState();
            state[2][0] = max(state[2][0]-0.050,0.05);
            mQuads->setDesiredState(state);
            mMutex_desiredAccess.unlock();
        }

        void FlightInterface::onMoveLeft()
        {
            mMutex_desiredAccess.lock();
            Array2D<double> state = mQuads->getDesiredState();
            state[0][0] -=0.100;
            mQuads->setDesiredState(state);
            mMutex_desiredAccess.unlock();
        }

        void FlightInterface::onMoveRight()
        {
            mMutex_desiredAccess.lock();
            Array2D<double> state = mQuads->getDesiredState();
            state[0][0] += 0.100;
            mQuads->setDesiredState(state);
            mMutex_desiredAccess.unlock();
        }

        void FlightInterface::onMoveForward()
        {
            mMutex_desiredAccess.lock();
            Array2D<double> state = mQuads->getDesiredState();
            state[1][0] += 0.100;
            mQuads->setDesiredState(state);
            mMutex_desiredAccess.unlock();
        }

        void FlightInterface::onMoveBackward()
        {
            mMutex_desiredAccess.lock();
            Array2D<double> state = mQuads->getDesiredState();
            state[1][0] -=0.100;
            mQuads->setDesiredState(state);
            mMutex_desiredAccess.lock();
        }

    }
}
