#include "Acicom.h"

int AciCom::fd=0;

int16_t AciCom::cx =0;
int16_t AciCom::cy =0;
int16_t AciCom::cz =0;

int16_t AciCom::cvx =0;
int16_t AciCom::cvy =0;
int16_t AciCom::cvz =0;

int16_t AciCom::cyaw =0;

int16_t AciCom::dx =0;
int16_t AciCom::dy =0;
int16_t AciCom::dz =0;
int16_t AciCom::dvx=0;
int16_t AciCom::dvy =0;
int16_t AciCom::dvz =0;
int16_t AciCom::dyaw=0; // control command

uint8_t AciCom::motor1=0;
uint8_t AciCom::motor2=0;
uint8_t AciCom::motor3=0;
uint8_t AciCom::motor4=0;
uint8_t AciCom::motor5=0;
uint8_t AciCom::motor6=0;


int32_t AciCom::angle_pitch=0;
int32_t AciCom::angle_roll=0;


int16_t AciCom::droll=0;
int16_t AciCom::dpitch=0;

uint32_t AciCom::x_accuracy;
uint32_t AciCom::y_accuracy;
uint32_t AciCom::z_accuracy;
uint32_t AciCom::num_sat;

int32_t AciCom::gps_x=0;
int32_t AciCom::gps_y=0;
int32_t AciCom::gps_z=0;
int32_t AciCom::est_dz=0;
int32_t AciCom::est_z=0;
int32_t AciCom::est_dx=0;
int32_t AciCom::est_dy=0;

unsigned char AciCom::var_getted;
unsigned char AciCom::ctrl_mode ;
unsigned char AciCom::ctrl_enabled ;
unsigned char AciCom::disable_motor_onoff_by_stick ;
unsigned char AciCom::cmd_ready ;

    AciCom::AciCom()
    {
    }


    AciCom::~AciCom()
    {

    }

    void AciCom::initialize()
    {

        fd = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);

        struct termios port_settings; // structure to store the port settings in
        cfsetispeed(&port_settings, B57600);    // set baud rates
        port_settings.c_cflag = B57600 | CS8 | CREAD | CLOCAL;
        port_settings.c_iflag = IGNPAR;
        port_settings.c_oflag = 0;
        port_settings.c_lflag = 0;
        tcsetattr(fd, TCSANOW, &port_settings); // apply the settings to the port
        aciInit();

        aciSetSendDataCallback(&transmit);
        aciSetVarListUpdateFinishedCallback(&varListUpdateFinished);
        aciSetCmdListUpdateFinishedCallback(&cmdListUpdateFinished);
        aciSetEngineRate(40, 25);
        cout<<"ACI initialize ok "<<endl;
    }

    void transmit(void* byte, unsigned short cnt)
    {
        unsigned char *tbyte = (unsigned char *)byte;
        for (int i = 0; i < cnt; i++) {
            write(AciCom::fd, &tbyte[i], 1);
        }
    }

    void varListUpdateFinished()
    {
        printf("variables updated\n");
        aciAddContentToVarPacket(0,0x0100,&AciCom::motor1);
        aciAddContentToVarPacket(0,0x0101,&AciCom::motor2);
        aciAddContentToVarPacket(0,0x0102,&AciCom::motor3);
        aciAddContentToVarPacket(0,0x0103,&AciCom::motor4);
        aciAddContentToVarPacket(0,0x0104,&AciCom::motor5);
        aciAddContentToVarPacket(0,0x0105,&AciCom::motor6);

        aciAddContentToVarPacket(0,0x0300,&AciCom::angle_pitch);
        aciAddContentToVarPacket(0,0x0301,&AciCom::angle_roll);

//        aciAddContentToVarPacket(0,0x010C,&AciCom::x_accuracy);
//        aciAddContentToVarPacket(0,0x010D,&AciCom::y_accuracy);
//        aciAddContentToVarPacket(0,0x010E,&AciCom::z_accuracy);
//        aciAddContentToVarPacket(0,0x010F,&AciCom::num_sat);
//
        aciAddContentToVarPacket(0,0x0106,&AciCom::gps_x); //106 for gps, 303 for fusion
        aciAddContentToVarPacket(0,0x0107,&AciCom::gps_y); //107, 304
        aciAddContentToVarPacket(0,0x0108,&AciCom::gps_z);
        aciAddContentToVarPacket(0,0x0305,&AciCom::est_dz);
        aciAddContentToVarPacket(0,0x0306,&AciCom::est_z);
        aciAddContentToVarPacket(0,0x0109,&AciCom::est_dx); //109, 307
        aciAddContentToVarPacket(0,0x010A,&AciCom::est_dy); //10A, 308

        aciAddContentToVarPacket(0,0x0900,&AciCom::droll);
        aciAddContentToVarPacket(0,0x0901,&AciCom::dpitch);


        aciSetVarPacketTransmissionRate(0,25);
        aciVarPacketUpdateTransmissionRates();
        aciSendVariablePacketConfiguration(0);
        AciCom::var_getted=1;
    }

    void cmdListUpdateFinished()
    {
        printf("command list getted!\n");
        aciAddContentToCmdPacket(0, 0x0510, &AciCom::cx);
        aciAddContentToCmdPacket(0, 0x0511, &AciCom::cy);
        aciAddContentToCmdPacket(0, 0x0512, &AciCom::cz);
        aciAddContentToCmdPacket(0, 0x0515, &AciCom::cvx);
        aciAddContentToCmdPacket(0, 0x0516, &AciCom::cvy);
        aciAddContentToCmdPacket(0, 0x0517, &AciCom::cvz);

        aciAddContentToCmdPacket(0, 0x051A, &AciCom::cyaw);

        aciAddContentToCmdPacket(0, 0x051B, &AciCom::dx);
        aciAddContentToCmdPacket(0, 0x051C, &AciCom::dy);
        aciAddContentToCmdPacket(0, 0x051D, &AciCom::dz);
        aciAddContentToCmdPacket(0, 0x051E, &AciCom::dvx);
        aciAddContentToCmdPacket(0, 0x0520, &AciCom::dvy);
        aciAddContentToCmdPacket(0, 0x0521, &AciCom::dvz);
        aciAddContentToCmdPacket(0, 0x0522, &AciCom::dyaw);


        aciSendCommandPacketConfiguration(0, 0);

        AciCom::cx = 0;
        AciCom::cy =0;
        AciCom::cz =0;

        AciCom::cvx = 0;
        AciCom::cvy =0;
        AciCom::cvz =0;

        AciCom::cyaw =0;

        AciCom::dx = 0;
        AciCom::dy =0;
        AciCom::dz =0;
        AciCom::dvx = 0;
        AciCom::dvy =0;
        AciCom::dvz =0;
        AciCom::dyaw =0;

        aciUpdateCmdPacket(0);
        AciCom::cmd_ready=1;
    }
