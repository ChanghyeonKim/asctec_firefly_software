#include "QuadrotorInterface.h"

using namespace std;
using namespace TNT;
using namespace ICSL;

namespace ICSL{
    namespace Quadrotor{
        QuadrotorInterface::QuadrotorInterface(QWidget *parent) : QWidget(parent)
        {
            setupUi(this);

            mFlightMode = QUAD_IDLE;
            mCommStatus = FREE;

            mInitialized = false;
            mDynamicModel = new SystemModelQuadrotor;

            mStateRefBuffer.clear();
            mMotBuffer.clear();
            mXyBuffer.clear();
            mGPSBuffer.clear();
            mStateBuffer.clear();
            mControlBuffer.clear();
            mTimeBuffer.clear();
            mEstBuffer.clear();
            mEstVelBuffer.clear();
            mControlCalcTimeBuffer.clear();
            mFlightModeBuffer.clear();

            mTotalNumState = 12;
        }

        QuadrotorInterface::~QuadrotorInterface()
        {
            delete mAcicom;
            delete mDynamicModel;
            delete mControllerFeedbackLin;
            delete mChangemode;

        }

        void QuadrotorInterface::initialize()
        {
            mChangemode = new QShortcut(Qt::Key_P, this);

            connect(mChangemode,SIGNAL(activated()), this, SLOT(FlightModechange()));
            connect(btnSetXyTarget,SIGNAL(clicked()),this,SLOT(onBtnSetXyDesired_clicked()));
            connect(chkEnabled,SIGNAL(clicked()),this,SLOT(onEnabled()));

            mAcicom = new AciCom;
            mControllerFeedbackLin = new SystemControllerFeedbackLin;

            if(mInitialized)
            {
                cout << "I'm already initialized, stupid." << endl;
                return;
            }
            mInitialized = true;
            mStartTimeUniverseMS = mTmr.getCurTimeMS();

            mControllerFeedbackLin->setDesiredState(Array2D<double>(12,1,0.0));
            mControllerFeedbackLin->setCurState(Array2D<double>(12,1,0.0));

            mAcicom->initialize();

            mcurstate=Array2D<double>(12,1,0.0);
            meststate=Array2D<double>(6,1,0.0);
            mestvelstate=Array2D<double>(6,1,0.0);
            mmotorstate=Array2D<double>(6,1,0.0);
            mdesstate=Array2D<double>(12,1,0.0);

            mxy=Array2D<double>(7,1,0.0);
            GPS_accuracy=Array2D<double>(4,1,0.0);

            inix=0.0;
            iniy=0.0;
            iniz=0.0;

            vision_flag=false;

            aciGetDeviceVariablesList();
            aciGetDeviceCommandsList();
        }

        Array2D<double> QuadrotorInterface::getCurState()
        {

            int result = 0;
            unsigned char data = 0;


            Array2D<double> curState(mTotalNumState,1,0.0);
            mMutex_des.lock();
            Array2D<double> desstate = mdesstate.copy();
            mMutex_des.unlock();

            timecounter1++;
            // cout<<timecounter1<<endl;
            mMutex_send.lock();
            result = read(mAcicom->fd, &data, 1);
            while ((result>0)) {
                aciReceiveHandler(data);
                result = read(mAcicom->fd, &data, 1);
            }

            if (timecounter1>0){
                if(mAcicom->var_getted && mAcicom->cmd_ready) {
                    aciSynchronizeVars();
                }

                timecounter1=0;
            }
            mmotorstate[0][0]=(double)(mAcicom->motor1);
            mmotorstate[1][0]=(double)(mAcicom->motor2);
            mmotorstate[2][0]=(double)(mAcicom->motor3);
            mmotorstate[3][0]=(double)(mAcicom->motor4);
            mmotorstate[4][0]=(double)(mAcicom->motor5);
            mmotorstate[5][0]=(double)(mAcicom->motor6);

            curState[3][0] = (double)(mAcicom->angle_roll*PI/180)/1000;
            curState[4][0] = (double)(-mAcicom->angle_pitch*PI/180)/1000;

            desstate[3][0] = (double)(mAcicom->droll*PI/180)/1000;
            desstate[4][0] = (double)(mAcicom->dpitch*PI/180)/1000;

            mxy[3][0]=(double)(mAcicom->est_dx)/1000;
            mxy[4][0]=(double)(mAcicom->est_dy)/1000;
            mxy[5][0]=(double)(mAcicom->est_dz)/1000;


            usleep(1000);

            mMutex_send.unlock();


            mControllerFeedbackLin->setDesiredState(desstate);

            Array2D<double> stateTemp = mDynamicModel->getCurState();


            for(int i=0;i<3;i++)
                curState[i][0] = stateTemp[i+6][0];
            if(stateTemp[2][0]>-50*PI/180 && stateTemp[2][0]<50*PI/180)
                curState[5][0] = stateTemp[2][0];
            for(int i=0;i<3;i++)
                curState[i+((int) mTotalNumState/2)][0] = stateTemp[i+9][0];
            for(int i=0;i<3;i++)
                curState[i+((int) mTotalNumState/2)+3][0] = stateTemp[i+3][0];


            return curState;


        } // get cur state


        void QuadrotorInterface::updateDisplay()
        {
            switch(mFlightMode)
            {
                case QUAD_IDLE:
                    lblFlightMode->setText("IDLE");
                    break;
                case QUAD_PROFILE_TRACKING:
                    lblFlightMode->setText("Profile Tracking");
                    break;
                case QUAD_MANUAL:
                    lblFlightMode->setText("Manual");
                    break;
                default:
                    lblFlightMode->setText("Unknown");
            }

            if(mStateBuffer.size() > 0)
            {
                lblState00->setText(QString::number(mcurstate[0][0],'f',3));
                lblState01->setText(QString::number(mcurstate[1][0],'f',3));
                lblState02->setText(QString::number(mcurstate[2][0],'f',3));
                lblState03->setText(QString::number(mcurstate[3][0]*RAD2DEG,'f',1));
                lblState04->setText(QString::number(mcurstate[4][0]*RAD2DEG,'f',1));
                lblState05->setText(QString::number(mcurstate[5][0]*RAD2DEG,'f',1));

                lblState00_2->setText(QString::number(meststate[0][0],'f',3));
                lblState01_2->setText(QString::number(meststate[1][0],'f',3));
                lblState02_2->setText(QString::number(meststate[2][0],'f',3));
                lblState03_2->setText(QString::number(meststate[3][0]*RAD2DEG,'f',1));
                lblState04_2->setText(QString::number(meststate[4][0]*RAD2DEG,'f',1));
                lblState05_2->setText(QString::number(meststate[5][0]*RAD2DEG,'f',1));

                lblState10->setText(QString::number(mdesstate[0][0],'f',3));
                lblState11->setText(QString::number(mdesstate[1][0],'f',3));
                lblState12->setText(QString::number(mdesstate[2][0],'f',3));
                lblState13->setText(QString::number(mdesstate[3][0]*RAD2DEG,'f',1));
                lblState14->setText(QString::number(mdesstate[4][0]*RAD2DEG,'f',1));
                lblState15->setText(QString::number(mdesstate[5][0]*RAD2DEG,'f',1));


                lblmh00->setText(QString::number(mestvelstate[0][0],'f',3));
                lblmh01->setText(QString::number(mestvelstate[1][0],'f',3));
                lblmh02->setText(QString::number(mestvelstate[2][0],'f',3));
                lblmh03->setText(QString::number(mxy[5][0],'f',3));
                lblmh04->setText(QString::number(mcurstate[6][0],'f',3));
                lblmh05->setText(QString::number(mcurstate[7][0],'f',3));
                lblmh06->setText(QString::number(mcurstate[8][0],'f',3));
                lblmh07->setText(QString::number(GPS_accuracy[3][0],'f',1));

                lblState40->setText(QString::number(mdesstate[6][0],'f',3));
                lblState41->setText(QString::number(mdesstate[7][0],'f',3));
                lblState42->setText(QString::number(mdesstate[8][0],'f',3));
                lblState43->setText(QString::number(mdesstate[9][0]*RAD2DEG,'f',1));
                lblState44->setText(QString::number(mdesstate[10][0]*RAD2DEG,'f',1));
                lblState45->setText(QString::number(mdesstate[11][0]*RAD2DEG,'f',1));

                lblendmot1->setText(QString::number(mmotorstate[0][0],'f',1));
                lblendmot2->setText(QString::number(mmotorstate[1][0],'f',1));
                lblendmot3->setText(QString::number(mmotorstate[2][0],'f',1));
                lblendmot4->setText(QString::number(mmotorstate[3][0],'f',1));
                lblendmot5->setText(QString::number(mmotorstate[4][0],'f',1));
                lblendmot6->setText(QString::number(mmotorstate[5][0],'f',1));

                lblControlCalcTime->setText(QString::number(calc_time-1));
            }
        }

        void QuadrotorInterface::run()
        {

            unsigned long dispTimeStart = mTmr.getCurTimeMS();

            if(isEnabled()){

                mTimeBuffer.push_back(dispTimeStart-mStartTimeUniverseMS);
                calcControl();
                if((mAcicom->cmd_ready)&&(mAcicom->var_getted)){
                    sendControl();
                }
                aciEngine();
                usleep(1000);
            }

            calc_time = mTmr.getCurTimeMS()-dispTimeStart;
        }

        void QuadrotorInterface::calcControl()
        {
            if(!mInitialized)
                throw("Quadrotor interface is not initialized.");

            unsigned long cntlStartTime = mTmr.getCurTimeMS();
            if (chkEnabled->isChecked()){
                mDynamicModel->estimateFullState();
            }
            Array2D<double> control;

            mdesstate = mControllerFeedbackLin->getDesiredState();

            mcurstate = getCurState();
            mControllerFeedbackLin->setinitialState(mcurstate.copy());
            mControllerFeedbackLin->setCurState(mcurstate);


            control = mControllerFeedbackLin->calcControl();


            mMutex_logBuffer.lock();
            mStateBuffer.push_back(mcurstate.copy());
            mMotBuffer.push_back(mmotorstate.copy());
            mStateRefBuffer.push_back(mdesstate.copy());
            mXyBuffer.push_back(mxy.copy());
            mGPSBuffer.push_back(GPS_accuracy.copy());
            mControlBuffer.push_back(control.copy());
            mFlightModeBuffer.push_back(mFlightMode);
            mEstBuffer.push_back(meststate.copy());
            mEstVelBuffer.push_back(mestvelstate.copy());

            mMutex_logBuffer.unlock();
        }

        void QuadrotorInterface::sendControl()
        {
			static int it_non_update;
			static int16_t prevCx=0, prevCy=0, prevCz=0, prevCvx=0, prevCvy=0, prevCvz=0, prevCyaw=0;
			int16_t cx, cy, cz, cvx, cvy, cvz, cyaw;

            if(!mInitialized)
                throw("Quadrotor interface is not initialized.");

            if(mFlightMode == QUAD_IDLE)
            {
                //do nothing
            }
            else

            {
                Array2D<double> curStateq = mcurstate.copy();
                Array2D<double> targetState = mdesstate.copy();


                //cout<<int16_t(control[0][0]*1000)<<endl;

                mMutex_send.lock();

                if (vision_flag==false)
                {
                    mAcicom->cx = (int16_t)((mcurstate[0][0])*10000);
                    mAcicom->cy = (int16_t)(mcurstate[1][0]*10000);
                    mAcicom->cz = (int16_t)((mcurstate[2][0])*10000);
                    mAcicom->cvx = (int16_t)(mcurstate[6][0]*1000); //6
                    mAcicom->cvy = (int16_t)(mcurstate[7][0]*1000);
                    mAcicom->cvz = (int16_t)(mcurstate[8][0]*1000);
                    mAcicom->cyaw = (int16_t)(mcurstate[5][0]*1000);
                    //cout<<"vicon mode"<<endl;
                }
                else
                {
                    cx = (int16_t)((meststate[0][0])*10000);
                    cy = (int16_t)(meststate[1][0]*10000);
                    cz = (int16_t)((meststate[2][0])*10000);
                    cvx = (int16_t)(mestvelstate[0][0]*1000); //6
                    cvy = (int16_t)(mestvelstate[1][0]*1000);
                    cvz = (int16_t)(mestvelstate[2][0]*1000);
                    cyaw = (int16_t)(-meststate[5][0]*1000);//mcurstate[5][0] or -meststate[5][0]

                    //cout<<"vision mode"<<endl;

					if( cx == prevCx && cy == prevCy && cz == prevCz && cvx == prevCvx && cvy == prevCvy && cvz == prevCvz && cyaw == prevCyaw ){
						it_non_update++;
						if(it_non_update > 30){
							cout << "!!!!!!!SGPVO is not updated!!!!!!!" << endl;
							cx = (int16_t)((mcurstate[0][0])*10000);
							cy = (int16_t)(mcurstate[1][0]*10000);
							cz = (int16_t)((mcurstate[2][0])*10000);
							cvx = (int16_t)(mcurstate[6][0]*1000); //6
							cvy = (int16_t)(mcurstate[7][0]*1000);
							cvz = (int16_t)(mcurstate[8][0]*1000);
							cyaw = (int16_t)(mcurstate[5][0]*1000);

							this->onBtnSetXyDesired_clicked();
							vision_flag = false;
							std::cout << "Vicon Mode" << std::endl;
						}
					}
					else
						it_non_update = 0;

					mAcicom->cx = cx;
					mAcicom->cy = cy;
					mAcicom->cz = cz;
					mAcicom->cvx = cvx;
					mAcicom->cvy = cvy;
					mAcicom->cvz = cvz;
					mAcicom->cyaw = cyaw;

                    prevCx = (int16_t)((meststate[0][0])*10000);
                    prevCy = (int16_t)(meststate[1][0]*10000);
                    prevCz = (int16_t)((meststate[2][0])*10000);
                    prevCvx = (int16_t)(mestvelstate[0][0]*1000); //6
                    prevCvy = (int16_t)(mestvelstate[1][0]*1000);
                    prevCvz = (int16_t)(mestvelstate[2][0]*1000);
                    prevCyaw = (int16_t)(-meststate[5][0]*1000);//mcurstate[5][0] or -meststate[5][0]
                }

//				std::cout << "cx: " << mAcicom->cx << std::endl;
//				std::cout << "cy: " << mAcicom->cy << std::endl;
//				std::cout << "cz: " << mAcicom->cz << std::endl;
//				std::cout << "cVx: " << mAcicom->cvx << std::endl;
//				std::cout << "cVy: " << mAcicom->cvy << std::endl;
//				std::cout << "cVz: " << mAcicom->cvz << std::endl;
//				std::cout << "cyaw: " << mAcicom->cyaw << std::endl;

                mAcicom->dx = (int16_t)(targetState[0][0]*10000);
                mAcicom->dy = (int16_t)(targetState[1][0]*10000);
                mAcicom->dz = (int16_t)(targetState[2][0]*10000);
                mAcicom->dvx = (int16_t)(targetState[6][0]*1000);
                mAcicom->dvy = (int16_t)(targetState[7][0]*1000);
                mAcicom->dvz = (int16_t)(targetState[8][0]*1000);
                mAcicom->dyaw = (int16_t)(targetState[5][0]*1000);
                //cout<<AciCom::take_off<<endl;
                aciUpdateCmdPacket(0);
                usleep(1000);
                mMutex_send.unlock();
            }
        }


        void QuadrotorInterface::sendStopAndShutdown()
        {
            mFlightMode = QUAD_IDLE;

            if(mDynamicModel->isSimulated())
                return;

            if(mCommStatus == BUSY)
                return;

            mTmr.sleepMS(100);
            cout << "done" << endl;

        }
        void QuadrotorInterface::FlightModechange()
        {

            if (vision_flag==false)
            {
                vision_flag=true;
                Array2D<double> curState(12,1,0.0);

                curState = getCurState();

                Array2D<double> targetState = mControllerFeedbackLin->getDesiredState();
                targetState[0][0]=meststate[0][0];
                targetState[1][0]=meststate[1][0];
				targetState[2][0]=meststate[2][0];

                iniz=meststate[2][0];
                inix=meststate[0][0];
                iniy=meststate[1][0];

                mControllerFeedbackLin->setDesiredState(targetState.copy());
                mProfileStartPoint = targetState.copy();

                cout<<"Vision Mode"<<endl;
            }
            else
            {
				this->onBtnSetXyDesired_clicked();
                vision_flag=false;
                cout<<"Vicon Mode"<<endl;
            }
        }

        void QuadrotorInterface::onBtnSetXyDesired_clicked()
        {
            Array2D<double> curState(12,1,0.0);

            curState = getCurState();

            inix=curState[0][0];
            iniy=curState[1][0];
            iniz=curState[2][0];

            Array2D<double> targetState = mControllerFeedbackLin->getDesiredState();
            targetState[0][0] = curState[0][0];
            targetState[1][0] = curState[1][0];
			targetState[2][0] = curState[2][0];

            mControllerFeedbackLin->setDesiredState(targetState.copy());
            mProfileStartPoint = targetState.copy();
        }

        void QuadrotorInterface::clearAllBuffers()
        {
            mMutex_logBuffer.lock();
            mDynamicModel->clearAllBuffers();
            mStateRefBuffer.clear();
            mStateBuffer.clear();
            mControlBuffer.clear();
            mMotBuffer.clear();
            mTimeBuffer.clear();
            mXyBuffer.clear();
            mGPSBuffer.clear();
            mControlCalcTimeBuffer.clear();
            mFlightModeBuffer.clear();
            mEstBuffer.clear();
            mEstVelBuffer.clear();
            mMutex_logBuffer.unlock();

        }

        void QuadrotorInterface::resetAll()
        {
            mDynamicModel->resetState();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        inline double QuadrotorInterface::constrain(double val, double minVal, double maxVal)
        {
            return min(maxVal, max(minVal, val));
        }


        void QuadrotorInterface::saveData(string dir, string filename)
        {

            fstream dataStream;

            dataStream.open((dir+"/"+filename).c_str(),fstream::out);
            if(dataStream.is_open()){

                dataStream << "frame,\tflight mode,\ttime,\tCommand 0,\tCommand 1";
                for(int j=0; j<12; j++)
                    dataStream << ",\tTarget " << SystemModelQuadrotor::StateNames[j];
                for(int j=0; j<12; j++)
                    dataStream << ",\tState" << SystemModelQuadrotor::StateNames[j];
                dataStream << ",\tmotor1,\tmotor2,\tmotor3,\tmotor4,\tmotor5,\tmotor6";
                for(int j=0; j<6; j++)
                    dataStream << ",\tEstState " << j;
                for(int j=0; j<6; j++)
                    dataStream << ",\tEstVelState " << j;
                dataStream << ",\tvx,\tvy";
                dataStream << endl;

                mMutex_logBuffer.lock();
                // save data
                list<int>::iterator iter_flightMode = mFlightModeBuffer.begin();
                list<unsigned long>::iterator iter_time = mTimeBuffer.begin();
                list<Array2D<double> >::iterator iter_cntl= mControlBuffer.begin();
                list<Array2D<double> >::iterator iter_stateRef = mStateRefBuffer.begin();
                list<Array2D<double> >::iterator iter_state = mStateBuffer.begin();
                list<Array2D<double> >::iterator iter_motor = mMotBuffer.begin();
                list<Array2D<double> >::iterator iter_eststate = mEstBuffer.begin();
                list<Array2D<double> >::iterator iter_estvelstate = mEstVelBuffer.begin();
                list<Array2D<double> >::iterator iter_xy = mXyBuffer.begin();
                list<Array2D<double> >::iterator iter_gps = mGPSBuffer.begin();


                list<unsigned long>::iterator iter_cntlCalcTime = mControlCalcTimeBuffer.begin();
                for(int i=0; i<mTimeBuffer.size(); i++)
                {

                    dataStream << i;
                    dataStream << ",\t" << *(iter_flightMode++);
                    dataStream << ",\t" << *(iter_time++);
                    for(int j=0; j<2; j++)
                        dataStream << ",\t" << (*(iter_cntl))[j][0];
                    iter_cntl++;
                    for(int j=0; j<12; j++)
                        dataStream << ",\t" << (*(iter_stateRef))[j][0];
                    iter_stateRef++;
                    for(int j=0; j<12; j++)
                        dataStream << ",\t" << (*(iter_state))[j][0];
                    iter_state++;
                    for(int j=0; j<6; j++)
                        dataStream << ",\t" << (*(iter_motor))[j][0];
                    iter_motor++;
                    for(int j=0; j<6; j++)
                        dataStream << ",\t" << (*(iter_eststate))[j][0];
                    iter_eststate++;
                    for(int j=0; j<6; j++)
                        dataStream << ",\t" << (*(iter_estvelstate))[j][0];
                    iter_estvelstate++;
                    for(int j=0; j<7; j++)
                        dataStream << ",\t" << (*(iter_xy))[j][0];
                    iter_xy++;
                    for(int j=0; j<4; j++)
                        dataStream << ",\t" << (*(iter_gps))[j][0];
                    iter_gps++;

                    dataStream << ",\t" << *(iter_cntlCalcTime++);
                    dataStream << endl;
                }

            }

            dataStream.close();
            mMutex_logBuffer.unlock();
        }


    }
}
