#include <iostream>
#include <fstream>
#include <thread>
#include <chrono>
#include <sys/stat.h>
#include <exception>

#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h>  // File control definitions
#include <termios.h> // POSIX terminal control definitionss

#include <QApplication>

#include "FlightInterface.h"
#include "TelemetryVicon.h"
#include "../Asctec_aci/asctecCommIntf.h"

using namespace std;
using namespace ICSL;
using namespace ICSL::Quadrotor;

int main(int argc, char **argv)
{
	cout << "start Multi quadrotor Flight System" << endl;

    FlightInterface *flightInterface;

	double deltaT;
	deltaT = 0.04; // need to be this slow for image capture

	qRegisterMetaType<TelemetryViconDataRecord>("TelemetryViconDataRecord");
	QApplication qtApp(argc, argv);

	flightInterface = new FlightInterface();
	flightInterface->initialize();
	flightInterface->setDeltaT(deltaT);
	flightInterface->show();
	flightInterface->run();
	qtApp.exec();

    string dir = "../runData";
	string filename = "data.csv";
	flightInterface->saveData(dir, filename);
		cout << "Multi quadrotor Flight System accomplished" << endl;
    delete flightInterface;

    return 0;
}
