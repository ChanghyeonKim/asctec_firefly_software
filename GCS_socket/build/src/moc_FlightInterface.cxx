/****************************************************************************
** Meta object code from reading C++ file 'FlightInterface.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/FlightInterface.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'FlightInterface.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ICSL__Quadrotor__FlightInterface[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      34,   33,   33,   33, 0x09,
      50,   33,   33,   33, 0x09,
      66,   33,   33,   33, 0x09,
      80,   33,   33,   33, 0x09,
     109,   33,   33,   33, 0x09,
     129,   33,   33,   33, 0x09,
     157,   33,   33,   33, 0x09,
     176,   33,   33,   33, 0x09,
     195,   33,   33,   33, 0x09,
     208,   33,   33,   33, 0x09,
     222,   33,   33,   33, 0x09,
     238,   33,   33,   33, 0x09,
     255,   33,   33,   33, 0x09,
     266,   33,   33,   33, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_ICSL__Quadrotor__FlightInterface[] = {
    "ICSL::Quadrotor::FlightInterface\0\0"
    "endModechange()\0updateDisplay()\0"
    "pathplanner()\0onBtnBeginTracking_clicked()\0"
    "onBtnQuit_clicked()\0onBtnClearBuffers_clicked()\0"
    "onIncreaseHeight()\0onDecreaseHeight()\0"
    "onMoveLeft()\0onMoveRight()\0onMoveForward()\0"
    "onMoveBackward()\0pollData()\0"
    "onBtnConnect_clicked()\0"
};

void ICSL::Quadrotor::FlightInterface::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        FlightInterface *_t = static_cast<FlightInterface *>(_o);
        switch (_id) {
        case 0: _t->endModechange(); break;
        case 1: _t->updateDisplay(); break;
        case 2: _t->pathplanner(); break;
        case 3: _t->onBtnBeginTracking_clicked(); break;
        case 4: _t->onBtnQuit_clicked(); break;
        case 5: _t->onBtnClearBuffers_clicked(); break;
        case 6: _t->onIncreaseHeight(); break;
        case 7: _t->onDecreaseHeight(); break;
        case 8: _t->onMoveLeft(); break;
        case 9: _t->onMoveRight(); break;
        case 10: _t->onMoveForward(); break;
        case 11: _t->onMoveBackward(); break;
        case 12: _t->pollData(); break;
        case 13: _t->onBtnConnect_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData ICSL::Quadrotor::FlightInterface::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ICSL::Quadrotor::FlightInterface::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_ICSL__Quadrotor__FlightInterface,
      qt_meta_data_ICSL__Quadrotor__FlightInterface, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ICSL::Quadrotor::FlightInterface::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ICSL::Quadrotor::FlightInterface::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ICSL::Quadrotor::FlightInterface::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ICSL__Quadrotor__FlightInterface))
        return static_cast<void*>(const_cast< FlightInterface*>(this));
    if (!strcmp(_clname, "Ui::FlightInterface"))
        return static_cast< Ui::FlightInterface*>(const_cast< FlightInterface*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int ICSL::Quadrotor::FlightInterface::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
