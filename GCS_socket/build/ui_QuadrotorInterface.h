/********************************************************************************
** Form generated from reading UI file 'QuadrotorInterface.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QUADROTORINTERFACE_H
#define UI_QUADROTORINTERFACE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QuadrotorInterface
{
public:
    QWidget *layoutWidget1;
    QHBoxLayout *horizontalLayout_5;
    QWidget *layoutWidget2;
    QHBoxLayout *horizontalLayout_6;
    QWidget *layoutWidget3;
    QHBoxLayout *horizontalLayout_7;
    QWidget *layoutWidget4;
    QVBoxLayout *verticalLayout_6;
    QWidget *widget;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_8;
    QHBoxLayout *horizontalLayout_3;
    QCheckBox *chkEnabled;
    QLabel *label;
    QLabel *lblControlCalcTime;
    QHBoxLayout *horizontalLayout_4;
    QLabel *lblFlightMode;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_4;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_4;
    QLabel *m1;
    QLabel *lblendmot5;
    QLabel *m6;
    QLabel *lblendmot6;
    QLabel *lblendmot2;
    QLabel *lblendmot1;
    QLabel *m2;
    QLabel *lblendmot4;
    QLabel *m5;
    QLabel *lblendmot3;
    QLabel *m3;
    QLabel *m4;
    QSpacerItem *horizontalSpacer_4;
    QVBoxLayout *verticalLayout_5;
    QGroupBox *groupBox_5;
    QGroupBox *groupBox_6;
    QWidget *layoutWidget6;
    QHBoxLayout *horizontalLayout_9;
    QHBoxLayout *horizontalLayout_10;
    QCheckBox *chkEnabled_2;
    QLabel *label_2;
    QLabel *lblControlCalcTime_2;
    QHBoxLayout *horizontalLayout_11;
    QLabel *lblFlightMode_2;
    QGridLayout *gridLayout_5;
    QLabel *label_17;
    QLabel *label_18;
    QLabel *lblendposz;
    QLabel *lblenddx;
    QLabel *label_19;
    QLabel *lblenddy;
    QLabel *lblendposx_5;
    QLabel *lblenddz;
    QLabel *lblendposy;
    QLabel *lblendposx_4;
    QLabel *lblendposx;
    QLabel *xc;
    QSpacerItem *horizontalSpacer_7;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox_3;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *btnSetXyTarget;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout_12;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QLabel *est_m2h;
    QLabel *est_m3h;
    QLabel *est_m3h_2;
    QLabel *est_m3h_3;
    QLabel *est_val;
    QLabel *lblmh00;
    QLabel *lblmh01;
    QLabel *lblmh02;
    QLabel *lblmh03;
    QLabel *est_m2h_3;
    QLabel *est_m3h_4;
    QLabel *est_m3h_5;
    QLabel *est_m2h_2;
    QLabel *est_val_2;
    QLabel *lblmh04;
    QLabel *lblmh05;
    QLabel *lblmh06;
    QLabel *lblmh07;
    QSpacerItem *horizontalSpacer_8;
    QGridLayout *gridLayout_2;
    QLabel *lblStateX;
    QLabel *lblStateY;
    QLabel *lblStateZ;
    QLabel *lblStateRoll;
    QLabel *lblStatePitch;
    QLabel *lblStateYaw;
    QLabel *lblStateq1;
    QLabel *lblStateq2;
    QLabel *lblStateCur;
    QLabel *lblState00;
    QLabel *lblState01;
    QLabel *lblState02;
    QLabel *lblState03;
    QLabel *lblState04;
    QLabel *lblState05;
    QLabel *lblState06;
    QLabel *lblState07;
    QLabel *lblStateCur_2;
    QLabel *lblState00_2;
    QLabel *lblState01_2;
    QLabel *lblState02_2;
    QLabel *lblState03_2;
    QLabel *lblState04_2;
    QLabel *lblState05_2;
    QLabel *lblState06_2;
    QLabel *lblState07_2;
    QLabel *lblStateDes;
    QLabel *lblState10;
    QLabel *lblState11;
    QLabel *lblState12;
    QLabel *lblState13;
    QLabel *lblState14;
    QLabel *lblState15;
    QLabel *lblState16;
    QLabel *lblState17;
    QLabel *lblStateErr;
    QLabel *lblState20;
    QLabel *lblState21;
    QLabel *lblState22;
    QLabel *lblState23;
    QLabel *lblState24;
    QLabel *lblState25;
    QLabel *lblState26;
    QLabel *lblState27;
    QLabel *lblStateCurVel;
    QLabel *lblState30;
    QLabel *lblState31;
    QLabel *lblState32;
    QLabel *lblState33;
    QLabel *lblState34;
    QLabel *lblState35;
    QLabel *lblState36;
    QLabel *lblState37;
    QLabel *lblStateDesVel;
    QLabel *lblState40;
    QLabel *lblState41;
    QLabel *lblState42;
    QLabel *lblState43;
    QLabel *lblState44;
    QLabel *lblState45;
    QLabel *lblState46;
    QLabel *lblState47;
    QSpacerItem *verticalSpacer_4;

    void setupUi(QWidget *QuadrotorInterface)
    {
        if (QuadrotorInterface->objectName().isEmpty())
            QuadrotorInterface->setObjectName(QString::fromUtf8("QuadrotorInterface"));
        QuadrotorInterface->resize(409, 472);
        QuadrotorInterface->setBaseSize(QSize(640, 560));
        layoutWidget1 = new QWidget(QuadrotorInterface);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(0, 0, 2, 2));
        horizontalLayout_5 = new QHBoxLayout(layoutWidget1);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(0, 0, 0, 0);
        layoutWidget2 = new QWidget(QuadrotorInterface);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(0, 0, 2, 2));
        horizontalLayout_6 = new QHBoxLayout(layoutWidget2);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(0, 0, 0, 0);
        layoutWidget3 = new QWidget(QuadrotorInterface);
        layoutWidget3->setObjectName(QString::fromUtf8("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(0, 0, 2, 2));
        horizontalLayout_7 = new QHBoxLayout(layoutWidget3);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(0, 0, 0, 0);
        layoutWidget4 = new QWidget(QuadrotorInterface);
        layoutWidget4->setObjectName(QString::fromUtf8("layoutWidget4"));
        layoutWidget4->setGeometry(QRect(0, 0, 2, 2));
        verticalLayout_6 = new QVBoxLayout(layoutWidget4);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        widget = new QWidget(QuadrotorInterface);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(6, 9, 381, 441));
        verticalLayout_2 = new QVBoxLayout(widget);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        chkEnabled = new QCheckBox(widget);
        chkEnabled->setObjectName(QString::fromUtf8("chkEnabled"));
        QFont font;
        font.setPointSize(10);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        chkEnabled->setFont(font);

        horizontalLayout_3->addWidget(chkEnabled);

        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font);

        horizontalLayout_3->addWidget(label);

        lblControlCalcTime = new QLabel(widget);
        lblControlCalcTime->setObjectName(QString::fromUtf8("lblControlCalcTime"));
        lblControlCalcTime->setFont(font);

        horizontalLayout_3->addWidget(lblControlCalcTime);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        lblFlightMode = new QLabel(widget);
        lblFlightMode->setObjectName(QString::fromUtf8("lblFlightMode"));
        QFont font1;
        font1.setPointSize(9);
        font1.setBold(true);
        font1.setItalic(true);
        font1.setWeight(75);
        lblFlightMode->setFont(font1);

        horizontalLayout_4->addWidget(lblFlightMode);


        horizontalLayout_3->addLayout(horizontalLayout_4);


        horizontalLayout_8->addLayout(horizontalLayout_3);


        verticalLayout_2->addLayout(horizontalLayout_8);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        groupBox_4 = new QGroupBox(widget);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBox_4->sizePolicy().hasHeightForWidth());
        groupBox_4->setSizePolicy(sizePolicy);
        groupBox_4->setMaximumSize(QSize(9999999, 16777215));
        groupBox_4->setFont(font1);

        verticalLayout_4->addWidget(groupBox_4);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        m1 = new QLabel(widget);
        m1->setObjectName(QString::fromUtf8("m1"));
        QFont font2;
        font2.setPointSize(8);
        font2.setBold(true);
        font2.setWeight(75);
        m1->setFont(font2);
        m1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_4->addWidget(m1, 0, 0, 1, 1);

        lblendmot5 = new QLabel(widget);
        lblendmot5->setObjectName(QString::fromUtf8("lblendmot5"));
        lblendmot5->setFont(font2);

        gridLayout_4->addWidget(lblendmot5, 1, 3, 1, 1);

        m6 = new QLabel(widget);
        m6->setObjectName(QString::fromUtf8("m6"));
        m6->setFont(font2);

        gridLayout_4->addWidget(m6, 1, 4, 1, 1);

        lblendmot6 = new QLabel(widget);
        lblendmot6->setObjectName(QString::fromUtf8("lblendmot6"));
        lblendmot6->setFont(font2);

        gridLayout_4->addWidget(lblendmot6, 1, 5, 1, 1);

        lblendmot2 = new QLabel(widget);
        lblendmot2->setObjectName(QString::fromUtf8("lblendmot2"));
        lblendmot2->setFont(font2);

        gridLayout_4->addWidget(lblendmot2, 0, 3, 1, 1);

        lblendmot1 = new QLabel(widget);
        lblendmot1->setObjectName(QString::fromUtf8("lblendmot1"));
        lblendmot1->setFont(font2);

        gridLayout_4->addWidget(lblendmot1, 0, 1, 1, 1);

        m2 = new QLabel(widget);
        m2->setObjectName(QString::fromUtf8("m2"));
        m2->setFont(font2);
        m2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_4->addWidget(m2, 0, 2, 1, 1);

        lblendmot4 = new QLabel(widget);
        lblendmot4->setObjectName(QString::fromUtf8("lblendmot4"));
        lblendmot4->setFont(font2);

        gridLayout_4->addWidget(lblendmot4, 1, 1, 1, 1);

        m5 = new QLabel(widget);
        m5->setObjectName(QString::fromUtf8("m5"));
        m5->setFont(font2);
        m5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_4->addWidget(m5, 1, 2, 1, 1);

        lblendmot3 = new QLabel(widget);
        lblendmot3->setObjectName(QString::fromUtf8("lblendmot3"));
        lblendmot3->setFont(font2);

        gridLayout_4->addWidget(lblendmot3, 0, 5, 1, 1);

        m3 = new QLabel(widget);
        m3->setObjectName(QString::fromUtf8("m3"));
        m3->setFont(font2);

        gridLayout_4->addWidget(m3, 0, 4, 1, 1);

        m4 = new QLabel(widget);
        m4->setObjectName(QString::fromUtf8("m4"));
        m4->setFont(font2);
        m4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_4->addWidget(m4, 1, 0, 1, 1);


        verticalLayout_4->addLayout(gridLayout_4);


        horizontalLayout_2->addLayout(verticalLayout_4);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        groupBox_5 = new QGroupBox(widget);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        sizePolicy.setHeightForWidth(groupBox_5->sizePolicy().hasHeightForWidth());
        groupBox_5->setSizePolicy(sizePolicy);
        groupBox_5->setMaximumSize(QSize(9999999, 16777215));
        groupBox_5->setFont(font1);
        groupBox_6 = new QGroupBox(groupBox_5);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        groupBox_6->setGeometry(QRect(0, 0, 158, 26));
        sizePolicy.setHeightForWidth(groupBox_6->sizePolicy().hasHeightForWidth());
        groupBox_6->setSizePolicy(sizePolicy);
        groupBox_6->setMaximumSize(QSize(9999999, 16777215));
        groupBox_6->setFont(font1);
        layoutWidget6 = new QWidget(groupBox_5);
        layoutWidget6->setObjectName(QString::fromUtf8("layoutWidget6"));
        layoutWidget6->setGeometry(QRect(-245, -33, 292, 25));
        horizontalLayout_9 = new QHBoxLayout(layoutWidget6);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        horizontalLayout_9->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        chkEnabled_2 = new QCheckBox(layoutWidget6);
        chkEnabled_2->setObjectName(QString::fromUtf8("chkEnabled_2"));
        chkEnabled_2->setFont(font);

        horizontalLayout_10->addWidget(chkEnabled_2);

        label_2 = new QLabel(layoutWidget6);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);

        horizontalLayout_10->addWidget(label_2);

        lblControlCalcTime_2 = new QLabel(layoutWidget6);
        lblControlCalcTime_2->setObjectName(QString::fromUtf8("lblControlCalcTime_2"));
        lblControlCalcTime_2->setFont(font);

        horizontalLayout_10->addWidget(lblControlCalcTime_2);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        lblFlightMode_2 = new QLabel(layoutWidget6);
        lblFlightMode_2->setObjectName(QString::fromUtf8("lblFlightMode_2"));
        lblFlightMode_2->setFont(font1);

        horizontalLayout_11->addWidget(lblFlightMode_2);


        horizontalLayout_10->addLayout(horizontalLayout_11);


        horizontalLayout_9->addLayout(horizontalLayout_10);


        verticalLayout_5->addWidget(groupBox_5);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        label_17 = new QLabel(widget);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        QFont font3;
        font3.setPointSize(8);
        font3.setBold(true);
        font3.setItalic(true);
        font3.setWeight(75);
        label_17->setFont(font3);
        label_17->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(label_17, 0, 2, 1, 1);

        label_18 = new QLabel(widget);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setFont(font3);
        label_18->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(label_18, 1, 0, 1, 1);

        lblendposz = new QLabel(widget);
        lblendposz->setObjectName(QString::fromUtf8("lblendposz"));
        lblendposz->setFont(font3);

        gridLayout_5->addWidget(lblendposz, 0, 5, 1, 1);

        lblenddx = new QLabel(widget);
        lblenddx->setObjectName(QString::fromUtf8("lblenddx"));
        lblenddx->setFont(font3);

        gridLayout_5->addWidget(lblenddx, 1, 1, 1, 1);

        label_19 = new QLabel(widget);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setFont(font3);
        label_19->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(label_19, 1, 2, 1, 1);

        lblenddy = new QLabel(widget);
        lblenddy->setObjectName(QString::fromUtf8("lblenddy"));
        lblenddy->setFont(font3);

        gridLayout_5->addWidget(lblenddy, 1, 3, 1, 1);

        lblendposx_5 = new QLabel(widget);
        lblendposx_5->setObjectName(QString::fromUtf8("lblendposx_5"));
        lblendposx_5->setFont(font3);

        gridLayout_5->addWidget(lblendposx_5, 1, 4, 1, 1);

        lblenddz = new QLabel(widget);
        lblenddz->setObjectName(QString::fromUtf8("lblenddz"));
        lblenddz->setFont(font3);

        gridLayout_5->addWidget(lblenddz, 1, 5, 1, 1);

        lblendposy = new QLabel(widget);
        lblendposy->setObjectName(QString::fromUtf8("lblendposy"));
        lblendposy->setFont(font3);

        gridLayout_5->addWidget(lblendposy, 0, 3, 1, 1);

        lblendposx_4 = new QLabel(widget);
        lblendposx_4->setObjectName(QString::fromUtf8("lblendposx_4"));
        lblendposx_4->setFont(font3);

        gridLayout_5->addWidget(lblendposx_4, 0, 4, 1, 1);

        lblendposx = new QLabel(widget);
        lblendposx->setObjectName(QString::fromUtf8("lblendposx"));
        lblendposx->setFont(font3);

        gridLayout_5->addWidget(lblendposx, 0, 1, 1, 1);

        xc = new QLabel(widget);
        xc->setObjectName(QString::fromUtf8("xc"));
        xc->setFont(font3);
        xc->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(xc, 0, 0, 1, 1);


        verticalLayout_5->addLayout(gridLayout_5);


        horizontalLayout_2->addLayout(verticalLayout_5);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_7);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        groupBox_3 = new QGroupBox(widget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));

        horizontalLayout->addWidget(groupBox_3);

        horizontalSpacer_3 = new QSpacerItem(13, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        btnSetXyTarget = new QPushButton(widget);
        btnSetXyTarget->setObjectName(QString::fromUtf8("btnSetXyTarget"));

        horizontalLayout->addWidget(btnSetXyTarget);

        pushButton = new QPushButton(widget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout->addWidget(pushButton);

        horizontalSpacer_6 = new QSpacerItem(13, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_6);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox = new QGroupBox(widget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        sizePolicy.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy);
        groupBox->setMaximumSize(QSize(9999999, 16777215));
        QFont font4;
        font4.setPointSize(11);
        font4.setBold(true);
        font4.setItalic(true);
        font4.setWeight(75);
        groupBox->setFont(font4);

        verticalLayout->addWidget(groupBox);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        est_m2h = new QLabel(widget);
        est_m2h->setObjectName(QString::fromUtf8("est_m2h"));
        est_m2h->setFont(font1);

        gridLayout->addWidget(est_m2h, 0, 1, 1, 1);

        est_m3h = new QLabel(widget);
        est_m3h->setObjectName(QString::fromUtf8("est_m3h"));
        est_m3h->setFont(font1);

        gridLayout->addWidget(est_m3h, 0, 2, 1, 1);

        est_m3h_2 = new QLabel(widget);
        est_m3h_2->setObjectName(QString::fromUtf8("est_m3h_2"));
        est_m3h_2->setFont(font1);

        gridLayout->addWidget(est_m3h_2, 0, 3, 1, 1);

        est_m3h_3 = new QLabel(widget);
        est_m3h_3->setObjectName(QString::fromUtf8("est_m3h_3"));
        est_m3h_3->setFont(font1);

        gridLayout->addWidget(est_m3h_3, 0, 4, 1, 1);

        est_val = new QLabel(widget);
        est_val->setObjectName(QString::fromUtf8("est_val"));
        est_val->setFont(font1);
        est_val->setLayoutDirection(Qt::LeftToRight);

        gridLayout->addWidget(est_val, 1, 0, 1, 1);

        lblmh00 = new QLabel(widget);
        lblmh00->setObjectName(QString::fromUtf8("lblmh00"));
        lblmh00->setFont(font3);

        gridLayout->addWidget(lblmh00, 1, 1, 1, 1);

        lblmh01 = new QLabel(widget);
        lblmh01->setObjectName(QString::fromUtf8("lblmh01"));
        lblmh01->setFont(font3);

        gridLayout->addWidget(lblmh01, 1, 2, 1, 1);

        lblmh02 = new QLabel(widget);
        lblmh02->setObjectName(QString::fromUtf8("lblmh02"));
        lblmh02->setFont(font3);

        gridLayout->addWidget(lblmh02, 1, 3, 1, 1);

        lblmh03 = new QLabel(widget);
        lblmh03->setObjectName(QString::fromUtf8("lblmh03"));
        lblmh03->setFont(font3);

        gridLayout->addWidget(lblmh03, 1, 4, 1, 1);

        est_m2h_3 = new QLabel(widget);
        est_m2h_3->setObjectName(QString::fromUtf8("est_m2h_3"));
        est_m2h_3->setFont(font1);

        gridLayout->addWidget(est_m2h_3, 2, 1, 1, 1);

        est_m3h_4 = new QLabel(widget);
        est_m3h_4->setObjectName(QString::fromUtf8("est_m3h_4"));
        est_m3h_4->setFont(font1);

        gridLayout->addWidget(est_m3h_4, 2, 2, 1, 1);

        est_m3h_5 = new QLabel(widget);
        est_m3h_5->setObjectName(QString::fromUtf8("est_m3h_5"));
        est_m3h_5->setFont(font1);

        gridLayout->addWidget(est_m3h_5, 2, 3, 1, 1);

        est_m2h_2 = new QLabel(widget);
        est_m2h_2->setObjectName(QString::fromUtf8("est_m2h_2"));
        est_m2h_2->setFont(font1);

        gridLayout->addWidget(est_m2h_2, 2, 4, 1, 1);

        est_val_2 = new QLabel(widget);
        est_val_2->setObjectName(QString::fromUtf8("est_val_2"));
        est_val_2->setFont(font1);
        est_val_2->setLayoutDirection(Qt::LeftToRight);

        gridLayout->addWidget(est_val_2, 3, 0, 1, 1);

        lblmh04 = new QLabel(widget);
        lblmh04->setObjectName(QString::fromUtf8("lblmh04"));
        lblmh04->setFont(font3);

        gridLayout->addWidget(lblmh04, 3, 1, 1, 1);

        lblmh05 = new QLabel(widget);
        lblmh05->setObjectName(QString::fromUtf8("lblmh05"));
        lblmh05->setFont(font3);

        gridLayout->addWidget(lblmh05, 3, 2, 1, 1);

        lblmh06 = new QLabel(widget);
        lblmh06->setObjectName(QString::fromUtf8("lblmh06"));
        lblmh06->setFont(font3);

        gridLayout->addWidget(lblmh06, 3, 3, 1, 1);

        lblmh07 = new QLabel(widget);
        lblmh07->setObjectName(QString::fromUtf8("lblmh07"));
        lblmh07->setFont(font3);

        gridLayout->addWidget(lblmh07, 3, 4, 1, 1);


        verticalLayout->addLayout(gridLayout);


        horizontalLayout_12->addLayout(verticalLayout);

        horizontalSpacer_8 = new QSpacerItem(13, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_8);


        verticalLayout_2->addLayout(horizontalLayout_12);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        lblStateX = new QLabel(widget);
        lblStateX->setObjectName(QString::fromUtf8("lblStateX"));
        lblStateX->setFont(font4);

        gridLayout_2->addWidget(lblStateX, 0, 1, 1, 1);

        lblStateY = new QLabel(widget);
        lblStateY->setObjectName(QString::fromUtf8("lblStateY"));
        QFont font5;
        font5.setBold(true);
        font5.setItalic(true);
        font5.setWeight(75);
        lblStateY->setFont(font5);

        gridLayout_2->addWidget(lblStateY, 0, 2, 1, 1);

        lblStateZ = new QLabel(widget);
        lblStateZ->setObjectName(QString::fromUtf8("lblStateZ"));
        lblStateZ->setFont(font5);

        gridLayout_2->addWidget(lblStateZ, 0, 3, 1, 1);

        lblStateRoll = new QLabel(widget);
        lblStateRoll->setObjectName(QString::fromUtf8("lblStateRoll"));
        lblStateRoll->setFont(font5);

        gridLayout_2->addWidget(lblStateRoll, 0, 4, 1, 1);

        lblStatePitch = new QLabel(widget);
        lblStatePitch->setObjectName(QString::fromUtf8("lblStatePitch"));
        lblStatePitch->setFont(font5);

        gridLayout_2->addWidget(lblStatePitch, 0, 5, 1, 1);

        lblStateYaw = new QLabel(widget);
        lblStateYaw->setObjectName(QString::fromUtf8("lblStateYaw"));
        lblStateYaw->setFont(font5);

        gridLayout_2->addWidget(lblStateYaw, 0, 6, 1, 1);

        lblStateq1 = new QLabel(widget);
        lblStateq1->setObjectName(QString::fromUtf8("lblStateq1"));
        lblStateq1->setFont(font5);

        gridLayout_2->addWidget(lblStateq1, 0, 7, 1, 1);

        lblStateq2 = new QLabel(widget);
        lblStateq2->setObjectName(QString::fromUtf8("lblStateq2"));
        lblStateq2->setFont(font5);

        gridLayout_2->addWidget(lblStateq2, 0, 8, 1, 1);

        lblStateCur = new QLabel(widget);
        lblStateCur->setObjectName(QString::fromUtf8("lblStateCur"));
        lblStateCur->setFont(font1);

        gridLayout_2->addWidget(lblStateCur, 1, 0, 1, 1);

        lblState00 = new QLabel(widget);
        lblState00->setObjectName(QString::fromUtf8("lblState00"));
        QFont font6;
        font6.setPointSize(8);
        lblState00->setFont(font6);

        gridLayout_2->addWidget(lblState00, 1, 1, 1, 1);

        lblState01 = new QLabel(widget);
        lblState01->setObjectName(QString::fromUtf8("lblState01"));
        lblState01->setFont(font6);

        gridLayout_2->addWidget(lblState01, 1, 2, 1, 1);

        lblState02 = new QLabel(widget);
        lblState02->setObjectName(QString::fromUtf8("lblState02"));
        lblState02->setFont(font6);

        gridLayout_2->addWidget(lblState02, 1, 3, 1, 1);

        lblState03 = new QLabel(widget);
        lblState03->setObjectName(QString::fromUtf8("lblState03"));
        lblState03->setFont(font6);

        gridLayout_2->addWidget(lblState03, 1, 4, 1, 1);

        lblState04 = new QLabel(widget);
        lblState04->setObjectName(QString::fromUtf8("lblState04"));
        lblState04->setFont(font6);

        gridLayout_2->addWidget(lblState04, 1, 5, 1, 1);

        lblState05 = new QLabel(widget);
        lblState05->setObjectName(QString::fromUtf8("lblState05"));
        lblState05->setFont(font6);

        gridLayout_2->addWidget(lblState05, 1, 6, 1, 1);

        lblState06 = new QLabel(widget);
        lblState06->setObjectName(QString::fromUtf8("lblState06"));
        lblState06->setFont(font6);

        gridLayout_2->addWidget(lblState06, 1, 7, 1, 1);

        lblState07 = new QLabel(widget);
        lblState07->setObjectName(QString::fromUtf8("lblState07"));
        lblState07->setFont(font6);

        gridLayout_2->addWidget(lblState07, 1, 8, 1, 1);

        lblStateCur_2 = new QLabel(widget);
        lblStateCur_2->setObjectName(QString::fromUtf8("lblStateCur_2"));
        lblStateCur_2->setFont(font1);

        gridLayout_2->addWidget(lblStateCur_2, 2, 0, 1, 1);

        lblState00_2 = new QLabel(widget);
        lblState00_2->setObjectName(QString::fromUtf8("lblState00_2"));
        lblState00_2->setFont(font6);

        gridLayout_2->addWidget(lblState00_2, 2, 1, 1, 1);

        lblState01_2 = new QLabel(widget);
        lblState01_2->setObjectName(QString::fromUtf8("lblState01_2"));
        lblState01_2->setFont(font6);

        gridLayout_2->addWidget(lblState01_2, 2, 2, 1, 1);

        lblState02_2 = new QLabel(widget);
        lblState02_2->setObjectName(QString::fromUtf8("lblState02_2"));
        lblState02_2->setFont(font6);

        gridLayout_2->addWidget(lblState02_2, 2, 3, 1, 1);

        lblState03_2 = new QLabel(widget);
        lblState03_2->setObjectName(QString::fromUtf8("lblState03_2"));
        lblState03_2->setFont(font6);

        gridLayout_2->addWidget(lblState03_2, 2, 4, 1, 1);

        lblState04_2 = new QLabel(widget);
        lblState04_2->setObjectName(QString::fromUtf8("lblState04_2"));
        lblState04_2->setFont(font6);

        gridLayout_2->addWidget(lblState04_2, 2, 5, 1, 1);

        lblState05_2 = new QLabel(widget);
        lblState05_2->setObjectName(QString::fromUtf8("lblState05_2"));
        lblState05_2->setFont(font6);

        gridLayout_2->addWidget(lblState05_2, 2, 6, 1, 1);

        lblState06_2 = new QLabel(widget);
        lblState06_2->setObjectName(QString::fromUtf8("lblState06_2"));
        lblState06_2->setFont(font6);

        gridLayout_2->addWidget(lblState06_2, 2, 7, 1, 1);

        lblState07_2 = new QLabel(widget);
        lblState07_2->setObjectName(QString::fromUtf8("lblState07_2"));
        lblState07_2->setFont(font6);

        gridLayout_2->addWidget(lblState07_2, 2, 8, 1, 1);

        lblStateDes = new QLabel(widget);
        lblStateDes->setObjectName(QString::fromUtf8("lblStateDes"));
        lblStateDes->setFont(font1);

        gridLayout_2->addWidget(lblStateDes, 3, 0, 1, 1);

        lblState10 = new QLabel(widget);
        lblState10->setObjectName(QString::fromUtf8("lblState10"));
        lblState10->setFont(font6);

        gridLayout_2->addWidget(lblState10, 3, 1, 1, 1);

        lblState11 = new QLabel(widget);
        lblState11->setObjectName(QString::fromUtf8("lblState11"));
        lblState11->setFont(font6);

        gridLayout_2->addWidget(lblState11, 3, 2, 1, 1);

        lblState12 = new QLabel(widget);
        lblState12->setObjectName(QString::fromUtf8("lblState12"));
        lblState12->setFont(font6);

        gridLayout_2->addWidget(lblState12, 3, 3, 1, 1);

        lblState13 = new QLabel(widget);
        lblState13->setObjectName(QString::fromUtf8("lblState13"));
        lblState13->setFont(font6);

        gridLayout_2->addWidget(lblState13, 3, 4, 1, 1);

        lblState14 = new QLabel(widget);
        lblState14->setObjectName(QString::fromUtf8("lblState14"));
        lblState14->setFont(font6);

        gridLayout_2->addWidget(lblState14, 3, 5, 1, 1);

        lblState15 = new QLabel(widget);
        lblState15->setObjectName(QString::fromUtf8("lblState15"));
        lblState15->setFont(font6);

        gridLayout_2->addWidget(lblState15, 3, 6, 1, 1);

        lblState16 = new QLabel(widget);
        lblState16->setObjectName(QString::fromUtf8("lblState16"));
        lblState16->setFont(font6);

        gridLayout_2->addWidget(lblState16, 3, 7, 1, 1);

        lblState17 = new QLabel(widget);
        lblState17->setObjectName(QString::fromUtf8("lblState17"));
        lblState17->setFont(font6);

        gridLayout_2->addWidget(lblState17, 3, 8, 1, 1);

        lblStateErr = new QLabel(widget);
        lblStateErr->setObjectName(QString::fromUtf8("lblStateErr"));
        lblStateErr->setFont(font1);

        gridLayout_2->addWidget(lblStateErr, 4, 0, 1, 1);

        lblState20 = new QLabel(widget);
        lblState20->setObjectName(QString::fromUtf8("lblState20"));
        lblState20->setFont(font6);

        gridLayout_2->addWidget(lblState20, 4, 1, 1, 1);

        lblState21 = new QLabel(widget);
        lblState21->setObjectName(QString::fromUtf8("lblState21"));
        lblState21->setFont(font6);

        gridLayout_2->addWidget(lblState21, 4, 2, 1, 1);

        lblState22 = new QLabel(widget);
        lblState22->setObjectName(QString::fromUtf8("lblState22"));
        lblState22->setFont(font6);

        gridLayout_2->addWidget(lblState22, 4, 3, 1, 1);

        lblState23 = new QLabel(widget);
        lblState23->setObjectName(QString::fromUtf8("lblState23"));
        lblState23->setFont(font6);

        gridLayout_2->addWidget(lblState23, 4, 4, 1, 1);

        lblState24 = new QLabel(widget);
        lblState24->setObjectName(QString::fromUtf8("lblState24"));
        lblState24->setFont(font6);

        gridLayout_2->addWidget(lblState24, 4, 5, 1, 1);

        lblState25 = new QLabel(widget);
        lblState25->setObjectName(QString::fromUtf8("lblState25"));
        lblState25->setFont(font6);

        gridLayout_2->addWidget(lblState25, 4, 6, 1, 1);

        lblState26 = new QLabel(widget);
        lblState26->setObjectName(QString::fromUtf8("lblState26"));
        lblState26->setFont(font6);

        gridLayout_2->addWidget(lblState26, 4, 7, 1, 1);

        lblState27 = new QLabel(widget);
        lblState27->setObjectName(QString::fromUtf8("lblState27"));
        lblState27->setFont(font6);

        gridLayout_2->addWidget(lblState27, 4, 8, 1, 1);

        lblStateCurVel = new QLabel(widget);
        lblStateCurVel->setObjectName(QString::fromUtf8("lblStateCurVel"));
        lblStateCurVel->setFont(font1);

        gridLayout_2->addWidget(lblStateCurVel, 5, 0, 1, 1);

        lblState30 = new QLabel(widget);
        lblState30->setObjectName(QString::fromUtf8("lblState30"));
        lblState30->setFont(font6);

        gridLayout_2->addWidget(lblState30, 5, 1, 1, 1);

        lblState31 = new QLabel(widget);
        lblState31->setObjectName(QString::fromUtf8("lblState31"));
        lblState31->setFont(font6);

        gridLayout_2->addWidget(lblState31, 5, 2, 1, 1);

        lblState32 = new QLabel(widget);
        lblState32->setObjectName(QString::fromUtf8("lblState32"));
        lblState32->setFont(font6);

        gridLayout_2->addWidget(lblState32, 5, 3, 1, 1);

        lblState33 = new QLabel(widget);
        lblState33->setObjectName(QString::fromUtf8("lblState33"));
        lblState33->setFont(font6);

        gridLayout_2->addWidget(lblState33, 5, 4, 1, 1);

        lblState34 = new QLabel(widget);
        lblState34->setObjectName(QString::fromUtf8("lblState34"));
        lblState34->setFont(font6);

        gridLayout_2->addWidget(lblState34, 5, 5, 1, 1);

        lblState35 = new QLabel(widget);
        lblState35->setObjectName(QString::fromUtf8("lblState35"));
        lblState35->setFont(font6);

        gridLayout_2->addWidget(lblState35, 5, 6, 1, 1);

        lblState36 = new QLabel(widget);
        lblState36->setObjectName(QString::fromUtf8("lblState36"));
        lblState36->setFont(font6);

        gridLayout_2->addWidget(lblState36, 5, 7, 1, 1);

        lblState37 = new QLabel(widget);
        lblState37->setObjectName(QString::fromUtf8("lblState37"));
        lblState37->setFont(font6);

        gridLayout_2->addWidget(lblState37, 5, 8, 1, 1);

        lblStateDesVel = new QLabel(widget);
        lblStateDesVel->setObjectName(QString::fromUtf8("lblStateDesVel"));
        lblStateDesVel->setFont(font1);

        gridLayout_2->addWidget(lblStateDesVel, 6, 0, 1, 1);

        lblState40 = new QLabel(widget);
        lblState40->setObjectName(QString::fromUtf8("lblState40"));
        lblState40->setFont(font6);

        gridLayout_2->addWidget(lblState40, 6, 1, 1, 1);

        lblState41 = new QLabel(widget);
        lblState41->setObjectName(QString::fromUtf8("lblState41"));
        lblState41->setFont(font6);

        gridLayout_2->addWidget(lblState41, 6, 2, 1, 1);

        lblState42 = new QLabel(widget);
        lblState42->setObjectName(QString::fromUtf8("lblState42"));
        lblState42->setFont(font6);

        gridLayout_2->addWidget(lblState42, 6, 3, 1, 1);

        lblState43 = new QLabel(widget);
        lblState43->setObjectName(QString::fromUtf8("lblState43"));
        lblState43->setFont(font6);

        gridLayout_2->addWidget(lblState43, 6, 4, 1, 1);

        lblState44 = new QLabel(widget);
        lblState44->setObjectName(QString::fromUtf8("lblState44"));
        lblState44->setFont(font6);

        gridLayout_2->addWidget(lblState44, 6, 5, 1, 1);

        lblState45 = new QLabel(widget);
        lblState45->setObjectName(QString::fromUtf8("lblState45"));
        lblState45->setFont(font6);

        gridLayout_2->addWidget(lblState45, 6, 6, 1, 1);

        lblState46 = new QLabel(widget);
        lblState46->setObjectName(QString::fromUtf8("lblState46"));
        lblState46->setFont(font6);

        gridLayout_2->addWidget(lblState46, 6, 7, 1, 1);

        lblState47 = new QLabel(widget);
        lblState47->setObjectName(QString::fromUtf8("lblState47"));
        lblState47->setFont(font6);

        gridLayout_2->addWidget(lblState47, 6, 8, 1, 1);


        verticalLayout_2->addLayout(gridLayout_2);

        verticalSpacer_4 = new QSpacerItem(20, 28, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_4);

        layoutWidget2->raise();
        layoutWidget3->raise();
        layoutWidget4->raise();
        layoutWidget1->raise();
        est_m3h_2->raise();
        est_m3h_3->raise();
        est_val_2->raise();
        est_m2h_2->raise();
        lblmh07->raise();
        est_m3h_4->raise();
        est_m2h_3->raise();
        est_m3h_5->raise();
        lblmh04->raise();
        lblmh05->raise();
        lblmh06->raise();
        lblmh02->raise();
        lblmh03->raise();

        retranslateUi(QuadrotorInterface);

        QMetaObject::connectSlotsByName(QuadrotorInterface);
    } // setupUi

    void retranslateUi(QWidget *QuadrotorInterface)
    {
        QuadrotorInterface->setWindowTitle(QApplication::translate("QuadrotorInterface", "Form", 0, QApplication::UnicodeUTF8));
        chkEnabled->setText(QApplication::translate("QuadrotorInterface", "Enabled", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("QuadrotorInterface", "ControCalc time :", 0, QApplication::UnicodeUTF8));
        lblControlCalcTime->setText(QApplication::translate("QuadrotorInterface", "000ms", 0, QApplication::UnicodeUTF8));
        lblFlightMode->setText(QApplication::translate("QuadrotorInterface", "IDLE", 0, QApplication::UnicodeUTF8));
        groupBox_4->setTitle(QApplication::translate("QuadrotorInterface", "Motor Speed", 0, QApplication::UnicodeUTF8));
        m1->setText(QApplication::translate("QuadrotorInterface", "M1", 0, QApplication::UnicodeUTF8));
        lblendmot5->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        m6->setText(QApplication::translate("QuadrotorInterface", "M6", 0, QApplication::UnicodeUTF8));
        lblendmot6->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        lblendmot2->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        lblendmot1->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        m2->setText(QApplication::translate("QuadrotorInterface", "M2", 0, QApplication::UnicodeUTF8));
        lblendmot4->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        m5->setText(QApplication::translate("QuadrotorInterface", "M5", 0, QApplication::UnicodeUTF8));
        lblendmot3->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        m3->setText(QApplication::translate("QuadrotorInterface", "M3", 0, QApplication::UnicodeUTF8));
        m4->setText(QApplication::translate("QuadrotorInterface", "M4", 0, QApplication::UnicodeUTF8));
        groupBox_5->setTitle(QApplication::translate("QuadrotorInterface", "End-effector pos", 0, QApplication::UnicodeUTF8));
        groupBox_6->setTitle(QApplication::translate("QuadrotorInterface", "End-effector pos", 0, QApplication::UnicodeUTF8));
        chkEnabled_2->setText(QApplication::translate("QuadrotorInterface", "Enabled", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("QuadrotorInterface", "ControCalc time :", 0, QApplication::UnicodeUTF8));
        lblControlCalcTime_2->setText(QApplication::translate("QuadrotorInterface", "000ms", 0, QApplication::UnicodeUTF8));
        lblFlightMode_2->setText(QApplication::translate("QuadrotorInterface", "IDLE", 0, QApplication::UnicodeUTF8));
        label_17->setText(QApplication::translate("QuadrotorInterface", "Yc", 0, QApplication::UnicodeUTF8));
        label_18->setText(QApplication::translate("QuadrotorInterface", "Xcd ", 0, QApplication::UnicodeUTF8));
        lblendposz->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        lblenddx->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        label_19->setText(QApplication::translate("QuadrotorInterface", "Ycd ", 0, QApplication::UnicodeUTF8));
        lblenddy->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        lblendposx_5->setText(QApplication::translate("QuadrotorInterface", "Zcd", 0, QApplication::UnicodeUTF8));
        lblenddz->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        lblendposy->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        lblendposx_4->setText(QApplication::translate("QuadrotorInterface", "Zc", 0, QApplication::UnicodeUTF8));
        lblendposx->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        xc->setText(QApplication::translate("QuadrotorInterface", "Xc ", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QApplication::translate("QuadrotorInterface", "State", 0, QApplication::UnicodeUTF8));
        btnSetXyTarget->setText(QApplication::translate("QuadrotorInterface", "Set XY", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("QuadrotorInterface", "Dummy", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("QuadrotorInterface", "GPS Status", 0, QApplication::UnicodeUTF8));
        est_m2h->setText(QApplication::translate("QuadrotorInterface", "dx", 0, QApplication::UnicodeUTF8));
        est_m3h->setText(QApplication::translate("QuadrotorInterface", "dy", 0, QApplication::UnicodeUTF8));
        est_m3h_2->setText(QApplication::translate("QuadrotorInterface", "z", 0, QApplication::UnicodeUTF8));
        est_m3h_3->setText(QApplication::translate("QuadrotorInterface", "dz", 0, QApplication::UnicodeUTF8));
        est_val->setText(QApplication::translate("QuadrotorInterface", "esti", 0, QApplication::UnicodeUTF8));
        lblmh00->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        lblmh01->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        lblmh02->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        lblmh03->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        est_m2h_3->setText(QApplication::translate("QuadrotorInterface", "x", 0, QApplication::UnicodeUTF8));
        est_m3h_4->setText(QApplication::translate("QuadrotorInterface", "y", 0, QApplication::UnicodeUTF8));
        est_m3h_5->setText(QApplication::translate("QuadrotorInterface", "z", 0, QApplication::UnicodeUTF8));
        est_m2h_2->setText(QApplication::translate("QuadrotorInterface", "Sat", 0, QApplication::UnicodeUTF8));
        est_val_2->setText(QApplication::translate("QuadrotorInterface", "accuracy", 0, QApplication::UnicodeUTF8));
        lblmh04->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        lblmh05->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        lblmh06->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        lblmh07->setText(QApplication::translate("QuadrotorInterface", "000", 0, QApplication::UnicodeUTF8));
        lblStateX->setText(QApplication::translate("QuadrotorInterface", "x", 0, QApplication::UnicodeUTF8));
        lblStateY->setText(QApplication::translate("QuadrotorInterface", "y", 0, QApplication::UnicodeUTF8));
        lblStateZ->setText(QApplication::translate("QuadrotorInterface", "z", 0, QApplication::UnicodeUTF8));
        lblStateRoll->setText(QApplication::translate("QuadrotorInterface", "roll", 0, QApplication::UnicodeUTF8));
        lblStatePitch->setText(QApplication::translate("QuadrotorInterface", "pitch", 0, QApplication::UnicodeUTF8));
        lblStateYaw->setText(QApplication::translate("QuadrotorInterface", "yaw", 0, QApplication::UnicodeUTF8));
        lblStateq1->setText(QApplication::translate("QuadrotorInterface", "q1", 0, QApplication::UnicodeUTF8));
        lblStateq2->setText(QApplication::translate("QuadrotorInterface", "q2", 0, QApplication::UnicodeUTF8));
        lblStateCur->setText(QApplication::translate("QuadrotorInterface", "cur", 0, QApplication::UnicodeUTF8));
        lblState00->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState01->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState02->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState03->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState04->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState05->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState06->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState07->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblStateCur_2->setText(QApplication::translate("QuadrotorInterface", "est", 0, QApplication::UnicodeUTF8));
        lblState00_2->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState01_2->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState02_2->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState03_2->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState04_2->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState05_2->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState06_2->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState07_2->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblStateDes->setText(QApplication::translate("QuadrotorInterface", "des", 0, QApplication::UnicodeUTF8));
        lblState10->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState11->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState12->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState13->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState14->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState15->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState16->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState17->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblStateErr->setText(QApplication::translate("QuadrotorInterface", "err", 0, QApplication::UnicodeUTF8));
        lblState20->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState21->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState22->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState23->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState24->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState25->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState26->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState27->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblStateCurVel->setText(QApplication::translate("QuadrotorInterface", "cur vel", 0, QApplication::UnicodeUTF8));
        lblState30->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState31->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState32->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState33->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState34->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState35->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState36->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState37->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblStateDesVel->setText(QApplication::translate("QuadrotorInterface", "des vel", 0, QApplication::UnicodeUTF8));
        lblState40->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState41->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState42->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState43->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState44->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState45->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState46->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
        lblState47->setText(QApplication::translate("QuadrotorInterface", "0.0", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class QuadrotorInterface: public Ui_QuadrotorInterface {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QUADROTORINTERFACE_H
