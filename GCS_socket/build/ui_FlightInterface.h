/********************************************************************************
** Form generated from reading UI file 'FlightInterface.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FLIGHTINTERFACE_H
#define UI_FLIGHTINTERFACE_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QStatusBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FlightInterface
{
public:
    QWidget *centralwidget;
    QWidget *layoutWidget6;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QLabel *lblRunTime;
    QFrame *line_3;
    QPushButton *btnBeginTracking;
    QPushButton *btnConnect;
    QPushButton *btnQuit;
    QSpacerItem *horizontalSpacer_2;
    QWidget *layoutWidget7;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *layQuadA;
    QHBoxLayout *layQuadSelectA;
    QSpacerItem *horizontalSpacer_4;
    QVBoxLayout *verticalLayout_2;
    QSpacerItem *verticalSpacer;
    QSpacerItem *verticalSpacer_3;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *FlightInterface)
    {
        if (FlightInterface->objectName().isEmpty())
            FlightInterface->setObjectName(QString::fromUtf8("FlightInterface"));
        FlightInterface->resize(463, 525);
        FlightInterface->setAutoFillBackground(true);
        FlightInterface->setLocale(QLocale(QLocale::English, QLocale::UnitedStates));
        centralwidget = new QWidget(FlightInterface);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        layoutWidget6 = new QWidget(centralwidget);
        layoutWidget6->setObjectName(QString::fromUtf8("layoutWidget6"));
        layoutWidget6->setGeometry(QRect(0, 0, 451, 29));
        horizontalLayout_2 = new QHBoxLayout(layoutWidget6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget6);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_2->addWidget(label);

        lblRunTime = new QLabel(layoutWidget6);
        lblRunTime->setObjectName(QString::fromUtf8("lblRunTime"));
        sizePolicy.setHeightForWidth(lblRunTime->sizePolicy().hasHeightForWidth());
        lblRunTime->setSizePolicy(sizePolicy);
        lblRunTime->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(lblRunTime);

        line_3 = new QFrame(layoutWidget6);
        line_3->setObjectName(QString::fromUtf8("line_3"));
        line_3->setFrameShape(QFrame::VLine);
        line_3->setFrameShadow(QFrame::Sunken);

        horizontalLayout_2->addWidget(line_3);

        btnBeginTracking = new QPushButton(layoutWidget6);
        btnBeginTracking->setObjectName(QString::fromUtf8("btnBeginTracking"));

        horizontalLayout_2->addWidget(btnBeginTracking);

        btnConnect = new QPushButton(layoutWidget6);
        btnConnect->setObjectName(QString::fromUtf8("btnConnect"));

        horizontalLayout_2->addWidget(btnConnect);

        btnQuit = new QPushButton(layoutWidget6);
        btnQuit->setObjectName(QString::fromUtf8("btnQuit"));

        horizontalLayout_2->addWidget(btnQuit);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        layoutWidget7 = new QWidget(centralwidget);
        layoutWidget7->setObjectName(QString::fromUtf8("layoutWidget7"));
        layoutWidget7->setGeometry(QRect(0, 30, 451, 431));
        horizontalLayout = new QHBoxLayout(layoutWidget7);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        layQuadA = new QVBoxLayout();
        layQuadA->setObjectName(QString::fromUtf8("layQuadA"));
        layQuadSelectA = new QHBoxLayout();
        layQuadSelectA->setObjectName(QString::fromUtf8("layQuadSelectA"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        layQuadSelectA->addItem(horizontalSpacer_4);


        layQuadA->addLayout(layQuadSelectA);


        verticalLayout->addLayout(layQuadA);


        horizontalLayout->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_3);


        horizontalLayout->addLayout(verticalLayout_2);

        FlightInterface->setCentralWidget(centralwidget);
        menubar = new QMenuBar(FlightInterface);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 463, 25));
        FlightInterface->setMenuBar(menubar);
        statusbar = new QStatusBar(FlightInterface);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        FlightInterface->setStatusBar(statusbar);

        retranslateUi(FlightInterface);

        QMetaObject::connectSlotsByName(FlightInterface);
    } // setupUi

    void retranslateUi(QMainWindow *FlightInterface)
    {
        FlightInterface->setWindowTitle(QApplication::translate("FlightInterface", "MainWindow", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("FlightInterface", "Run Time:", 0, QApplication::UnicodeUTF8));
        lblRunTime->setText(QApplication::translate("FlightInterface", "000", 0, QApplication::UnicodeUTF8));
        btnBeginTracking->setText(QApplication::translate("FlightInterface", "Begin Tracking", 0, QApplication::UnicodeUTF8));
        btnConnect->setText(QApplication::translate("FlightInterface", "Connect", 0, QApplication::UnicodeUTF8));
        btnQuit->setText(QApplication::translate("FlightInterface", "Quit", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class FlightInterface: public Ui_FlightInterface {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FLIGHTINTERFACE_H
