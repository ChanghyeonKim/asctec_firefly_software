close all; clear; clc
%%
DELIMITER = ',';
HEADERLINES = 1;
Rm2=0.0;

start=1;
enp=270;

% Import the file
newData1 = importdata('runData/Firefly0/data.csv', DELIMITER, HEADERLINES);


% vars = fieldnames(newData1);
% for i = 1:length(var s)
%     assignin('base', vars{i}, newData1.(vars{i}));
% end

frame           = newData1.data(:,1)';
flightMode      = newData1.data(:,2)';
time            = newData1.data(:,3)'/1000;
commandVals     = newData1.data(:,4:5)';
targetPos       = newData1.data(:,6:17)';
state           = newData1.data(:,18:29)';
motor           = newData1.data(:,30:35)';
eststate        = newData1.data(:,36:41)';
estvelstate        = newData1.data(:,42:47)';
estxy           = newData1.data(:,48:54)';
gpss           = newData1.data(:,55:58)';
calctime        = newData1.data(:,59)';

targetPos(6,:) =- targetPos(6,:);
nm=size(targetPos(6,:),2);

ango=-pi/3;
for i=1:nm
    estxy(1:2,i)=[cos(ango) sin(ango);-sin(ango) cos(ango)]*estxy(1:2,i);
        
end
estxy(1,:)=-estxy(1,:);
state(1,:)=state(1,:);
state(2,:)=state(2,:);
targetPos(1,:)=targetPos(1,:);
targetPos(2,:)=targetPos(2,:);

targetPos(4:6,:) = targetPos(4:6,:)*180/pi;

state(4:6,:) = state(4:6,:)*180/pi;
eststate(4:6,:) = eststate(4:6,:)*180/pi;
eststate(6,:) =-eststate(6,:);
targetPos(5,:) = targetPos(5,:)+0;

ns=size(targetPos(5,:),2);

%%

figure,
set(gcf,'Name','Desired Postion Tracking')
subplot(3,2,1), plot(time(start:end),targetPos(1,start:end),'--r',time(start:end),state(1,start:end),'-b',time(start:end),eststate(1,start:end),'-.m');
axis([0 time(end) -1.0 1.0]);
subplot(3,2,3), plot(time(start:end),targetPos(2,start:end),'--r',time(start:end),state(2,start:end),'-b',time(start:end),eststate(2,start:end),'-.m');
axis([0 time(end) -1.0 1.0]);
subplot(3,2,5), plot(time(start:end),targetPos(3,start:end),'--r',time(start:end),state(3,start:end),'-b',time(start:end),eststate(3,start:end),'-.m');
axis([0 time(end) 0 2]);


subplot(3,2,2), plot(time(start:end),targetPos(4,start:end),'--r',time(start:end),state(4,start:end),'-b',time(start:end),eststate(4,start:end),'-.m');
subplot(3,2,4), plot(time(start:end),targetPos(5,start:end),'--r',time(start:end),state(5,start:end),'-b',time(start:end),eststate(5,start:end),'-.m');
subplot(3,2,6), plot(time(start:end),targetPos(6,start:end),'--r',time(start:end),state(6,start:end),'-b',time(start:end),eststate(6,start:end),'-.m');
axis([0 time(end) -20 20]);

figure,
set(gcf,'Name','Desired Velocity Tracking')
subplot(3,1,1), plot(time(start:end),targetPos(7,start:end),'--r',time(start:end),state(7,start:end),'-b',time(start:end),estvelstate(1,start:end),'-.m');
% axis([0 time(end) -1.5 1.5]);
subplot(3,1,2), plot(time(start:end),targetPos(8,start:end),'--r',time(start:end),state(8,start:end),'-b',time(start:end),estvelstate(2,start:end),'-.m');
% axis([0 time(end) -1.5 1.5]);
subplot(3,1,3), plot(time(start:end),targetPos(9,start:end),'--r',time(start:end),state(9,start:end),'-b',time(start:end),estvelstate(3,start:end),'-.m');
% axis([0 time(end) 0 2]);


figure
plot(eststate(1,1),eststate(2,1),'bo',eststate(1,start:end),eststate(2,start:end),'-b',state(1,start:end),state(2,start:end),'-g',state(1,1),state(2,1),'go');
axis([-1 1 -1 1]);







