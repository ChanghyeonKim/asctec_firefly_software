#include "core/weight_calculation.h"



void computeWeight(const Eigen::MatrixXd& r_tot, const optsPIVO& opts, double& outputScale, Eigen::MatrixXd& W_tot)
{
    // (1) tDistriScaleEstimator part (estimate the scale of residual vector)

    const double initial_sigma = opts.iniSigma;
    const double default_dof = opts.tDistriDof;

    double lambda = 1.0f / (initial_sigma * initial_sigma);
    double initial_lamda = lambda;
    outputScale = 0;

    double num = 0;
    size_t idx = 0;

    while(1)
    {
        // initial value
        initial_lamda = lambda;
        num = 0.0f;
        lambda = 0.0f;


        // estimate scale parameter
        for(idx = 0; idx < r_tot.rows(); ++idx)
        {
            if(r_tot(idx) != -1000)
            {
                num += 1.0f;
                lambda += (r_tot(idx) * r_tot(idx)) * ( (default_dof + 1.0f) / (default_dof + initial_lamda * r_tot(idx) * r_tot(idx)) );
            }
        }


        // transform
        lambda = (lambda / num);
        lambda = (1.0f / lambda);


        // exit condition
        if(std::abs(lambda - initial_lamda) <= 1e-6)
        {
            break;
        }
    }

    outputScale = std::sqrt(1.0f / lambda);

    if(outputScale < 0.001)
    {
        outputScale = 0.001;
    }

    // for debug
    //cout << outputScale << endl;
    //usleep(10000000);



    // (2) tDistriWeightFtn part (calculate the weights of each residual value in residual vector)

    for(idx = 0; idx < r_tot.rows(); ++idx)
    {
        if(r_tot(idx) != -1000)
        {
            W_tot(idx) = ( default_dof + 1.0f ) / ( default_dof + ( (r_tot(idx) * r_tot(idx)) / (outputScale * outputScale) ) );
        }
    }

    // for debug
    //saveEigenMatrix("W_tot.txt",W_tot);
    //usleep(10000000);
}



