#include <cstdio>
#include <fstream>
#include <opencv2/highgui/highgui_c.h>
#include "core/fileIO.h"


void getImgInKITTIdataset(const std::string& leftImgFileName, const std::string& rightImgFileName, const camCalibrationParam& cam, cv::Mat& outputGreyImg, cv::Mat& outputDepthImg)
{
    // declare Mat variables
    cv::Mat grey_64F, depth_64F;


    // read left gray image
    cv::Mat imLeft, imRight;
    imLeft = cv::imread(leftImgFileName, 0);      // read a grayscale image
    imRight = cv::imread(rightImgFileName, 0);    // read a grayscale image

    // for debug
    //cv::imshow("imLeft view",imLeft);
    //cv::waitKey(0);
    //std::cout << imLeft.cols << std::endl;
    //std::cout << imLeft.rows << std::endl;
    //std::cout << (imLeft.type() == CV_8UC1) << std::endl;
    //usleep(1000000);


    // fit the pyramid size (32 times)
    cv::Mat targetImage;
    cv::Rect roi(0, 0, 32*38, 32*11);  // roi(uStart,vStart,uLength,vLength)
    targetImage = imLeft(roi);

    // for debug
    //cv::imshow("targetImage view",targetImage);
    //cv::waitKey(0);
    //std::cout << targetImage.cols << std::endl;
    //std::cout << targetImage.rows << std::endl;
    //std::cout << (targetImage.type() == CV_8UC1) << std::endl;
    //usleep(10000000);


    // transform from integer to double
    targetImage.convertTo(grey_64F, CV_64FC1); // grey_64F.type() is CV_64FC1 (double)

    // for debug
    //std::cout << grey_64F.cols << std::endl;
    //std::cout << grey_64F.rows << std::endl;
    //std::cout << grey_64F.at<double>(17,33)<< std::endl;
    //std::cout << (grey_64F.type() == CV_64FC1) << std::endl;
    //usleep(10000000);




    // perform stereo matching
    cv::Mat disLeftTranspose(imLeft.cols, imLeft.rows, CV_64FC1);
    imwrite("tempLeftImg.pgm", imLeft);
    imwrite("tempRightImg.pgm", imRight);
    process("tempLeftImg.pgm", "tempRightImg.pgm", disLeftTranspose);

    cv::Mat disLeft;
    cv::transpose(disLeftTranspose, disLeft);

    // for debug
    //std::cout << disLeft.cols << std::endl;
    //std::cout << disLeft.rows << std::endl;
    //std::cout << disLeft.at<double>(114,575) << std::endl;
    //std::cout << (disLeft.type() == CV_64FC1) << std::endl;
    //usleep(10000000);


    // extract depth map (Z) from disparity map
    cv::Mat imgZ = disLeft;
    for(int32_t v = 0; v < disLeft.rows; v++)
    {
        for(int32_t u = 0; u < disLeft.cols; u++)
        {
            if (disLeft.at<double>(v,u) > 0)
            {
                imgZ.at<double>(v,u) = (1/disLeft.at<double>(v,u));
            }
            else
            {
                imgZ.at<double>(v,u) = 0;
            }
        }
    }
    imgZ = cam.K(0,0) * cam.baseline * imgZ;


    // remove depth value over 50 m (unreliable value)
    for(int32_t v = 0; v < imgZ.rows; v++)
    {
        for(int32_t u = 0; u < imgZ.cols; u++)
        {
            if (imgZ.at<double>(v,u) > 50)
            {
                imgZ.at<double>(v,u) = 0;
            }
        }
    }

    // for debug
    //std::cout << imgZ.cols << std::endl;
    //std::cout << imgZ.rows << std::endl;
    //std::cout << imgZ.at<double>(86,516) << std::endl;
    //std::cout << (imgZ.type() == CV_64FC1) << std::endl;
    //usleep(10000000);


    // fit the pyramid size (32 times)
    cv::Mat targetDepthImage;
    targetDepthImage = imgZ(roi);


    // transform from double to double
    targetDepthImage.convertTo(depth_64F, CV_64FC1); // depth_64F.type() is CV_64FC1 (double)

    // for debug
    //std::cout << depth_64F.cols << std::endl;
    //std::cout << depth_64F.rows << std::endl;
    //std::cout << depth_64F.at<double>(38,517) << std::endl;
    //std::cout << (depth_64F.type() == CV_64FC1) << std::endl;
    //usleep(10000000);


    // return the results
    outputGreyImg = grey_64F;
    outputDepthImg = depth_64F;
}



void saveEigenMatrix(const char *_Filename, const Eigen::MatrixXd& inputMatrix)
{
    // initialize text stream
    std::ofstream matrixTextFile_opener(_Filename);


    // save the inputMatrix
    if (matrixTextFile_opener.is_open())
    {
        matrixTextFile_opener << inputMatrix << '\n';
        matrixTextFile_opener.close();
        std::cout << "File save success in saveEigenMatrix!" << std::endl;
    }
    else
    {
        std::cout << "File open error in saveEigenMatrix!" << std::endl;
    }
}



void saveMatImage(const char *_Filename, const cv::Mat& inputImage)
{
    // initialize text stream
    std::ofstream matTextFile_opener(_Filename);


    // save the inputImage
    if (matTextFile_opener.is_open())
    {
        for(size_t i = 0; i < inputImage.rows; i++)
        {
            for(size_t j = 0; j < inputImage.cols; j++)
            {
                matTextFile_opener << inputImage.at<double>(i,j) << "\t";
            }
            matTextFile_opener << std::endl;
        }
        matTextFile_opener.close();
        std::cout << "File save success in saveMatImage!" << std::endl;
    }
    else
    {
        std::cout << "File open error in saveMatImage!" << std::endl;
    }
}



void saveSGPVOresults(const std::vector<Eigen::Matrix4d>& T_1i_Save, const Eigen::MatrixXd& stateEsti, const std::vector<illParam_perImg>& illParamPIVO_Save,
                      const std::vector<double>& errPIVO_Save, const std::vector<int>& keyIdx_Save)
{
    // save the motion estimation results (T_1i_Save)
    Eigen::MatrixXd Tvector(16,T_1i_Save.size());
    Eigen::Matrix4d Tcurrent = Eigen::Matrix4d::Identity(4,4);

    for(size_t T_idx = 0; T_idx < T_1i_Save.size(); ++T_idx)
    {
        // assign current estimated transformation matrix : Tcurrent
        Tcurrent = T_1i_Save[T_idx];

        // vectorization from (Tcurrent) to (Tvector)
        Tvector(0,T_idx) = Tcurrent(0,0);
        Tvector(1,T_idx) = Tcurrent(1,0);
        Tvector(2,T_idx) = Tcurrent(2,0);
        Tvector(3,T_idx) = Tcurrent(3,0);

        Tvector(4,T_idx) = Tcurrent(0,1);
        Tvector(5,T_idx) = Tcurrent(1,1);
        Tvector(6,T_idx) = Tcurrent(2,1);
        Tvector(7,T_idx) = Tcurrent(3,1);

        Tvector(8,T_idx) = Tcurrent(0,2);
        Tvector(9,T_idx) = Tcurrent(1,2);
        Tvector(10,T_idx) = Tcurrent(2,2);
        Tvector(11,T_idx) = Tcurrent(3,2);

        Tvector(12,T_idx) = Tcurrent(0,3);
        Tvector(13,T_idx) = Tcurrent(1,3);
        Tvector(14,T_idx) = Tcurrent(2,3);
        Tvector(15,T_idx) = Tcurrent(3,3);
    }

    saveEigenMatrix("Tvector.txt",Tvector);



    // save the motion estimation results (stateEsti)
    saveEigenMatrix("stateEsti.txt",stateEsti);



    // save the light variation model parameters (illParamPIVO_Save)
    Eigen::MatrixXd illParameterPIVO(54,illParamPIVO_Save.size());

    for(size_t Ill_idx = 0; Ill_idx < illParamPIVO_Save.size(); ++Ill_idx)
    {

        for(size_t patchIdx = 0; patchIdx < 27; ++patchIdx )
        {
            illParameterPIVO(( 2*patchIdx ),Ill_idx) = illParamPIVO_Save[Ill_idx].patch[patchIdx].contrast;
            illParameterPIVO((2*patchIdx+1),Ill_idx) = illParamPIVO_Save[Ill_idx].patch[patchIdx].bias;
        }

        //illParameterPIVO(0,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[0].contrast;
        //illParameterPIVO(1,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[0].bias;

        //illParameterPIVO(2,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[1].contrast;
        //illParameterPIVO(3,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[1].bias;

        //illParameterPIVO(4,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[2].contrast;
        //illParameterPIVO(5,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[2].bias;

        //illParameterPIVO(6,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[3].contrast;
        //illParameterPIVO(7,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[3].bias;

        //illParameterPIVO(8,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[4].contrast;
        //illParameterPIVO(9,Ill_idx) = illParamPIVO_Save[Ill_idx].patch[4].bias;
    }

    saveEigenMatrix("illParameterPIVO.txt",illParameterPIVO);



    // save the error values from the proposed cost function (errPIVO_Save)
    Eigen::RowVectorXd errVector(errPIVO_Save.size());

    for(size_t err_idx = 0; err_idx < errPIVO_Save.size(); ++err_idx)
    {
        errVector(err_idx) = errPIVO_Save[err_idx];
    }

    saveEigenMatrix("errVector.txt",errVector);



    // save the keyframe index for debug
    Eigen::RowVectorXd keyIdxVector(keyIdx_Save.size());

    for(size_t key_idx = 0; key_idx < keyIdx_Save.size(); ++key_idx)
    {
        keyIdxVector(key_idx) = keyIdx_Save[key_idx];
    }

    saveEigenMatrix("keyIdxVector.txt",keyIdxVector);
}




