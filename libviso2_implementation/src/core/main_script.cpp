#include "core/libelas.h"
#include "core/libviso2.h"
#include "core/datatypes.h"
#include "core/rgbd_image.h"
#include "core/intrinsic_matrix.h"
#include "core/weight_calculation.h"
#include "core/least_squares.h"
#include "core/fileIO.h"
#include "core/Liegroup_operation.h"
#include "core/dense_tracking.h"
#include "core/vi_sensor_interface.hpp"
#include "core/communication.h"
#include "core/time.h"



int main(void)
{
    // set camera calibration parameters for libviso2 (look at: viso_stereo.h)
    VisualOdometryStereo::parameters param;
    param.calib.f  = 473.726386937359;   // focal length in pixels
    param.calib.cu = 437.321655295377;   // principal point (u-coordinate) in pixels
    param.calib.cv = 274.151488797300;   // principal point (v-coordinate) in pixels
    param.base     = 0.109249033006859;  // baseline in meters

    // for debug
    //std::cout << param.calib.f << std::endl;
    //std::cout << param.calib.cu << std::endl;
    //std::cout << param.calib.cv << std::endl;
    //std::cout << param.base << std::endl;
    //usleep(10000000);


    // init visual odometry (libviso2)
    VisualOdometryStereo viso(param);


    /**********************************************
     *
     * PART 1:
     * Main Part -> Calculate Transformation matrix
     *
     ***********************************************/

    // declare trivial variables for drawing
    Eigen::Vector3d x_axis_end_c, x_axis_end_g;
    Eigen::Vector3d y_axis_end_c, y_axis_end_g;
    Eigen::Vector3d z_axis_end_c, z_axis_end_g;
    x_axis_end_c << 0.5, 0, 0;
    y_axis_end_c << 0, 0.5, 0;
    z_axis_end_c << 0, 0, 0.5;
    x_axis_end_g.fill(0);
    y_axis_end_g.fill(0);
    z_axis_end_g.fill(0);
    Eigen::Vector3d center_position, euler_angle;
    center_position.fill(0);
    euler_angle.fill(0);
    int image_counter = 0;


    // initialize variables
    cv::Mat vi_cam0_image_rect, vi_cam1_image_rect;


    // initialize rigid body transformation matrices
    Eigen::Matrix4d Tr_total = Eigen::Matrix4d::Identity(4,4);
    Eigen::Matrix4d T_temp = Eigen::Matrix4d::Identity(4,4);


    // initialize WiFi communication with server (host computer)
    onBtnConnect_clicked();
    TNT::Array2D<double> Est_data(9,1,0.0);


    // initialize vi sensor
    ViSensorInterface visensor(25, 200);
    visensor.run();
    std::cout << "VI sensor start." << std::endl;
    usleep(3000000);


    // do stereo visual odometry
    while(visensor.vi_sensor_connected_)
    {
        usleep(10);

        if (visensor.image_rect_flag_)
        {
            // read left and right rectified images from vi sensor
            vi_cam0_image_rect = visensor.left_image_rect_;
            vi_cam1_image_rect = visensor.right_image_rect_;
            visensor.image_rect_flag_ = 0;

            // for debug
            //std::cout << left_image_name << std::endl;
            //std::cout << right_image_name << std::endl;
            //usleep(1000000);


            // compute and accumulate egomotion with libviso2
            T_temp = Eigen::Matrix4d::Identity(4,4);
            visualOdometryStereoMex(vi_cam0_image_rect, vi_cam1_image_rect, viso, T_temp);
            if (image_counter > 0)
            {
                Tr_total = Tr_total * T_temp.inverse();
            }


            // display the current libviso2 tracking status
            image_counter += 1;
            double num_matches = viso.getNumberOfMatches();
            double num_inliers = viso.getNumberOfInliers();
            printf("Frame: %04d, Matches: %04d, Inliers: %4.1lf %% \n", image_counter, (int)num_matches, (100.0*num_inliers/num_matches));


            // Inertial frame compensation (GPS or VICON)
            std::string current_inertial_frame = "VICON"; // or VICON
            Eigen::Matrix4d Tr_total_final = Eigen::Matrix4d::Identity(4,4);
            if ( !strcmp("GPS", current_inertial_frame.c_str()) )
            {
              Eigen::Vector3d euler_angle_GPS;
              euler_angle_GPS.fill(0);
              euler_angle_GPS(0) = -110 * (3.14/180);   // rotation along X axis from inertial to body
              euler_angle_GPS(1) = 0 * (3.14/180);      // rotation along Y axis from inertial to body
              euler_angle_GPS(2) = 0 * (3.14/180);      // rotation along Z axis from inertial to body

              Eigen::Matrix3d temp, rotation_matrix_GPS;
              temp.fill(0);
              rotation_matrix_GPS.fill(0);
              angle2rotmtx(euler_angle_GPS, temp);
              rotation_matrix_GPS = temp.inverse();

              Eigen::Matrix4d transformation_GPS = Eigen::Matrix4d::Identity(4,4);
              transformation_GPS(0,0) = rotation_matrix_GPS(0,0);
              transformation_GPS(0,1) = rotation_matrix_GPS(0,1);
              transformation_GPS(0,2) = rotation_matrix_GPS(0,2);

              transformation_GPS(1,0) = rotation_matrix_GPS(1,0);
              transformation_GPS(1,1) = rotation_matrix_GPS(1,1);
              transformation_GPS(1,2) = rotation_matrix_GPS(1,2);

              transformation_GPS(2,0) = rotation_matrix_GPS(2,0);
              transformation_GPS(2,1) = rotation_matrix_GPS(2,1);
              transformation_GPS(2,2) = rotation_matrix_GPS(2,2);

              Tr_total_final = transformation_GPS * Tr_total;
            }
            else if ( !strcmp("VICON", current_inertial_frame.c_str()) )
            {
              Eigen::Vector3d euler_angle_VICON;
              euler_angle_VICON.fill(0);
              euler_angle_VICON(0) = -110 * (3.14/180);   //  rotation along X axis from inertial to body
              euler_angle_VICON(1) = 0 * (3.14/180);      //  rotation along Y axis from inertial to body
              euler_angle_VICON(2) = -90 * (3.14/180);    //  rotation along Z axis from inertial to body

              Eigen::Matrix3d temp, rotation_matrix_VICON;
              temp.fill(0);
              rotation_matrix_VICON.fill(0);
              angle2rotmtx(euler_angle_VICON, temp);
              rotation_matrix_VICON = temp.inverse();

              Eigen::Matrix4d transformation_VICON = Eigen::Matrix4d::Identity(4,4);
              transformation_VICON(0,0) = rotation_matrix_VICON(0,0);
              transformation_VICON(0,1) = rotation_matrix_VICON(0,1);
              transformation_VICON(0,2) = rotation_matrix_VICON(0,2);

              transformation_VICON(1,0) = rotation_matrix_VICON(1,0);
              transformation_VICON(1,1) = rotation_matrix_VICON(1,1);
              transformation_VICON(1,2) = rotation_matrix_VICON(1,2);

              transformation_VICON(2,0) = rotation_matrix_VICON(2,0);
              transformation_VICON(2,1) = rotation_matrix_VICON(2,1);
              transformation_VICON(2,2) = rotation_matrix_VICON(2,2);

              Eigen::Vector3d euler_angle_BODY;
              euler_angle_BODY.fill(0);
              euler_angle_BODY(0) = -110 * (3.14/180);   //  rotation along X axis from inertial to body
              euler_angle_BODY(1) = 0 * (3.14/180);      //  rotation along Y axis from inertial to body
              euler_angle_BODY(2) = -90 * (3.14/180);    //  rotation along Z axis from inertial to body

              Eigen::Matrix3d rotation_matrix_BODY;
              rotation_matrix_BODY.fill(0);
              angle2rotmtx(euler_angle_BODY, rotation_matrix_BODY);

              Eigen::Matrix4d transformation_BODY = Eigen::Matrix4d::Identity(4,4);
              transformation_BODY(0,0) = rotation_matrix_BODY(0,0);
              transformation_BODY(0,1) = rotation_matrix_BODY(0,1);
              transformation_BODY(0,2) = rotation_matrix_BODY(0,2);

              transformation_BODY(1,0) = rotation_matrix_BODY(1,0);
              transformation_BODY(1,1) = rotation_matrix_BODY(1,1);
              transformation_BODY(1,2) = rotation_matrix_BODY(1,2);

              transformation_BODY(2,0) = rotation_matrix_BODY(2,0);
              transformation_BODY(2,1) = rotation_matrix_BODY(2,1);
              transformation_BODY(2,2) = rotation_matrix_BODY(2,2);

              Tr_total_final = transformation_VICON * Tr_total * transformation_BODY;
            }


            // calculate current estimated velocity
            unsigned long diff_ticks = get_diff_ticks_us();
            double dt = (double) diff_ticks * 1e-6;
            Eigen::Vector3d current_velocity;
            current_velocity(0) = (Tr_total_final(0,3) - center_position(0)) / dt;
            current_velocity(1) = (Tr_total_final(1,3) - center_position(1)) / dt;
            current_velocity(2) = (Tr_total_final(2,3) - center_position(2)) / dt;







            // calculate current 6 DoF pose and velocity
            center_position = Tr_total_final.block(0, 3, 3, 1);
            rotmtx2angle(Tr_total_final.block(0, 0, 3, 3).inverse(), euler_angle);
            std::cout << "X : " << center_position(0) << "/ Y : " << center_position(1) << "/ Z : " << center_position(2) << std::endl;
            std::cout << "roll : " << euler_angle(0)*(180/3.14) << "/ pitch : " << euler_angle(1)*(180/3.14) << "/ yaw : " << euler_angle(2)*(180/3.14) << std::endl;
            printf("Xdot : %8.6lf / Ydot : %8.6lf / Zdot : %8.6lf / dt : %8.6lf \n", current_velocity(0), current_velocity(1), current_velocity(2), dt);


            // WiFi commuincation with server
            Est_data[0][0] = center_position(0);
            Est_data[1][0] = center_position(1);
            Est_data[2][0] = center_position(2);
            Est_data[3][0] = euler_angle(0);
            Est_data[4][0] = euler_angle(1);
            Est_data[5][0] = euler_angle(2);
            Est_data[6][0] = current_velocity(0);
            Est_data[7][0] = current_velocity(1);
            Est_data[8][0] = current_velocity(2);
            onSenddata(Est_data);


            /* // display the current trajectory (x-z plane) and (y-z plane)
            cv::Mat traj_xz = cv::Mat::zeros(800, 800, CV_8UC3);
            cv::Mat traj_yz = cv::Mat::zeros(800, 800, CV_8UC3);
            x_axis_end_g = Tr_total.block(0, 0, 3, 3) * x_axis_end_c + center_position;
            y_axis_end_g = Tr_total.block(0, 0, 3, 3) * y_axis_end_c + center_position;
            z_axis_end_g = Tr_total.block(0, 0, 3, 3) * z_axis_end_c + center_position;

            line(traj_xz, cv::Point((center_position(0)*100) + 400, -(center_position(2)*100) + 400),
                          cv::Point((x_axis_end_g(0)*100) + 400, -(x_axis_end_g(2)*100) + 400), CV_RGB(255,0,0), 3);
            line(traj_xz, cv::Point((center_position(0)*100) + 400, -(center_position(2)*100) + 400),
                          cv::Point((z_axis_end_g(0)*100) + 400, -(z_axis_end_g(2)*100) + 400), CV_RGB(0,0,255), 3);
            cv::circle(traj_xz, cv::Point((center_position(0)*100) + 400, -(center_position(2)*100) + 400), 2, CV_RGB(255,255,255), 2);
            cv::imshow("Estimated Trajectory (x-z plane)", traj_xz);

            line(traj_yz, cv::Point((center_position(2)*100) + 400, (center_position(1)*100) + 400),
                          cv::Point((y_axis_end_g(2)*100) + 400, (y_axis_end_g(1)*100) + 400), CV_RGB(0,255,0), 3);
            line(traj_yz, cv::Point((center_position(2)*100) + 400, (center_position(1)*100) + 400),
                          cv::Point((z_axis_end_g(2)*100) + 400, (z_axis_end_g(1)*100) + 400), CV_RGB(0,0,255), 3);
            cv::circle(traj_yz, cv::Point((center_position(2)*100) + 400, (center_position(1)*100) + 400), 2, CV_RGB(255,255,255), 2);
            cv::imshow("Estimated Trajectory (y-z plane)", traj_yz);


            // display the current image frames
            cv::imshow("vi_cam0_image_rect", vi_cam0_image_rect);
            cv::imshow("vi_cam1_image_rect", vi_cam1_image_rect);
            cv::waitKey(30); */
        }
    }


    cv::waitKey(0);
    // exit the program
    return 0;
}
