#include <vector>
#include "core/dense_tracking.h"
#include "core/Liegroup_operation.h"
#include "core/least_squares.h"



void initParameters(const char *_Mode, camCalibrationParam *cam, optsPIVO *optsforPIVO)
{
    // 1. initialize camera calibration parameters
    cam->K = Eigen::Matrix3f::Identity(3,3);
    cam->K(0,0) = 473.726386937359; // fx
    cam->K(1,1) = 473.726386937359; // fy
    cam->K(0,2) = 437.321655295377; // cx
    cam->K(1,2) = 274.151488797300; // cy
    cam->baseline = 0.109249033006859; // baseline length [m] between the rectified stereo camera


    // 2. User-defined parameters
    optsforPIVO->numHeightBlock = 4;           /// the number of height blocks
    optsforPIVO->numWidthBlock = 4;            /// the number of width blocks
    optsforPIVO->numPtsInSingleBucket = 1;     /// the number of points in single bucket

    optsforPIVO->winSize = 91;                 /// size of each patch (both height & width)
    optsforPIVO->minDepth = 0.1;               /// the valid range of depth value [m]
    optsforPIVO->thresPlane = 0.03;            /// threshold for determining the plane or not in Plane RANSAC
    optsforPIVO->numPyr = 5;                   /// the number of the image pyramid
    optsforPIVO->pyrEndLevel = 2;              /// final level of image pyramid in coarse to fine process, level 1 :the original image size

    optsforPIVO->jacobianFlag = "ESM";         /// the type of Jacobian matrix

    optsforPIVO->iniSigma = 5;                 /// initial scale
    optsforPIVO->tDistriDof = 5;               /// Degree of freedom in t-distribution
    optsforPIVO->eps = 0.000005;               /// error convergence criteria
    optsforPIVO->maxNumIter = 40;              /// maximum number of iteration
}



void updateZ(const Eigen::Matrix4d& inputT, const illParam_perImg& inputIllParam, const Eigen::MatrixXd& deltaZ, Eigen::Matrix4d& outputT, illParam_perImg& outputIllParam)
{
    // initialize variables
    Eigen::Matrix4d deltaT = Eigen::MatrixXd::Identity(4,4);
    outputIllParam = inputIllParam;


    // update Transformation matrix
    LieAlgebra2LieGroup(deltaZ, deltaT);
    outputT = inputT * deltaT;


    // update light variation model parameters for each patch
    for(size_t patchIdx = 1; patchIdx <= 27; ++patchIdx)
    {
        outputIllParam.patch[patchIdx-1].contrast = inputIllParam.patch[patchIdx-1].contrast + deltaZ(2*patchIdx+5-1);
        outputIllParam.patch[patchIdx-1].bias = inputIllParam.patch[patchIdx-1].bias + deltaZ(2*patchIdx+6-1);
    }
}



void EstimateCameraMotionPIVO(const cv::Mat& I1RefImgPyr, const cv::Mat& D1RefImgPyr, const Eigen::MatrixXd& inputKeyPtsPyr, const cv::Mat& I2RefImgPyr,
                              const Eigen::Matrix4d& T_ini, const illParam_perImg& illParam_ini, const Eigen::Matrix3f& inputKmatrixPyr, const double& winSize, const optsPIVO& opts,
                              Eigen::Matrix4d& T_Result, illParam_perImg& illParam_Result, double& errResult, Eigen::MatrixXd& ReResult, Eigen::MatrixXd& WeResult)
{
    // initialize variables
    Eigen::Matrix4d T = T_ini;
    illParam_perImg illParam = illParam_ini;
    Eigen::Matrix4d TNext;
    illParam_perImg illParamNext;
    Eigen::MatrixXd deltaZ((6 + 2 * 27),1);
    std::vector<int> invalidPatchIdx;

    double meanErrLast = 0;
    double meanErr = 10000;
    size_t k = 1;
    size_t numIter = 1;

    // Jacobian matrix, residual vector, weighting vector
    Eigen::MatrixXd J_tot((27 * (size_t)winSize * (size_t)winSize), (6 + 2 * 27));
    Eigen::MatrixXd r_tot((27 * (size_t)winSize * (size_t)winSize), 1);
    Eigen::MatrixXd W_tot((r_tot.rows()), 1);

    // create storage variables
    Eigen::Matrix4d TSave = Eigen::MatrixXd::Identity(4,4);
    illParam_perImg illParamSave;
    std::vector<double> errSave;
    Eigen::MatrixXd ReSave;
    Eigen::MatrixXd WeSave;


    // motion estimation Part
    for(k = 1; k <= opts.maxNumIter; ++k)
    {
        // compute J_tot, r_tot, W_tot
        computeJRW(I1RefImgPyr, D1RefImgPyr, inputKeyPtsPyr, I2RefImgPyr, T, illParam, inputKmatrixPyr, winSize, opts, J_tot, r_tot, W_tot);

        // for debug
        //saveEigenMatrix("J_tot.txt",J_tot);
        //saveEigenMatrix("r_tot.txt",r_tot);
        //saveEigenMatrix("W_tot.txt",W_tot);
        //usleep(10000000);


        // calculate mean cost(energy) function
        meanErrLast = meanErr;
        calcCostFtn(W_tot, r_tot, meanErr);
        errSave.push_back(meanErr);

        // for debug
        //cout << meanErr << endl;
        //usleep(10000000);


        if (meanErr >= meanErrLast)
        {
            printf("mean cost function increases at [%03d / %03d] \n", k, opts.maxNumIter);
            numIter = k;
            break;
        }
        else
        {
            // check invalid patches in all patches
            getInvalidPatchIdx(J_tot, invalidPatchIdx);

            // for debug
            //std::cout << invalidPatchIdx.size() << std::endl;
            //usleep(10000000);


            // calculate delta z (optimization part)
            calcDeltaZ(J_tot, r_tot, W_tot, opts, invalidPatchIdx, deltaZ);

            // for debug
            //cout <<  deltaZ << endl;
            //usleep(10000000);


            // update the camera motion T and brightness parameters for each patch
            updateZ(T, illParam, deltaZ, TNext, illParamNext);

            // for debug
            //cout << T << endl;
            //cout << TNext << endl;
            //for(int patchIdx = 0; patchIdx < 27; ++patchIdx)
            //{
            //    cout <<"<<"<<patchIdx<<">>"<< endl;
            //    cout << illParamNext.patch[patchIdx].contrast << endl;
            //    cout << illParamNext.patch[patchIdx].bias << endl;
            //}
            //usleep(10000000);


            // save the previous results
            TSave = T;
            illParamSave = illParam;
            ReSave = r_tot;
            WeSave = W_tot;

            // for debug
            //for(int patchIdx = 0; patchIdx < 27; ++patchIdx)
            //{
            //    cout <<"<<"<<patchIdx<<">>"<< endl;
            //    cout << illParamSave.patch[patchIdx].contrast << endl;
            //    cout << illParamSave.patch[patchIdx].bias << endl;
            //}
            //usleep(10000000);


            // for next iteration
            T = TNext;
            illParam = illParamNext;
        }


        // exit condition
        if ( (std::abs(meanErrLast-meanErr) <= opts.eps) || (k == opts.maxNumIter) )
        {
            printf("satisfy convergence condition at [%03d / %03d] \n", k, opts.maxNumIter);
            numIter = k;
            break;
        }
    }

    // save final estimation results & analysis parameters
    T_Result = TSave;
    illParam_Result = illParamSave;
    errResult = errSave[numIter-2];
    ReResult = ReSave;
    WeResult = WeSave;

    // for debug
    //for(size_t index = 0; index < errSave.size(); ++index)
    //{
    //    printf("errSave[%d] : %lf \n", index, errSave[index]);
    //}
    //cout << "errResult : " << errResult << endl;

    //cout << "T_Result = " << endl;
    //cout << T_Result << endl;

    //for(int patchIdx = 0; patchIdx < 27; ++patchIdx)
    //{
    //    cout <<"<<"<<patchIdx<<">>"<< endl;
    //    cout << illParam_Result.patch[patchIdx].contrast << endl;
    //    cout << illParam_Result.patch[patchIdx].bias << endl;
    //}
    //usleep(10000000);
}
