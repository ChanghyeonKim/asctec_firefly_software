#ifndef __TIME_H__
#define __TIME_H__

#include <time.h>
#include <random>
#include <chrono>

unsigned long get_ticks_us();
unsigned long get_diff_ticks_us();

#endif
