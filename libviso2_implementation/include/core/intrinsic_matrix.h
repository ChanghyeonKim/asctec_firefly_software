#ifndef _INTRINSIC_MATRIX_H_
#define _INTRINSIC_MATRIX_H_

#include <vector>
#include <eigen3/Eigen/Dense>
#include "core/datatypes.h"

void downsampleKmatrix(const Eigen::Matrix3f& inputKmatrix, Eigen::Matrix3f& outputKmatrix);

void genPyramidKmatrix(std::vector<Eigen::Matrix3f>& KmatrixPyramid, const Eigen::Matrix3f& intrinsicMatrix, const optsPIVO& opts);










#endif /* _INTRINSIC_MATRIX_H_ */
