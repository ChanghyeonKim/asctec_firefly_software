#ifndef _FILEIO_H_
#define _FILEIO_H_

#include <string>
#include <eigen3/Eigen/Dense>
#include <opencv2/opencv.hpp>
#include "core/datatypes.h"
#include "core/libelas.h"



void getImgInKITTIdataset(const std::string& leftImgFileName, const std::string& rightImgFileName, const camCalibrationParam& cam, cv::Mat& outputGreyImg, cv::Mat& outputDepthImg);

void saveEigenMatrix(const char *_Filename, const Eigen::MatrixXd& inputMatrix);

void saveMatImage(const char *_Filename, const cv::Mat& inputImage);

void saveSGPVOresults(const std::vector<Eigen::Matrix4d>& T_1i_Save, const Eigen::MatrixXd& stateEsti, const std::vector<illParam_perImg>& illParamPIVO_Save,
                      const std::vector<double>& errPIVO_Save, const std::vector<int>& keyIdx_Save);


































#endif /* _FILEIO_H_ */
