#ifndef _RGBDIMAGE_H_
#define _RGBDIMAGE_H_

#include <vector>
#include <eigen3/Eigen/Dense>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "core/datatypes.h"


void downsampleImage(const cv::Mat& inputImg, cv::Mat& outputImg);

void downsampleDepth(const cv::Mat& inputImg, cv::Mat& outputImg);

void genPyramidImage(std::vector<cv::Mat>& greyPyramidImg, std::vector<cv::Mat>& depthPyramidImg, const cv::Mat& I1RefImg, const cv::Mat& D1RefImg, const optsPIVO& opts);

void detectKeypoints(const cv::Mat& inputImg, Eigen::MatrixXd& outputKeyPts);

void downsampleKeyPts(const Eigen::MatrixXd& inputKeyPts, Eigen::MatrixXd& outputKeyPts);

void genPyramidKeyPts(std::vector<Eigen::MatrixXd>& keyPtsPyramid, const Eigen::MatrixXd& inputKeyPts, const optsPIVO& opts);

bool inImage(const size_t& u, const size_t& v, const cv::Mat& inputImg);

void warpPoints(const cv::Mat& D1RefImgPyr, const Eigen::MatrixXd& inputKeyPtsPyr, const Eigen::Matrix4d& inputT, const Eigen::Matrix3f& inputKmatrixPyr, const double& winSize, const optsPIVO& opts,
                Eigen::MatrixXd& pixelPtsRef, Eigen::MatrixXd& ptsCloudRef, Eigen::RowVectorXi& patchIdxRef, Eigen::MatrixXd& pixelPtsNext, Eigen::MatrixXd& ptsCloudNext);

void calcDerivX(const cv::Mat& inputImg, cv::Mat& outputImg);

void calcDerivY(const cv::Mat& inputImg, cv::Mat& outputImg);

double interp2(const cv::Mat& inputImg, const double& u, const double& v);




















#endif /* _RGBDIMAGE_H_ */
