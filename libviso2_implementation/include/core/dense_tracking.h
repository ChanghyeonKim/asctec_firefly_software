#ifndef _DENSE_TRACKING_H_
#define _DENSE_TRACKING_H_

#include <eigen3/Eigen/Dense>
#include <opencv2/opencv.hpp>
#include "core/datatypes.h"

void initParameters(const char *_Mode, camCalibrationParam *cam, optsPIVO *optsforPIVO);

void updateZ(const Eigen::Matrix4d& inputT, const illParam_perImg& inputIllParam, const Eigen::MatrixXd& deltaZ, Eigen::Matrix4d& outputT, illParam_perImg& outputIllParam);

void EstimateCameraMotionPIVO(const cv::Mat& I1RefImgPyr, const cv::Mat& D1RefImgPyr, const Eigen::MatrixXd& inputKeyPtsPyr, const cv::Mat& I2RefImgPyr,
                              const Eigen::Matrix4d& T_ini, const illParam_perImg& illParam_ini, const Eigen::Matrix3f& inputKmatrixPyr, const double& winSize, const optsPIVO& opts,
                              Eigen::Matrix4d& T_Result, illParam_perImg& illParam_Result, double& errResult, Eigen::MatrixXd& ReResult, Eigen::MatrixXd& WeResult);








































#endif /* _DENSE_TRACKING_H_ */
