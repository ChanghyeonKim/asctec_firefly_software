#include "core/rgbd_image.h"



void downsampleImage(const cv::Mat& inputImg, cv::Mat& outputImg)
{
    outputImg.create(cv::Size(inputImg.size().width / 2, inputImg.size().height / 2), inputImg.type());

    int x0 = 0, x1 = 0, y0 = 0, y1 = 0;

    for(int y = 0; y < outputImg.rows; ++y)
    {
        for(int x = 0; x < outputImg.cols; ++x)
        {
            x0 = x * 2;
            x1 = x0 + 1;
            y0 = y * 2;
            y1 = y0 + 1;

            outputImg.at<double>(y, x) = ( (inputImg.at<double>(y0, x0) + inputImg.at<double>(y0, x1) + inputImg.at<double>(y1, x0) + inputImg.at<double>(y1, x1)) / 4.0f );
        }
    }
}



void downsampleDepth(const cv::Mat& inputImg, cv::Mat& outputImg)
{
    outputImg.create(cv::Size(inputImg.size().width / 2, inputImg.size().height / 2), inputImg.type());

    int x0 = 0, x1 = 0, y0 = 0, y1 = 0;
    double sum, cnt;

    for(int y = 0; y < outputImg.rows; ++y)
    {
        for(int x = 0; x < outputImg.cols; ++x)
        {
            x0 = x * 2;
            x1 = x0 + 1;
            y0 = y * 2;
            y1 = y0 + 1;

            // initialize
            sum = 0;
            cnt = 0;

            if((inputImg.at<double>(y0, x0) != 0.0f))
            {
                sum += inputImg.at<double>(y0, x0);
                cnt += 1;
            }

            if((inputImg.at<double>(y0, x1) != 0.0f))
            {
                sum += inputImg.at<double>(y0, x1);
                cnt += 1;
            }

            if((inputImg.at<double>(y1, x0) != 0.0f))
            {
                sum += inputImg.at<double>(y1, x0);
                cnt += 1;
            }

            if((inputImg.at<double>(y1, x1) != 0.0f))
            {
                sum += inputImg.at<double>(y1, x1);
                cnt += 1;
            }

            if(cnt > 0)
            {
                outputImg.at<double>(y, x) = ( sum / cnt );
            }
            else
            {
                outputImg.at<double>(y, x) = 0;
            }
        }
    }
}



void genPyramidImage(std::vector<cv::Mat>& greyPyramidImg, std::vector<cv::Mat>& depthPyramidImg, const cv::Mat& I1RefImg, const cv::Mat& D1RefImg, const optsPIVO& opts)
{
    // initialize
    greyPyramidImg.push_back(I1RefImg);
    depthPyramidImg.push_back(D1RefImg);


    // perform pyramid reduction with average values
    cv::Mat tempGreyOutputImg, tempDepthOutputImg;

    for(int pyrIdx=0; pyrIdx < (opts.numPyr-1); ++pyrIdx)
    {
        // grey image
        tempGreyOutputImg.release();
        downsampleImage(greyPyramidImg[pyrIdx],tempGreyOutputImg);
        greyPyramidImg.push_back(tempGreyOutputImg);

        // depth image
        tempDepthOutputImg.release();
        downsampleDepth(depthPyramidImg[pyrIdx],tempDepthOutputImg);
        depthPyramidImg.push_back(tempDepthOutputImg);
    }
}



void detectKeypoints(const cv::Mat& inputImg, Eigen::MatrixXd& outputKeyPts)
{
    size_t rowsUnit = round(inputImg.rows / 4);
    size_t colsUnit = round(inputImg.cols / 4);
    size_t cnt = 0;

    for(size_t u = 1; u <= 4 ; ++u)
    {
        for(size_t v = 1; v <= 4 ; ++v)
        {
            cnt += 1;
            outputKeyPts(0,(cnt-1)) = round( colsUnit * v - (colsUnit / 2) + 1 ); // u pixel
            outputKeyPts(1,(cnt-1)) = round( rowsUnit * u - (rowsUnit / 2) + 1 ); // v pixel
        }
    }
}



void downsampleKeyPts(const Eigen::MatrixXd& inputKeyPts, Eigen::MatrixXd& outputKeyPts)
{
    outputKeyPts = (inputKeyPts / 2);

    for(int y = 0; y < outputKeyPts.rows(); ++y)
    {
        for(int x = 0; x < outputKeyPts.cols(); ++x)
        {
            outputKeyPts(y,x) = ceil(outputKeyPts(y,x));
        }
    }
}



void genPyramidKeyPts(std::vector<Eigen::MatrixXd>& keyPtsPyramid, const Eigen::MatrixXd& inputKeyPts, const optsPIVO& opts)
{
    // initialize
    keyPtsPyramid.push_back(inputKeyPts);


    // perform pyramid reduction of the extracted keyPts
    Eigen::MatrixXd tempKeyPts;

    for(int pyrIdx=0; pyrIdx < (opts.numPyr-1); ++pyrIdx)
    {
        downsampleKeyPts(keyPtsPyramid[pyrIdx], tempKeyPts);
        keyPtsPyramid.push_back(tempKeyPts);
    }
}



bool inImage(const size_t& u, const size_t& v, const cv::Mat& inputImg)
{
    // 1-based pixel coordinates (u,v)
    return (u >= 1) && (u <= (inputImg.cols)) && (v >= 1) && (v <= (inputImg.rows));
}



void warpPoints(const cv::Mat& D1RefImgPyr, const Eigen::MatrixXd& inputKeyPtsPyr, const Eigen::Matrix4d& inputT, const Eigen::Matrix3f& inputKmatrixPyr, const double& winSize, const optsPIVO& opts,
                Eigen::MatrixXd& pixelPtsRef, Eigen::MatrixXd& ptsCloudRef, Eigen::RowVectorXi& patchIdxRef, Eigen::MatrixXd& pixelPtsNext, Eigen::MatrixXd& ptsCloudNext)
{
    // (1) I1 valid pixel coordinate -> I1 camera frame coordinate
    // [u v] : the pixel coordinates in inertial frame (I1) ( 1-based pixel coordinates (u,v) )

    size_t pixelCnt = 0;
    double depthVal = 0;
    double uStart = 0, vStart = 0;

    for(size_t patchIdx = 1; patchIdx <= 27; ++patchIdx)  // 1-based patch index (patchIdx)
    {
        // setting starting point ( top-left pixel point of the patch )
        // ( 1-based pixel coordinates (u,v) )
        uStart = inputKeyPtsPyr((1-1),(patchIdx-1)) - ((winSize-1)/2);
        vStart = inputKeyPtsPyr((2-1),(patchIdx-1)) - ((winSize-1)/2);


        // for debug
        //cout << inputKeyPtsPyr << endl;
        //cout << uStart << " " << vStart << endl;
        //cout << pixelPtsRef.rows() << endl;
        //cout << pixelPtsRef.cols() << endl;
        //double temp = 35.913123;
        //cout << temp << endl;
        //cout << (size_t)temp << endl;
        //usleep(10000000);


        for(size_t rows = 0; rows <= (winSize-1); ++rows)
        {
            for(size_t cols = 0; cols <= (winSize-1); ++cols)
            {
                // count (27 x winSize x winSize)
                pixelCnt += 1;

                if(inImage(uStart+cols,vStart+rows,D1RefImgPyr))
                {
                    if( D1RefImgPyr.at<double>((vStart+rows-1),(uStart+cols-1)) >= opts.minDepth )
                    {
                        depthVal = D1RefImgPyr.at<double>((vStart+rows-1),(uStart+cols-1));

                        // save the location of the valid patch pixel coordinates in I1
                        pixelPtsRef((1-1),(pixelCnt-1)) = uStart+cols;
                        pixelPtsRef((2-1),(pixelCnt-1)) = vStart+rows;
                        pixelPtsRef((3-1),(pixelCnt-1)) = 1;

                        // build point cloud of each patch's pixel
                        ptsCloudRef((1-1),(pixelCnt-1)) = (((uStart+cols) - inputKmatrixPyr((1-1),(3-1)))/inputKmatrixPyr((1-1),(1-1))) * depthVal;
                        ptsCloudRef((2-1),(pixelCnt-1)) = (((vStart+rows) - inputKmatrixPyr((2-1),(3-1)))/inputKmatrixPyr((2-1),(2-1))) * depthVal;
                        ptsCloudRef((3-1),(pixelCnt-1)) = depthVal;
                        ptsCloudRef((4-1),(pixelCnt-1)) = 1;

                        // save the index of patch in that pixel
                        patchIdxRef((pixelCnt-1)) = patchIdx;

                        // for debug
                        //cout << (uStart+cols) << endl;
                        //cout << (vStart+rows) << endl;
                        //cout << depthVal << endl;
                        //cout << pixelPtsRef.col((pixelCnt-1)) << endl;
                        //cout << ptsCloudRef.col((pixelCnt-1)) << endl;
                        //cout << patchIdxRef((pixelCnt-1)) << endl;
                        //usleep(10000000);
                    }
                }
            }
        }
    }

    // for debug
    //cout << pixelCnt << endl;
    //saveEigenMatrix("pixelPtsRef.txt", pixelPtsRef);
    //saveEigenMatrix("ptsCloudRef.txt", ptsCloudRef);
    //usleep(10000000);


    //
    Eigen::Matrix4d R_rect_cam = Eigen::Matrix4d::Identity(4,4);
    R_rect_cam(0,0) = 0.999928000000000;
    R_rect_cam(0,1) = 0.008085985000000;
    R_rect_cam(0,2) = -0.00886679700000000;

    R_rect_cam(1,0) = -0.00812320500000000;
    R_rect_cam(1,1) = 0.999958300000000;
    R_rect_cam(1,2) = -0.00416975000000000;

    R_rect_cam(2,0) = 0.00883271100000000;
    R_rect_cam(2,1) = 0.00424147700000000;
    R_rect_cam(2,2) = 0.999952000000000;

    ptsCloudRef = R_rect_cam.inverse() * ptsCloudRef;


    // (2) I1 camera frame coordinate -> I2 valid pixel coordinate
    // [u v] : the pixel coordinates in inertial frame (I1) ( 1-based pixel coordinates (u,v) )
    // [x y] : the pixel coordinates in body frame (I2)     ( 1-based pixel coordinates (x,y) )


    // I1 camera frame coordinate -> I2 camera frame coordinate
    ptsCloudNext = R_rect_cam * inputT * ptsCloudRef;


    // I2 camera frame coordinate -> I2 valid pixel coordinate

    pixelCnt = 0;
    double x = 0, y = 0;

    // for debug
    //pixelCnt = 88;
    //x = round( (ptsCloudNext((1-1),(pixelCnt-1))/ptsCloudNext((3-1),(pixelCnt-1))) * inputKmatrixPyr((1-1),(1-1)) + inputKmatrixPyr((1-1),(3-1)) );
    //y = round( (ptsCloudNext((2-1),(pixelCnt-1))/ptsCloudNext((3-1),(pixelCnt-1))) * inputKmatrixPyr((2-1),(2-1)) + inputKmatrixPyr((2-1),(3-1)) );
    //cout << x << endl;
    //cout << y << endl;
    //cout << inImage(x,y,D1RefImgPyr) << endl;
    //usleep(10000000);


    for(pixelCnt = 1; pixelCnt <= ptsCloudNext.cols(); ++pixelCnt)
    {
        // setting pixel point in I2
        // ( 1-based pixel coordinates (x,y) )
        x = round( (ptsCloudNext((1-1),(pixelCnt-1))/ptsCloudNext((3-1),(pixelCnt-1))) * inputKmatrixPyr((1-1),(1-1)) + inputKmatrixPyr((1-1),(3-1)) );
        y = round( (ptsCloudNext((2-1),(pixelCnt-1))/ptsCloudNext((3-1),(pixelCnt-1))) * inputKmatrixPyr((2-1),(2-1)) + inputKmatrixPyr((2-1),(3-1)) );

        if(inImage(x,y,D1RefImgPyr))
        {
            // save the location of the valid pixel coordinates in I2
            pixelPtsNext((1-1),(pixelCnt-1)) = x;
            pixelPtsNext((2-1),(pixelCnt-1)) = y;
            pixelPtsNext((3-1),(pixelCnt-1)) = 1;
        }
    }

    // for debug
    //cout << pixelCnt << endl;
    //saveEigenMatrix("pixelPtsNext.txt", pixelPtsNext);
    //saveEigenMatrix("ptsCloudNext.txt", ptsCloudNext);
    //usleep(10000000);
}



void calcDerivX(const cv::Mat& inputImg, cv::Mat& outputImg)
{
    outputImg.create(inputImg.size(), inputImg.type());

    int prev = 0, next = 0;

    for(int y = 0; y < inputImg.rows; ++y)
    {
        for(int x = 0; x < inputImg.cols; ++x)
        {
            prev = std::max(x - 1, 0);
            next = std::min(x + 1, inputImg.cols - 1);

            outputImg.at<double>(y, x) = ( (inputImg.at<double>(y, next) - inputImg.at<double>(y, prev)) * 0.5f );
        }
    }
}



void calcDerivY(const cv::Mat& inputImg, cv::Mat& outputImg)
{
    outputImg.create(inputImg.size(), inputImg.type());

    int prev = 0, next = 0;

    for(int y = 0; y < inputImg.rows; ++y)
    {
        for(int x = 0; x < inputImg.cols; ++x)
        {
            prev = std::max(y - 1, 0);
            next = std::min(y + 1, inputImg.rows - 1);

            outputImg.at<double>(y, x) = ( (inputImg.at<double>(next, x) - inputImg.at<double>(prev, x)) * 0.5f );
        }
    }
}



double interp2(const cv::Mat& inputImg, const double& u, const double& v)
{
    // 1-based pixel coordinates (u,v) and (x0, y0) / (x1, y1)
    int x0 = (int) (std::floor(u));
    int y0 = (int) (std::floor(v));
    int x1 = x0 + 1;
    int y1 = y0 + 1;

    double x_weight = (u - x0);
    double y_weight = (v - y0);

    double interpolated =
        ( inputImg.at<double>((y0-1), (x0-1)) * x_weight + inputImg.at<double>((y0-1), (x1-1)) * (1 - x_weight) +
          inputImg.at<double>((y1-1), (x0-1)) * x_weight + inputImg.at<double>((y1-1), (x1-1)) * (1 - x_weight) +
          inputImg.at<double>((y0-1), (x0-1)) * y_weight + inputImg.at<double>((y1-1), (x0-1)) * (1 - y_weight) +
          inputImg.at<double>((y0-1), (x1-1)) * y_weight + inputImg.at<double>((y1-1), (x1-1)) * (1 - y_weight) );

    return (interpolated * 0.25f);
}
