/*
 * Copyright (c) 2014, Skybotix AG, Switzerland (info@skybotix.com)
 *
 * All rights reserved.
 *
 * Redistribution and non-commercial use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the {organization} nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "core/vi_sensor_interface.hpp"
#include <cstdio>
#include <fstream>
#include <opencv2/highgui/highgui_c.h>
#include "core/fileIO.h"

ViSensorInterface::ViSensorInterface(uint32_t image_rate, uint32_t imu_rate)
    : computed_rectification_map_(false)
{
    StartIntegratedSensor(image_rate, imu_rate);
}

ViSensorInterface::ViSensorInterface()
    : computed_rectification_map_(false)
{

    StartIntegratedSensor(20, 200);
}

ViSensorInterface::~ViSensorInterface()
{

}

void ViSensorInterface::run(void)
{
    // set callback for completed frames
    drv_.setCameraCallback(boost::bind(&ViSensorInterface::ImageCallback, this, _1));

    // set callback for completed IMU messages
    drv_.setImuCallback(boost::bind(&ViSensorInterface::ImuCallback, this, _1));

    //Lets wait for the user to end program.
    //This is ok as program is callback-driven anyways
    boost::mutex m;
    boost::mutex::scoped_lock l(m);
    boost::condition_variable c;
//    c.wait(l);
}

void ViSensorInterface::StartIntegratedSensor(uint32_t image_rate, uint32_t imu_rate)
{

    if (image_rate > 30)
    {
        image_rate = 30;
        std::cout << "Desired image rate is too high, setting it to 30 Hz." << std::endl;
    }
    if (imu_rate > 800)
    {
        imu_rate = 800;
        std::cout << "Desired imu rate is too high, setting it to 800 Hz." << std::endl;
    }

    try
    {
        drv_.init();
    }
    catch (visensor::exceptions::ConnectionException const &ex)
    {
        std::cout << ex.what() << "\n";
        return;
    }

    // re-configure camera parameters
    drv_.setSensorConfigParam(visensor::SensorId::SensorId::CAM0, "row_flip", 0);
    drv_.setSensorConfigParam(visensor::SensorId::SensorId::CAM0, "column_flip", 0);

    drv_.setSensorConfigParam(visensor::SensorId::SensorId::CAM1, "row_flip", 1);
    drv_.setSensorConfigParam(visensor::SensorId::SensorId::CAM1, "column_flip", 1);

    drv_.setSensorConfigParam(visensor::SensorId::SensorId::CAM0, "min_coarse_shutter_width", 2);
    drv_.setSensorConfigParam(visensor::SensorId::SensorId::CAM0, "max_coarse_shutter_width", 550);

    drv_.setSensorConfigParam(visensor::SensorId::SensorId::CAM1, "min_coarse_shutter_width", 2);
    drv_.setSensorConfigParam(visensor::SensorId::SensorId::CAM1, "max_coarse_shutter_width", 550);

    drv_.startAllCameras(image_rate);
    drv_.startAllImus(imu_rate);
    vi_sensor_connected_ = true;
}

void ViSensorInterface::ImageCallback(visensor::ViFrame::Ptr frame_ptr)
{
    boost::mutex::scoped_lock lock(io_mutex_);  //lock thread as opencv does seem to have problems with multithreading
    uint32_t camera_id = frame_ptr->camera_id;
    frameQueue[camera_id].emplace_back(frame_ptr);
    process_data();
}

void ViSensorInterface::ImuCallback(boost::shared_ptr<visensor::ViImuMsg> imu_ptr)
{
    Eigen::Vector3d gyro(imu_ptr->gyro[0], imu_ptr->gyro[1], imu_ptr->gyro[2]);
    Eigen::Vector3d acc(imu_ptr->acc[0], imu_ptr->acc[1], imu_ptr->acc[2]);
}

void ViSensorInterface::process_data()
{

    //Lets sync image streams from cam0 & cam1
    if (frameQueue[idxCam0_].empty() || frameQueue[idxCam1_].empty())
        return;
    while (frameQueue[idxCam0_].front()->timestamp > frameQueue[idxCam1_].front()->timestamp)
    {
        frameQueue[idxCam1_].pop_front();
        if (frameQueue[idxCam0_].empty() || frameQueue[idxCam1_].empty())
            return;
    }
    while (frameQueue[idxCam0_].front()->timestamp < frameQueue[idxCam1_].front()->timestamp)
    {
        frameQueue[idxCam0_].pop_front();
        if (frameQueue[idxCam0_].empty() || frameQueue[idxCam1_].empty())
            return;
    }

    //If we arrive here, both queues have synced timestamps
    visensor::ViFrame::Ptr frame0 = frameQueue[idxCam0_].front();
    visensor::ViFrame::Ptr frame1 = frameQueue[idxCam1_].front();

    if (!computed_rectification_map_)
        computeRectificationMaps();

    cv::Mat img0, img1;
    img0.create(frame0->height, frame0->width, CV_8UC1);
    img0.data = frame0->getImageRawPtr();

    img1.create(frame1->height, frame1->width, CV_8UC1);
    img1.data = frame1->getImageRawPtr();


    // stereo image rectification for libviso2
    cv::Mat img0rect, img1rect;
    cv::remap(img0, img0rect, map00_, map01_, cv::INTER_LINEAR);
    cv::remap(img1, img1rect, map10_, map11_, cv::INTER_LINEAR);

    this->left_image_rect_ = img0rect;
    this->right_image_rect_ = img1rect;
    this->image_rect_flag_ = 1;


    /* // perform stereo matching for PIVO
    cv::Rect roi(0, 0, 32*23, 32*15);  // roi(uStart,vStart,uLength,vLength)
    cv::Mat grey_64F, disp_64F;
    img0rect(roi).convertTo(grey_64F, CV_64FC1);

    cv::Mat disLeftTranspose(img0rect.cols, img0rect.rows, CV_64FC1);
    imwrite("img0rect.pgm", img0rect);
    imwrite("img1rect.pgm", img1rect);
    process("img0rect.pgm", "img1rect.pgm", disLeftTranspose);
    cv::transpose(disLeftTranspose, disp_64F);

    this->left_image_double_rect_ = grey_64F;
    this->left_disp_double_rect_ = disp_64F(roi);
    this->disp_rect_flag_ = 1; */

    // for debug
    // cv::namedWindow("img_rect_cam0", CV_WINDOW_AUTOSIZE);
    // cv::imshow("img_rect_cam0", this->left_image_rect_ );
    // cv::namedWindow("img_rect_cam1", CV_WINDOW_AUTOSIZE);
    // cv::imshow("img_rect_cam1", this->right_image_rect_ );
    //
    // cv::namedWindow("left_image_double_rect_", CV_WINDOW_AUTOSIZE);
    // cv::imshow("left_image_double_rect_", this->left_image_double_rect_  );
    // cv::namedWindow("left_disp_double_rect_", CV_WINDOW_AUTOSIZE);
    // cv::imshow("left_disp_double_rect_", this->left_disp_double_rect_ );
    // cv::waitKey(0);


    //We dont need the frames anymore, lets drop them
    frameQueue[idxCam0_].pop_front();
    frameQueue[idxCam1_].pop_front();
}

bool ViSensorInterface::computeRectificationMaps(void)
{
    visensor::ViCameraCalibration camera_calibration_0, camera_calibration_1;
    if (!drv_.getCameraCalibration(idxCam0_, camera_calibration_0))
        return false;
    if (!drv_.getCameraCalibration(idxCam1_, camera_calibration_1))
        return false;
    int image_width = 752;
    int image_height = 480;
    double c0[9];
    double d0[5];
    double r0[9];
    double p0[12];
    double rot0[9];
    double t0[3];
    double c1[9];
    double d1[5];
    double r1[9];
    double p1[12];
    double rot1[9];
    double t1[3];
    double r[9];
    double t[3];

    d0[0] = -0.293253358441277;
    d0[1] = 0.0880652209769358;
    d0[2] = camera_calibration_0.dist_coeff[2];
    d0[3] = camera_calibration_0.dist_coeff[3];
    d0[4] = 0.0;
    c0[0] = 474.287260117510;
    c0[1] = 0.0;
    c0[2] = 393.321665748649;
    c0[3] = 0.0;
    c0[4] = 475.020204688627;
    c0[5] = 255.547400714026;
    c0[6] = 0.0;
    c0[7] = 0.0;
    c0[8] = 1.0;
    d1[0] = -0.288619119686433;
    d1[1] = 0.0827149217964409;
    d1[2] = camera_calibration_1.dist_coeff[2];
    d1[3] = camera_calibration_1.dist_coeff[3];
    d1[4] = 0.0;
    c1[0] = 473.726344788349;
    c1[1] = 0.0;
    c1[2] = 356.080702251162;
    c1[3] = 0.0;
    c1[4] = 474.789206409847;
    c1[5] = 260.755592898289;
    c1[6] = 0.0;
    c1[7] = 0.0;
    c1[8] = 1.0;

    for (int i = 0; i < 9; ++i)
    {
        rot0[i] = camera_calibration_0.R[i];
        rot1[i] = camera_calibration_1.R[i];
    }
    for (int i = 0; i < 3; ++i)
    {
        t0[i] = camera_calibration_0.t[i];
        t1[i] = camera_calibration_1.t[i];
    }
    Eigen::Map<Eigen::Matrix3d> RR0(rot0);
    Eigen::Map<Eigen::Vector3d> tt0(t0);
    Eigen::Map<Eigen::Matrix3d> RR1(rot1);
    Eigen::Map<Eigen::Vector3d> tt1(t1);
    Eigen::Matrix4d T0 = Eigen::Matrix4d::Zero();
    Eigen::Matrix4d T1 = Eigen::Matrix4d::Zero();
    T0.block<3, 3>(0, 0) = RR0;
    T0.block<3, 1>(0, 3) = tt0;
    T0(3, 3) = 1.0;
    T1.block<3, 3>(0, 0) = RR1;
    T1.block<3, 1>(0, 3) = tt1;
    T1(3, 3) = 1.0;
    Eigen::Matrix4d T_rel = Eigen::Matrix4d::Zero();
    T_rel = T1 * T0.inverse();
    Eigen::Map<Eigen::Matrix3d> R_rel(r);
    Eigen::Map<Eigen::Vector3d> t_rel(t);
    R_rel = T_rel.block<3, 3>(0, 0);
    t_rel << T_rel(0, 3), T_rel(1, 3), T_rel(2, 3);
    double r_temp[9];
    r_temp[0] = R_rel(0, 0);
    r_temp[1] = R_rel(0, 1);
    r_temp[2] = R_rel(0, 2);
    r_temp[3] = R_rel(1, 0);
    r_temp[4] = R_rel(1, 1);
    r_temp[5] = R_rel(1, 2);
    r_temp[6] = R_rel(2, 0);
    r_temp[7] = R_rel(2, 1);
    r_temp[8] = R_rel(2, 2);

    //cv::Mat wrapped(rows, cols, CV_32FC1, external_mem, CV_AUTOSTEP);
    cv::Mat C0(3, 3, CV_64FC1, c0, 3 * sizeof(double));
    cv::Mat D0(5, 1, CV_64FC1, d0, 1 * sizeof(double));
    cv::Mat R0(3, 3, CV_64FC1, r0, 3 * sizeof(double));
    cv::Mat P0(3, 4, CV_64FC1, p0, 4 * sizeof(double));

    cv::Mat C1(3, 3, CV_64FC1, c1, 3 * sizeof(double));
    cv::Mat D1(5, 1, CV_64FC1, d1, 1 * sizeof(double));
    cv::Mat R1(3, 3, CV_64FC1, r1, 3 * sizeof(double));
    cv::Mat P1(3, 4, CV_64FC1, p1, 4 * sizeof(double));

    cv::Mat R(3, 3, CV_64FC1, r_temp, 3 * sizeof(double));
    R.at<double>(0,0) = 0.999846198179162;
    R.at<double>(0,1) = 0.00584058576763007;
    R.at<double>(0,2) = 0.0165368541315247;
    R.at<double>(1,0) = -0.00584002447244078;
    R.at<double>(1,1) = 0.999982943530453;
    R.at<double>(1,2) = -8.22334044186012e-05;
    R.at<double>(2,0) = -0.0165370523624273;
    R.at<double>(2,1) = -1.43548760540450e-05;
    R.at<double>(2,2) = 0.999863253496747;

    cv::Mat T(3, 1, CV_64FC1, t, 1 * sizeof(double));
    T.at<double>(0,0) = 0.109239986268189;
    T.at<double>(1,0) = 0.000101962420640836;
    T.at<double>(2,0) = 0.00140221850095609;

    cv::Size img_size(image_width, image_height);

    cv::Rect roi1, roi2;
    cv::Mat Q;

    // for debug
    //std::cout << "C0 : " << C0 << std::endl;
    //std::cout << "D0 : " << D0 << std::endl;
    //std::cout << "C1 : " << C1 << std::endl;
    //std::cout << "D1 : " << D1 << std::endl;
    //std::cout << "R : " << R << std::endl;
    //std::cout << "T : " << T << std::endl;
    //std::cout << "img_size.width : " << img_size.width << std::endl;
    //std::cout << "img_size.height : " << img_size.height << std::endl;
    //usleep(1000000);

    cv::stereoRectify(C0, D0, C1, D1, img_size, R, T, R0, R1, P0, P1, Q, cv::CALIB_ZERO_DISPARITY, 0,
                      img_size, &roi1, &roi2);


    cv::initUndistortRectifyMap(C0, D0, R0, P0, img_size, CV_16SC2, map00_, map01_);
    cv::initUndistortRectifyMap(C1, D1, R1, P1, img_size, CV_16SC2, map10_, map11_);

    //std::cout << "img_size.width : " << img_size.width << std::endl;
    //std::cout << "img_size.height : " << img_size.height << std::endl;

    computed_rectification_map_ = true;
    return true;
}
