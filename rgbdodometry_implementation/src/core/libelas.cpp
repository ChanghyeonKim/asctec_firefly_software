#include "core/libelas.h"
#include "core/image.h"
#include "core/timer.h"




// compute disparities of pgm image input pair file_1, file_2
void process(const char* file_1, const char* file_2, cv::Mat& disLeftTranspose)
{
    // load images
    image<uchar> *I1,*I2;
    I1 = loadPGM(file_1);
    I2 = loadPGM(file_2);


    // check for correct size
    if ( I1->width()<=0 || I1->height() <=0 || I2->width()<=0 || I2->height() <=0 || I1->width()!=I2->width() || I1->height()!=I2->height() )
    {
        std::cout << "ERROR: Images must be of same size, but" << std::endl;
        std::cout << "       I1: " << I1->width() <<  " x " << I1->height() <<
                  ", I2: " << I2->width() <<  " x " << I2->height() << std::endl;
        delete I1;
        delete I2;
        return;
    }


    // get image width and height
    int32_t width  = I1->width();
    int32_t height = I1->height();


    // allocate memory for disparity images
    const int32_t dims[3] = {width,height,width}; // bytes per line = width
    float* D1_data = (float*)malloc(width*height*sizeof(float));
    float* D2_data = (float*)malloc(width*height*sizeof(float));


    // process the libelas algorithm
    Elas::parameters param;
    param.disp_min = 0;
    param.disp_max = 64;
    param.subsampling = false;
    param.postprocess_only_left = true;

    Elas elas(param);
    elas.process(I1->data,I2->data,D1_data,D2_data,dims);


    // save the OpenCV Mat data format
    int32_t k = 0;
    for(int32_t v = 0; v < height; v++)
    {
        for(int32_t u = 0; u < width; u++)
        {
            disLeftTranspose.at<double>(u,v) = (double)D1_data[k]; // .at<double>(u,v) because it is transposed image matrix
            k++;

            // for debug
            //std::cout << disLeftTranspose.at<double>(u,v) << std::endl;
            //std::cout << k << std::endl;
            //usleep(1000000);
        }
    }


    // free memory
    delete I1;
    delete I2;
    free(D1_data);
    free(D2_data);
}
