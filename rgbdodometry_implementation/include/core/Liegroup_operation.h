#ifndef _LIEGROUP_OPERATION_H_
#define _LIEGROUP_OPERATION_H_

#include <eigen3/Eigen/Dense>



void hatOperator(const Eigen::Vector3d& columnVector, Eigen::Matrix3d& skewSymmetricMatrix);

void LieAlgebra2LieGroup(const Eigen::MatrixXd& deltaZ, Eigen::Matrix4d& outputDeltaT);

void angle2rotmtx(const Eigen::Vector3d& eulerAngle, Eigen::Matrix3d& rotationMatrix);

void statevector2transformatrix(const Eigen::VectorXd& stateVector, Eigen::Matrix4d& transformationMatrix);

void rotmtx2angle(const Eigen::Matrix3d& rotationMatrix, Eigen::Vector3d& eulerAngle);

void transformatrix2statevector(const Eigen::Matrix4d& transformationMatrix, Eigen::VectorXd& stateVector);

void updateStateEstiData(const Eigen::Matrix4d& T_vo_esti, const int& keyIdx, const int& imgIdx, Eigen::Matrix4d& outputT_vo_esti, Eigen::MatrixXd& stateEsti);

double EuclideanDist(const Eigen::MatrixXd& stateEsti, const int& keyIdx, const int& imgIdx);


































#endif /* _LIEGROUP_OPERATION_H_ */
