/*

AscTec SDK 3.0

Copyright (c) 2011, Ascending Technologies GmbH
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

 */

#include "main.h"
#include "math.h"
#include "sdk.h"
#include "LL_HL_comm.h"
#include "gpsmath.h"
#include "sdk_telemetry.h"
#include "uart.h"
#include "mymath.h"
#include "system.h"
#include "lpc_aci_eeprom.h"
#ifdef MATLAB
#include "..\custom_mdl\onboard_matlab_ert_rtw\onboard_matlab.h"
#endif

struct RO_CMD_DATA RO_CMD_Data;
struct WO_SDK_STRUCT WO_SDK;
struct WO_CTRL_INPUT WO_CTRL_Input;
struct WO_CMD_INPUT WO_CMD_Input;
struct RO_RC_DATA RO_RC_Data;
struct RO_ALL_DATA RO_ALL_Data;
struct WO_DIRECT_MOTOR_CONTROL WO_Direct_Motor_Control;
struct WO_DIRECT_INDIVIDUAL_MOTOR_CONTROL WO_Direct_Individual_Motor_Control;

kalmat k; // M, G matrix calculation
kals n; //
kalvel evel;
MAV_CTRL_CMD mav; // 

//waypoint example global variables for jeti display
unsigned char wpExampleWpNr=0;
unsigned char wpExampleActive=0;

//emergency mode variables
unsigned char emergencyMode;
unsigned char emergencyModeUpdate=0;

#ifdef MATLAB
unsigned char xbee_send_flag=0;
unsigned char triggerSaveMatlabParams=0; //trigger command to save matlab parameters to flash
struct MATLAB_DEBUG matlab_debug;
struct MATLAB_UART matlab_uart, matlab_uart_tmp;
struct MATLAB_PARAMS matlab_params, matlab_params_tmp;

void SDK_matlabMainLoop(void);
#endif
void SDK_EXAMPLE_direct_individual_motor_commands(void);
void SDK_EXAMPLE_direct_motor_commands_with_standard_output_mapping(void);
void SDK_EXAMPLE_attitude_commands(void);
int SDK_EXAMPLE_turn_motors_on(void);
int SDK_EXAMPLE_turn_motors_off(void);

/******** SDK in general ************
 *
 * You can find further information about the AscTec SDK in our AscTec Wiki: http://wiki.asctec.de
 *
 * Scroll down for information about how to change the Eclipse settings to use the SDK with the AscTec Simulink Toolkit.
 *
 *
 * SDK_mainloop(void) is triggered @ 1kHz.
 *
 * RO_(Read Only) data is updated before entering this function
 * and can be read to obtain information for supervision or control
 *
 * WO_(Write Only) data is written to the LL processor after
 * execution of this function.
 *
 * WO_ and RO_ structs are defined in sdk.h
 *
 * The struct RO_ALL_Data (defined in sdk.h)
 * is used to read all sensor data, results of the data fusion
 * and R/C inputs transmitted from the LL-processor. This struct is
 * automatically updated at 1 kHz.
 *
 */

/******* How to flash the high level processor ********
 *
 * The easiest way to get your code on the high level processor is to use the JTAG-adapter.
 *
 * It needs three steps to get your code on the high level processor.
 * 1. Build your code ("Build Project")
 * 2. Connect your JTAG adapter and start the JTAG connection (Run "OpenOCD Asctec-JTAG")
 * 3. Flash the processor (Run "Asctec JTAG Debug")
 *
 * In the menu "Run -> External Tools -> External Tools Configuration..." you
 * will find "OpenOCD Asctec-JTAG". If the JTAG connection was activated
 * correctly, the console will show only the following line:
 * "Info:    openocd.c:92 main(): Open On-Chip Debugger (2007-08-10 22:30 CEST)"
 *
 * Do not launch more than ONE JTAG-connection at the same time!
 *
 * In the menu "Run -> Debug Configurations..." you will find "Asctec JTAG Debug"
 * If the code was successfully flashed on the processor, the program will switch
 * to the Debug window.
 *
 * If you want to flash the high level processor using a serial interface (AscTec USB adapter)
 * and bootloader software like "Flash Magic", you can find the main.hex in your workspace folder.
 *
 */

/********* Debugging and testing your code ************
 *
 * After flashing the HL, your code can be debugged online via JTAG. The ARM7 supports ONE hardware breakpoint.
 * You can monitor the CPU-load by looking at RO_ALL_Data.HL_cpu_load. As long as this value is below 1 your
 * code in SDK_mainloop() is executed at 1 kHz. Example: 0.021 means the HL code uses 21% of the available capacity.
 *
 * To activate the SDK controls, the serial interface switch on your R/C (channel 5) needs to be in ON position.
 * If you use our standard RC Futaba FF7, the black marked switch on the right hand side needs to be pulled towards
 * the pilot to enable the serial interface.
 *
 * To test if the flashing of the HLP worked and how to enable the serial interface, you can run the example "turn
 * motors on and off every 2 seconds" in the SDK_mainloop.
 *
 */

/********** Serial communication **********
 *
 * If your project needs communication via HL serial 0 port, the easiest way is to use the AscTec Communication Interface
 * (information and tutorials can be found in the AscTec Wiki) -
 * or you can directly program the serial port (you find an example of how to do so in main.c, line 284).
 *
 */

/********** Simulink Toolkit ***********
 *
 * To use this SDK in combination with the AscTec Simulink Toolkit you need to change the Build Configuration.
 * Click on "Project - Build Configuration - Set Active - AscTec Simulink Toolkit" in the menu bar.
 *
 * Please carefully follow the complete instructions of the Simulink Toolkit manual before flashing your HLP.
 * You have to go through all steps until you can receive any data with the UART_Communication model.
 *
 * If you want to combine the automatically generated C-code from the Simulink Toolkit with additional C-code,
 * please put the additional C-code into SDK_matlabMainLoop().
 */

/********** Emergency Modes ************
 *
 * Now you can set the Emergency Modes directly from the HLP. For more information about the EMs, please have a
 * look at the AscTec Wiki. Please set an emergency mode according to the flight path of your flight mission
 * with SDK_SetEmergencyMode(). If non was set, Direct Landing is activated.
 */

void sdkInit(void)
{


	n.si[0]=0;
	n.si[1]=0;
	n.si[2]=0;
	n.si[3]=0;
	n.si[4]=0;
	n.si[5]=0;

	evel.vi[0]=0;
	evel.vi[1]=0;


	mav.fx=0;
	mav.fy=0;
	mav.fo=(1.6)*9.81;
	mav.taur=0;
	mav.taup=0;
	mav.tauy=0;

	RO_CMD_Data.dphi=0;
	RO_CMD_Data.dthe=0;

}

void SDK_mainloop(void)
{

#ifdef MATLAB
	SDK_matlabMainLoop(); //this runs only in combination with the AscTec Simulink Toolkit

	//jeti telemetry can always be activated. You may deactivate this call if you don't have the AscTec Telemetry package.
	SDK_jetiAscTecExampleRun();

#else //write your own C-code within this function
	unsigned int j;
	unsigned int i;

	float dt=0.001;

	float cx = (float)(WO_CMD_Input.cx) / (10000.0);
	float cy = (float)(WO_CMD_Input.cy) / (10000.0);
	float cz = (float)(WO_CMD_Input.cz) / (10000.0);

	float cvx = (float)(WO_CMD_Input.cvx) / (1000.0);
	float cvy = (float)(WO_CMD_Input.cvy) / (1000.0);
	float cvz = (float)(WO_CMD_Input.cvz) / (1000.0);

	float dx = (float)(WO_CMD_Input.dx) / (10000.0);
	float dy = (float)(WO_CMD_Input.dy) / (10000.0);
	float dz = (float)(WO_CMD_Input.dz) / (10000.0);
	float dvx = (float)(WO_CMD_Input.dvx) / (1000.0);
	float dvy = (float)(WO_CMD_Input.dvy) / (1000.0);
	float dvz = (float)(WO_CMD_Input.dvz) / (1000.0);
	float dyaw = (float)(WO_CMD_Input.dyaw) / (1000.0); //desired yaws

	float ks[6]; //gain for sliding surface
	ks[0]=4.5; //10.5
	ks[1]=4.5;
	ks[2]=8.5;
	ks[3]=1.0;
	ks[4]=1.0;
	ks[5]=1.0;


	float ki[6]; //integral gain
	ki[0]=0.3;
	ki[1]=0.3;
	ki[2]=2.5;
	ki[3]=0.5;
	ki[4]=0.5;
	ki[5]=0.5;

	float gam[6]; //gain for sliding surface of positon
	gam[0]=0.05;  //0.25
	gam[1]=0.05; //0.25
	gam[2]=2.5;
	gam[3]=3.5;
	gam[4]=3.5;
	gam[5]=2.5;

	float Offset[6]; //offset 
	Offset[0]=0.0;
	Offset[1]=0.0;
	Offset[2]=2.0;
	Offset[3]=0.0;
	Offset[4]=0.1;
	Offset[5]=0.0;

	float Comg=2.0; //gain for complementary filter


	float pitch = -((float)(RO_ALL_Data.angle_pitch) * M_PI) / (180.0 * 1000.0);
	float roll = (float)(RO_ALL_Data.angle_roll) * M_PI / (180.0 * 1000.0);
	float yaw =  -(float)(WO_CMD_Input.cyaw) / (1000.0); //current yaws


	if ((int)abs(RO_ALL_Data.fusion_latitude)>0)
	{

		float ax=(float)RO_ALL_Data.acc_x/(10000.0);
		float ay=-(float)RO_ALL_Data.acc_y/(10000.0);
		
		float qx=(ax+9.81*sin(pitch))/cos(pitch);
		float qy=(ay-9.81*sin(roll))/cos(roll);

		float errorx=(float)(RO_ALL_Data.fusion_speed_y)/(1000.0)-evel.vi[0];
		if (errorx>0.2)
			errorx=0.2;
		if (errorx<-0.2)
			errorx=-0.2;
		float errory=(float)(-RO_ALL_Data.fusion_speed_x)/(1000.0)-evel.vi[1];
		if (errory>0.2)
			errory=0.2;
		if (errory<-0.2)
			errory=-0.2;

		evel.vi[0]=evel.vi[0]+0.001*(qx+Comg*errorx);
		evel.vi[1]=evel.vi[1]+0.001*(qy+Comg*errory);
		
		RO_CMD_Data.velx=evel.vi[0]*1000;
		RO_CMD_Data.vely=evel.vi[1]*1000;

	}
	else
	{
		evel.vi[0]=0;
		evel.vi[1]=0;
		RO_CMD_Data.velx=evel.vi[0]*1000;
		RO_CMD_Data.vely=evel.vi[1]*1000;

	}


		
	if (dz>0.15){
	



	calcGs_mat(&k,pitch,0,0);


	float pb = (float)(RO_ALL_Data.angvel_roll)*0.0154*M_PI/180.0;
	float qb = -(float)(RO_ALL_Data.angvel_pitch)*0.0154*M_PI/180.0; // convering to ENU frame
	float rb = -(float)(RO_ALL_Data.angvel_yaw)*0.0154*M_PI/180.0;// convering to ENU frame

	float u1u2[2];

	u1u2[1]=mav.fy/mav.fo;
	u1u2[0]=mav.fx/mav.fo;
	
	float droll = sin(yaw)*u1u2[0]-cos(yaw)*u1u2[1];
	float dpitch = cos(yaw)*u1u2[0]+sin(yaw)*u1u2[1];

	if(droll>0.3)
		droll=0.3;
	else if(droll<-0.3)
		droll=-0.3; // seturated roll control

	if(dpitch>0.3)
		dpitch=0.3;
	else if(dpitch<-0.3)
		dpitch=-0.3; // seturated pitch control

	RO_CMD_Data.dphi=droll*180*1000/M_PI;
	RO_CMD_Data.dthe=dpitch*180*1000/M_PI;

	float X[8];
	X[0]=cx;
	X[1]=cy;
	X[2]=cz;
	X[3]=roll;
	X[4]=pitch;
	X[5]=yaw;
	X[6]=0;
	X[7]=0;

	float vX[8];
	vX[0]=cvx;
	vX[1]=cvy;
	vX[2]=cvz;
	vX[3]=pb;
	vX[4]=qb;
	vX[5]=rb;
	vX[6]=0;
	vX[7]=0;

	float Xd[8];
	Xd[0]=dx;
	Xd[1]=dy;
	Xd[2]=dz;
	Xd[3]=droll;
	Xd[4]=dpitch;
	Xd[5]=dyaw;
	Xd[6]=0;
	Xd[7]=0;

	float vXd[8];
	vXd[0]=dvx;
	vXd[1]=dvy;
	vXd[2]=dvz;
	vXd[3]=0;
	vXd[4]=0;
	vXd[5]=0;
	vXd[6]=0;
	vXd[7]=0;

	float ddqr[6];
	ddqr[0]=-gam[0]*(cvx-dvx);
	ddqr[1]=-gam[1]*(cvy-dvy);
	ddqr[2]=-gam[2]*(cvz-dvz);
	ddqr[3]=-gam[3]*(droll-roll);
	ddqr[4]=-gam[4]*(dpitch-pitch);
	ddqr[5]=-gam[5]*(dyaw-yaw);


 

	float sl[6];
	for(j=0; j < 6; j++){
           sl[j]=(vX[j]-vXd[j])+gam[j]*(X[j]-Xd[j]);
	}

	for (j=0;j<6;j++)
	{	
		if (cz>0.2){
			n.si[j]=n.si[j]+dt*sl[j];

			if (j==2)
			{
				if(n.si[j]>4.0)
					n.si[j]=4.0;
				if(n.si[j]<-4.0)
					n.si[j]=-4.0; // saturation of intgration
			}
			else
			{
				if(n.si[j]>0.5)
					n.si[j]=0.5;
				if(n.si[j]<-0.5)
					n.si[j]=-0.5; // saturation of intgration
			}

		}
		else 
		{
			n.si[j]=0;
		}


	}
	
	float u[6];
	u[0]=0.0;
	u[1]=0.0;
	u[2]=0.0;
	u[3]=0.0;
	u[4]=0.0;
	u[5]=0.0;

	float M[6];
	M[0]=1.1;
	M[1]=1.1;
	M[2]=1.1;
	M[3]=0.013;
	M[4]=0.013;
	M[5]=0.021;

	for(j=0; j < 6; j++){

		u[j] = M[j]*ddqr[j]+k.mGs_mat[j]-ks[j]*(sl[j])-ki[j]*(n.si[j])+Offset[j];
        }//for

	mav.fx=u[0];
	mav.fy=u[1];
	mav.fo=u[2];
	mav.taur=u[3];
	mav.taup=u[4];
	mav.tauy=u[5];

	if(mav.taur>0.3)
		mav.taur=0.3;
	else if(mav.taur<-0.3)
		mav.taur=-0.3; // seturated roll control

	if(mav.taup>0.2)
		mav.taup=0.3;
	else if(mav.taup<-0.3)
		mav.taup=-0.3; // seturated pitch control

	if(mav.tauy>0.3)
		mav.tauy=0.3;
	else if(mav.tauy<-0.3)
		mav.tauy=-0.3; // seturated yaw control

	int m1 = fast_sqrt((int)(761.3033*(mav.fo)+3540.9*(mav.taur)-6133.3*(mav.taup)-14177.6*(mav.tauy)));
	int m2 = fast_sqrt((int)(761.3033*(mav.fo)+7081.9*(mav.taur)-0.0000*(mav.taup)+14177.6*(mav.tauy)));
	int m3 = fast_sqrt((int)(761.3033*(mav.fo)+3540.9*(mav.taur)+6133.3*(mav.taup)-14177.6*(mav.tauy)));
	int m4 = fast_sqrt((int)(761.3033*(mav.fo)-3540.9*(mav.taur)+6133.3*(mav.taup)+14177.6*(mav.tauy)));
	int m5 = fast_sqrt((int)(761.3033*(mav.fo)-7081.9*(mav.taur)+0.0000*(mav.taup)-14177.6*(mav.tauy)));
	int m6 = fast_sqrt((int)(761.3033*(mav.fo)-3540.9*(mav.taur)-6133.3*(mav.taup)+14177.6*(mav.tauy)));

	if (m1>180)
		m1=180;
	if (m2>180)
		m2=180;
	if (m3>180)
		m3=180;
	if (m4>180)
		m4=180;
	if (m5>180)
		m5=180;
	if (m6>180)
		m6=180;

	WO_SDK.ctrl_mode=0x00;
	WO_SDK.ctrl_enabled=1;
	WO_SDK.disable_motor_onoff_by_stick=0;

	WO_Direct_Individual_Motor_Control.motor[0]=m1;
	WO_Direct_Individual_Motor_Control.motor[1]=m2;
	WO_Direct_Individual_Motor_Control.motor[2]=m3;
	WO_Direct_Individual_Motor_Control.motor[3]=m4;
	WO_Direct_Individual_Motor_Control.motor[4]=m5;
	WO_Direct_Individual_Motor_Control.motor[5]=m6;
	
	if (fast_abs((int)(roll))>15*M_PI/180 || fast_abs((int)(pitch))>15*M_PI/180){		
	   for(j=0;j<6;j++){
		 WO_Direct_Individual_Motor_Control.motor[j]=10;
		}
	}

	}

	for(i=0;i<6;i++)
	{
	    	if(!WO_Direct_Individual_Motor_Control.motor[i]) WO_Direct_Individual_Motor_Control.motor[i]=1;
	    	else if (WO_Direct_Individual_Motor_Control.motor[i]>190) WO_Direct_Individual_Motor_Control.motor[i]=190;
	}

#endif
}
/*
 *
 * Sets emergency mode on LowLevel processor. Select one of the EM_ defines as mode option. See EM_ defines for details
 */
inline void calcGs_mat(kalmat *kg, float the, float q1, float q2){

	for(unsigned int i=0;i<8;i++)
	{
	    kg->mGs_mat[i]=0;
	}

        float mb=1.6;

        kg->mGs_mat[0]=0.0;
        kg->mGs_mat[1]=0.0;
        kg->mGs_mat[2]=mb*(9.81);
        kg->mGs_mat[3]=0.0;
        kg->mGs_mat[4]=0.0;
        kg->mGs_mat[5]=0.0;

}


void SDK_SetEmergencyMode(unsigned char mode) {
	if ((mode != EM_SAVE_EXTENDED_WAITING_TIME) && (mode != EM_SAVE) && (mode
			!= EM_RETURN_AT_MISSION_SUMMIT) && (mode
			!= EM_RETURN_AT_PREDEFINED_HEIGHT))
		return;
	emergencyMode = mode;
	emergencyModeUpdate = 1;
}

/*
 * the following example shows the direct motor command usage by mapping the stick directly to the motor outputs (do NOT try to fly ;-) )
 */
void SDK_EXAMPLE_direct_individual_motor_commands(void)
{

	WO_SDK.ctrl_mode=0x00;	//0x00: direct individual motor control: individual commands for motors 0..3
							//0x01: direct motor control using standard output mapping: commands are interpreted as pitch, roll, yaw and thrust inputs; no attitude controller active
							//0x02: attitude and throttle control: commands are input for standard attitude controller
							//0x03: GPS waypoint control

	WO_SDK.ctrl_enabled=1;  //0: disable control by HL processor
							//1: enable control by HL processor

	WO_SDK.disable_motor_onoff_by_stick=0;

	unsigned int i;

	//scale throttle stick to [0..200] and map it to all motors
	WO_Direct_Individual_Motor_Control.motor[0]=RO_ALL_Data.channel[2]/21;
	WO_Direct_Individual_Motor_Control.motor[1]=RO_ALL_Data.channel[2]/21;
	WO_Direct_Individual_Motor_Control.motor[2]=RO_ALL_Data.channel[2]/21;
	WO_Direct_Individual_Motor_Control.motor[3]=RO_ALL_Data.channel[2]/21;
	WO_Direct_Individual_Motor_Control.motor[4]=RO_ALL_Data.channel[2]/21;
	WO_Direct_Individual_Motor_Control.motor[5]=RO_ALL_Data.channel[2]/21;

	if (RO_ALL_Data.channel[6]>2500)
	{
		WO_Direct_Individual_Motor_Control.motorReverseMask=0x01; //invert motor 0 if AUX switch is enabled
		//limit inverted speed (IMPORTANT! THIS IS NOT DONE AUTOMATICALLY!)
		if (WO_Direct_Individual_Motor_Control.motor[0]>80)
			WO_Direct_Individual_Motor_Control.motor[0]=80;
	}else
		WO_Direct_Individual_Motor_Control.motorReverseMask=0x00;


	//make sure commands are never 0 so that motors will always keep spinning
    //also make sure that commands stay within range
    for(i=0;i<6;i++)
    {
    	if(!WO_Direct_Individual_Motor_Control.motor[i]) WO_Direct_Individual_Motor_Control.motor[i]=1;
    	else if (WO_Direct_Individual_Motor_Control.motor[i]>200) WO_Direct_Individual_Motor_Control.motor[i]=200;
    }
}


void SDK_EXAMPLE_direct_motor_commands_with_standard_output_mapping(void)
{
	WO_SDK.ctrl_mode=0x01;	//0x00: direct individual motor control: individual commands for motors 0..3
							//0x01: direct motor control using standard output mapping: commands are interpreted as pitch, roll, yaw and thrust inputs; no attitude controller active
							//0x02: attitude and throttle control: commands are input for standard attitude controller
							//0x03: GPS waypoint control

	WO_SDK.ctrl_enabled=1;  //0: disable control by HL processor
							//1: enable control by HL processor

	/*
	 *  Stick commands directly mapped to motors, NO attitude control! Do NOT try to fly!
	 * */

	WO_Direct_Motor_Control.pitch=(4095-RO_ALL_Data.channel[0])/21;
	WO_Direct_Motor_Control.roll=RO_ALL_Data.channel[1]/21;
	WO_Direct_Motor_Control.thrust=RO_ALL_Data.channel[2]/21;
	WO_Direct_Motor_Control.yaw=(4095-RO_ALL_Data.channel[3])/21;

}


void SDK_EXAMPLE_attitude_commands(void)
{
	WO_SDK.ctrl_mode=0x02;	//0x00: direct individual motor control: individual commands for motors 0..3
							//0x01: direct motor control using standard output mapping: commands are interpreted as pitch, roll, yaw and thrust inputs; no attitude controller active
							//0x02: attitude and throttle control: commands are input for standard attitude controller
							//0x03: GPS waypoint control

	WO_SDK.ctrl_enabled=1;  //0: disable control by HL processor
							//1: enable control by HL processor

	//with this example the UAV will go to ~10% throttle when SDK control is activated
	WO_CTRL_Input.ctrl=0x08;	//0x08: enable throttle control by HL. Height control and GPS are deactivated!!
								//pitch, roll and yaw are still commanded via the remote control

	WO_CTRL_Input.thrust=400;	//10% throttle command


}


int SDK_EXAMPLE_turn_motors_on(void) //hold throttle stick down and yaw stick fully left to turn motors on
{
	static int timeout=0;

	WO_SDK.ctrl_mode=0x02;	//0x00: direct individual motor control: individual commands for motors 0..3
							//0x01: direct motor control using standard output mapping: commands are interpreted as pitch, roll, yaw and thrust inputs; no attitude controller active
							//0x02: attitude and throttle control: commands are input for standard attitude controller
							//0x03: GPS waypoint control

	WO_SDK.ctrl_enabled=1;  //0: disable control by HL processor
							//1: enable control by HL processor

	WO_SDK.disable_motor_onoff_by_stick=0; //make sure stick command is accepted

	if(++timeout>=1000)
	{
		timeout=0;
		return(1); //1 => start sequence completed => motors running => user can stop calling this function
	}
	else if(timeout>500) //neutral stick command for 500 ms
	{
		WO_CTRL_Input.ctrl=0x0C;	//0x0C: enable throttle control and yaw control
		WO_CTRL_Input.thrust=0;
		WO_CTRL_Input.yaw=0;
		return(0);
	}
	else //hold stick command for 500 ms
	{
		WO_CTRL_Input.ctrl=0x0C;	//0x0C: enable throttle control and yaw control
		WO_CTRL_Input.thrust=0;
		WO_CTRL_Input.yaw=-2047;
		return(0);
	}

}

int SDK_EXAMPLE_turn_motors_off(void) //hold throttle stick down and yaw stick fully right to turn motors off
{
	static int timeout=0;

	WO_SDK.ctrl_mode=0x02;	//0x00: direct individual motor control: individual commands for motors 0..3
							//0x01: direct motor control using standard output mapping: commands are interpreted as pitch, roll, yaw and thrust inputs; no attitude controller active
							//0x02: attitude and throttle control: commands are input for standard attitude controller
							//0x03: GPS waypoint control

	WO_SDK.ctrl_enabled=1;  //0: disable control by HL processor
							//1: enable control by HL processor

	WO_SDK.disable_motor_onoff_by_stick=0; //make sure stick command is accepted

	if(++timeout>=1000)
	{
		timeout=0;
		return(1); //1 => stop sequence completed => motors turned off => user can stop calling this function
	}
	else if(timeout>500) //neutral stick command for 500 ms
	{
		WO_CTRL_Input.ctrl=0x0C;	//0x0C: enable throttle control and yaw control
		WO_CTRL_Input.thrust=0;
		WO_CTRL_Input.yaw=0;
		return(0);
	}
	else //hold stick command for 500 ms
	{
		WO_CTRL_Input.ctrl=0x0C;	//0x0C: enable throttle control and yaw control
		WO_CTRL_Input.thrust=0;
		WO_CTRL_Input.yaw=2047;
		return(0);
	}
}


#ifdef MATLAB

void SDK_matlabMainLoop()
{
	static unsigned short uart_count = 1; //counter for uart communication


		/* put your own c-code here */

		rt_OneStep(); //call RTW function rt_OneStep
		//ctrl_mode is set in rt_one_step

		/* put your own c-code here */


		//don't touch anything below here

		//debug packet handling
		if (uart_count==0 && xbee_send_flag) //call function for uart transmission with 50 Hz
		{
			matlab_debug.cpu_load = HL_Status.cpu_load;
			matlab_debug.battery_voltage = HL_Status.battery_voltage_1;

			UART_Matlab_SendPacket(&matlab_debug, sizeof(matlab_debug), 'd');
		}
		uart_count++;
		uart_count%=ControllerCyclesPerSecond/50;

		//save parameters only while not flying
		if ((!RO_ALL_Data.flying) && (triggerSaveMatlabParams))
		{
			triggerSaveMatlabParams=0;
			lpc_aci_SavePara();
			lpc_aci_WriteParatoFlash();
		}

}


#endif
