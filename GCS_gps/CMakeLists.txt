# Standard setup cmake_minimum_required (VERSION 2.6)
cmake_minimum_required (VERSION 2.6)
project (gps_Vision CXX C)

set(VICON_LIB_DIR "/home/icsl/Desktop/Vision_flight/GCS_gps/src/vicon_sdk/Linux" CACHE PATH " ")
set(ICSL_FIREFLY_DIR "/home/icsl/Desktop/Vision_flight/GCS_gps" CACHE PATH " ")
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH};/home/icsl/Desktop/lmprojects/toadlet_rel/cmake/Modules CACHE PATH " ")

FIND_PACKAGE(Qt4 REQUIRED)
INCLUDE(${QT_USE_FILE})
ADD_DEFINITIONS(${QT_DEFINITIONS})

find_package (toadlet REQUIRED)
find_package (OpenCV REQUIRED core highgui imgproc)

message("OpenCV include: " )
foreach (dir ${OpenCV_INCLUDE_DIRS})
	message("\t" ${dir})
endforeach()

message("OpenCV libs: ")
foreach (lib ${OpenCV_LIBS})
	message("\t" ${lib})
endforeach()

set(LIBS ${LIBS}
	${TOADLET_EGG_LIB}
	${OpenCV_LIBS}
	${QT_LIBRARIES}
	${VICON_LIB_DIR}/libViconDataStreamSDK_CPP.so
	)

# Includes
set (includeDirs ${includeDirs}
	${TOADLET_INCLUDE_DIR}
	${OpenCV_INCLUDE_DIRS}
	${CMAKE_CURRENT_BINARY_DIR}
	)

set(includeDirs ${includeDirs} src/)
set(includeDirs ${includeDirs} src/TNT)
set(includeDirs ${includeDirs} src/TNT_Utils)
set(includeDirs ${includeDirs} src/MeasurementSystems)
set(includeDirs ${includeDirs} src/vicon_sdk/Linux)

# Source files
set (SRC ${SRC}
	src/main.cpp
	src/FlightInterface.cpp
	src/SystemControllerFeedbackLin.cpp
	src/QuadrotorInterface.cpp
	src/Acicom.cpp
	Asctec_aci/asctecCommIntf.c

)

# Headers
set (H ${H}
	src/FlightInterface.h
	src/Common.h
	src/SystemControllerFeedbackLin.h
	src/QuadrotorInterface.h
	src/Timer.h
	src/quadrotorConstants.h
	src/constants.h
	src/Acicom.h
	Asctec_aci/asctecCommIntf.h
	Asctec_aci/asctecDefines.h
	src/vicon_sdk/Linux/Client.h
)

set (FORMS ${FORMS}
	src/FlightInterface.ui
	src/QuadrotorInterface.ui
	)

set (RES ${RES})

set (SRC ${SRC} ${ICSL_SRC})
set (H ${H} ${ICSL_H})
set (LIBS ${LIBS} ${ICSL_LIBS})

message("ICSL_LIBS")
foreach (LIB ${ICSL_LIBS})
	message("\t" ${LIB})
endforeach()

set (H ${H} ${EXT_H})
set (LIBS ${LIBS} ${EXT_LIBS})

# Create QT intermediates
QT4_WRAP_CPP(H_MOC ${H})
QT4_WRAP_UI(FORM_HEADERS ${FORMS})
QT4_ADD_RESOURCES(RES_RCC ${RES})

SOURCE_GROUP(FIREFLY FILES ${ICSL_FIREFLY_DIR}/src/main.cpp)
SOURCE_GROUP(FIREFLY FILES ${ICSL_FIREFLY_DIR}/src/FlightInterface.ui)
SOURCE_GROUP(FIREFLY FILES ${ICSL_FIREFLY_DIR}/src/FlightInterface.h)
SOURCE_GROUP(FIREFLY FILES ${ICSL_FIREFLY_DIR}/src/Common.h)
SOURCE_GROUP(FIREFLY FILES ${ICSL_FIREFLY_DIR}/src/FlightInterface.cpp)
SOURCE_GROUP(FIREFLY FILES ${ICSL_FIREFLY_DIR}/src/QuadrotorInterface.ui)
SOURCE_GROUP(FIREFLY FILES ${ICSL_FIREFLY_DIR}/src/QuadrotorInterface.h)
SOURCE_GROUP(FIREFLY FILES ${ICSL_FIREFLY_DIR}/src/QuadrotorInterface.cpp)
SOURCE_GROUP(FIREFLY FILES ${ICSL_FIREFLY_DIR}/src/Acicom.cpp)
SOURCE_GROUP(FIREFLY FILES ${ICSL_FIREFLY_DIR}/src/Acicom.h)
SOURCE_GROUP(FIREFLY FILES ${ICSL_FIREFLY_DIR}/src/SystemControllerFeedbackLin.h)
SOURCE_GROUP(FIREFLY FILES ${ICSL_FIREFLY_DIR}/src/SystemControllerFeedbackLin.cpp)
SOURCE_GROUP(FIREFLY\\Telemetry\\Vicon ${ICSL_FIREFLY_DIR}/src/TelemetryVicon.cpp)
SOURCE_GROUP(FIREFLY\\Telemetry\\Vicon ${ICSL_FIREFLY_DIR}/src/TelemetryVicon.h)
SOURCE_GROUP(FIREFLY\\ACI ${ICSL_FIREFLY_DIR}/Asctec_aci/.*)
SOURCE_GROUP(FIREFLY\\Moc ${ICSL_FIREFLY_DIR}/.*moc.*)

set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} -std=c++11)


include_directories (${includeDirs})
# Executable
add_executable (gps_Vision ${SRC} ${H} ${H_MOC} ${FORM_HEADERS} ${RES_RCC})
target_link_libraries (gps_Vision ${LIBS} -lmxml -lpthread)
install (TARGETS gps_Vision DESTINATION ${gps_Vision_BINARY_DIR} )
