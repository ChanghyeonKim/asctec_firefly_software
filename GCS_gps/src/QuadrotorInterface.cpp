#include "QuadrotorInterface.h"

using namespace std;
using namespace TNT;
using namespace ICSL;

namespace ICSL{
namespace Quadrotor{
    QuadrotorInterface::QuadrotorInterface(QWidget *parent) : QWidget(parent)
    {
        setupUi(this);

        mFlightMode = QUAD_IDLE;
        mCommStatus = FREE;

        mInitialized = false;

        mStateRefBuffer.clear();
        mMotBuffer.clear();
        mXyBuffer.clear();
        mGPSBuffer.clear();
        mStateBuffer.clear();
        mControlBuffer.clear();
        mTimeBuffer.clear();
        mEstBuffer.clear();
        mEstVelBuffer.clear();
        mControlCalcTimeBuffer.clear();
        mFlightModeBuffer.clear();

		mTotalNumState = 12;
    }

    QuadrotorInterface::~QuadrotorInterface()
    {
        delete mAcicom;
        delete mControllerFeedbackLin;
        delete mChangemode;

    }

    void QuadrotorInterface::initialize()
    {
        mChangemode = new QShortcut(Qt::Key_P, this);

        connect(mChangemode,SIGNAL(activated()), this, SLOT(FlightModechange()));
        connect(btnSetXyTarget,SIGNAL(clicked()),this,SLOT(onBtnSetXyDesired_clicked()));

        mAcicom = new AciCom;
        mControllerFeedbackLin = new SystemControllerFeedbackLin;

        if(mInitialized)
        {
            cout << "I'm already initialized, stupid." << endl;
            return;
        }
        mInitialized = true;
        mStartTimeUniverseMS = mTmr.getCurTimeMS();

        mControllerFeedbackLin->setDesiredState(Array2D<double>(12,1,0.0));
        mControllerFeedbackLin->setCurState(Array2D<double>(12,1,0.0));

        mAcicom->initialize();

        mcurstate=Array2D<double>(12,1,0.0);
        meststate=Array2D<double>(6,1,0.0);
        mestvelstate=Array2D<double>(6,1,0.0);

        mmotorstate=Array2D<double>(6,1,0.0);
        mdesstate=Array2D<double>(12,1,0.0);

        mxy=Array2D<double>(7,1,0.0);
        GPS_accuracy=Array2D<double>(4,1,0.0);

		inix=0.0;
		iniy=0.0;
        iniz=0.0;

		GPS_lat=0.0;
		GPS_lon=0.0;

        GX_ref = 0;
        GY_ref = 0;
        GZ_ref = 0;

        vision_flag=false;

        aciGetDeviceVariablesList();
        aciGetDeviceCommandsList();
    }

        Array2D<double> QuadrotorInterface::getCurState()
	{

        int result = 0;
        unsigned char data = 0;


		Array2D<double> curState(mTotalNumState,1,0.0);
		mMutex_des.lock();
        Array2D<double> desstate = mdesstate.copy();
        mMutex_des.unlock();

            timecounter1++;
           // cout<<timecounter1<<endl;
            mMutex_send.lock();
                    result = read(mAcicom->fd, &data, 1);
                     while ((result>0)) {
                        aciReceiveHandler(data);
                        result = read(mAcicom->fd, &data, 1);
                    }

              if (timecounter1>0){
                    if(mAcicom->var_getted && mAcicom->cmd_ready) {
                        aciSynchronizeVars();
                    }

                timecounter1=0;
            }
                mmotorstate[0][0]=(double)(mAcicom->motor1);
                mmotorstate[1][0]=(double)(mAcicom->motor2);
                mmotorstate[2][0]=(double)(mAcicom->motor3);
                mmotorstate[3][0]=(double)(mAcicom->motor4);
                mmotorstate[4][0]=(double)(mAcicom->motor5);
                mmotorstate[5][0]=(double)(mAcicom->motor6);

                curState[3][0] = (double)(mAcicom->angle_roll*PI/180)/1000;
                curState[4][0] = (double)(-mAcicom->angle_pitch*PI/180)/1000;

                desstate[3][0] = (double)(mAcicom->droll*PI/180)/1000;
                desstate[4][0] = (double)(mAcicom->dpitch*PI/180)/1000;

                GPS_lat=(double)(mAcicom->gps_x)*PI/180/10000000;
                GPS_lon=(double)(mAcicom->gps_y)*PI/180/10000000;

                mestvelstate[0][0]=(double)(mAcicom->evelx)/1000;
                mestvelstate[1][0]=(double)(mAcicom->evely)/1000;

                mxy[2][0]=(double)(mAcicom->est_z)/1000;
                mxy[3][0]=(double)(mAcicom->est_dy)/1000;
                mxy[4][0]=(double)(-mAcicom->est_dx)/1000;
                mxy[5][0]=(double)(mAcicom->est_dz)/1000;
                mxy[6][0]=(double)(mAcicom->angle_yaw)*PI/180/1000;
                if (mxy[6][0]>180*PI/180)
                 mxy[6][0]=mxy[6][0]-2*PI;

                GPS_accuracy[0][0]=(double)(mAcicom->x_accuracy)/1000;
                GPS_accuracy[1][0]=(double)(mAcicom->y_accuracy)/1000;
                GPS_accuracy[2][0]=(double)(mAcicom->z_accuracy)/1000;
                GPS_accuracy[3][0]=(double)(mAcicom->num_sat);

                usleep(1000);

                mMutex_send.unlock();


		mControllerFeedbackLin->setDesiredState(desstate);

        gpsToxyz();
		return curState;


	} // get cur state


    void QuadrotorInterface::updateDisplay()
    {
        switch(mFlightMode)
        {
            case QUAD_IDLE:
                lblFlightMode->setText("IDLE");
                break;
            case QUAD_PROFILE_TRACKING:
                lblFlightMode->setText("Profile Tracking");
                break;
            case QUAD_MANUAL:
                lblFlightMode->setText("Manual");
                break;
            default:
                lblFlightMode->setText("Unknown");
        }

        if(mStateBuffer.size() > 0)
        {
            lblState00->setText(QString::number(mcurstate[0][0],'f',3));
            lblState01->setText(QString::number(mcurstate[1][0],'f',3));
            lblState02->setText(QString::number(mcurstate[2][0],'f',3));
            lblState03->setText(QString::number(mcurstate[3][0]*RAD2DEG,'f',1));
            lblState04->setText(QString::number(mcurstate[4][0]*RAD2DEG,'f',1));
            lblState05->setText(QString::number(mcurstate[5][0]*RAD2DEG,'f',1));

            lblState00_2->setText(QString::number(meststate[0][0],'f',3));
            lblState01_2->setText(QString::number(meststate[1][0],'f',3));
            lblState02_2->setText(QString::number(meststate[2][0],'f',3));
            lblState03_2->setText(QString::number(meststate[3][0]*RAD2DEG,'f',1));
            lblState04_2->setText(QString::number(meststate[4][0]*RAD2DEG,'f',1));
            lblState05_2->setText(QString::number(meststate[5][0]*RAD2DEG,'f',1));

            lblState20->setText(QString::number(mestvelstate[0][0],'f',3));
            lblState21->setText(QString::number(mestvelstate[1][0],'f',3));
            lblState22->setText(QString::number(mestvelstate[2][0],'f',3));
            lblState23->setText(QString::number(mestvelstate[3][0]*RAD2DEG,'f',1));
            lblState24->setText(QString::number(mestvelstate[4][0]*RAD2DEG,'f',1));
            lblState25->setText(QString::number(mestvelstate[5][0]*RAD2DEG,'f',1));

            lblState10->setText(QString::number(mdesstate[0][0],'f',3));
            lblState11->setText(QString::number(mdesstate[1][0],'f',3));
            lblState12->setText(QString::number(mdesstate[2][0],'f',3));
            lblState13->setText(QString::number(mdesstate[3][0]*RAD2DEG,'f',1));
            lblState14->setText(QString::number(mdesstate[4][0]*RAD2DEG,'f',1));
            lblState15->setText(QString::number(mdesstate[5][0]*RAD2DEG,'f',1));

            lblmh00->setText(QString::number(mxy[0][0],'f',3));
            lblmh01->setText(QString::number(mxy[1][0],'f',3));
            lblmh02->setText(QString::number(mxy[2][0],'f',3));
            lblmh04->setText(QString::number(mxy[3][0],'f',3));
            lblmh05->setText(QString::number(mxy[4][0],'f',3));
            lblmh06->setText(QString::number(mxy[5][0],'f',3));
            lblmh07->setText(QString::number(GPS_accuracy[3][0],'f',1));
            lblmh08->setText(QString::number(mxy[6][0]*RAD2DEG,'f',3));

            lblState40->setText(QString::number(mdesstate[6][0],'f',3));
            lblState41->setText(QString::number(mdesstate[7][0],'f',3));
            lblState42->setText(QString::number(mdesstate[8][0],'f',3));
            lblState43->setText(QString::number(mdesstate[9][0]*RAD2DEG,'f',1));
            lblState44->setText(QString::number(mdesstate[10][0]*RAD2DEG,'f',1));
            lblState45->setText(QString::number(mdesstate[11][0]*RAD2DEG,'f',1));

            lblendmot1->setText(QString::number(mmotorstate[0][0],'f',1));
            lblendmot2->setText(QString::number(mmotorstate[1][0],'f',1));
            lblendmot3->setText(QString::number(mmotorstate[2][0],'f',1));
            lblendmot4->setText(QString::number(mmotorstate[3][0],'f',1));
            lblendmot5->setText(QString::number(mmotorstate[4][0],'f',1));
            lblendmot6->setText(QString::number(mmotorstate[5][0],'f',1));

            lblControlCalcTime->setText(QString::number(calc_time-1));
        }
    }

    void QuadrotorInterface::run()
    {

        unsigned long dispTimeStart = mTmr.getCurTimeMS();

        if(isEnabled()){



            mTimeBuffer.push_back(dispTimeStart-mStartTimeUniverseMS);
            calcControl();
            if((mAcicom->cmd_ready)&&(mAcicom->var_getted)){
                sendControl();
            }
            aciEngine();
            usleep(1000);
        }

        calc_time = mTmr.getCurTimeMS()-dispTimeStart;
    }

    void QuadrotorInterface::calcControl()
    {
        if(!mInitialized)
            throw("Quadrotor interface is not initialized.");

        unsigned long cntlStartTime = mTmr.getCurTimeMS();

        Array2D<double> control;

        mdesstate = mControllerFeedbackLin->getDesiredState();

        mcurstate = getCurState();
        mControllerFeedbackLin->setinitialState(mcurstate.copy());
        mControllerFeedbackLin->setCurState(mcurstate);


        control = mControllerFeedbackLin->calcControl();


        mMutex_logBuffer.lock();
        mStateBuffer.push_back(mcurstate.copy());
        mMotBuffer.push_back(mmotorstate.copy());
        mStateRefBuffer.push_back(mdesstate.copy());
        mXyBuffer.push_back(mxy.copy());
        mGPSBuffer.push_back(GPS_accuracy.copy());
        mControlBuffer.push_back(control.copy());
        mFlightModeBuffer.push_back(mFlightMode);
        mEstBuffer.push_back(meststate.copy());
        mEstVelBuffer.push_back(mestvelstate.copy());
        mMutex_logBuffer.unlock();
    }

    void QuadrotorInterface::sendControl()
    {
        if(!mInitialized)
            throw("Quadrotor interface is not initialized.");

        if(mFlightMode == QUAD_IDLE)
        {
            //do nothing
        }
        else

        {
            Array2D<double> curStateq = mcurstate.copy();
            Array2D<double> targetState = mdesstate.copy();


            //cout<<int16_t(control[0][0]*1000)<<endl;
                mMutex_send.lock();
                //cout<<"vision_flag"<<endl;
                if (vision_flag==false)
                {
                    mAcicom->cx = (int16_t)((meststate[0][0])*10000);
                    mAcicom->cy = (int16_t)(meststate[1][0]*10000);
                    mAcicom->cz = (int16_t)((meststate[2][0])*10000);

                    mAcicom->cvx = (int16_t)(mestvelstate[0][0]*1000); //6
                    mAcicom->cvy = (int16_t)(mestvelstate[1][0]*1000);
                    mAcicom->cvz = (int16_t)(mestvelstate[2][0]*1000);

                    mAcicom->cyaw = (int16_t)(-meststate[5][0]*1000);

                }
                else
                {
                    mAcicom->cx = (int16_t)((meststate[0][0])*10000);
                    mAcicom->cy = (int16_t)(meststate[1][0]*10000);
                    mAcicom->cz = (int16_t)((meststate[2][0])*10000);
                    mAcicom->cvx = (int16_t)(mestvelstate[0][0]*1000); //6
                    mAcicom->cvy = (int16_t)(mestvelstate[1][0]*1000);
                    mAcicom->cvz = (int16_t)(mestvelstate[2][0]*1000);
                    mAcicom->cyaw = (int16_t)(-meststate[5][0]*1000);
                }



                mAcicom->dx = (int16_t)(targetState[0][0]*10000);
                mAcicom->dy = (int16_t)(targetState[1][0]*10000);
                mAcicom->dz = (int16_t)(targetState[2][0]*10000);
                mAcicom->dvx = (int16_t)(targetState[6][0]*1000);
                mAcicom->dvy = (int16_t)(targetState[7][0]*1000);
                mAcicom->dvz = (int16_t)(targetState[8][0]*1000);
                mAcicom->dyaw = (int16_t)(targetState[5][0]*1000);
                //cout<<AciCom::take_off<<endl;
                aciUpdateCmdPacket(0);
                usleep(1000);
                mMutex_send.unlock();
        }
    }

   void QuadrotorInterface::gps_ref()
   {
        double chi, R0, e;
        R0 = 6378137; // radius of the Earth. [m]
        e = sqrt(6.69437999014 / 1000.0);
        chi = sqrt(1 - pow(e, 2)*pow(sin(GPS_lat), 2));

        GX_ref = (R0 / chi + GPS_h)*cos(GPS_lat)*cos(GPS_lon);
        GY_ref = (R0 / chi + GPS_h)*cos(GPS_lat)*sin(GPS_lon);
        GZ_ref = (R0*(1 - pow(e, 2)) / chi + GPS_h)*sin(GPS_lat);
        cout<<GX_ref<<"\t"<<GY_ref<<"\t"<<GZ_ref<<endl;
   }

   void QuadrotorInterface::gpsToxyz()
   {
        double chi, R0, e, X, Y, Z, xt, yt, zt;
        R0 = 6378137; // radius of the Earth. [m]
        e = sqrt(6.69437999014 / 1000.0);
        chi = sqrt(1 - pow(e, 2)*pow(sin(GPS_lat), 2));

        xt = (R0 / chi + GPS_h)*cos(GPS_lat)*cos(GPS_lon);
        yt = (R0 / chi + GPS_h)*cos(GPS_lat)*sin(GPS_lon);
        zt = (R0*(1 - pow(e, 2)) / chi + GPS_h)*sin(GPS_lat);
        X = xt - GX_ref;
        Y = yt - GY_ref;
        Z = zt - GZ_ref;

        // ECEF to ENU
        mxy[1][0] = sin(GPS_lon)*X + cos(GPS_lon)*Y;
        mxy[0][0] = -(sin(GPS_lat)*cos(GPS_lon)*X - sin(GPS_lat)*sin(GPS_lon)*Y + cos(GPS_lat)*Z);


   }

    void QuadrotorInterface::sendStopAndShutdown()
    {
        mFlightMode = QUAD_IDLE;

        if(mCommStatus == BUSY)
            return;

        mTmr.sleepMS(100);
        cout << "done" << endl;

    }


    void QuadrotorInterface::FlightModechange()
    {

        if (vision_flag==false)
        {
            vision_flag=true;

            Array2D<double> curState(12,1,0.0);

            curState = getCurState();

            Array2D<double> targetState = mControllerFeedbackLin->getDesiredState();
            targetState[0][0]=meststate[0][0];
            targetState[1][0]=meststate[1][0];

            iniz=meststate[2][0];
            inix=meststate[0][0];
            iniy=meststate[1][0];

            mControllerFeedbackLin->setDesiredState(targetState.copy());
            mProfileStartPoint = targetState.copy();

            cout<<"Vision Mode"<<endl;
        }
        else
        {
            vision_flag=false;
            cout<<"Vicon Mode"<<endl;
        }
    }

    void QuadrotorInterface::onBtnSetXyDesired_clicked()
    {

        gps_ref();

        Array2D<double> curState(12,1,0.0);

        curState = getCurState();

        inix=meststate[0][0];
        iniy=meststate[1][0];

        Array2D<double> targetState = mControllerFeedbackLin->getDesiredState();

        targetState[0][0]=meststate[0][0];
        targetState[1][0]=meststate[1][0];
        targetState[2][0]=meststate[2][0];

        mControllerFeedbackLin->setDesiredState(targetState.copy());
        mProfileStartPoint = targetState.copy();
    }

    void QuadrotorInterface::clearAllBuffers()
    {
        mMutex_logBuffer.lock();
        mStateRefBuffer.clear();
        mStateBuffer.clear();
        mControlBuffer.clear();
        mMotBuffer.clear();
        mTimeBuffer.clear();
        mXyBuffer.clear();
        mGPSBuffer.clear();
        mControlCalcTimeBuffer.clear();
        mFlightModeBuffer.clear();
        mEstBuffer.clear();
        mEstVelBuffer.clear();
        mMutex_logBuffer.unlock();
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////

    inline double QuadrotorInterface::constrain(double val, double minVal, double maxVal)
    {
        return min(maxVal, max(minVal, val));
    }


    void QuadrotorInterface::saveData(string dir, string filename)
    {

        fstream dataStream;

        dataStream.open((dir+"/"+filename).c_str(),fstream::out);
        if(dataStream.is_open()){

            dataStream << "frame,\tflight mode,\ttime,\tCommand 0,\tCommand 1";
            for(int j=0; j<12; j++)
                dataStream << "\tTx,\tTy,\tTz,\tTr,\tTp,\tTyaw,\tTdx,\tTdy,\tTdz,\tTdr,\tTdp,\tTdyaw";
            for(int j=0; j<12; j++)
                dataStream << ",tCx,\tCy,\tCz,\tCr,\tCp,\tTCaw,\tCdx,\tCdy,\tCdz,\tCdr,\tCdp,\tCdyaw";;
            dataStream << ",\tmotor1,\tmotor2,\tmotor3,\tmotor4,\tmotor5,\tmotor6";
            for(int j=0; j<6; j++)
                dataStream << ",\tEstState " << j;
            for(int j=0; j<6; j++)
                dataStream << ",\tEstVelState " << j;
            for(int j=0; j<7; j++)
                dataStream << ",\tGPS"<< j;
            for(int j=0; j<4; j++)
                dataStream << ",\tGAccuracy"<< j;
            dataStream << ",\tCalctime";
            dataStream << endl;

            mMutex_logBuffer.lock();
            // save data
            list<int>::iterator iter_flightMode = mFlightModeBuffer.begin();
            list<unsigned long>::iterator iter_time = mTimeBuffer.begin();
            list<Array2D<double> >::iterator iter_cntl= mControlBuffer.begin();
            list<Array2D<double> >::iterator iter_stateRef = mStateRefBuffer.begin();
            list<Array2D<double> >::iterator iter_state = mStateBuffer.begin();
            list<Array2D<double> >::iterator iter_motor = mMotBuffer.begin();
            list<Array2D<double> >::iterator iter_eststate = mEstBuffer.begin();
            list<Array2D<double> >::iterator iter_estvelstate = mEstVelBuffer.begin();
            list<Array2D<double> >::iterator iter_xy = mXyBuffer.begin();
            list<Array2D<double> >::iterator iter_gps = mGPSBuffer.begin();


            list<unsigned long>::iterator iter_cntlCalcTime = mControlCalcTimeBuffer.begin();
            for(int i=0; i<mTimeBuffer.size(); i++)
            {

                dataStream << i;
                dataStream << ",\t" << *(iter_flightMode++);
                dataStream << ",\t" << *(iter_time++);
                for(int j=0; j<2; j++)
                   dataStream << ",\t" << (*(iter_cntl))[j][0];
                iter_cntl++;
                for(int j=0; j<12; j++)
                    dataStream << ",\t" << (*(iter_stateRef))[j][0];
                iter_stateRef++;
                for(int j=0; j<12; j++)
                    dataStream << ",\t" << (*(iter_state))[j][0];
                iter_state++;
                for(int j=0; j<6; j++)
                   dataStream << ",\t" << (*(iter_motor))[j][0];
                iter_motor++;
                for(int j=0; j<6; j++)
                   dataStream << ",\t" << (*(iter_eststate))[j][0];
                iter_eststate++;
                for(int j=0; j<6; j++)
                   dataStream << ",\t" << (*(iter_estvelstate))[j][0];
                iter_estvelstate++;
                for(int j=0; j<7; j++)
                   dataStream << ",\t" << (*(iter_xy))[j][0];
                iter_xy++;
                for(int j=0; j<4; j++)
                   dataStream << ",\t" << (*(iter_gps))[j][0];
                iter_gps++;
                dataStream << ",\t" << *(iter_cntlCalcTime++);
                dataStream << endl;
            }

        }

		dataStream.close();
        mMutex_logBuffer.unlock();
    }


}
}
