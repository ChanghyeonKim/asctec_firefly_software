#include "TNT/jama_lu.h"
#include "TNT_Utils/TNT_Utils.h"
#include "SystemControllerFeedbackLin.h"
namespace ICSL{
namespace Quadrotor{

    using namespace std;
    using namespace ICSL;
    using namespace TNT;

    SystemControllerFeedbackLin::SystemControllerFeedbackLin() :
        mDesiredState(12,1,0.0),
        mCurState(12,1,0.0),
        miniState(12,1,0.0)
    {
        mDeltaT = 0.04;

    }

    SystemControllerFeedbackLin::~SystemControllerFeedbackLin()
    {

    }

    Array2D<double> SystemControllerFeedbackLin::calcControl()

    {

        //mErrorFilterSysFilename = "";
        Array2D<double> input(2,1);
        input[0][0]=0.0;
        input[1][0]=0.0;
        return input;
    }



    void SystemControllerFeedbackLin::setDesiredState(TNT::Array2D<double> x)
    {
        mMutex_des.lock();
        mDesiredState = x.copy();
        mMutex_des.unlock();
    }


    void SystemControllerFeedbackLin::setinitialState(TNT::Array2D<double> x)
    {
        mMutex_ini.lock();
        miniState = x.copy();
        mMutex_ini.unlock();
    }

    TNT::Array2D<double> SystemControllerFeedbackLin::getDesiredState()
    {

        Array2D<double> desState;
        mMutex_des.lock();
        desState=mDesiredState.copy();
        mMutex_des.unlock();
        return desState;
    }


}
}
