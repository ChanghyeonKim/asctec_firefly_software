#include <mxml.h>

#ifndef ICSL_XML_UTILS
#define ICSL_XML_UTILS
using namespace std;

namespace ICSL
{
	class XmlUtils
	{
		public:
			static const char* whitespaceCallback(mxml_node_t *node, int where)
			{
				if(node->parent == NULL)
					return NULL;
				else if(where == MXML_WS_BEFORE_OPEN ||
						(where == MXML_WS_BEFORE_CLOSE && node->child != node->last_child))
				{
					string str = "\n";
					mxml_node_t *parent = node->parent->parent; 
					while(parent != NULL)
					{
						str += "   ";
						parent = parent->parent;
					}
					return str.c_str();
				}
				else
					return NULL;
			}

	};
}
#endif
