#ifndef ICSL_CLASS_TIMER
#define ICSL_CLASS_TIMER

#include <iostream>

#include "toadlet/egg/System.h"
#include "toadlet/egg/Thread.h"
#include "toadlet/egg/Mutex.h"
#include "toadlet/egg/WaitCondition.h"

//#include "config.h"
namespace ICSL
{

    class TimerCallbackParams
    {
        public:
            TimerCallbackParams(){};
            virtual ~TimerCallbackParams(){};

            void setData(int d){mData = d;};
            int getData(){return mData;};

        protected:
            int mData;
    };

    class Timer
    {
        public:
            Timer(){};
            virtual ~Timer()
            {
                if(mRepeatedTimer.isAlive())
                {
                    mRepeatedTimerIsRunning = false;
                    mRepeatedTimer.join();
                }
            }

            unsigned long getCurTimeMS()
            {
                return mSys.mtime();
            }

            /*!
              I'm not sure that the microsecond timing is working correctly
              */
            unsigned long  getCurTimeUS()
            {
                return mSys.utime();
            }

            void sleepMS(unsigned long t)
            {
                mSys.msleep(t);
            }

            /*!
              I'm not sure that the microsecond timing is working correctly
              */
            void sleepUS(unsigned long t)
            {
                mSys.usleep(t);
            }

            void runCallback(unsigned long tMS, void (*callback)(TimerCallbackParams *params))
            {
                if(!mRepeatedTimerIsRunning)
                {
                    mCallback = callback;
                    mRepeatedTimerIsRunning = true;
                    mRepeatedTimer.mIntervalMS = tMS;
                    mRepeatedTimer.parent = this;
                    mRepeatedTimer.start();
                }
            }

            void runCallback(unsigned long tMS)
            {
                if(mCallback!=NULL)
                    runCallback(tMS, mCallback);
            }

            void setCallback(void (*callback)(TimerCallbackParams *params))
            {
                if(!mRepeatedTimerIsRunning)
                    mCallback = callback;
                else
                    throw("cannot change callback while timer is running");
            }

            void setCallbackParams(TimerCallbackParams *params)
            {
                mMutex_callback.lock();
                    mCallbackParams = params;
                mMutex_callback.unlock();
            }
            void stopCallback()
            {
                if(mRepeatedTimerIsRunning)
                {
                    mMutex_callback.lock();
                        mRepeatedTimerIsRunning = false;
                    mMutex_callback.unlock();
                    mRepeatedTimer.join();
                }
            }

        protected:
            toadlet::egg::System mSys;
            void (*mCallback)(TimerCallbackParams*);
            TimerCallbackParams *mCallbackParams;
            toadlet::egg::Mutex mMutex_callback;

            class RepeatedTimer : public toadlet::egg::Thread
            {
                public:
                    unsigned long mIntervalMS;
                    Timer *parent;

                private:
                    void run()
                    {
                        Timer tmr;
                        unsigned long mStartTime;
                        bool isRunning;
                        toadlet::egg::Mutex *mut = &(parent->mMutex_callback);
                        mut->lock();
                            isRunning = parent->mRepeatedTimerIsRunning;
                        mut->unlock();
                        while(isRunning)
                        {
                            mStartTime = tmr.getCurTimeMS();
                            while(isRunning && tmr.getCurTimeMS()-mStartTime < mIntervalMS)
                            {
                                tmr.sleepMS(1); // polling
                                mut->lock();
                                    isRunning = parent->mRepeatedTimerIsRunning;
                                mut->unlock();
                            }
                            if(!isRunning)
                                break; // in case a kill signal came in the middle of an interval

                            mStartTime = tmr.getCurTimeMS();
                            mut->lock();
                                (*parent->mCallback)(parent->mCallbackParams);
                            mut->unlock();
                        }
                    }
            };
            RepeatedTimer mRepeatedTimer;
            bool mRepeatedTimerIsRunning;

    };
}
#endif
