#include <fstream>
#include <vector>
#include <exception>

#include "ICSL/icsl_config.h" // stores platform specific configuration variables
#ifdef HAVE_CONIO_H
	#include <conio.h> // this is a Windows lib
#else // assume I am on Linux
	#include <termios.h> 
#endif

#include "TNT/tnt.h"
#include "ICSL/TNT_Utils/TNT_Utils.h"
#include "MeasurementSystem.h"
#include "MeasurementSystem_Vicon.h"

using namespace std;
using namespace TNT;
using namespace ICSL;

int getKeypressInstant();
void clearScreen();
const char* whitespaceCallback(mxml_node_t *node, int where);

#define UINT unsigned int

/*! 
 documenting the main function

 \param argv chad
 \param argc is bad
 \return status

 \todo make this function useful
*/
int main(int argv, char* argc[])
{
    cout << "Start chadding" << endl;
    srand ( (UINT)time(NULL) );

	// Create the measurement object
    MeasurementSystem_Vicon* chadMS = new MeasurementSystem_Vicon();
	chadMS->setSourceID("localhost:801");
	chadMS->setSimulationMode(true);

	// Connect to the vicon system
	ConnectionStatus connResult = chadMS->connectToSource();
	if(connResult.getStatus() == ConnectionStatus::Connected)
		cout << chadMS->getSourceID()<< " successfully connected." << endl;
	else
		cout << chadMS->getSourceID() << ": " << connResult.getMessage() << endl;

	// add a single object, which has 5 markers, to be tracked
	vector<string> markerNames(5);
	markerNames.at(0) = "x_head";
	markerNames.at(1) = "y_head";
	markerNames.at(2) = "x_tail";
	markerNames.at(3) = "y_tail";
	markerNames.at(4) = "Unlabeled10158";
	chadMS->addTrackingObject("quad_hyon_1",markerNames,0);

	// create the data logger
	mxml_node_t *log = mxmlNewXML("1.0");
	chadMS->setDataLog(log);

	// set the replay log
	FILE *fpReplay = fopen("replay.xml","r");
	if(fpReplay == NULL)
		throw("Replay log not found");
//	mxml_node_t *replayLog = mxmlLoadFile(NULL,fpReplay,determineNodeType);
	mxml_node_t *replayLog = mxmlLoadFile(NULL,fpReplay,MXML_NO_CALLBACK);
	fclose(fpReplay);
	chadMS->startReplay(replayLog);
	
	// Update measurements and display to console when the user presses enter
	int keypress = getKeypressInstant();
	while(keypress != (int)'q' && keypress != (int)'Q')
	{
		switch(keypress)
		{
		case(10):case(13): // newline
				{
					clearScreen();

					try {
						chadMS->updateMeasurements();

						TrackedObject* trackObj =  chadMS->getTrackedObject(0);
						for(int i=0; i<5; i++)
						{
							cout << trackObj->name+"/" + trackObj->markers.at(i).name + ":  ";
							printTNTArray(transpose(trackObj->markers.at(i).position));
						}
					}
					catch(string e)
					{ cout << e << endl; }
					catch(char const* e)
					{ cout << e << endl; }
					catch(exception &e)
					{ cout << e.what() << endl; }
				}
				break;
			default:
				break;
		}

		keypress = getKeypressInstant();
	}

	chadMS->disconnectFromSource();

	FILE *fp = fopen("log.xml","w");
	mxmlSaveFile(log, fp, whitespaceCallback);
	fclose(fp);

    delete chadMS;
    return 0;
}

/*!
 Function to get a single keypress instantaneously (i.e. without waiting
 for the terminal buffer). It does wait indefinitely for a keypress.

 \return ASCII code of first key pressed

 \todo make this work for windows too
 */
int getKeypressInstant()
{
#ifdef HAVE_CONIO_H
	int keypress = (int)_getch();
#else
	struct termios ttystate;

	// disables terminal buffering
	tcgetattr(STDIN_FILENO, &ttystate);
	ttystate.c_lflag &= ~ICANON;
	ttystate.c_lflag &= ~ECHO;
	tcsetattr(STDIN_FILENO, TCSANOW, & ttystate);

	int keypress = cin.get();

	// reenable terminal buffering
	tcgetattr(STDIN_FILENO, &ttystate);
	ttystate.c_lflag |= ICANON;
	ttystate.c_lflag |= ECHO;
	tcsetattr(STDIN_FILENO, TCSANOW, & ttystate);
#endif

	return keypress;
}

/*!
 Function to clear the consol screen

 \todo do this in a better way if we are going to stick with consoles
 */
void clearScreen()
{
#ifdef ICSL_OS_WIN
	system("cls");
#else
	system("clear");
#endif
}

const char* whitespaceCallback(mxml_node_t *node, int where)
{
	if(node->parent == NULL)
		return NULL;
	else if(where == MXML_WS_BEFORE_OPEN ||
	  (where == MXML_WS_BEFORE_CLOSE && node->child != node->last_child))
	{
		string str = "\n";
		if(node->parent != NULL)
		{
			mxml_node_t *parent = node->parent->parent; 
			while(parent != NULL)
			{
				str += "   ";
				parent = parent->parent;
			}
		}
		return str.c_str();
	}
	else
		return NULL;
}

