#include <sstream>

#include "MeasurementSystem_Vicon.h"


namespace ICSL
{
using namespace ViconDataStreamSDK::CPP;
MeasurementSystem_Vicon::MeasurementSystem_Vicon()
{
	mInSimulationMode = false;
	mDataLogger = NULL;
	mReplayLog = NULL;
	mLogIndexSave = -1;

//	mTrackingList.reserve(5);
}

MeasurementSystem_Vicon::~MeasurementSystem_Vicon()
{
}

ConnectionStatus MeasurementSystem_Vicon::connectToSource()
{
	if(mInSimulationMode)
		mConnectionStatus.setStatus(ConnectionStatus::Connected,"");
	else
		if(mClient.Connect(mSourceID).Result == Result::Success)
			mConnectionStatus.setStatus(ConnectionStatus::Connected,"");
		else
			mConnectionStatus.setStatus(ConnectionStatus::UnknownFailure,"Unknown connection failure.");

	// Needed to do measurements based on markers
	mClient.EnableMarkerData();

	// other stuff that was included in the old software
	/// \todo see if this stuff in still necessary
	mClient.EnableUnlabeledMarkerData();
	mClient.EnableDeviceData();
	mClient.SetStreamMode( StreamMode::ClientPull );

	return mConnectionStatus;
}

ConnectionStatus MeasurementSystem_Vicon::disconnectFromSource()
{
	if(mInSimulationMode)
		mConnectionStatus.setStatus(ConnectionStatus::Connected,"");

	Output_Disconnect connResult = mClient.Disconnect();
	if(connResult.Result != Result::Success)
		mConnectionStatus.setStatus(ConnectionStatus::UnknownFailure,"Unknown failure.");

	return mConnectionStatus;
}

void MeasurementSystem_Vicon::setSourceID(string sourceID)
{
	mSourceID = sourceID;
}

string MeasurementSystem_Vicon::getSourceID()
{
	return mSourceID;
}

void MeasurementSystem_Vicon::updateMeasurements()
{
	if( mTrackingList.size() == 0 )
		throw("Cannot update measurements: No objects being tracked.");

	startNewLogEntry();
	if(mInSimulationMode)
	{
		if(mReplayLog != NULL)
		{
			stringstream replaySS; replaySS << "index" << ++mLogIndexReplay;
			mxml_node_t *replayIndexNode = mxmlFindElement(mReplayLog,mReplayLog,replaySS.str().c_str(),NULL,NULL,MXML_DESCEND);
			if(replayIndexNode == NULL)
			{
				mLogIndexReplay = 0;
				stringstream replaySS2; replaySS2 << "index" << mLogIndexReplay;
				replayIndexNode = mxmlFindElement(mReplayLog,mReplayLog,replaySS2.str().c_str(),NULL,NULL,MXML_DESCEND);
			}

			for(UINT trackObjIndex=0; trackObjIndex<mTrackingList.size(); trackObjIndex++)
			{
				TrackedObject* trackObj = &mTrackingList.at(trackObjIndex);
				mxml_node_t *objectNode = mxmlFindElement(replayIndexNode,replayIndexNode,trackObj->name.c_str(),NULL,NULL,MXML_DESCEND);
				loadObjectReplay(objectNode, trackObj);

				// TODO: This is just temporary until I have more real data logs
				startObjectLog(trackObj->name);
				for(UINT markerIndex=0; markerIndex<trackObj->markers.size(); markerIndex++)
				{
					TrackingMarker* marker = &(trackObj->markers.at(markerIndex));
					logMarkerPosition(marker->name, marker->position, false);
				}
			}
		}
		else
		{
			for(UINT trackObjIndex=0; trackObjIndex<mTrackingList.size(); trackObjIndex++)
			{
				TrackedObject* trackObj = &mTrackingList.at(trackObjIndex);

				// TODO: This is just temporary until I have more real data logs
				for(UINT markerIndex=0; markerIndex<trackObj->markers.size(); markerIndex++)
				{
					TrackingMarker* marker = &(trackObj->markers.at(markerIndex));

					marker->position[0][0] = rand() % 200-100;
					marker->position[1][0] = rand() % 200-100;
					marker->position[2][0] = rand() % 200-100;
					marker->isOccluded = false;

					logMarkerPosition(marker->name, marker->position, false);
				}
			}
		}
	}
	else
	{
		// Make sure the conenction is still valid
//		Output_IsConnected outputConnect = mClient.IsConnected();
		if( !mClient.IsConnected().Connected )
			throw("Cannot update measurements: Not connected to measurement server.");

		// Get measurement frame. For now assume that if it doesn't work
		// right the first time then there is something wrong.
		if(mClient.GetFrame().Result != Result::Success)
			throw("Cannot update measurements: Error retrieving measurement frame.");

		// Iterate through the tracking list and update
		for(UINT trackObjIndex=0; trackObjIndex<mTrackingList.size(); trackObjIndex++)
		{
			TrackedObject* trackObj = &mTrackingList.at(trackObjIndex);
			if( trackObj->markers.size() == 0)
				throw("Cannot update measurements: Tracked object " + trackObj->name + " does not have any markers.");

			// First get the positions of all the markers
			for(UINT markerIndex=0; markerIndex<trackObj->markers.size(); markerIndex++)
			{
				TrackingMarker* marker = &(trackObj->markers.at(markerIndex));

				Output_GetMarkerGlobalTranslation measResult;
				measResult = mClient.GetMarkerGlobalTranslation(trackObj->name,marker->name);

				if( measResult.Result != Result::Success )
				{
					switch(measResult.Result)
					{
						case(Result::InvalidSubjectName):
							throw("Measurement failure: Invalid Subject Name - " + trackObj->name);
						case(Result::InvalidMarkerName):
							throw("Measurement failure: Invalid Marker Name - " + trackObj->name + "/" + marker->name);
						default: // other possible errors should have already been caught before getting here
							throw("Measurement failure: Unknown error.");
					}
				}

				if(	measResult.Occluded )
					marker->isOccluded = true;
				else // finally, we know we have a good measurement
				{
					marker->isOccluded = false;
					marker->position[0][0] = measResult.Translation[0];
					marker->position[1][0] = measResult.Translation[1];
					marker->position[2][0] = measResult.Translation[2];
				}

				if(mDataLogger != NULL)
					logMarkerPosition(marker->name, marker->position, marker->isOccluded);
			}
		}
	}
}

vector<TrackedObject> MeasurementSystem_Vicon::updateMeasurements2()
{
	vector<TrackedObject> trackingList;
//	startNewLogEntry();
	if(mInSimulationMode)
	{
//		if(mReplayLog != NULL)
//		{
//			stringstream replaySS; replaySS << "index" << ++mLogIndexReplay;
//			mxml_node_t *replayIndexNode = mxmlFindElement(mReplayLog,mReplayLog,replaySS.str().c_str(),NULL,NULL,MXML_DESCEND);
//			if(replayIndexNode == NULL)
//			{
//				mLogIndexReplay = 0;
//				stringstream replaySS2; replaySS2 << "index" << mLogIndexReplay;
//				replayIndexNode = mxmlFindElement(mReplayLog,mReplayLog,replaySS2.str().c_str(),NULL,NULL,MXML_DESCEND);
//			}
//
//			for(UINT trackObjIndex=0; trackObjIndex<mTrackingList.size(); trackObjIndex++)
//			{
//				TrackedObject* trackObj = &mTrackingList.at(trackObjIndex);
//				mxml_node_t *objectNode = mxmlFindElement(replayIndexNode,replayIndexNode,trackObj->name.c_str(),NULL,NULL,MXML_DESCEND);
//				loadObjectReplay(objectNode, trackObj);
//
//				// TODO: This is just temporary until I have more real data logs
//				startObjectLog(trackObj->name);
//				for(UINT markerIndex=0; markerIndex<trackObj->markers.size(); markerIndex++)
//				{
//					TrackingMarker* marker = &(trackObj->markers.at(markerIndex));
//					logMarkerPosition(marker->name, marker->position, false);
//				}
//			}
//		}
//		else
//		{
			for(UINT trackObjIndex=0; trackObjIndex<mTrackingList.size(); trackObjIndex++)
			{
				trackingList.push_back(mTrackingList[trackObjIndex]);

//				TrackedObject* trackObj = &mTrackingList.at(trackObjIndex);
				TrackedObject* trackObj = &trackingList.back();

				if(trackObj->markers.size() == 5)
				{
					TrackingMarker *mk0 = &(trackObj->markers[0]);
					TrackingMarker *mk1 = &(trackObj->markers[1]);
					TrackingMarker *mk2 = &(trackObj->markers[2]);
					TrackingMarker *mk3 = &(trackObj->markers[3]);
					TrackingMarker *mk4 = &(trackObj->markers[4]);
					double dx = 100*sin(mSys.mtime()/10.0e3);
					double dy = 100*cos(mSys.mtime()/5.0e3);
					double dz = 10*sin(mSys.mtime()/20e3);
					mk0->position[0][0] =  1e3+dx; mk0->position[1][0] =    0+dy; mk0->position[2][0] = 1e3+dz;
					mk1->position[0][0] = -1e3+dx; mk1->position[1][0] =    0+dy; mk1->position[2][0] = 1e3+dz;
					mk2->position[0][0] =    0+dx; mk2->position[1][0] = -1e3+dy; mk2->position[2][0] = 1e3+dz;
					mk3->position[0][0] =    0+dx; mk3->position[1][0] =  1e3+dy; mk3->position[2][0] = 1e3+dz;
					mk4->position[0][0] =  100+dx; mk4->position[1][0] =  100+dy; mk4->position[2][0] = 1100+dz;

					mk0->isOccluded = false;
					mk1->isOccluded = false;
					mk2->isOccluded = false;
					mk3->isOccluded = false;
					mk4->isOccluded = false;
				}
				else
				{
					for(UINT markerIndex=0; markerIndex<trackObj->markers.size(); markerIndex++)
					{
						TrackingMarker* marker = &(trackObj->markers.at(markerIndex));

						marker->position[0][0] = markerIndex/10.0;//+sin(mSys.mtime()/3.0e3);
						marker->position[1][0] = (((markerIndex+1)%trackObj->markers.size()-1)+1)/5.0;//+cos(mSys.mtime()/5.0e3);
						marker->position[2][0] = 0;//*0.01*sin(mSys.mtime()/10.0e3);
						marker->isOccluded = false;

						logMarkerPosition(marker->name, marker->position, false);
					}
				}
			}
//		}
	}
	else
	{
		// Make sure the conenction is still valid
//		Output_IsConnected outputConnect = mClient.IsConnected();
		if( !mClient.IsConnected().Connected )
			throw("Cannot update measurements: Not connected to measurement server.");

		// Get measurement frame. For now assume that if it doesn't work
		// right the first time then there is something wrong.
		if(mClient.GetFrame().Result != Result::Success)
			throw("Cannot update measurements: Error retrieving measurement frame.");


		Output_GetSubjectCount cntOut = mClient.GetSubjectCount();
		int objCnt = cntOut.SubjectCount;
		// add tracking objects for every object found
		for(int trackObjIndex=0; trackObjIndex<objCnt; trackObjIndex++)
		{
			TrackedObject trackObj;
			Output_GetSubjectName nameOut = mClient.GetSubjectName(trackObjIndex);
			trackObj.name = nameOut.SubjectName;

			Output_GetMarkerCount cntOut= mClient.GetMarkerCount(nameOut.SubjectName);
			int markerCnt = cntOut.MarkerCount;
			// First get the positions of all the markers
			for(int markerIndex=0; markerIndex<markerCnt; markerIndex++)
			{
				TrackingMarker marker;

				Output_GetMarkerName nameOut = mClient.GetMarkerName(trackObj.name,markerIndex);
				marker.name = nameOut.MarkerName;

				Output_GetMarkerGlobalTranslation measResult;
				measResult = mClient.GetMarkerGlobalTranslation(trackObj.name,marker.name);

				if( measResult.Result != Result::Success )
				{
					switch(measResult.Result)
					{
						case(Result::InvalidSubjectName):
							throw("Measurement failure: Invalid Subject Name - " + trackObj.name);
						case(Result::InvalidMarkerName):
							throw("Measurement failure: Invalid Marker Name - " + trackObj.name + "/" + marker.name);
						default: // other possible errors should have already been caught before getting here
							throw("Measurement failure: Unknown error.");
					}
				}

				if(	measResult.Occluded )
					marker.isOccluded = true;
				else // finally, we know we have a good measurement
				{
					marker.isOccluded = false;
					marker.position[0][0] = measResult.Translation[0];
					marker.position[1][0] = measResult.Translation[1];
					marker.position[2][0] = measResult.Translation[2];
				}

				trackObj.markers.push_back(marker);
//				if(mDataLogger != NULL)
//					logMarkerPosition(marker->name, marker->position, marker->isOccluded);
			}

			trackingList.push_back(trackObj);
		}
	}

	return trackingList;
}

TrackedObject* MeasurementSystem_Vicon::getTrackedObject(int i)
{
	return &(mTrackingList.at(i));
}

void MeasurementSystem_Vicon::setSimulationMode(bool mode)
{
	mInSimulationMode = mode;
}

int MeasurementSystem_Vicon::addTrackingObject(string name, int nMarkers, int originMarkerIndex)
{
	TrackedObject newObj;
	newObj.name = name;
	int i;
	for(i=0; i<nMarkers; i++)
	{
		TrackingMarker newMark;

		std::stringstream myStream;
		myStream << "Marker " << i;
		newMark.name = myStream.str();
		newObj.markers.push_back(newMark);
	}

	newObj.originMarker = &(newObj.markers.at(originMarkerIndex));

	mTrackingList.push_back(newObj);

	return (int)mTrackingList.size();
}

int MeasurementSystem_Vicon::addTrackingObject(string name, vector<string> markerNames, int originMarkerIndex)
{

	TrackedObject newObj;
	newObj.name = name;
	int i;
	int nMarkers = (int)markerNames.size();
	for(i=0; i<nMarkers; i++)
	{
		TrackingMarker newMark;

		newMark.name = markerNames.at(i);
		newObj.markers.push_back(newMark);
	}

	newObj.originMarker = &(newObj.markers.at(originMarkerIndex));

	mTrackingList.push_back(newObj);

	return (int)mTrackingList.size();
}

void MeasurementSystem_Vicon::startNewLogEntry()
{
	if(mDataLogger == NULL)
		return;

	stringstream logSS; logSS << "index" << ++mLogIndexSave;
	mLogIndexNode = mxmlNewElement(mDataLogger, logSS.str().c_str());
}

void MeasurementSystem_Vicon::startObjectLog(string name)
{
	if(mDataLogger == NULL)
		return;

	mLogObjectNode = mxmlNewElement(mLogIndexNode,name.c_str());
}

void MeasurementSystem_Vicon::logMarkerPosition(string name, Array2D<double> pos, bool isOccluded)
{
	if(mDataLogger == NULL)
		return;

	mxml_node_t *markerNode = mxmlNewElement(mLogObjectNode, name.c_str());
		mxml_node_t *occludedNode = mxmlNewElement(markerNode, "isOccluded"); mxmlNewInteger(occludedNode, (int)isOccluded);

		mxml_node_t *xNode = mxmlNewElement(markerNode, "x"); mxmlNewReal(xNode, pos[0][0]);
		mxml_node_t *yNode = mxmlNewElement(markerNode, "y"); mxmlNewReal(yNode, pos[1][0]);
		mxml_node_t *zNode = mxmlNewElement(markerNode, "z"); mxmlNewReal(zNode, pos[2][0]);
}

void MeasurementSystem_Vicon::loadObjectReplay(mxml_node_t *xml, TrackedObject *object)
{
	if(mDataLogger == NULL)
		return;

	for(UINT markerIndex=0; markerIndex<object->markers.size(); markerIndex++)
	{
		TrackingMarker *marker = &(object->markers[markerIndex]);

		mxml_node_t *markerNode = mxmlFindElement(xml,xml,marker->name.c_str(),NULL,NULL,MXML_DESCEND);
			mxml_node_t *occludedNode = mxmlFindElement(markerNode,markerNode,"isOccluded",NULL,NULL,MXML_DESCEND);
				marker->isOccluded = (bool)atoi(occludedNode->child->value.text.string);
			mxml_node_t *xNode = mxmlFindElement(markerNode,markerNode,"x",NULL,NULL,MXML_DESCEND);
				marker->position[0][0] = strtod(xNode->child->value.text.string, NULL);
			mxml_node_t *yNode = mxmlFindElement(markerNode,markerNode,"y",NULL,NULL,MXML_DESCEND);
				marker->position[1][0] =strtod(yNode->child->value.text.string, NULL);
			mxml_node_t *zNode = mxmlFindElement(markerNode,markerNode,"z",NULL,NULL,MXML_DESCEND);
				marker->position[2][0] =strtod(zNode->child->value.text.string, NULL);
	}
}

}
