#ifndef CLASS_MEASUREMENTSYSTEM
#define CLASS_MEASUREMENTSYSTEM
#include <vector>

#include "TNT/tnt.h"

#include "toadlet/egg.h"

/// Class to facilitate a common way of accessing connection results
namespace ICSL
{
using namespace std;
using namespace TNT;

#define UINT unsigned int

struct TrackingMarker
{
	Array2D<double> position;
	string name;
	bool isOccluded;

	TrackingMarker():position(3,1){};
};

struct TrackedObject
{
	TrackingMarker* originMarker;
	Array2D<double> position;
	string name;
	vector<TrackingMarker> markers;

	TrackedObject(): position(3,1){};
};

class ConnectionStatus
{ 
	public:
		enum Status	{ Connected, Disconnected, InvalidID, UnknownFailure};

		ConnectionStatus()
		{
			mStatus = UnknownFailure;
			mMessage = "";
		}
		ConnectionStatus(Status s,string m)
		{
			mStatus = s;
			mMessage = m;
		}
		~ConnectionStatus(){};

		Status getStatus()
		{ return mStatus; }

		void setStatus(Status s)
		{ mStatus = s; }

		void setStatus(Status s, string m)
		{ mStatus = s; mMessage = m; }

		string getMessage()
		{ return mMessage; }

		void setMessage(string m)
		{ mMessage = m; }

		Status mStatus;
		string mMessage;
};


/// Abstract class to represent different measurement systems
class MeasurementSystem
{
	public:
		/// Connect to the hardware measurement system. Given the lack of an abstract 
		// method of identifying the source, it is assumed that specific implementations
		// will provide other methods of identifying the source such that the following
		// function connects properly.
		//
		// \return bool indicating successful connection or not
		virtual ConnectionStatus connectToSource()=0;

		/// Close connection to the current source
		virtual ConnectionStatus disconnectFromSource()=0;

		/// Force implementations to have some string representation of the source
		// available for display purposes
		//
		// \return source
		virtual string getSourceID()=0;
		
		
		/// Updates the internal measurements for position and orientation
		//  \return update performed successfully
		virtual void updateMeasurements()=0;
		/// Returns a reference to a tracking object
		// 
		// \param index of object in mTrackingList
		// \return pointer to tracking object
		virtual TrackedObject* getTrackedObject(int i)=0;

		/// Set whether or not we want to be in simulation mode
		// \param mode flag indicating status that mInSimulationMode will be set to
		virtual void setSimulationMode(bool mode)=0;

		/// Adds a new object to be tracked
		//
		// \param name name of the object
		// \param nMarkers number of markers on the object
		// \param originMarkerIndex marker used to define object position
		// \return index in the tracking list that represents the newly added object
		virtual int addTrackingObject(string name, int nMarkers, int originMarkerIndex)=0;
		// \param name of the object
		// \param list of marker names (list size will be the assumed number of markers)
		// \param originMarkerIndex marker used to define object position
		// \return index in the tracking list that represents the newly added object
		virtual int addTrackingObject(string name, vector<string> markerNames, int originMarkerIndex)=0;

	protected:
		/// Toggle a simulation mode for development without hardware
		bool mInSimulationMode;

		/// Keep the current connection status
		ConnectionStatus mConnectionStatus;

		/// For systems that can track multiple objects at a time
		vector<TrackedObject> mTrackingList;

		toadlet::egg::System mSys;
};
}
#endif
