#ifndef QUADROTOR_QUADROTORINTERFACE
#define QUADROTOR_QUADROTORINTERFACE
#pragma warning(disable : 4996)
#include <fstream>
#include <fcntl.h>  // File control definitions
#include <iostream>
#include <list>
#include <vector>
#include <unistd.h>
#include <thread>
#include <chrono>

#include <QObject>
#include <QWidget>
#include <QtGui>
#include <QMutex>

#include "TNT/tnt.h"

#include "constants.h"
#include "TNT_Utils/TNT_Utils.h"
#include "Timer.h"
#include "xml_utils.h"

#include "quadrotor_config.h"
#include "quadrotorConstants.h"
#include "quadrotorConstants.h"
#include "SystemControllerFeedbackLin.h"
#include "ui_QuadrotorInterface.h"
#include "Timer.h"
#include "../Asctec_aci/asctecCommIntf.h"
#include "Acicom.h"


using namespace std;

namespace ICSL{
namespace Quadrotor{


    class QuadrotorInterface : public QWidget, protected Ui::QuadrotorInterface
    {
        Q_OBJECT

        public:
            int arm3;
            enum CommStatus
            {FREE, BUSY};

            explicit QuadrotorInterface(QWidget *parent=0);
            virtual ~QuadrotorInterface();
            void initialize();

            void setDesiredState(TNT::Array2D<double> state){mMutex_des.lock(); mControllerFeedbackLin->setDesiredState(state); mMutex_des.unlock();};
            void setFlightMode(FlightMode mode){mFlightMode = mode;}; /// this is only used for data logging purposes

            TNT::Array2D<double> getCurState();

            TNT::Array2D<double> getDesiredState(){return mControllerFeedbackLin->getDesiredState();};

            void syncStartTime(unsigned long start){mStartTimeUniverseMS = start;};

            void updateDisplay();
            void calcControl();
            void sendControl();
            void sendStopAndShutdown();
            void saveData(string dir, string filename);
            bool isEnabled(){return chkEnabled->isChecked();};
            void clearAllBuffers();
            void setVEstState(TNT::Array2D<double> state){mMutex_est.lock(); meststate=state.copy(); mMutex_est.unlock();};
            void setVEstVelState(TNT::Array2D<double> state){mMutex_estvel.lock(); mestvelstate=state.copy(); mMutex_estvel.unlock();};

            void startrun(){
                 std::thread th(&QuadrotorInterface::run,this);
                 th.detach();
            };

            double inix, iniy, iniz;

            // GPS code
            void gps_ref();
            void gpsToxyz();

        protected slots:
            void onBtnSetXyDesired_clicked();
            void FlightModechange();

        protected:

            bool vision_flag;

            //GPS
            double GPS_lat, GPS_lon,GPS_h,GX_ref,GY_ref,GZ_ref;
            //
            int calc_time;
            TNT::Array2D<double> mcurstate,mmotorstate,mdesstate,endp,mdendstate,meststate,mestvelstate,mxy,GPS_accuracy;
            int id;
            void run();

            int arm1=0, arm2=0, varm1=0, varm2=0;
            int timecounter2=0;
            int timecounter1=0;
            int timecounter3=0;


            int mNumState, mNumInput, mTotalNumState, mNumAllStates, mNumMotor, mNumJoint;
                FlightMode mFlightMode;
                CommStatus mCommStatus;
                string mName;

            int temp=0;
            double mDeltaT;


            unsigned long mStartTimeUniverseMS, mProfileStartTimeMS;
            TNT::Array2D<double> mProfileStartPoint;
            bool mInitialized;


            QShortcut *mChangemode;

            SystemControllerFeedbackLin *mControllerFeedbackLin;
            AciCom *mAcicom;

            ICSL::Timer mTmr;

            toadlet::egg::Mutex mMutex_send,mMutex_logBuffer,mMutex_des,mMutex_est,mMutex_estvel,mMutex_btn;
            toadlet::egg::Mutex mMutex_endp,mMutex_mode,mMutex_ini;

            list<TNT::Array2D<double> > mStateRefBuffer;
            list<TNT::Array2D<double> > mEstBuffer;
            list<TNT::Array2D<double> > mEstVelBuffer;
            list<TNT::Array2D<double> > mXyBuffer;
            list<TNT::Array2D<double> > mGPSBuffer;
            list<TNT::Array2D<double> > mStateBuffer;
            list<TNT::Array2D<double> > mControlBuffer;
            list<TNT::Array2D<double> > mMotBuffer;
            list<unsigned long> mTimeBuffer, mControlCalcTimeBuffer;
            list<int> mFlightModeBuffer;

            double constrain(double val, double minVal, double maxVal);

    };
}
}
#endif
