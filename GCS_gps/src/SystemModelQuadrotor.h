#ifndef CLASS_SYSTEMODELQUADROTOR
#define CLASS_SYSTEMODELQUADROTOR
#include <string>
#include <list>
#include <math.h>
#include <mxml.h>


#include "TNT/tnt.h"
#include "toadlet/egg/Mutex.h"

//#include "ICSL/icsl_config.h"
#include "vicon_sdk/Linux/Client.h"
#include "constants.h"
#include "TNT_Utils/TNT_Utils.h"

#include "TelemetryVicon.h"
namespace ICSL{
namespace Quadrotor{

    using namespace std;

    class SystemModelQuadrotor : public TelemetryViconListener
    {
        public:
            SystemModelQuadrotor();
            virtual ~SystemModelQuadrotor(){};

            virtual void estimateFullState();

            void setDeltaT(double dt){mDeltaT = dt;};
            void setSimulated(bool b){mIsSimulated = b;};

            bool isSimulated(){return mIsSimulated;};

            const list<TNT::Array2D<double> >* getStateBuffer(){return &mStateBuffer;}

            void resetState(); // just there for use with the simulated quadrotor
            void clearAllBuffers(){mMutex_buffers.lock();mStateBuffer.clear();mStateFilteredBuffer.clear();mMutex_buffers.unlock();}
            void onTelemetryUpdated(TelemetryViconDataRecord const &rec);

            virtual Array2D<double> getCurState()
            {
            mMutex_DataAccess.lock();
            Array2D<double> temp = mCurState.copy();
            mMutex_DataAccess.unlock();
            return temp;
            };
            static const string StateNames[];
            static const string ActuatorNames[];

            virtual void setName(string s){mName = s;}
            virtual string getName(){return mName;};

        protected:
            double mDeltaT, mMass;
            TNT::Array2D<double> mCurPos;
            TNT::Array2D<double> mCurState;
            TNT::Array2D<double> mCurStateFiltered;
            TNT::Array2D<double> mInertiaMat, mInertiaMatInv;


            list<TNT::Array2D<double> > mStateBuffer;
            list<TNT::Array2D<double> > mStateFilteredBuffer;
            void applyInertiaConfigTree(QTreeWidgetItem *root);


            TNT::Array2D<double> convertTelemViconToState(TelemetryViconDataRecord const &rec);
            TNT::Array2D<double> estimateState(TelemetryViconDataRecord const &rec, list<TNT::Array2D<double> > const & stateBuffer, double deltaT);
            TNT::Array2D<double> estimateState(TNT::Array2D<double> state, list<TNT::Array2D<double> > const & stateBufferOrig, double deltaT);

            toadlet::egg::Mutex mMutex_curPos, mMutex_SysMats, mMutex_buffers, mMutex_DataAccess;

            bool mIsSimulated;
            unsigned long mTimeOfLastUpdate;
            TNT::Array2D<double> mSimStartState, mSimState, mSimStateMin, mSimStateMax;
            ICSL::Timer mTmr;
            string mName;

            void constrainVector(TNT::Array2D<double> &v, TNT::Array2D<double> const &vMin, TNT::Array2D<double> const &vMax);
    };

}
}
#endif
